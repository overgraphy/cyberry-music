<?php

$myURL = $_SERVER['REQUEST_URI'];
$og_image = "http://www.cyberrymusic.com/images/ogimage-rules.jpg";
$hd = array(
	"title" => "Cyberry Music: การสมัครโดยใช้ Facebook",
	"description" => "การสมัคร Cyberry Music โดยใช้ Facebook Login",
	"keyword" => "cyberry, cyberry music, กติกา, แข่งขัน, ประกวด, ร้องเพลง, เล่นดนตรี, youtube",
	"og_title" => "การสมัคร Cyberry Music โดยใช้ Facebook Login",
	"og_sitename" => "CyberryMusic.com",
	"og_url" => "http://www.cyberrymusic.com/rules.php",
	"og_image" => "$og_image",
	"og_description" => "การสมัคร Cyberry Music โดยใช้ Facebook Login"
);
?>

<?php include realpath( __DIR__ . '/__display.php' ); ?>
<?php include_once realpath(__DIR__ . "/googleanalytic.php") ?>
<?php
write_header($hd);
?>
<div class="container">
<!---- Content ----->

<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
  <tr>
    <td align="left" valign="top" bgcolor="#F4F4F4"><br />
      <table width="90%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <td height="50" align="left" valign="top" class="Arial_gray">
		  <h2>การใช้ Facebook ในการ Login สำหรับเว็บ Cyberry Music</h2>

		  <ol>
			<li>ใช้เพื่อการ Login และนำ User Avatar มาใช้เท่านั้น</li>
			<li>ไม่โพสลงบน Wall ของผู้สมัคร</li>
			<li>ไม่สามารถเข้าถึงความเป็นส่วนตัวของผู้สมัคร</li>
		  </ol>
		  
		  
		  </td>
        </tr>
    </table>
	</td>  
	  
  </tr>
</table>



<!---- /Content ----->
</div>
<?php
write_footer();
?>
</body>
</html>