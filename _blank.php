<?php

$myURL = $_SERVER['REQUEST_URI'];
$og_image = "http://www.cyberrymusic.com/images/ogimage-rules.jpg";
$hd = array(
	"title" => "Cyberry Music: กติกา",
	"description" => "กติกาการแข่งขันประกวดคลิปเพลง Cyberry Music",
	"keyword" => "cyberry, cyberry music, กติกา, แข่งขัน, ประกวด, ร้องเพลง, เล่นดนตรี, youtube",
	"og_title" => "กติกาการแข่งขันประกวดคลิปเพลง Cyberry Music",
	"og_sitename" => "CyberryMusic.com",
	"og_url" => "http://www.cyberrymusic.com/rules.php",
	"og_image" => "$og_image",
	"og_description" => "กติกาสำหรับการประกวดคลิปเพลงบน Cyberry Music"
);
?>

<?php include realpath( __DIR__ . '/display.php' ); ?>
<?php include_once realpath(__DIR__ . "/googleanalytic.php") ?>
<?php
write_header($hd);
?>
<link href="css/list-order.css" rel="stylesheet" type="text/css" />
</head>

<body onload="MM_preloadImages('images/icon_fb2.jpg','images/icon_twitter2.jpg','images/icon_youtube2.jpg','images/icon_googleplus2.jpg','images/footer_fb2.jpg','images/footer_twitter2.jpg','images/footer_youtube2.jpg','images/footer_googleplus2.jpg')">
<table width="1200" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="225" height="100" align="left" valign="middle" scope="col"><img src="images/logo_cyberrymusic.jpg" width="225" height="100" /></td>
    <td align="center" valign="bottom" scope="col"><table width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
      <tr>
        <td height="60" align="right" valign="top"><table width="160" border="0" align="right" cellpadding="2" cellspacing="2">
          <tr>
            <td width="40" align="right" valign="top" class="blue"><div align="right"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image21','','images/icon_fb2.jpg',1)"><img src="images/icon_fb.jpg" width="40" height="40" id="Image21" /></a></div>
              <a href="#"></a></td>
            <td width="40" align="right" valign="top" class="blue"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image20','','images/icon_twitter2.jpg',1)"><img src="images/icon_twitter.jpg" width="40" height="40" id="Image20" /></a></td>
            <td width="40" align="right" valign="top" class="blue"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image19','','images/icon_youtube2.jpg',1)"><img src="images/icon_youtube.jpg" width="40" height="40" id="Image19" /></a></td>
            <td width="40" align="right" valign="top" class="blue"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image23','','images/icon_googleplus2.jpg',1)"><img src="images/icon_googleplus.jpg" width="40" height="40" id="Image23" /></a></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="35" align="right" valign="middle" class="blue">
		<div align="right">
			<span class="Arial_gray">
					<a href="index.php">หน้าแรก</a>&nbsp;&nbsp;
					<a href="rules.php">&nbsp;กติกา</a>&nbsp;&nbsp;
					&nbsp;ผู้เข้าแข่งขัน&nbsp;&nbsp;&nbsp;
					สมัครสมาชิก&nbsp;&nbsp;
					&nbsp;เข้าสู่ระบบ
				</span>
			
			</span></div></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="1200" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="190" align="left" valign="middle" background="images/head_register.jpg" scope="col"><table width="1000" border="0" align="center" cellpadding="0" cellspacing="5">
      <tr>
        <td class="whitebig"><span id="result_box" lang="en" xml:lang="en">กติกา</span></td>
      </tr>
    </table>
      <br /></td>
  </tr>
</table>
<table width="1180" border="0" align="center" cellpadding="2" cellspacing="2">
  <tr>
    <td height="30" align="left" valign="middle" class="blue"><a href="#">Home</a> &gt;<span id="result_box2" lang="en" xml:lang="en"> กติกา</span></td>
  </tr>
</table>
<br />
<br />

<!--- DAta-->
data
<p>&nbsp;</p>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="100" align="center" valign="top" bgcolor="#1e1e1e"><table width="1100" border="0" align="center" cellpadding="1" cellspacing="1">
      <tr>
        <td width="714" align="left" valign="top" class="Arial_gray">
		
		<!--
		<table width="250" border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td colspan="2" align="left" valign="middle"><span class="blue"><img src="images/space.jpg" alt="" width="5" height="5" /></span></td>
          </tr>
          <tr>
            <td width="150" align="left" valign="middle">Visitor Counter</td>
            <td align="right" valign="middle" class="white">23</td>
          </tr>
          <tr>
            <td align="left" valign="middle">Online</td>
            <td align="right" valign="middle" class="white">45</td>
          </tr>
          <tr>
            <td align="left" valign="middle">Today</td>
            <td align="right" valign="middle" class="white">356</td>
          </tr>
          <tr>
            <td align="left" valign="middle">This Week</td>
            <td align="right" valign="middle" class="white">3,671</td>
          </tr>
          <tr>
            <td align="left" valign="middle">All</td>
            <td align="right" valign="middle" class="white">6,970</td>
          </tr>
        </table>
		-->
          <p><br />
          </p></td>
        <td width="379" align="right" valign="top" class="gray"><table width="98%" border="0" align="left" cellpadding="2" cellspacing="2">
          <tr>
            <td colspan="5" align="right" valign="top" class="blue"><img src="images/space.jpg" width="5" height="5" /></td>
          </tr>
          <tr>
            <td align="right" valign="top" class="blue">&nbsp;</td>
            <td width="45" align="right" valign="top" class="blue"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image14','','images/footer_fb2.jpg',1)"><img src="images/footer_fb.jpg" width="40" height="40" id="Image14" /></a></td>
            <td width="45" align="right" valign="top" class="blue"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image15','','images/footer_twitter2.jpg',1)"><img src="images/footer_twitter.jpg" width="40" height="40" id="Image15" /></a></td>
            <td width="45" align="right" valign="top" class="blue"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image16','','images/footer_youtube2.jpg',1)"><img src="images/footer_youtube.jpg" width="40" height="40" id="Image16" /></a></td>
            <td width="45" align="right" valign="top" class="blue"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image17','','images/footer_googleplus2.jpg',1)"><img src="images/footer_googleplus.jpg" width="40" height="40" id="Image17" /></a></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="left" valign="middle" class="black">&nbsp;</td>
        <td align="right" valign="top" class="gray"><table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td height="40" align="right" valign="top"><table width="100%" border="0" align="right" cellpadding="2" cellspacing="2">
              <tr>
                <td align="right" valign="top" class="blue"><span class="Arial_gray">Copyright © 2015 Cyberrymusic.com All Rights Reserved</span></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
