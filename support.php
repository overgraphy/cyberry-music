<?php

$myURL = $_SERVER['REQUEST_URI'];
$og_image = "http://www.cyberrymusic.com/images/ogimage-rules.jpg";
$hd = array(
	"title" => "Cyberry Music: Support",
	"description" => "ขอความช่วยเหลือจาก Cyberry Music Support Team",
	"keyword" => "cyberry, cyberry music, กติกา, แข่งขัน, ประกวด, ร้องเพลง, เล่นดนตรี, youtube",
	"og_title" => "ขอความช่วยเหลือจาก Cyberry Music Support Team",
	"og_sitename" => "CyberryMusic.com",
	"og_url" => "http://www.cyberrymusic.com/rules.php",
	"og_image" => "$og_image",
	"og_description" => "ขอความช่วยเหลือจาก Cyberry Music Support Team"
);
?>

<?php include realpath( __DIR__ . '/__display.php' ); ?>
<?php include_once realpath(__DIR__ . "/googleanalytic.php") ?>
<?php
write_header($hd);
?>
<div class="container">
<!---- Content ----->

<form method="post" action="support-send.php">
<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
  <tr>
    <td align="left" valign="top" bgcolor="#F4F4F4"><br />
      <table width="90%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <td height="50" align="left" valign="top" class="Arial_gray" colspan=2>
		  <h2>Cyberry Music Support</h2>

		  <p>กรอกข้อความเพื่อขอความช่วยเหลือจาก Support Team</p>
		  
		  
		  
		  </td>
        </tr>
		<tr>
			<td>Email ของคุณ</td>
			<td><input type="text" class="form-control" name="email"></td>
		</tr>
		<tr>
			<td valign=top>ข้อความ</td>
			<td>
				<textarea name="message" placeholder="กรอกข้อความที่ท่านต้องการขอความช่วยเหลือ"></textarea>
			</td>
		</tr>
		<tr>
			<td colspan=2 align=center>
				<input type=submit value=" ส่ง ">
			</td>
		</tr>
    </table>
	</td>  
	  
  </tr>
</table>

</form>

<!---- /Content ----->
</div>
<?php
write_footer();
?>
</body>
</html>