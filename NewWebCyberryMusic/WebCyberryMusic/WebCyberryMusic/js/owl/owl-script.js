
$(document).ready(function() {
  "use strict";
 
  $("#service-carousel").owlCarousel({
 
      autoPlay: 4000, //Set AutoPlay to 3 seconds
 
      items : 1,
      itemsDesktop : [1199,4],
      itemsDesktopSmall : [979,4],
    
 
  });

 
});
