$(document).ready(function($) {
  "use strict";



    /*------------------------------ Body Background Image ----------------------*/
    (function () {
        /*
         Background image
         */
        $.backstretch([
            "/images/backgrounds/bg.png"
        ]);

    })();

    /*------------------------------ FitVids for Video Responsive ----------------------*/

        $('.fitvids').fitVids();



    /*------------------------------ smooth content show ----------------------*/

      if ($(window).width() > 992) {
            new WOW().init();
        };



    /*------------------------------ Music player script   ----------------------*/
       var description = ' ';
          $('.lan-music-player').ttwMusicPlayer(myPlaylist, {
              autoPlay:false,
              description:description,
              jPlayer:{
                  swfPath:'plugin/jquery-jplayer' //You need to override the default swf path any time the directory structure changes
              }
          });


    /*------------------------------Featured Music slider  ----------------------*/

    $('#myCarousel').carousel({ cycle: true });


    /*------------------------------ Rev slider  ----------------------*/

    (function() {
        $('.fullwidthbanner').revolution( { 
          delay: 5000,
          startwidth: 940,
          startheight: 600,
          onHoverStop: 'on',            // Stop Banner Timet at Hover on Slide on/off
          thumbWidth: 100,            // Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
          thumbHeight: 50,
          thumbAmount: 4,
          hideThumbs: 200,
          navigationType: 'both',         // Bullet, thumb, none, both   (No Shadow in Fullwidth Version !)
          navigationArrows: 'verticalcentered', // Nexttobullets, verticalcentered, none
          navigationStyle: 'round',       // Round,square,navbar
          touchenabled: 'on',           // Enable Swipe Function : on/off
          navOffsetHorizontal: 0,
          navOffsetVertical: 20,
          fullWidth: 'on',
          shadow: 0               //0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)
        }); 
      })(); 



      /*------------------------------ Artists gallery scroll script  ----------------------*/

      (function(){
              $("#content-6").mCustomScrollbar({
                  axis:"x",
                  theme:"light-3",
                  advanced:{autoExpandHorizontalScroll:true}
              });
          })();




 });






/*------------------------------ SITE LOADER ----------------------*/

$(window).load(function() {
    $('#status').fadeOut();
    $('#preloader').delay(5).fadeOut('slow');
    $('body').delay(5).css({'overflow':'visible'});
})

