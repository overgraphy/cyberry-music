﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DINFDAL.Interfaces;
using DINFDAL.Models;

namespace WebCyberryMusic.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BuyPointController : Controller
    {
        private IPaysbuyService _paysbuyService;

        public BuyPointController(IPaysbuyService paysbuyService)
        {
            _paysbuyService = paysbuyService;
        }

        // GET: Admin/BuyPoint
        public ActionResult Index()
        {
            var buypoint = _paysbuyService.GetAll()
                .Where(i => !String.IsNullOrEmpty(i.result)
                && i.result.Length > 2
                && i.result.Substring(0, 2).Equals("00"));

            return View(buypoint);
        }
        // GET: Admin/BuyPoint
        public ActionResult Edit(int id)
        {
            var buypoint = _paysbuyService.GetById(id);

            return View(buypoint);
        }
        // POST: Admin/BuyPoint
        [HttpPost]
        public ActionResult Edit(Paysbuy model)
        {
            if (ModelState.IsValid)
            {
                var buypoint = _paysbuyService.GetById(model.int_paysbuy_id);
                if (buypoint != null)
                {
                    buypoint.receipt_no = model.receipt_no;
                    buypoint.tax_inv_no = model.tax_inv_no;
                    buypoint.remark = model.remark;
                    buypoint.datetime_modified=DateTime.Now;
                    if (_paysbuyService.Update(buypoint) > 0)
                    {
                        ViewBag.IsSuccess = true;
                        model = buypoint;
                    }
                }
            }
            return View(model);
        }
    }
}