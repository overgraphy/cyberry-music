﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DINFDAL.Interfaces;
using DINFDAL.Models;
using DINFDAL.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using WebCyberryMusic.Models;

namespace WebCyberryMusic.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        //public UsersController()
        //{
        //    context = new ApplicationDbContext();
        //    _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
        //    _roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
        //}

        public UsersController(IUserService userService,
            IPaysbuyService paysbuyService,
            IWithdrawal_TransactionService withdrawalTransactionService,
            IBankService bankService,
            IBalanceService balanceService
            )
        {
            context = new ApplicationDbContext();
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            _roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            _userService = userService;
            _paysbuyService = paysbuyService;
            _withdrawalTransactionService = withdrawalTransactionService;
            _bankService = bankService;
            _balanceService = balanceService;
        }

        public UserManager<ApplicationUser> _userManager { get; private set; }
        public RoleManager<IdentityRole> _roleManager { get; private set; }
        public ApplicationDbContext context { get; private set; }
        private IUserService _userService;
        private IPaysbuyService _paysbuyService;
        private IWithdrawal_TransactionService _withdrawalTransactionService;
        private IBankService _bankService;
        private IBalanceService _balanceService;
        //
        // GET: /Users/
        public async Task<ActionResult> Index()
        {
            var users = _userManager.Users.Include("Roles").ToList();
            foreach (var item in users)
            {

            }
            return View(users);
        }
        //
        // GET: /Users/
        public async Task<ActionResult> GetIncome()
        {
            var users = _userService.GetAll();
            var incomeTransact = _balanceService.GetAllIncomeTransact();
            ViewBag.IncomeTransact = incomeTransact;
            return View(users);
        }
        //
        // GET: /Users/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = _userService.GetById(id);
            var userid = user.Id;
            var incomeTransact = _balanceService.GetIncomeTransactById(userid);
            var balance = _balanceService.GetByUserId(userid);
            ViewBag.Balance = balance;
            ViewBag.IncomeTransact = incomeTransact;
            ViewBag.PaysBuy =
                _paysbuyService.GetByUserId(id)
                    .Where(i => i.result != null && i.result.Substring(0, 2).Equals("00"))
                    .ToList();
            ViewBag.Withdrawaltrans = _withdrawalTransactionService.GetByUserId(id);
            ViewBag.RoleName = _userManager.GetRoles(id);
            return View(user);
        }

        //
        // GET: /Users/Create
        public async Task<ActionResult> Create()
        {
            //Get the list of Roles
            ViewBag.RoleId = new SelectList(await _roleManager.Roles.ToListAsync(), "Id", "Name");
            return View();
        }

        //
        // POST: /Users/Create
        [HttpPost]
        public async Task<ActionResult> Create(RegisterViewModel userViewModel, string RoleId)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser();
                user.UserName = userViewModel.UserName;
                var adminresult = await _userManager.CreateAsync(user, userViewModel.Password);

                //Add User Admin to Role Admin
                if (adminresult.Succeeded)
                {
                    if (!String.IsNullOrEmpty(RoleId))
                    {
                        //Find Role Admin
                        var role = await _roleManager.FindByIdAsync(RoleId);
                        var result = await _userManager.AddToRoleAsync(user.Id, role.Name);
                        if (!result.Succeeded)
                        {
                            ModelState.AddModelError("", result.Errors.First().ToString());
                            ViewBag.RoleId = new SelectList(await _roleManager.Roles.ToListAsync(), "Id", "Name");
                            return View();
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", adminresult.Errors.First().ToString());
                    ViewBag.RoleId = new SelectList(_roleManager.Roles, "Id", "Name");
                    return View();

                }
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.RoleId = new SelectList(_roleManager.Roles, "Id", "Name");
                return View();
            }
        }

        //
        // GET: /Users/Edit/1
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.RoleId = new SelectList(_roleManager.Roles, "Id", "Name");
            ViewBag.BankId = new SelectList(_bankService.GetAll(), "int_bank_id", "txt_bank_name");

            var user = _userService.GetById(id);
            var role = _userManager.GetRoles(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            var model = new EditMemberViewModel()
            {
                Id = user.Id,
                UserName = user.UserName,
                FisrtName = user.txt_firstname,
                LastName = user.txt_lastname,
                Email = user.Email,
                EmailConfirmed = user.EmailConfirmed,
                IsContest = user.is_contest ?? false,
                BirthDate = user.date_birth.Value.ToString("dd/MM/yyyy"),
                BankId = (user.int_bank_id ?? 0).ToString(),
                RoleId = _roleManager.Roles.FirstOrDefault(i => i.Name.Equals(role.FirstOrDefault())).Id,
                BankAccount = user.txt_bank_account,
                Phone = user.txt_phone,
                Address = user.txt_address,
                Avatar = user.txt_avatar,
                IsActive = user.is_activated ?? false,
                IsBanned = user.is_banned ?? false,
                BannedReason = user.txt_banned_reason
            };
            return View(model);
        }

        //
        // POST: /Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(EditMemberViewModel model, string id, string RoleId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.RoleId = new SelectList(_roleManager.Roles, "Id", "Name");
            ViewBag.BankId = new SelectList(_bankService.GetAll(), "int_bank_id", "txt_bank_name");

            if (ModelState.IsValid)
            {
                var user = _userManager.FindById(model.Id);
                user.txt_firstname = model.FisrtName;
                user.txt_lastname = model.LastName;
                user.txt_address = model.Address;
                user.txt_phone = model.Phone;
                if (!string.IsNullOrEmpty(model.BankId))
                {
                    user.int_bank_id = int.Parse(model.BankId);
                }
                user.txt_bank_account = model.BankAccount;
                user.txt_bank_name = model.BankName;
                user.txt_id_card = model.IdCard;
                user.date_birth = DateTime.ParseExact(model.BirthDate, "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US"));
                user.is_banned = model.IsBanned;
                user.txt_banned_reason = model.BannedReason;
                user.is_activated = model.IsActive;
                user.EmailConfirmed = model.EmailConfirmed;
                user.datetime_modified = DateTime.Now;
                if (user.is_activated == false || user.is_banned == true)
                {
                    user.LockoutEnabled = true;
                    user.LockoutEndDateUtc = DateTime.Now.AddYears(100);
                }
                else
                {
                    user.LockoutEnabled = false;
                    user.LockoutEndDateUtc = null;
                }
                if (_userManager.Update(user).Succeeded)
                {
                    ViewBag.IsSuccess = true;
                }
                //If user has existing Role then remove the user from the role
                // This also accounts for the case when the Admin selected Empty from the drop-down and
                // this means that all roles for the user must be removed
                var rolesForUser = await _userManager.GetRolesAsync(id);
                if (rolesForUser.Count() > 0)
                {
                    foreach (var item in rolesForUser)
                    {
                        var result = await _userManager.RemoveFromRoleAsync(id, item);
                    }
                }

                if (!String.IsNullOrEmpty(RoleId))
                {
                    //Find Role
                    var role = await _roleManager.FindByIdAsync(RoleId);
                    //Add user to new role
                    var result = await _userManager.AddToRoleAsync(id, role.Name);
                    if (!result.Succeeded)
                    {
                        ModelState.AddModelError("", result.Errors.First().ToString());
                        return View(model);
                    }
                }
                return View(model);
            }
            else
            {
                return View(model);
            }
        }

        ////
        //// GET: /Users/Delete/5
        //public async Task<ActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var user = await context.Users.FindAsync(id);
        //    if (user == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(user);
        //}

        ////
        //// POST: /Users/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(string id)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        if (id == null)
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //        }

        //        var user = await context.Users.FindAsync(id);
        //        var logins = user.Logins;
        //        foreach (var login in logins)
        //        {
        //            context.UserLogins.Remove(login);
        //        }
        //        var rolesForUser = await IdentityManager.Roles.GetRolesForUserAsync(id, CancellationToken.None);
        //        if (rolesForUser.Count() > 0)
        //        {

        //            foreach (var item in rolesForUser)
        //            {
        //                var result = await IdentityManager.Roles.RemoveUserFromRoleAsync(user.Id, item.Id, CancellationToken.None);
        //            }
        //        }
        //        context.Users.Remove(user);
        //        await context.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    else
        //    {
        //        return View();
        //    }
        //}
    }
}