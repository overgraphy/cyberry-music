﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DINF.Helpers;
using DINFDAL.Interfaces;
using DINFDAL.Models;

namespace WebCyberryMusic.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class WithdrawalTransactionController : Controller
    {
        private IWithdrawal_TransactionService _withdrawalTransactionService;
        private IUserService _userService;
        private IBalanceService _balanceService;

        public WithdrawalTransactionController(IWithdrawal_TransactionService withdrawalTransactionService,
            IUserService userService,
            IBalanceService balanceService)
        {
            _withdrawalTransactionService = withdrawalTransactionService;
            _userService = userService;
            _balanceService = balanceService;
        }

        // GET: Admin/WithdrawalTransaction
        public ActionResult Index()
        {
            var transat = _withdrawalTransactionService.GetAll();
            return View(transat);
        }

        // GET: Admin/WithdrawalTransaction
        public ActionResult Edit(int id)
        {
            var transsat = _withdrawalTransactionService.GetById(id);
            return View(new WithdrawalTransactionViewModel()
            {
                int_user_id = transsat.int_user_id,
                int_balance_id = transsat.int_balance_id,
                int_withdrawal_transaction_id = transsat.int_withdrawal_transaction_id,
                datetime_transfer_date = transsat.datetime_create,
                amount = transsat.amount.ToString(),
                include_vat = transsat.include_vat.ToString(),
                vat = transsat.vat.ToString(),
                transfer_amount = transsat.transfer_amount.ToString(),
                txt_remark = transsat.txt_remark,
                datetime_create = transsat.datetime_create,
                txt_payment_slip = transsat.txt_payment_slip,
                txt_withholding_tax = transsat.txt_withholding_tax,
                balance = transsat.balance.ToString()
            });
        }

        // POST: Admin/WithdrawalTransaction
        [HttpPost]
        public ActionResult Edit(WithdrawalTransactionViewModel model, FormCollection form)
        {
            if (ModelState.IsValid)
            {
                if (String.IsNullOrEmpty(model.transfer_amount))
                {
                    ModelState.AddModelError("transfer_amount", "The Field is required");
                    return View(model);
                }
                var transsat = _withdrawalTransactionService.GetById(model.int_withdrawal_transaction_id);
                transsat.datetime_transfer_date = DateTime.ParseExact(form["strTransferDate"], "dd/MM/yyyy",
                    CultureInfo.CreateSpecificCulture("en-US"));
                transsat.transfer_amount = Convert.ToDecimal(model.transfer_amount);
                transsat.txt_payment_slip = model.txt_payment_slip;
                transsat.txt_withholding_tax = model.txt_withholding_tax;
                transsat.txt_remark = model.txt_remark;
                model.datetime_transfer_date = transsat.datetime_transfer_date;
                if (_withdrawalTransactionService.Update(transsat) > 0)
                {
                    ViewBag.IsSuccess = true;
                    var user = _userService.GetById(transsat.int_user_id);
                    var balance = _balanceService.GetByUserId(transsat.int_user_id);
                    var mail = new SendMail();
                    //Send mail to User
                    if (!String.IsNullOrEmpty(transsat.txt_payment_slip) 
                        && !String.IsNullOrEmpty(transsat.txt_withholding_tax))
                    {
                        var attactfiles = new List<string>
                        {
                            Server.MapPath(transsat.txt_payment_slip),
                            Server.MapPath(transsat.txt_withholding_tax)
                        };

                        mail.Send(user.Email, "แจ้งโอนเงิน",
                            String.Format(
                                "***บริษัทได้ทำการโอนเงินเข้าบัญชีหมายเลข {0}  จำนวน {1} บาท  คุณมีวงเงินรางวัลคงเหลือที่สามารถถอนได้ {2} บาท  (ก่อนหักค่าธรรมเนียมธนาคาร)***",
                                user.txt_bank_account, String.Format("{0:#,0.00}", transsat.transfer_amount),
                                String.Format("{0:#,0.00}", balance.net_balance)),
                                attactfiles
                            );
                    }
                }
            }
            return View(model);
        }
    }
}