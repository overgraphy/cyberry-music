﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DINFDAL.Interfaces;

namespace WebCyberryMusic.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class IncomeController : Controller
    {
        private IBalanceService _balanceService;

        public IncomeController(IBalanceService balanceService)
        {
            _balanceService = balanceService;
        }

        // GET: Admin/Income
        public ActionResult Index()
        {
            var incomeTransact = _balanceService.GetAllIncomeTransact();
            ViewBag.IncomeTransact = incomeTransact;
            return View();
        }

    }
}