﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.WebPages;
using DINF.Helpers;
using DINFDAL.Helpers;
using DINFDAL.Interfaces;
using DINFDAL.Models;
using Microsoft.AspNet.Identity;
using WebCyberryMusic.Models;

namespace WebCyberryMusic.Controllers
{
    [Authorize]
    public class ProfileController : BaseController
    {

        private ISong_OriginalService _originalService;
        private ISong_ContestService _songContestService;
        private IUserService _userService;
        private IBalanceService _balanceService;
        private IWithdrawal_TransactionService _withdrawalTransactionService;
        private readonly ApplicationUserManager _userManager;

        public ProfileController(ISong_OriginalService originalService,
            ISong_ContestService songContestService,
            IUserService userService,
            IBalanceService balanceService,
            ApplicationUserManager userManager,
            IWithdrawal_TransactionService withdrawalTransactionService)
        {
            _originalService = originalService;
            _songContestService = songContestService;
            _userService = userService;
            _userManager = userManager;
            _balanceService = balanceService;
            _withdrawalTransactionService = withdrawalTransactionService;
        }
        //
        [Authorize(Roles = "Composor/Artist")]
        public ActionResult Withdrawal()
        {
            var userid = User.Identity.GetUserId();
            var incomeTransact = _balanceService.GetIncomeTransactById(userid);
            var balance = _balanceService.GetByUserId(userid);
            ViewBag.Balance = balance;
            ViewBag.IncomeTransact = incomeTransact;

            return View();
        }

        public ActionResult addWithdrawal(WithdrawalTransactionViewModel model)
        {
            var userid = User.Identity.GetUserId();
            var user = _userService.GetById(userid);
            var balance = _balanceService.GetByUserId(userid);
            var netBalance = balance.net_balance ?? 0;
            if (model.amount.Replace(",","").AsDecimal() < 1000)
            {
                ModelState.AddModelError("amount", "ถอนขั้นต่ำ 1,000 บาทขึ้นไป");
            }
            if (model.amount.Replace(",", "").AsDecimal() > netBalance)
            {
                ModelState.AddModelError("amount", "ยอดเงินคงเหลือไม่พอ");
            }
            if (ModelState.IsValid)
            {
                var withdrawal = new Withdrawal_Transaction()
                {
                    int_balance_id = balance.int_balance_id,
                    int_user_id = model.int_user_id,
                    balance = balance.net_balance ?? 0,
                    amount = model.amount.Replace(",", "").AsDecimal(),
                    vat = model.vat.AsDecimal(),
                    datetime_create = DateTime.Now
                };
                if (_withdrawalTransactionService.Add(withdrawal) > 0)
                {
                    ViewBag.IsSuccess = true;
                    ModelState.Clear();
                    var newbalance = _balanceService.GetByUserId(userid);
                    model = new WithdrawalTransactionViewModel()
                    {
                        int_balance_id = balance.int_balance_id,
                        int_user_id = User.Identity.GetUserId(),
                        balance = (newbalance.net_balance ?? 0).ToString(),
                        vat = "5"
                    };
                    var mail = new SendMail();
                    //Send mail to User
                    mail.Send(user.Email, "แจ้งขอถอนเงิน", String.Format("***คุณได้ทำการถอนเงินรางวัลจำนวน {0} บาท บริษัทจะทำการโอนเงินให้ท่านภายใน 2 วันทำการ <br> ยกเว้นวันเสาร์ อาทิตย์ และวันหยุดนักขัตฤกษ์***  <span style='color:red'>(รายละเอียดตามข้อตกลงการจ่ายเงิน)</span>", String.Format("{0:#,0.00}", withdrawal.amount)));
                    //Send mail to Accounting
                    string mBody = String.Format("Username:{0} ชื่อ-สกุล:{1} {2} <br>ยอดเงินที่ถอน:{3} บาท <br> วันที่ถอน: {4} <br>หมายเลขบัญชี: {5}",
                        user.UserName, user.txt_firstname, user.txt_lastname, String.Format("{0:#,0.00}",
                        withdrawal.amount),withdrawal.datetime_create.ToString("dd/MM/yyyy HH:mm"),user.txt_bank_account);
                    mail.Send("account@cyberrymusic.com", "แจ้งขอถอนเงิน", mBody);
                }
            }
            return PartialView("_WithdrawalPartial", model);
        }

        //
        [Authorize(Roles = "Composor/Artist")]
        public ActionResult WithdrawalList()
        {
            var withdrawaltrans = _withdrawalTransactionService.GetByUserId(User.Identity.GetUserId());
            return View(withdrawaltrans);
        }

        // GET: Profile
        //[Authorize(Roles = "Composor/Artist")]
        public ActionResult Index()
        {
            ViewBag.Profile = _userService.GetById(User.Identity.GetUserId());
            ViewBag.Balance = _balanceService.GetByUserId(User.Identity.GetUserId());
            ViewBag.SongContent = _songContestService.GetByUserId(User.Identity.GetUserId());
            ViewBag.OriginalSong = _originalService.GetByUserId(User.Identity.GetUserId());
            //Update ยอดเงิน
            _balanceService.Update();
            //
            return View();
        }
        // GET: Profile/Edit/1
        public ActionResult Edit()
        {
            ViewBag.ListOfBankIds = GetListOfBankIds();
            var currentuser = _userService.GetById(User.Identity.GetUserId());
            var user = ModelHelper.ModelMapping<User, EditMemberViewModel>(currentuser);
            user.Email = currentuser.Email;
            user.FisrtName = currentuser.txt_firstname;
            user.LastName = currentuser.txt_lastname;
            user.Address = currentuser.txt_address;
            user.IsContest = currentuser.is_contest ?? false;
            user.BankId = (currentuser.int_bank_id ?? 0).ToString();
            user.BankAccount = currentuser.txt_bank_account;
            user.BankName = currentuser.txt_bank_name;
            user.IdCard = currentuser.txt_id_card;
            user.BirthDate = user.date_birth.Value.ToString("dd/MM/yyyy");
            user.Avatar = currentuser.txt_avatar;
            user.Phone = currentuser.txt_phone;
            user.ImageIdCardPath = currentuser.txt_img_id_card_path;
            return View(user);
        }

        // POST: Profile/Edit
        [HttpPost]
        public ActionResult Edit(EditMemberViewModel model)
        {
            ViewBag.ListOfBankIds = GetListOfBankIds();
            if (ModelState.IsValid)
            {
                var user = _userService.GetById(User.Identity.GetUserId());
                user.txt_firstname = model.FisrtName;
                user.txt_lastname = model.LastName;
                user.txt_address = model.Address;
                user.txt_phone = model.Phone;
                if (!string.IsNullOrEmpty(model.BankId))
                {
                    user.int_bank_id = int.Parse(model.BankId);
                }
                user.txt_bank_account = model.BankAccount;
                user.txt_bank_name = model.BankName;
                user.txt_id_card = model.IdCard;
                user.txt_avatar = model.Avatar;
                user.date_birth = DateTime.ParseExact(model.BirthDate, "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US"));
                user.txt_img_id_card_path = model.ImageIdCardPath;
                user.datetime_modified = DateTime.Now;
                if (_userService.Update(user) > 0)
                {
                    ViewBag.IsSuccess = true;
                }
            }
            return View(model);
        }

        // GET: Profile/Member
        [AllowAnonymous]
        public ActionResult Member(string id)
        {
            ViewBag.SongContent = _songContestService.GetByUserId(id);
            ViewBag.OriginalSong = _originalService.GetByUserId(id);
            ViewBag.Member = _userService.GetById(id);
            if (ViewBag.Member == null)
            {
                return RedirectToAction("PageNotFound", "Error");
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult SongDetail(int id)
        {

            var song = _songContestService.GetById(id);
            if (song == null)
            {
                return RedirectToAction("PageNotFound", "Error");
            }
            //if (id == 222)
            //{
            //    return RedirectToAction("SongDetail", "Profile", new { id = id, d = 1 });
            //}
            ViewBag.Profile = _userService.GetById(song.int_user_id);
            ViewBag.ArtistSong = _songContestService.GetByUserId(song.int_user_id);
            ViewBag.ComposerSong = _songContestService.GetByUserId(song.Song_Original.int_user_id);

            return View(song);
        }

        [AllowAnonymous]
        public ActionResult OriginalSongDetail(int id)
        {
            var song = _originalService.GetById(id);
            if (song == null)
            {
                return RedirectToAction("PageNotFound", "Error");
            }
            ViewBag.Profile = _userService.GetById(song.int_user_id);
            ViewBag.ArtistSong = _songContestService.GetByUserId(song.int_user_id);
            ViewBag.ComposerSong = _songContestService.GetByUserId(song.int_user_id);

            return View(song);
        }
        //
        public ActionResult UploadNewSong()
        {
            ViewBag.Profile = _userService.GetById(User.Identity.GetUserId());
            ViewBag.Balance = _balanceService.GetByUserId(User.Identity.GetUserId());
            return View(new SongUploadViewModel() { IsContest = true });
        }
        //
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadNewSong(SongUploadViewModel model)
        {
            ViewBag.Profile = _userService.GetById(User.Identity.GetUserId());
            ViewBag.Balance = _balanceService.GetByUserId(User.Identity.GetUserId());
            if (String.IsNullOrEmpty(model.txt_youtube_url) && String.IsNullOrEmpty(model.txt_soundcloud_url))
            {
                ModelState.AddModelError("", "Youtube Url or SoundCloud Url  is required");
            }
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var original = new Song_Original()
            {
                txt_youtube_url = (model.txt_youtube_url ?? "").Trim(),
                txt_lyric_file = (model.txt_lyric_file ?? "").Trim(),
                txt_soundcloud_url = (model.txt_soundcloud_url ?? "").Trim(),
                int_user_id = User.Identity.GetUserId(),
                txt_songname = (model.txt_song_name ?? "").Trim(),
                is_active = true,
                is_approved = true,
                datetime_create = DateTime.Now

            };
            var song = new Song_Contest()
            {
                txt_youtube_url = (model.txt_youtube_url ?? "").Trim(),
                txt_soundcloud_url = (model.txt_soundcloud_url ?? "").Trim(),
                txt_youtube_id = (model.txt_youtube_id ?? "").Trim(),
                int_user_id = User.Identity.GetUserId(),
                txt_song_name = (model.txt_song_name ?? "").Trim(),
                is_active = true,
                is_approved = true,
                datetime_create = DateTime.Now
            };
            //ต้องการส่งเพลงเข้าประกวด
            if (model.IsContest)
            {
                if (_originalService.Add(original, song) > 0)
                {
                    ViewBag.IsSuccess = true;
                    ModelState.Clear();
                    model = new SongUploadViewModel() { IsContest = true };
                }
            }
            //ไม่ต้องการส่งเพลงเข้าประกวด
            else
            {
                if (_originalService.Add(original) > 0)
                {
                    ViewBag.IsSuccess = true;
                    ModelState.Clear();
                    model = new SongUploadViewModel() { IsContest = true };
                }
            }
            return View(model);
        }
        //
        public ActionResult UploadSong()
        {
            ViewBag.Profile = _userService.GetById(User.Identity.GetUserId());
            ViewBag.Balance = _balanceService.GetByUserId(User.Identity.GetUserId());
            ViewBag.ListOfOriginalSongIds = GetListOfOriginalSongIds();
            return View(new Song_Contest() { int_user_id = User.Identity.GetUserId() });
        }
        //
        // POST: /Account/SongUpload
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadSong(Song_Contest model)
        {
            ViewBag.Profile = _userService.GetById(User.Identity.GetUserId());
            ViewBag.Balance = _balanceService.GetByUserId(User.Identity.GetUserId());
            ViewBag.ListOfOriginalSongIds = GetListOfOriginalSongIds();
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var song = new Song_Contest()
            {
                txt_youtube_url = (model.txt_youtube_url ?? "").Trim(),
                txt_youtube_id = (model.txt_youtube_id ?? "").Trim(),
                int_user_id = User.Identity.GetUserId(),
                txt_song_name = (model.txt_song_name ?? "").Trim(),
                int_song_original_id = model.int_song_original_id,
                is_active = true,
                is_approved = true,
                datetime_create = DateTime.Now
            };
            if (_songContestService.Add(song) > 0)
            {
                ViewBag.IsSuccess = true;
                ModelState.Clear();
                model = new Song_Contest();
            }

            return View(model);
        }


        public ActionResult OriginalSongUpload()
        {
            ViewBag.Profile = _userService.GetById(User.Identity.GetUserId());
            ViewBag.Balance = _balanceService.GetByUserId(User.Identity.GetUserId());
            return View(new Song_Original() { int_user_id = User.Identity.GetUserId() });
        }
        //
        // POST: /Account/OriginalSongUpload
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OriginalSongUpload(Song_Original model)
        {
            ViewBag.Profile = _userService.GetById(User.Identity.GetUserId());
            ViewBag.Balance = _balanceService.GetByUserId(User.Identity.GetUserId());
            model.int_user_id = User.Identity.GetUserId();
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (string.IsNullOrEmpty(model.txt_youtube_url) &&
                string.IsNullOrEmpty(model.txt_soundcloud_url))
            {
                ModelState.AddModelError("", "The Youtube URL field and Sound cound URL is required.");
                return View(model);
            }
            var song = new Song_Original()
            {
                txt_youtube_url = (model.txt_youtube_url ?? "").Trim(),
                txt_lyric_file = (model.txt_lyric_file ?? "").Trim(),
                txt_soundcloud_url = (model.txt_soundcloud_url ?? "").Trim(),
                int_user_id = User.Identity.GetUserId(),
                txt_songname = (model.txt_songname ?? "").Trim(),
                is_active = true,
                is_approved = true,
                datetime_create = DateTime.Now

            };
            if (_originalService.Add(song) > 0)
            {
                ViewBag.IsSuccess = true;
                ModelState.Clear();
                model = new Song_Original();
            }
            return View(model);
        }

        public List<SelectListItem> GetListOfOriginalSongIds()
        {
            var originalsongs = _originalService.GetAll();
            return originalsongs.Select(item => new SelectListItem
            {
                Text = String.Format("Song name: {0} By: {1} ", item.txt_songname, item.User.UserName),
                Value = item.int_song_original_id.ToString()
            }).ToList();
        }
    }
}