﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DINF.Helpers;
using DINF.Models;

namespace WebCyberryMusic.Controllers
{
    public class BaseController : Controller
    {
        [HttpPost]
        public JsonResult UploadSongLyrics()
        {
            HttpPostedFileBase fileupload = Request.Files[0];
            string[] sAllowedExt = new string[] { ".pdf", ".doc", ".docx" };
            if (fileupload == null || fileupload.ContentLength == 0)
            {
                return Json(new FileUpload
                {
                    IsValid = false,
                    Message = "Invalid file size.",
                    ShortPath = "",
                    FullPath = ""
                });
            }
            else if (!sAllowedExt.Contains(fileupload.FileName.Substring(fileupload.FileName.LastIndexOf('.')).ToLower()))
            {
                return Json(new FileUpload
                {
                    IsValid = false,
                    Message = "Invalid file type.",
                    ShortPath = "",
                    FullPath = ""
                });
            }
            var fileName = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), fileupload.FileName);
            var imagePath = Path.Combine(Server.MapPath(Url.Content("~/Upload/SongOriginals")), fileName);

            fileupload.SaveAs(imagePath);
            var file = new FileUpload
            {
                IsValid = true,
                Message = "",
                FileName = fileupload.FileName,
                ShortPath = String.Format("Upload/SongOriginals/{0}", fileName),
                FullPath = Url.Content(String.Format("~/Upload/SongOriginals/{0}", fileName))
            };
            return Json(file);
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult UploadImage()
        {
            HttpPostedFileBase fileupload = Request.Files[0];
            string[] sAllowedExt = new string[] { ".jpg", ".gif", ".png" };
            if (fileupload == null || fileupload.ContentLength == 0)
            {
                return Json(new FileUpload
                {
                    IsValid = false,
                    Message = "Invalid file size.",
                    ShortPath = "",
                    FullPath = ""
                });
            }
            else if (!sAllowedExt.Contains(fileupload.FileName.Substring(fileupload.FileName.LastIndexOf('.')).ToLower()))
            {
                return Json(new FileUpload
                {
                    IsValid = false,
                    Message = "Invalid file type.",
                    ShortPath = "",
                    FullPath = ""
                });
            }
            var fileName = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), fileupload.FileName);
            var imagePath = Path.Combine(Server.MapPath(Url.Content("~/Upload/Images")), fileName);

            fileupload.SaveAs(imagePath);
            var file = new FileUpload
            {
                IsValid = true,
                Message = "",
                FileName = fileupload.FileName,
                ShortPath = String.Format("Upload/Images/{0}", fileName),
                FullPath = Url.Content(String.Format("~/Upload/Images/{0}", fileName))
            };
            return Json(file);
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult UploadAttactFile()
        {
            HttpPostedFileBase fileupload = Request.Files[0];
            string[] sAllowedExt = new string[] { ".jpg", ".gif", ".png", ".pdf" };
            if (fileupload == null || fileupload.ContentLength == 0)
            {
                return Json(new FileUpload
                {
                    IsValid = false,
                    Message = "Invalid file size.",
                    ShortPath = "",
                    FullPath = ""
                });
            }
            else if (!sAllowedExt.Contains(fileupload.FileName.Substring(fileupload.FileName.LastIndexOf('.')).ToLower()))
            {
                return Json(new FileUpload
                {
                    IsValid = false,
                    Message = "Invalid file type.",
                    ShortPath = "",
                    FullPath = ""
                });
            }
            var fileName = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), fileupload.FileName);
            var imagePath = Path.Combine(Server.MapPath(Url.Content("~/Upload/AttactFiles")), fileName);

            fileupload.SaveAs(imagePath);
            var file = new FileUpload
            {
                IsValid = true,
                Message = "",
                FileName = fileupload.FileName,
                ShortPath = String.Format("Upload/AttactFiles/{0}", fileName),
                FullPath = Url.Content(String.Format("~/Upload/AttactFiles/{0}", fileName))
            };
            return Json(file);
        }

        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult CropImage(string imagePath, int? cropPointX, int? cropPointY, int? imageCropWidth, int? imageCropHeight)
        {
            if (string.IsNullOrEmpty(imagePath) || !cropPointX.HasValue || !cropPointY.HasValue || !imageCropWidth.HasValue || !imageCropHeight.HasValue)
            {
                return new HttpStatusCodeResult((int)HttpStatusCode.BadRequest);
            }

            byte[] imageBytes = System.IO.File.ReadAllBytes(Server.MapPath(imagePath));
            byte[] croppedImage = ImageHelper.CropImage(imageBytes, cropPointX.Value, cropPointY.Value, imageCropWidth.Value, imageCropHeight.Value);

            string tempFolderName = Server.MapPath("~/" + ConfigurationManager.AppSettings["Image.TempFolderName"]);
            string fileName = Path.GetFileName(imagePath);

            try
            {
                FileHelper.SaveFile(croppedImage, Path.Combine(tempFolderName, fileName));
            }
            catch (Exception ex)
            {
                //Log an error     
                return new HttpStatusCodeResult((int)HttpStatusCode.InternalServerError);
            }

            string photoPath = string.Concat("/", ConfigurationManager.AppSettings["Image.TempFolderName"], "/", fileName);
            return Json(new { photoPath = photoPath }, JsonRequestBehavior.AllowGet);
        }

        public static List<SelectListItem> GetListOfBankIds()
        {
            var listOfBankIds = new List<SelectListItem>
            {
                new SelectListItem {Text = "ธนาคารไทยพาณิชย์", Value = "1"},
                new SelectListItem {Text = "ธนาคารกสิกรไทย", Value = "2"},
                new SelectListItem {Text = "ธนาคารทหารไทย", Value = "3"}
            };

            //var listOfBankIds = _originalService.GetAll();
            //return originalsongs.Select(item => new SelectListItem
            //{
            //    Text = String.Format("Song name: {0} By: {1} ", item.txt_songname, item.User.UserName),
            //    Value = item.int_song_original_id.ToString()
            //}).ToList();


            return listOfBankIds;
        }
    }
}