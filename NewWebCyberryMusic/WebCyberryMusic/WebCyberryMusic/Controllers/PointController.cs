﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DINF.Helpers;
using DINFDAL.Interfaces;
using DINFDAL.Models;
using Fluentx.Mvc;
using Microsoft.AspNet.Identity;
using WebCyberryMusic.PaysBuyService;
using WebCyberryMusic.Helpers;
using LINEPay;
using Newtonsoft.Json.Linq;

namespace WebCyberryMusic.Controllers
{
    public class PointController : Controller
    {
        readonly log4net.ILog _logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IUserService _userService;
        private IPointService _pointService;
        private IScore_SongService _scoreService;
        private ISong_ContestService _songContestService;
        private IPaysbuyService _paysbuyService;
        private IPaysbuy_logService _paysbuyLogService;
        private IPaysBuyConfigService _paysBuyConfigService;
        private IProductService _productService;
        private ILINEPayConfigService _linepayConfigService;

        public PointController(IUserService userService,
            IPointService pointService,
            IScore_SongService scoreService,
            ISong_ContestService songContestService,
            IPaysbuyService paysbuyService,
            IPaysbuy_logService paysbuyLogService,
            IPaysBuyConfigService paysBuyConfigService,
            IProductService productService,
            ILINEPayConfigService linepayConfigService)
        {
            _userService = userService;
            _pointService = pointService;
            _scoreService = scoreService;
            _songContestService = songContestService;
            _paysbuyService = paysbuyService;
            _paysbuyLogService = paysbuyLogService;
            _paysBuyConfigService = paysBuyConfigService;
            _productService = productService;
            _linepayConfigService = linepayConfigService;
        }
        // GET: Point
        [Authorize]
        public ActionResult Index()
        {
            return View(_productService.GetAll());
        }

        // GET: Point
        [Authorize]
        public ActionResult GetPoint(string id)
        {
            var product = _productService.GetByNo(id);
            if (product == null)
            {
                return RedirectToAction("PageNotFound", "Error");
            }
            var paysbuyconfig = _paysBuyConfigService.GetAll().Single();
            var user = _userService.GetById(User.Identity.GetUserId());
            var paysbuy = _paysbuyService.GetMax();
            var inv = "";
            if (paysbuy == null)
            {
                inv = DateTime.Now.ToString("yyyyMMddhhmmss") + "0001";
            }
            else
            {
                var year = paysbuy.inv.Substring(0, 4);
                if (year.Equals(DateTime.Now.Year.ToString()))
                {
                    var runno = int.Parse(paysbuy.inv.Substring(14, 4));
                    runno++;
                    inv = DateTime.Now.ToString("yyyyMMddhhmmss") + runno.ToString("0000");
                }
                else
                {
                    inv = DateTime.Now.ToString("yyyyMMddhhmmss") + "0001";
                }
            }
            var newPaysbuy = new Paysbuy()
            {
                int_user_id = User.Identity.GetUserId(),
                inv = inv,
                itm = product.txt_product_desc,
                amt = product.dec_price.ToString(),
                point = int.Parse(product.txt_product_no),
                opt_name = string.Format("{0} {1}", user.txt_firstname, user.txt_lastname),
                opt_email = user.Email,
                opt_mobile = user.txt_phone,
                opt_address = user.txt_address,
                datetime_create = DateTime.Now,
                datetime_modified = DateTime.Now,
                payment_type = "Paysbuy"
            };
            var newPaysbuyLog = new Paysbuy_log()
            {
                int_user_id = User.Identity.GetUserId(),
                inv = inv,
                itm = product.txt_product_desc,
                amt = product.dec_price.ToString(),
                point = int.Parse(product.txt_product_no),
                opt_name = string.Format("{0} {1}", user.txt_firstname, user.txt_lastname),
                opt_email = user.Email,
                opt_mobile = user.txt_phone,
                opt_address = user.txt_address,
                txt_method = "send to paysbuy",
                datetime_create = DateTime.Now
            };
            Dictionary<string, object> postData = new Dictionary<string, object>
            {
                {"amt", product.dec_price},
                {"psbID", paysbuyconfig.psb},
                {"psb", paysbuyconfig.psb},
                {"secureCode", paysbuyconfig.secureCode},
                {"username","cyberrymusic_merchant@paysbuy.com"},
                {"biz", paysbuyconfig.biz},
                {"inv", inv},
                {"paypal_amt", ""},
                {"curr_type", "TH"},
                {"com", ""},
                {"method", ""},
                {"language", "T"},
                {"resp_front_url", paysbuyconfig.postUrl},
                {"resp_back_url", paysbuyconfig.postUrl},
                {"opt_fix_redirect", ""},
                {"opt_fix_method", ""},
                {"opt_detail", ""},
                {"opt_param", ""},
                {"itm", product.txt_product_desc},
                {"opt_name", string.Format("{0} {1}",user.txt_firstname,user.txt_lastname)},
                {"opt_email", user.Email},
                {"opt_mobile", user.txt_phone},
                {"opt_address", user.txt_address},
                {"postURL", paysbuyconfig.postUrl},
                {"reqURL", paysbuyconfig.postUrl}
            };

            if (_paysbuyService.Add(newPaysbuy, newPaysbuyLog) > 0)
            {
                SendOrderToMail(product, newPaysbuy);
                return this.RedirectAndPost(paysbuyconfig.paysbuyUrl, postData);
            }
            return View();
        }

        // GET: Point      
        [Authorize]
        public ActionResult GetPointWithLinePay(string id)
        {
            var product = _productService.GetByNo(id);
            if (product == null)
            {
                return RedirectToAction("PageNotFound", "Error");
            }
            var paysbuyconfig = _paysBuyConfigService.GetAll().Single();
            var linepayconfig = _linepayConfigService.GetAll().Single();
            var user = _userService.GetById(User.Identity.GetUserId());
            var paysbuy = _paysbuyService.GetMax();
            var inv = "";
            if (paysbuy == null)
            {
                inv = DateTime.Now.ToString("yyyyMMddhhmmss") + "0001";
            }
            else
            {
                var year = paysbuy.inv.Substring(0, 4);
                if (year.Equals(DateTime.Now.Year.ToString()))
                {
                    var runno = int.Parse(paysbuy.inv.Substring(14, 4));
                    runno++;
                    inv = DateTime.Now.ToString("yyyyMMddhhmmss") + runno.ToString("0000");
                }
                else
                {
                    inv = DateTime.Now.ToString("yyyyMMddhhmmss") + "0001";
                }
            }
            var newPaysbuy = new Paysbuy()
            {
                int_user_id = User.Identity.GetUserId(),
                inv = inv,
                itm = product.txt_product_desc,
                amt = product.dec_price.ToString(),
                point = int.Parse(product.txt_product_no),
                opt_name = string.Format("{0} {1}", user.txt_firstname, user.txt_lastname),
                opt_email = user.Email,
                opt_mobile = user.txt_phone,
                opt_address = user.txt_address,
                datetime_create = DateTime.Now,
                datetime_modified = DateTime.Now,
                payment_type = "LINEPay"
            };
            var newPaysbuyLog = new Paysbuy_log()
            {
                int_user_id = User.Identity.GetUserId(),
                inv = inv,
                itm = product.txt_product_desc,
                amt = product.dec_price.ToString(),
                point = int.Parse(product.txt_product_no),
                opt_name = string.Format("{0} {1}", user.txt_firstname, user.txt_lastname),
                opt_email = user.Email,
                opt_mobile = user.txt_phone,
                opt_address = user.txt_address,
                txt_method = "send to LINEPay",
                datetime_create = DateTime.Now
            };
            string json =
                "{\"productName\":\"" + product.txt_product_name + "\"," +
                "\"productImageUrl\":\"http://cyberrymusic.com" + product.txt_image + "\"," +
                "\"amount\":\"" + product.dec_price + "\"," +
                "\"currency\":\"" + linepayconfig.Currency + "\"," +
                "\"orderId\":\"" + inv + "\"," +
                "\"confirmUrl\":\""+ linepayconfig.ConfirmUrl +"\"," +
                "\"cancelUrl\":\""+ linepayconfig.CancelUrl  +"\"," +
                "\"capture\":\"" + linepayconfig.Capture + "\"," +
                "\"confirmUrlType\":\"" + linepayconfig.ConfirmUrlType + "\"" +
                "}";
            if (_paysbuyService.Add(newPaysbuy, newPaysbuyLog) > 0)
            {
                var linepay = new LINEPayAPI(linepayconfig.ChanelId.ToString(), linepayconfig.ChanelSecretKey, linepayconfig.LINEPayUrl);
                linepay.request(json);
                string jReturn = linepay.request(json);
                var obj = JObject.Parse(jReturn);
                //obj["info"]["paymentUrl"]["web"]

                //Update LINEPay
                var updatelinepay = _paysbuyService.GetByInvoiceId(inv);
                updatelinepay.result = obj["returnCode"].ToString();
                updatelinepay.remark = obj["returnMessage"].ToString();
                updatelinepay.transactionId = obj["info"]["transactionId"].ToString();
                //Update LINEPay Log
                var newLinepayLog = new Paysbuy_log()
                {
                    result = obj["returnCode"].ToString(),
                    remark = obj["returnMessage"].ToString(),
                    int_user_id = paysbuy.int_user_id,
                    inv = inv,
                    transactionId = obj["info"]["transactionId"].ToString(),
                    point = paysbuy.point,
                    datetime_create = DateTime.Now
                };
                _paysbuyService.Update(updatelinepay, newLinepayLog);
                if (obj["returnCode"].ToString() == "0000")
                {
                    SendOrderToMail(product, newPaysbuy);
                    return Redirect(obj["info"]["paymentUrl"]["web"].ToString());
                }
            }
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public ActionResult LinepayConfirm()
        {
            _logger.Info("Step1:LINEPay Confrim Start");
            try
            {
                string tid = Request.QueryString["transactionId"];
                _logger.Info("Step1:LINEPay TransactionId:" + tid);
                var linepayconfig = _linepayConfigService.GetAll().Single();
                var updatelinepay = _paysbuyService.GetByTransactionId(tid);
                var linepay = new LINEPayAPI(linepayconfig.ChanelId.ToString(), linepayconfig.ChanelSecretKey, linepayconfig.LINEPayUrl);
                string json = "{\"amount\":\"" + updatelinepay.amt + "\",\"currency\":\"" + linepayconfig.Currency + "\"}";
                string jReturn = linepay.confirm(tid, json);
                var obj = JObject.Parse(jReturn);
                _logger.Info("Step1:LINEPay Return Json:" + json);
                //Update LINEPay Log
                var newLinepayLog = new Paysbuy_log()
                {
                    result = obj["returnCode"].ToString(),
                    remark = obj["returnMessage"].ToString(),
                    int_user_id = updatelinepay.int_user_id,
                    inv = updatelinepay.inv,
                    transactionId = obj["info"]["transactionId"].ToString(),
                    point = updatelinepay.point,
                    datetime_create = DateTime.Now
                };
                if (obj["returnCode"].ToString() == "0000")
                {
                    var point = _pointService.GetByUserId(updatelinepay.int_user_id) ?? new Point()
                    {
                        int_user_id = updatelinepay.int_user_id,
                        int_point_id = 0,
                        int_value = 0,
                        datetime_create = DateTime.Now
                    };
                    point.int_value += updatelinepay.point ?? 0;
                    point.datetime_modified = DateTime.Now;
                    var pointTransac = new Point_Transaction()
                    {
                        int_user_id = updatelinepay.int_user_id,
                        int_value = updatelinepay.point,
                        txt_method = "buy point",
                        datetime_create = DateTime.Now
                    };
                    var updateResult = _paysbuyService.UpdateSuccess(updatelinepay, newLinepayLog, point, pointTransac);
                    _logger.Info(updateResult > 0
                        ? string.Format("Step3:LINEPay Success")
                        : string.Format("Step3:LINEPay Fail"));
                    if (updateResult > 0)
                    {
                        ViewBag.PaysBuy = updatelinepay;
                        var v = PartialView("~/Views/Shared/MailTemplates/PaymentConfirmation.cshtml");
                        var body = Utility.RenderPartialViewToString(this.ControllerContext, v);
                        var mail = new SendMail();
                        mail.Send(updatelinepay.opt_email, "ยืนยันการชำระเงิน", body);
                    }
                }
            }catch(Exception ex)
            {
                _logger.Error("LINEPay Confrim ErrorMsg:"+ex.Message);
            }
           
            return RedirectToAction("index");
        }

        [AllowAnonymous]
        public ActionResult LinepayCancel()
        {
            _logger.Info("Step1:LINEPay Cancel Start");
            return RedirectToAction("index");
        }

        private void SendOrderToMail(Product product, Paysbuy newPaysbuy)
        {
            ViewBag.Product = product;
            ViewBag.PaysBuy = newPaysbuy;
            var v = PartialView("~/Views/Shared/MailTemplates/OrderConfirmation.cshtml");
            var body = Utility.RenderPartialViewToString(this.ControllerContext, v);
            var mail = new SendMail();
            mail.Send(newPaysbuy.opt_email, "ยืนยันการสั่งซื้อสินค้า", body);
        }

        // GET: Point
        [Authorize]
        public ActionResult Transfer()
        {
            ViewBag.ListOfUserIds = GetListOfUserIds();
            var point = _pointService.GetByUserId(User.Identity.GetUserId()) ?? new Point() { int_value = 0 };
            return View(new PointTransferViewModel() { int_user_id = User.Identity.GetUserId(), CurrentPoint = point.int_value });
        }

        // GET: Point
        [Authorize]
        [HttpPost]
        public ActionResult Transfer(PointTransferViewModel model)
        {
            ViewBag.ListOfUserIds = GetListOfUserIds();
            var formPoint = _pointService.GetByUserId(User.Identity.GetUserId());
            var toPoint = _pointService.GetByUserId(model.int_target_user_id) ?? new Point() { int_point_id = 0, datetime_create = DateTime.Now };
            var targetUser = _userService.GetById(model.int_target_user_id);
            if (ModelState.IsValid)
            {
                if (targetUser == null)
                {
                    ModelState.AddModelError("int_target_user_id", "User not found");
                    return View(model);
                }
                if (formPoint == null)
                {
                    ModelState.AddModelError("Point", "Not enough points");
                    return View(model);
                }
                else if (model.Point <= 0)
                {
                    ModelState.AddModelError("Point", "Point less than zero");
                    return View(model);
                }
                else if (model.Point > formPoint.int_value)
                {
                    ModelState.AddModelError("Point", "Not enough points");
                    return View(model);
                }
                var transfer = new Point_Transaction()
                {
                    int_user_id = model.int_user_id,
                    int_target_user_id = model.int_target_user_id,
                    int_value = model.Point,
                    txt_method = "transfer",
                    datetime_create = DateTime.Now
                };
                formPoint.datetime_modified = DateTime.Now;
                formPoint.int_value -= model.Point;
                toPoint.datetime_modified = DateTime.Now;
                toPoint.int_value += model.Point;
                toPoint.int_user_id = targetUser.Id;
                if (_pointService.TransferPoint(formPoint, toPoint, transfer) > 0)
                {
                    return RedirectToAction("index", "Profile");
                }

            }
            return View(model);
        }

        // GET: Point
        [Authorize]
        public ActionResult TransferTo(string id)
        {
            ViewBag.ListOfUserIds = GetListOfUserIds();
            var formPoint = _pointService.GetByUserId(User.Identity.GetUserId()) ?? new Point() { int_value = 0 };
            var targetUser = _userService.GetById(id);
            if (targetUser == null)
            {
                return RedirectToAction("PageNotFound", "Error");
            }
            ViewBag.TargetUser = targetUser;
            return View(new PointTransferViewModel() { int_user_id = User.Identity.GetUserId(), int_target_user_id = targetUser.Id, CurrentPoint = formPoint.int_value });
        }

        // GET: Point
        [Authorize]
        [HttpPost]
        public ActionResult TransferTo(PointTransferViewModel model)
        {
            ViewBag.ListOfUserIds = GetListOfUserIds();
            var formPoint = _pointService.GetByUserId(User.Identity.GetUserId());
            var toPoint = _pointService.GetByUserId(model.int_target_user_id) ?? new Point() { int_point_id = 0, datetime_create = DateTime.Now };
            var targetUser = _userService.GetById(model.int_target_user_id);
            ViewBag.TargetUser = targetUser;
            if (ModelState.IsValid)
            {
                if (targetUser == null)
                {
                    ModelState.AddModelError("int_target_user_id", "User not found");
                    return View(model);
                }
                if (formPoint == null)
                {
                    ModelState.AddModelError("Point", "Not enough points");
                    return View(model);
                }
                else if (model.Point <= 0)
                {
                    ModelState.AddModelError("Point", "Point less than zero");
                    return View(model);
                }
                else if (model.Point > formPoint.int_value)
                {
                    ModelState.AddModelError("Point", "Not enough points");
                    return View(model);
                }
                var transfer = new Point_Transaction()
                {
                    int_user_id = model.int_user_id,
                    int_target_user_id = model.int_target_user_id,
                    int_value = model.Point,
                    txt_method = "transfer",
                    datetime_create = DateTime.Now
                };
                formPoint.datetime_modified = DateTime.Now;
                formPoint.int_value -= model.Point;
                toPoint.datetime_modified = DateTime.Now;
                toPoint.int_value += model.Point;
                toPoint.int_user_id = targetUser.Id;
                if (_pointService.TransferPoint(formPoint, toPoint, transfer) > 0)
                {
                    return RedirectToAction("index", "Profile");
                }

            }
            return View(model);
        }

        // GET: Point
        [Authorize]
        public ActionResult Vote(int id)
        {
            var song = _songContestService.GetById(id);
            if (song == null)
            {
                return RedirectToAction("PageNotFound", "Error");
            }
            var point = _pointService.GetByUserId(User.Identity.GetUserId()) ?? new Point() { int_value = 0 };
            return View(new VoteViewModel() { CurrentPoint = point.int_value, SongId = id });
        }

        // POST: Point
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Vote(VoteViewModel model)
        {
            var point = _pointService.GetByUserId(User.Identity.GetUserId());
            var score = _scoreService.GetBySongId(model.SongId);
            var song = _songContestService.GetById(model.SongId);
            if (song == null)
            {
                return RedirectToAction("PageNotFound", "Error");
            }
            if (ModelState.IsValid)
            {
                if (point == null)
                {
                    ModelState.AddModelError("Point", "Not enough points");
                    return View(model);
                }
                else if (model.Point <= 0)
                {
                    ModelState.AddModelError("Point", "Point less than zero");
                    return View(model);
                }
                else if (model.Point > point.int_value)
                {
                    ModelState.AddModelError("Point", "Not enough points");
                    return View(model);
                }
                if (score == null)
                {
                    score = new Score_Song() { int_score_id = 0, int_song_contest_id = model.SongId, int_value = model.Point, datetime_create = DateTime.Now, datetime_modified = DateTime.Now };
                }
                else
                {
                    score.int_value += model.Point;
                    score.datetime_modified = DateTime.Now;
                }
                //ตัดPoint
                point.int_value -= model.Point;
                point.datetime_modified = DateTime.Now;
                //
                var transfer = new Point_Transaction()
                {
                    int_user_id = User.Identity.GetUserId(),
                    int_target_user_id = song.int_user_id,
                    int_value = model.Point,
                    txt_method = "vote",
                    datetime_create = DateTime.Now
                };
                var votehis = new Vote_History()
                {
                    int_vote_by_user_id = User.Identity.GetUserId(),
                    int_song_contest_id = song.int_song_contest_id,
                    int_vote_method = 2,
                    int_vote_value = model.Point,
                    is_calculated = false,
                    datetime_create = DateTime.Now
                };
                if (_pointService.VotePoint(point, score, transfer, votehis) > 0)
                {
                    ViewBag.CurrentPoint = _pointService.GetByUserId(User.Identity.GetUserId()).int_value;
                    return View("VoteSuccess");
                }
            }
            return View(model);
        }
        //
        [HttpPost]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Receive(FormCollection collection)
        {
            var service = new gettransactionSoapClient();
            //service.getTransactionByInvoice()

            _logger.Info("Step1:Start");
            var resultmsg = "";
            var msg = "";
            try
            {
                _logger.Info(string.Format("Step2:Get Data Collections [ result: {0} apCode: {1} amt: {2} fee: {3} method: {4} confirm_cs: {5}]", collection["result"], collection["apCode"], collection["amt"], collection["fee"], collection["method"], collection["confirm_cs"]));
                string result = collection["result"];
                //string result = "02inv500"; 

                string result2 = result.Substring(0, 2);

                string inv = result.Substring(2, 18);

                string apCode = collection["apCode"];
                //string apCode = "123456";

                string amt = collection["amt"];
                //string amt = "1000";

                string fee = collection["fee"];
                //string fee = "0";

                string method = collection["method"];
                //string method = "06";

                string confirm_cs = collection["confirm_cs"];
                //string confirm_cs = "false";
                //Update PaysBuy
                var paysbuy = _paysbuyService.GetByInvoiceId(inv);
                if (paysbuy.apCode == apCode && paysbuy.result == result)
                {
                    _logger.Info("Step2:Duplicate data");
                    return RedirectToAction("Index");
                }
                paysbuy.result = result;
                paysbuy.apCode = apCode;
                paysbuy.amt = amt;
                paysbuy.fee = fee;
                paysbuy.method = method;
                paysbuy.datetime_modified = DateTime.Now;
                //Update PaysBuy Log
                var newPaysbuyLog = new Paysbuy_log()
                {
                    result = result,
                    apCode = apCode,
                    int_user_id = paysbuy.int_user_id,
                    inv = inv,
                    amt = amt,
                    point = paysbuy.point,
                    fee = fee,
                    confirm_cs = confirm_cs,
                    datetime_create = DateTime.Now
                };

                /* status result
                00=Success
                99=Fail
                02=Process
                */
                switch (result2)
                {
                    case "00":
                        if (method == "06")
                        {
                            switch (confirm_cs)
                            {
                                case "true":
                                    resultmsg = "Success";
                                    break;
                                case "false":
                                    resultmsg = "Fail";
                                    break;
                                default:
                                    resultmsg = "Process";
                                    break;
                            }
                        }
                        else
                        {
                            resultmsg = "Success";
                        }
                        break;
                    case "99":
                        resultmsg = "Fail";
                        break;
                    case "02":
                        resultmsg = "Process";
                        break;
                    default:
                        resultmsg = "Error";
                        break;
                }

                newPaysbuyLog.txt_method = resultmsg;
                //PaysBuy Success
                if (resultmsg.Equals("Success"))
                {
                    var point = _pointService.GetByUserId(paysbuy.int_user_id) ?? new Point()
                    {
                        int_user_id = paysbuy.int_user_id,
                        int_point_id = 0,
                        int_value = 0,
                        datetime_create = DateTime.Now
                    };
                    point.int_value += paysbuy.point ?? 0;
                    point.datetime_modified = DateTime.Now;
                    var pointTransac = new Point_Transaction()
                    {
                        int_user_id = paysbuy.int_user_id,
                        int_value = paysbuy.point,
                        txt_method = "buy point",
                        datetime_create = DateTime.Now
                    };
                    var paysbuyconfig = _paysBuyConfigService.GetAll().Single();
                    var gettransaction = service.getTransactionByInvoice(
                        paysbuyconfig.psb,
                        paysbuyconfig.biz,
                        paysbuyconfig.secureCode,
                        paysbuy.inv);
                    if (gettransaction[0].result.Equals(result2)
                        && gettransaction[0].apCode.Equals(apCode))
                    {
                        var updateResult = _paysbuyService.UpdateSuccess(paysbuy, newPaysbuyLog, point, pointTransac);
                        _logger.Info(updateResult > 0
                            ? string.Format("Step3:PaysBuy Success")
                            : string.Format("Step3:PaysBuy Fail"));
                        if (updateResult > 0)
                        {
                            ViewBag.PaysBuy = paysbuy;
                            var v = PartialView("~/Views/Shared/MailTemplates/PaymentConfirmation.cshtml");
                            var body = Utility.RenderPartialViewToString(this.ControllerContext, v);
                            var mail = new SendMail();
                            mail.Send(paysbuy.opt_email, "ยืนยันการชำระเงิน", body);
                        }
                    }
                    else
                    {
                        _logger.Info(string.Format("Step3:Get Transaction Mismatch"));
                    }
                }
                else
                {
                    _logger.Info(_paysbuyService.Update(paysbuy, newPaysbuyLog) > 0
                        ? string.Format("Step3:Update PaysBuy Success")
                        : string.Format("Step3:Update PaysBuy Fail"));
                }

                msg = "result=" + result + "," + "apCode=" + apCode + "," + "ราคา ="
                + amt + "," + "fee =" + fee + "," + "result2 =" + result2 + "," + "inv=" + inv;

            }
            catch (Exception ex)
            {
                _logger.Error("Error:" + ex.Message + ex.Source + ex.StackTrace);
                return RedirectToAction("Index");
            }
            _logger.Info("Step4:Success,Msg:" + msg);
            return RedirectToAction("Index");
        }


        public List<SelectListItem> GetListOfUserIds()
        {
            var users = _userService.GetAll().Where(i => i.EmailConfirmed);
            return users.Select(item => new SelectListItem
            {
                Text = String.Format("User: {0} Full Name: {1} {2} ", item.UserName, item.txt_firstname, item.txt_lastname),
                Value = item.Id.ToString()
            }).ToList();
        }
    }
}