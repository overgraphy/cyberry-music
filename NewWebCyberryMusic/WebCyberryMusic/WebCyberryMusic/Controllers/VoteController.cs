﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DINFDAL.Helpers;
using DINFDAL.Interfaces;
using DINFDAL.Models;
using WebCyberryMusic.Models;

namespace WebCyberryMusic.Controllers
{
    public class VoteController : Controller
    {
        readonly log4net.ILog _logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //private ISms_DataService _smsDataService;

        //public VoteController(ISms_DataService smsDataService)
        //{
        //  //  _smsDataService = smsDataService;
        //}

        // POST: Vote/Sms
        [HttpPost]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public JsonResult SMS(FormCollection collection)
        {
            try
            {
                _logger.Info("Step1:Start");
                _logger.Info(string.Format("Step2:Get Data Collections [ MSG_ID: {0} PHONENUMBER: {1} DATETIME: {2} MESSAGE: {3}]", collection["MSG_ID"], collection["PHONENUMBER"], collection["DATETIME"], collection["MESSAGE"]));
                _logger.Info("date:" + DateTime.ParseExact(collection["DATETIME"], "yyyy/MM/dd HH:mm:ss", CultureInfo.CreateSpecificCulture("en-US")));
                var db = new CyberryMusicEntities();
                var sms = new Sms_Data
                {
                    txt_refer_id = collection["MSG_ID"],
                    txt_phone = collection["PHONENUMBER"],
                    datetime_sent_sms = DateTime.ParseExact(collection["DATETIME"], "yyyy/MM/dd HH:mm:ss", CultureInfo.CreateSpecificCulture("en-US")),
                    datetime_create = DateTime.Now,
                    int_vote_number = int.Parse(collection["MESSAGE"].Substring(2, (collection["MESSAGE"].Length)-2))
                };

                var oldSms = db.Sms_Data.FirstOrDefault(i => i.txt_refer_id.Equals(sms.txt_refer_id));
                if (oldSms != null)
                {
                    _logger.Info("Step3_1:Update Old Sms");
                    oldSms.txt_phone = sms.txt_phone;
                    oldSms.int_vote_number = sms.int_vote_number;
                    oldSms.datetime_sent_sms = sms.datetime_sent_sms;
                    oldSms.datetime_create = DateTime.Now;
                    _logger.Info(string.Format("Step3_1 SanveChange:{0}", db.SaveChanges()));
                }
                else
                {
                    _logger.Info("Step3_2:Add new Sms and Update score");
                    db.Sms_Data.Add(sms);
                    var votehis = new Vote_History()
                    {
                        int_song_contest_id = sms.int_vote_number,
                        int_vote_method = 1,
                        int_vote_value = 1,
                        is_calculated = false,
                        datetime_create = DateTime.Now,
                        int_vote_ss = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Session"].ToString())
                    };
                    db.Vote_History.Add(votehis);
                    var scoreSong = db.Score_Song.FirstOrDefault(i => i.int_song_contest_id == sms.int_vote_number);
                    if (scoreSong != null)
                    {
                        scoreSong.int_value = scoreSong.int_value + 1;
                    }
                    else
                    {
                        db.Entry(new Score_Song() { int_song_contest_id = sms.int_vote_number, int_value = 1, datetime_create = DateTime.Now, datetime_modified = DateTime.Now }).State = EntityState.Added;
                    }
                    _logger.Info(string.Format("Step3_2 SanveChange:{0}", db.SaveChanges()));
                }
                db.Dispose();
            }
            catch (Exception ex)
            {
                _logger.Error("Error:" + ex.Message + ex.Source + ex.StackTrace);
                return Json(new { result = "Fail", error = ex.Message });
            }
            _logger.Info("Step3:Success");
            return Json(new { result = "Success" });
        }

    }
}
