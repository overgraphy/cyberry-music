﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WebCyberryMusic.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public partial class ApplicationUser : IdentityUser
    {
        public string txt_firstname { get; set; }

        public string txt_lastname { get; set; }

        public string txt_username { get; set; }

        public string txt_avatar { get; set; }

        public bool is_contest { get; set; }

        public bool is_composer { get; set; }

        public bool is_activated { get; set; }

        public bool is_banned { get; set; }

        public string txt_banned_reason { get; set; }

        public int? int_bank_id { get; set; }

        public string txt_bank_name { get; set; }

        public string txt_bank_account { get; set; }

        public string txt_phone { get; set; }

        public string txt_id_card { get; set; }

        public string txt_img_id_card_path { get; set; }

        public string txt_new_password_key { get; set; }

        public string txt_new_password_requested { get; set; }

        public string txt_new_email { get; set; }

        public string txt_new_email_key { get; set; }

        public System.DateTime? date_birth { get; set; }

        public string txt_address { get; set; }

        public string txt_last_ip { get; set; }

        public System.DateTime? datetime_lastlogin { get; set; }

        public System.DateTime? datetime_create { get; set; }

        public System.DateTime? datetime_modified { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("CyberrymusicConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Change the name of the table to be Users instead of AspNetUsers
            modelBuilder.Entity<IdentityUser>()
                .ToTable("Users");
            modelBuilder.Entity<ApplicationUser>()
                .ToTable("Users");
        }
    }
}