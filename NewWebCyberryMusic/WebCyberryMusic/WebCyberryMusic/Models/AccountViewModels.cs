﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DINFDAL.Models;

namespace WebCyberryMusic.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        public string Id { get; set; }
        public bool EmailConfirmed { get; set; }
        public string RoleId { get; set; }
        public bool IsActive { get; set; }

        [Required]
        [Display(Name = "User name")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 5)]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Fisrt Name")]
        [StringLength(200, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string FisrtName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(200, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Address")]
        [StringLength(1000, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "Phone")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string Phone { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public string BirthDate { get; set; }

        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

        public int? BankId { get; set; }

        [Display(Name = "Bank Account")]
        [StringLength(1000, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string BankAccount { get; set; }

        [Display(Name = "ID Card")]
        [IdCardValidator]
        [DataType(DataType.Text)]
        public string IdCard { get; set; }

        public string ImageIdCardPath { get; set; }

        public bool IsContest { get; set; }

        public bool IsAccept { get; set; }
    }

    public class ExternalRegisterViewModel
    {
        [Required]
        [Display(Name = "User name")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 5)]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Fisrt Name")]
        [StringLength(200, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string FisrtName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(200, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Address")]
        [StringLength(1000, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "Phone")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string Phone { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public string BirthDate { get; set; }

        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

        public int? BankId { get; set; }

        [Display(Name = "Bank Account")]
        [StringLength(1000, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string BankAccount { get; set; }

        [Display(Name = "ID Card")]
        [IdCardValidator]
        [DataType(DataType.Text)]
        public string IdCard { get; set; }

        public string ImageIdCardPath { get; set; }

        public bool IsContest { get; set; }

        public bool IsAccept { get; set; }
    }

    public class EditMemberViewModel
    {
        public string Id { get; set; }
        public bool EmailConfirmed { get; set; }
        public string RoleId { get; set; }
        public bool IsActive { get; set; }

        [Required]
        [Display(Name = "User name")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 5)]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Fisrt Name")]
        [StringLength(200, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string FisrtName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(200, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Address")]
        [StringLength(1000, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "Phone")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string Phone { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public string BirthDate { get; set; }

        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

        public string BankId { get; set; }

        [Display(Name = "Bank Account")]
        [StringLength(1000, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string BankAccount { get; set; }

        [Display(Name = "ID Card")]
        [IdCardValidator]
        [DataType(DataType.Text)]
        public string IdCard { get; set; }

        public string ImageIdCardPath { get; set; }

        public bool IsContest { get; set; }

        public bool IsAccept { get; set; }

        public bool IsBanned { get; set; }

        public string BannedReason { get; set; }

        public Nullable<System.DateTime> date_birth { get; set; }

        public string Avatar { get; set; }
    }


    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }
    }

    public class ManageUserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
