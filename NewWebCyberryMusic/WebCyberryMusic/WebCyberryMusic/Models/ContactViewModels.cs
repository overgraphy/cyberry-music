﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebCyberryMusic.Models
{
    public class ContactViewModels
    {
        [Required]
        [Display(Name = "Name")]
        [StringLength(200, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Email")]
        [StringLength(200, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Subject")]
        [StringLength(200, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string Subject { get; set; }

        [Required]
        [Display(Name = "Message")]
        [StringLength(2000, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string Message { get; set; }
    }
}