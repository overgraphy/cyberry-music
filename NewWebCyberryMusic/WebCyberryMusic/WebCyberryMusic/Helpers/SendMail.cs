﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace DINF.Helpers
{
    public class SendMail
    {
        readonly log4net.ILog _logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public bool Send(string SendToEmail, string Subject, string Body, List<string> Attactfiles=null)
        {
            try
            {
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.From = new System.Net.Mail.MailAddress(System.Web.Configuration.WebConfigurationManager.AppSettings["EmailSender"].ToString(), "Cyberry Music Admin");
                message.To.Add(new System.Net.Mail.MailAddress(SendToEmail));
                message.IsBodyHtml = true;
                message.BodyEncoding = Encoding.UTF8;
                message.Subject = Subject;
                message.Body = Body;
                if (Attactfiles != null)
                    foreach (var item in Attactfiles)
                    {
                        message.Attachments.Add(new Attachment(item));
                    }
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
                client.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("Send Mail Error:{0} {1}", SendToEmail, ex.Message));
                return false;
            }
        }
    }
}