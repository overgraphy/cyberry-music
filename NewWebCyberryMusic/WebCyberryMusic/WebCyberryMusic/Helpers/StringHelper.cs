﻿using System;

namespace DINF.Helpers
{
    public class StringHelper
    {
        public static string Culture(string lang, string strth, string stren)
        {
            lang = lang.ToUpper();
            return String.Format(lang.Equals("TH") ? strth ?? "" : stren ?? "");
        }

        public static string RandomPassword(int passwordLength)
        {
            string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
            var randNum = new Random();
            char[] chars = new char[passwordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }
    }
}