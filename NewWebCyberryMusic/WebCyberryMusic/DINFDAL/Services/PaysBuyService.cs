﻿using DINFDAL.Interfaces;
using DINFDAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DINFDAL.Services
{
    public class PaysbuyService : IPaysbuyService
    {
        private CyberryMusicEntities _db = new CyberryMusicEntities();
        private DbContextTransaction _dbContextTransaction;

        //Insert
        public int Add(Paysbuy paysbuy)
        {
            int result = 0;

            try
            {
                _db.Entry(paysbuy).State = EntityState.Added;
                result = _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Insert
        public int Add(Paysbuy paysbuy, Paysbuy_log paysbuyLog)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        _db.Entry(paysbuy).State = EntityState.Added;
                        _db.Entry(paysbuyLog).State = EntityState.Added;
                        result = _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Add(IList<Paysbuy> paysbuys)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var model in paysbuys)
                        {
                            _db.Entry(model).State = EntityState.Added;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Update
        public int Update(Paysbuy paysbuy)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value

                        _db.Entry(paysbuy).State = EntityState.Modified;
                        result = _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }


        //Update
        public int Update(Paysbuy paysbuy, Paysbuy_log paysbuyLog)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        _db.Entry(paysbuy).State = EntityState.Modified;
                        _db.Entry(paysbuyLog).State = EntityState.Added;
                        result = _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Update
        public int UpdateSuccess(Paysbuy paysbuy, Paysbuy_log paysbuyLog, Point point, Point_Transaction pointTransaction)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        _db.Entry(paysbuy).State = EntityState.Modified;
                        _db.Paysbuy_log.Add(paysbuyLog);
                        //update point
                        if (point.int_point_id > 0)
                        {
                            var getpoint = _db.Points.Find(point.int_point_id);
                            getpoint.datetime_modified = point.datetime_modified;
                            getpoint.int_value = point.int_value;
                        }
                        else
                        {
                            _db.Points.Add(point);
                        }
                        //
                        _db.Point_Transaction.Add(pointTransaction);
                        result = _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Update(IList<Paysbuy> paysbuys)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value
                        foreach (var model in paysbuys)
                        {
                            _db.Entry(model).State = EntityState.Modified;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Select all
        public IList<Paysbuy> GetAll()
        {
            IList<Paysbuy> models;

            try
            {
                models = _db.Paysbuys.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        public IList<Paysbuy> GetByUserId(string id)
        {
            IList<Paysbuy> models;

            try
            {
                models = _db.Paysbuys.Where(i => i.int_user_id.Equals(id)).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select by id
        public Paysbuy GetById(int id)
        {
            Paysbuy model;

            try
            {
                model = _db.Paysbuys.Find(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        //Select by id
        public Paysbuy GetByInvoiceId(string id)
        {
            Paysbuy model;

            try
            {
                model = _db.Paysbuys.FirstOrDefault(i => i.inv.Equals(id));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        //Select by id
        public Paysbuy GetByTransactionId(string id)
        {
            Paysbuy model;

            try
            {
                model = _db.Paysbuys.FirstOrDefault(i => i.transactionId.Equals(id));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        //Select Max
        public Paysbuy GetMax()
        {
            Paysbuy model;

            try
            {
                model = _db.Paysbuys.OrderByDescending(i => i.int_paysbuy_id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }


        public IList<Paysbuy> Fetch(Expression<Func<Paysbuy, bool>> predicate, bool active = true)
        {
            IList<Paysbuy> models;

            try
            {
                models = _db.Set<Paysbuy>().Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }
    }
}

namespace DINFDAL.Interfaces
{
    public interface IPaysbuyService
    {
        int Add(Paysbuy paysbuy);

        int Add(Paysbuy paysbuy, Paysbuy_log paysbuyLog);

        int Add(IList<Paysbuy> paysbuys);

        int Update(Paysbuy paysbuy);

        int Update(Paysbuy paysbuy, Paysbuy_log paysbuyLog);

        int UpdateSuccess(Paysbuy paysbuy, Paysbuy_log paysbuyLog, Point point, Point_Transaction pointTransaction);

        int Update(IList<Paysbuy> paysbuys);

        IList<Paysbuy> GetAll();

        IList<Paysbuy> GetByUserId(string id);

        Paysbuy GetById(int id);

        Paysbuy GetByInvoiceId(string id);

        Paysbuy GetByTransactionId(string id);

        Paysbuy GetMax();

        IList<Paysbuy> Fetch(Expression<Func<Paysbuy, bool>> predicate, bool active = true);
    }
}