﻿using DINFDAL.Interfaces;
using DINFDAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DINFDAL.Services
{
    public class Withdrawal_TransactionService : IWithdrawal_TransactionService
    {
        private CyberryMusicEntities _db = new CyberryMusicEntities();
        private DbContextTransaction _dbContextTransaction;

        //Insert
        public int Add(Withdrawal_Transaction withdrawal_Transaction)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        var balance =
                            _db.Balances.FirstOrDefault(i => i.int_balance_id == withdrawal_Transaction.int_balance_id);
                        balance.net_balance -= withdrawal_Transaction.amount;
                        if (balance.net_balance < 0)
                        {
                            throw new Exception("ยอดเงินคงเหลือไม่พอ");
                        }
                        balance.datetime_modified = DateTime.Now;
                        _db.Withdrawal_Transaction.Add(withdrawal_Transaction);
                        result += _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Add(IList<Withdrawal_Transaction> withdrawal_Transactions)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var model in withdrawal_Transactions)
                        {
                            _db.Entry(model).State = EntityState.Added;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Update
        public int Update(Withdrawal_Transaction withdrawal_Transaction)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value

                        _db.Entry(withdrawal_Transaction).State = EntityState.Modified;
                        result = _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Update(IList<Withdrawal_Transaction> withdrawal_Transactions)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value
                        foreach (var model in withdrawal_Transactions)
                        {
                            _db.Entry(model).State = EntityState.Modified;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Select all
        public IList<Withdrawal_Transaction> GetAll()
        {
            IList<Withdrawal_Transaction> models;

            try
            {
                models = _db.Withdrawal_Transaction.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select by id
        public Withdrawal_Transaction GetById(int id)
        {
            Withdrawal_Transaction model;

            try
            {
                model = _db.Withdrawal_Transaction.Find(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        //Select by id
        public IList<Withdrawal_Transaction> GetByUserId(string id)
        {
            IList<Withdrawal_Transaction> models;
            try
            {
                models = _db.Withdrawal_Transaction.Where(i => i.int_user_id.Equals(id)).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        public IList<Withdrawal_Transaction> Fetch(Expression<Func<Withdrawal_Transaction, bool>> predicate, bool active = true)
        {
            IList<Withdrawal_Transaction> models;

            try
            {
                models = _db.Set<Withdrawal_Transaction>().Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }
    }
}

namespace DINFDAL.Interfaces
{
    public interface IWithdrawal_TransactionService
    {
        int Add(Withdrawal_Transaction withdrawal_Transaction);

        int Add(IList<Withdrawal_Transaction> withdrawal_Transactions);

        int Update(Withdrawal_Transaction withdrawal_Transaction);

        int Update(IList<Withdrawal_Transaction> withdrawal_Transactions);

        IList<Withdrawal_Transaction> GetAll();

        Withdrawal_Transaction GetById(int id);

        IList<Withdrawal_Transaction> GetByUserId(string id);

        IList<Withdrawal_Transaction> Fetch(Expression<Func<Withdrawal_Transaction, bool>> predicate, bool active = true);
    }
}