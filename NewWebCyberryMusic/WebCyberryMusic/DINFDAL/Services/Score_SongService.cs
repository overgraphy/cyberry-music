﻿using DINFDAL.Interfaces;
using DINFDAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DINFDAL.Services
{
    public class Score_SongService : IScore_SongService
    {
        private CyberryMusicEntities _db = new CyberryMusicEntities();
        private DbContextTransaction _dbContextTransaction;

        //Insert
        public int Add(Score_Song score_Song)
        {
            int result = 0;

            try
            {
                _db.Entry(score_Song).State = EntityState.Added;
                result = _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Add(IList<Score_Song> score_Songs)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var model in score_Songs)
                        {
                            _db.Entry(model).State = EntityState.Added;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Update
        public int Update(Score_Song score_Song)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value

                        _db.Entry(score_Song).State = EntityState.Modified;
                        result = _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Update(IList<Score_Song> score_Songs)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value
                        foreach (var model in score_Songs)
                        {
                            _db.Entry(model).State = EntityState.Modified;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Select all
        public IList<Score_Song> GetAll()
        {
            IList<Score_Song> models;

            try
            {
                models = _db.Score_Song.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select by id
        public Score_Song GetById(int id)
        {
            Score_Song model;

            try
            {
                model = _db.Score_Song.Find(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        //Select by id
        public Score_Song GetBySongId(int id)
        {
            Score_Song model;

            try
            {
                model = _db.Score_Song.FirstOrDefault(i => i.int_song_contest_id == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        public IList<Score_Song> Fetch(Expression<Func<Score_Song, bool>> predicate, bool active = true)
        {
            IList<Score_Song> models;

            try
            {
                models = _db.Set<Score_Song>().Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }
    }
}

namespace DINFDAL.Interfaces
{
    public interface IScore_SongService
    {
        int Add(Score_Song score_Song);

        int Add(IList<Score_Song> score_Songs);

        int Update(Score_Song score_Song);

        int Update(IList<Score_Song> score_Songs);

        IList<Score_Song> GetAll();

        Score_Song GetById(int id);

        Score_Song GetBySongId(int id);

        IList<Score_Song> Fetch(Expression<Func<Score_Song, bool>> predicate, bool active = true);
    }
}