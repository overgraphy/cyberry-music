﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Transactions;
using DINFDAL.Models;
using DINFDAL.Interfaces;

namespace DINFDAL.Services
{
    public class LINEPayConfigService : ILINEPayConfigService
    {
        private CyberryMusicEntities _db = new CyberryMusicEntities();
        private DbContextTransaction _dbContextTransaction;
        //Insert
        public int Add(LINEPayConfig lINEPayConfig)
        {
            int result = 0;

            try
            {
                _db.Entry(lINEPayConfig).State = EntityState.Added;
                result = _db.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Add(IList<LINEPayConfig> lINEPayConfigs)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var model in lINEPayConfigs)
                        {
                            _db.Entry(model).State = EntityState.Added;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Update
        public int Update(LINEPayConfig lINEPayConfig)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value

                        _db.Entry(lINEPayConfig).State = EntityState.Modified;
                        result = _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Update(IList<LINEPayConfig> lINEPayConfigs)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value
                        foreach (var model in lINEPayConfigs)
                        {
                            _db.Entry(model).State = EntityState.Modified;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }
        //Select all
        public IList<LINEPayConfig> GetAll()
        {
            IList<LINEPayConfig> models;

            try
            {
                models = _db.LINEPayConfigs.ToList();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }
        //Select by id
        public LINEPayConfig GetById(int id)
        {
            LINEPayConfig model;

            try
            {
                model = _db.LINEPayConfigs.Find(id);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        public IList<LINEPayConfig> Fetch(Expression<Func<LINEPayConfig, bool>> predicate, bool active = true)
        {
            IList<LINEPayConfig> models;

            try
            {
                models = _db.Set<LINEPayConfig>().Where(predicate).ToList();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }
    }
}

namespace DINFDAL.Interfaces
{
    public interface ILINEPayConfigService
    {
        int Add(LINEPayConfig lINEPayConfig);
        int Add(IList<LINEPayConfig> lINEPayConfigs);
        int Update(LINEPayConfig lINEPayConfig);
        int Update(IList<LINEPayConfig> lINEPayConfigs);
        IList<LINEPayConfig> GetAll();
        LINEPayConfig GetById(int id);
        IList<LINEPayConfig> Fetch(Expression<Func<LINEPayConfig, bool>> predicate, bool active = true);
    }
}
