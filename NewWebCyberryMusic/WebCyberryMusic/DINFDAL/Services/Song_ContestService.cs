﻿using DINFDAL.Interfaces;
using DINFDAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DINFDAL.Services
{
    public class Song_ContestService : ISong_ContestService
    {
        private CyberryMusicEntities _db = new CyberryMusicEntities();
        private DbContextTransaction _dbContextTransaction;

        //Insert
        public int Add(Song_Contest song_Contest)
        {
            int result = 0;

            try
            {
                _db.Entry(song_Contest).State = EntityState.Added;
                result = _db.SaveChanges();
                song_Contest.txt_vote_code = song_Contest.int_song_contest_id.ToString("CM00000");
                result = _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Add(IList<Song_Contest> song_Contests)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var model in song_Contests)
                        {
                            _db.Entry(model).State = EntityState.Added;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Update
        public int Update(Song_Contest song_Contest)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value

                        _db.Entry(song_Contest).State = EntityState.Modified;
                        result = _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Update(IList<Song_Contest> song_Contests)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value
                        foreach (var model in song_Contests)
                        {
                            _db.Entry(model).State = EntityState.Modified;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Select all
        public IList<Song_Contest> GetAll()
        {
            IList<Song_Contest> models;

            try
            {
                models = _db.Song_Contest.Where(i => i.is_active == true && i.is_approved == true).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        public IList<Song_Contest> GetPopularSong(int take = 0)
        {
            IList<Song_Contest> models;

            try
            {
                if (take > 0)
                {
                    models = _db.Song_Contest.Where(i => i.is_active == true && i.is_approved == true)
                        .OrderByDescending(i => i.Score_Song.Sum(t => t.int_value)).Take(take).ToList();
                }
                else
                {
                    models = _db.Song_Contest.Where(i => i.is_active == true && i.is_approved == true)
                        .OrderByDescending(i => i.Score_Song.Sum(t => t.int_value)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        public IList<Song_Contest> GetNewSong(int take = 0)
        {
            IList<Song_Contest> models;

            try
            {
                if (take > 0)
                {
                    models =
                        _db.Song_Contest.Where(i => i.is_active == true && i.is_approved == true)
                            .Take(take)
                            .OrderByDescending(i => i.datetime_create)
                            .ToList();
                }
                else
                {
                    models =
                      _db.Song_Contest.Where(i => i.is_active == true && i.is_approved == true)
                          .OrderByDescending(i => i.datetime_create)
                          .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        public IList<Song_Contest> GetRandomSong(int take = 0)
        {
            IList<Song_Contest> models;

            try
            {
                if (take > 0)
                {
                    models = _db.Song_Contest.Where(i => i.is_active == true && i.is_approved == true).OrderBy(r => Guid.NewGuid()).Take(take).ToList();
                }
                else
                {
                    models = _db.Song_Contest.Where(i => i.is_active == true && i.is_approved == true).OrderBy(r => Guid.NewGuid()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        public IList<Song_Contest> GetEditorPickSong(int take = 0)
        {
            IList<Song_Contest> models;

            try
            {
                if (take > 0)
                {
                    models = _db.Song_Contest.Where(i => i.is_active == true && i.is_approved == true && i.is_edittor_pick == true).Take(take).ToList();
                }
                else
                {
                    models = _db.Song_Contest.Where(i => i.is_active == true && i.is_approved == true && i.is_edittor_pick == true).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select Hot
        public IList<Song_Contest> GetHotVideo()
        {
            IList<Song_Contest> models;

            try
            {
                models = _db.Song_Contest.Where(i => i.is_active == true && i.is_approved == true)
                    .OrderByDescending(i => i.Score_Song.Sum(t => t.int_value)).Take(20).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select Hot
        public IList<vUserScoreSong> GetHotArtistlist()
        {
            IList<vUserScoreSong> models;

            try
            {
                models = _db.vUserScoreSongs.Where(i => i.score > 0).Take(12).OrderByDescending(i => i.score).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select Hot
        public IList<vUserScoreSong> GetNewArtistlist()
        {
            IList<vUserScoreSong> models;

            try
            {
                models = _db.vUserScoreSongs.Take(12).OrderByDescending(i => i.datetime_create).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select New
        public IList<Song_Contest> GetNewVideo()
        {
            IList<Song_Contest> models;

            try
            {
                models = _db.Song_Contest.Where(i => i.is_active == true && i.is_approved == true).Take(20).OrderByDescending(i => i.datetime_create).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select Hot
        public IList<vUserScoreSong> GetAllArtistlist()
        {
            IList<vUserScoreSong> models;

            try
            {
                models = _db.vUserScoreSongs.OrderByDescending(i => i.datetime_create).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select by User Id
        public Song_Contest GetById(int id)
        {
            Song_Contest model;

            try
            {
                model = _db.Song_Contest.FirstOrDefault(i => i.is_active && i.is_approved && i.int_song_contest_id == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        public IList<vTopScore> GetTopScores()
        {
            IList<vTopScore> models;

            try
            {
                models = _db.vTopScores.OrderByDescending(i => i.int_value).Take(20).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select by id
        public IList<Song_Contest> GetByUserId(string id)
        {
            IList<Song_Contest> model;

            try
            {
                model =
                    _db.Song_Contest.Where(i => i.is_active == true && i.is_approved == true && i.int_user_id == id)
                        .ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        public IList<Song_Contest> Fetch(Expression<Func<Song_Contest, bool>> predicate, bool active = true)
        {
            IList<Song_Contest> models;

            try
            {
                models = _db.Set<Song_Contest>().Where(m => m.is_active == active).Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }
    }
}

namespace DINFDAL.Interfaces
{
    public interface ISong_ContestService
    {
        int Add(Song_Contest song_Contest);

        int Add(IList<Song_Contest> song_Contests);

        int Update(Song_Contest song_Contest);

        int Update(IList<Song_Contest> song_Contests);

        IList<Song_Contest> GetAll();

        IList<Song_Contest> GetPopularSong(int take = 0);

        IList<Song_Contest> GetNewSong(int take = 0);

        IList<Song_Contest> GetRandomSong(int take = 0);

        IList<Song_Contest> GetEditorPickSong(int take = 0);

        IList<Song_Contest> GetHotVideo();

        IList<Song_Contest> GetNewVideo();

        IList<vUserScoreSong> GetHotArtistlist();

        IList<vUserScoreSong> GetNewArtistlist();

        IList<vUserScoreSong> GetAllArtistlist();

        Song_Contest GetById(int id);

        IList<vTopScore> GetTopScores();

        IList<Song_Contest> GetByUserId(string id);

        IList<Song_Contest> Fetch(Expression<Func<Song_Contest, bool>> predicate, bool active = true);
    }
}