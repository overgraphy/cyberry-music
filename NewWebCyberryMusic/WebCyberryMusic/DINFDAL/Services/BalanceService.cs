﻿using DINFDAL.Interfaces;
using DINFDAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DINFDAL.Services
{
    public class BalanceService : IBalanceService
    {
        private CyberryMusicEntities _db = new CyberryMusicEntities();
        private DbContextTransaction _dbContextTransaction;

        //Insert
        public int Add(Balance balance)
        {
            int result = 0;

            try
            {
                _db.Entry(balance).State = EntityState.Added;
                result = _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Add(IList<Balance> balances)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var model in balances)
                        {
                            _db.Entry(model).State = EntityState.Added;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Update
        public int Update()
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        var votehis = _db.Vote_History.Where(i => i.is_calculated == false);
                        foreach (var item in votehis)
                        {
                            decimal money = 1;
                            decimal money2 = 0;
                            if (item.int_vote_method == 1)//Vote By SMS
                            {
                                //ก่อนหักค่าใช้จ่าย
                                money = Convert.ToDecimal(0.25);
                                //หลังหักค่าใช้จ่าย
                                money2 = Convert.ToDecimal(0.21);
                            }
                            else if (item.int_vote_method == 2)//Vote By Point
                            {
                                //ก่อนหักค่าใช้จ่าย
                                money = 1;
                                //หลังหักค่าใช้จ่าย
                                money2 = Convert.ToDecimal(0.88);
                            }
                            //
                            var songcontentuserid = item.Song_Contest.int_user_id;
                            var originalsonguserid = item.Song_Contest.Song_Original.int_user_id;

                            //เพิ่มเงินสำหรับนักร้อง
                            #region "เพิ่มเงินสำหรับนักร้อง"
                            var songContentUserBalance =
                                _db.Balances.FirstOrDefault(i => i.int_user_id.Equals(songcontentuserid));

                            if (songContentUserBalance == null)
                            {
                                //เพิ่มข้อมูลใหม่ลงตาราง Balance
                                songContentUserBalance = new Balance
                                {
                                    balance1 = (item.int_vote_value * money),
                                    net_balance = (item.int_vote_value * money2),
                                    datetime_modified = DateTime.Now,
                                    int_user_id = songcontentuserid
                                };
                                songContentUserBalance.datetime_modified = DateTime.Now;
                                songContentUserBalance.datetime_create = DateTime.Now;
                                _db.Balances.Add(songContentUserBalance);
                            }
                            else
                            {
                                //Update ข้อมูลลงตาราง Balance
                                songContentUserBalance.balance1 += (item.int_vote_value * money);
                                songContentUserBalance.net_balance += (item.int_vote_value * money2);
                                songContentUserBalance.datetime_modified = DateTime.Now;
                            }
                            //เก็บข้อมูลลงตาราง Income_Transaction
                            var incomeTrans1 = new Income_Transaction()
                            {
                                int_user_id = songcontentuserid,
                                int_vote_history_id = item.int_vote_history_id,
                                int_song_contest_id = item.int_song_contest_id,
                                int_song_original_id = item.Song_Contest.int_song_original_id,
                                int_type = 1,
                                int_vote_value = item.int_vote_value,
                                datetime_create = DateTime.Now,
                                dec_amount = (item.int_vote_value * money),
                                dec_net_amount = (item.int_vote_value * money2),
                                int_vote_ss=item.int_vote_ss
                            };
                            _db.Income_Transaction.Add(incomeTrans1);
                            result += _db.SaveChanges();
                            #endregion

                            //เพิ่มเงินสำหรับนักแต่งเพลง
                            #region "เพิ่มเงินสำหรับนักแต่งเพลง"
                            var originalSongUserBalance =
                                _db.Balances.FirstOrDefault(i => i.int_user_id.Equals(originalsonguserid));
                            if (originalSongUserBalance == null)
                            {
                                //เพิ่มข้อมูลใหม่ลงตาราง Balance
                                originalSongUserBalance = new Balance
                                {
                                    balance1 = (item.int_vote_value * money),
                                    net_balance = (item.int_vote_value * money2),
                                    datetime_modified = DateTime.Now,
                                    int_user_id = originalsonguserid
                                };
                                originalSongUserBalance.datetime_modified = DateTime.Now;
                                originalSongUserBalance.datetime_create = DateTime.Now;
                                _db.Balances.Add(originalSongUserBalance);
                            }
                            else
                            {
                                //Update ข้อมูลลงตาราง Balance
                                originalSongUserBalance.balance1 += (item.int_vote_value * money);
                                originalSongUserBalance.net_balance += (item.int_vote_value * money2);
                                originalSongUserBalance.datetime_modified = DateTime.Now;
                            }
                            //เก็บข้อมูลลงตาราง Income_Transaction
                            var incomeTrans2 = new Income_Transaction()
                            {
                                int_user_id = originalsonguserid,
                                int_vote_history_id = item.int_vote_history_id,
                                int_song_contest_id = item.int_song_contest_id,
                                int_song_original_id = item.Song_Contest.int_song_original_id,
                                int_type = 2,
                                int_vote_value = item.int_vote_value,
                                datetime_create = DateTime.Now,
                                dec_amount = (item.int_vote_value * money),
                                dec_net_amount = (item.int_vote_value * money2),
                                int_vote_ss = item.int_vote_ss
                            };
                            _db.Income_Transaction.Add(incomeTrans2);
                            #endregion

                            //Update เป็น true เมื่อนำ vote ไปคำนวณเป็นเงินแล้ว
                            item.is_calculated = true;
                        }
                        result += _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Update
        public int UpdateAll()
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        var incomeTransact = _db.Income_Transaction.ToList();
                        foreach (var item in incomeTransact)
                        {
                            decimal money = 1;
                            decimal money2 = 0;
                            var votehis =
                                _db.Vote_History.FirstOrDefault(i => i.int_vote_history_id == item.int_vote_history_id);
                            if (votehis.int_vote_method == 1) //Vote By SMS
                            {
                                //ก่อนหักค่าใช้จ่าย
                                money = Convert.ToDecimal(0.25);
                                //หลังหักค่าใช้จ่าย
                                money2 = Convert.ToDecimal(0.21);
                            }
                            else if (votehis.int_vote_method == 2) //Vote By Point
                            {
                                //ก่อนหักค่าใช้จ่าย
                                money = 1;
                                //หลังหักค่าใช้จ่าย
                                money2 = Convert.ToDecimal(0.88);
                            }
                            //ปรับปรุงยอด
                            item.dec_amount = (item.int_vote_value * money);
                            item.dec_net_amount = (item.int_vote_value * money2);
                        }
                        result += _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //
        //GetReward
        public Decimal GetReward()
        {
            decimal result = 0;
            try
            {
                var expDate = new DateTime(2015, 12, 24, 21, 38, 00);
                var smsvote = _db.Vote_History.Where(i => i.is_calculated == true && i.int_vote_method == 1 && i.datetime_create < expDate).Sum(q => q.int_vote_value);
                var pointvote = _db.Vote_History.Where(i => i.is_calculated == true && i.int_vote_method == 2 && i.datetime_create < expDate).Sum(q => q.int_vote_value);
                result = Convert.ToDecimal((smsvote * 0.25) + (pointvote * 1));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }


        public int Update(IList<Balance> balances)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value
                        foreach (var model in balances)
                        {
                            _db.Entry(model).State = EntityState.Modified;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Select all
        public IList<Balance> GetAll()
        {
            IList<Balance> models;

            try
            {
                models = _db.Balances.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select by id
        public Balance GetById(int id)
        {
            Balance model;

            try
            {
                model = _db.Balances.Find(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }
        //
        public IList<vVoteIncomeTransaction> GetAllIncomeTransact()
        {
            IList<vVoteIncomeTransaction> models;
            try
            {
                var expDate = new DateTime(2015, 12, 24, 21, 38, 00);
               // models = _db.vVoteIncomeTransactions.Where(i => i.datetime_create < expDate).ToList();
                models = _db.vVoteIncomeTransactions.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }
        //
        public IList<vVoteIncomeTransaction> GetIncomeTransactById(string id)
        {
            IList<vVoteIncomeTransaction> models;
            try
            {
                models = _db.vVoteIncomeTransactions.Where(i => i.int_user_id == id).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }
        //Select by id
        public Balance GetByUserId(string id)
        {
            Balance model;

            try
            {
                model = _db.Balances.FirstOrDefault(i => i.int_user_id.Equals(id));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        public IList<Balance> Fetch(Expression<Func<Balance, bool>> predicate, bool active = true)
        {
            IList<Balance> models;

            try
            {
                models = _db.Set<Balance>().Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }
    }
}

namespace DINFDAL.Interfaces
{
    public interface IBalanceService
    {
        int Add(Balance balance);

        int Add(IList<Balance> balances);

        int Update();

        int UpdateAll();

        Decimal GetReward();

        int Update(IList<Balance> balances);

        IList<Balance> GetAll();

        Balance GetById(int id);

        IList<vVoteIncomeTransaction> GetAllIncomeTransact();

        IList<vVoteIncomeTransaction> GetIncomeTransactById(string id);

        Balance GetByUserId(string id);

        IList<Balance> Fetch(Expression<Func<Balance, bool>> predicate, bool active = true);
    }
}