﻿using DINFDAL.Interfaces;
using DINFDAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DINFDAL.Services
{
    public class Sms_DataService : ISms_DataService
    {
        private CyberryMusicEntities _db = new CyberryMusicEntities();
        private DbContextTransaction _dbContextTransaction;

        //Insert
        public int Add(Sms_Data sms_Data)
        {
            int result = 0;

            try
            {
                _db.Entry(sms_Data).State = EntityState.Added;
                var scoreSong =
                    _db.Score_Song.FirstOrDefault(i => i.int_song_contest_id == sms_Data.int_vote_number);
                if (scoreSong != null)
                {
                    scoreSong.int_value = scoreSong.int_value + 1;
                }
                else
                {
                    _db.Entry(new Score_Song() { int_song_contest_id = sms_Data.int_vote_number, int_value = 1, datetime_create = DateTime.Now, datetime_modified = DateTime.Now }).State = EntityState.Added;
                }
                result = _db.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Add(IList<Sms_Data> sms_Datas)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var model in sms_Datas)
                        {
                            _db.Entry(model).State = EntityState.Added;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Update
        public int Update(Sms_Data sms_Data)
        {
            int result = 0;
            try
            {
                //var sms = _db.Sms_Data.Find(sms_Data.int_sms_id);
                //sms.int_vote_number = sms_Data.int_vote_number;
                //sms.datetime_create = sms_Data.datetime_create;
                //sms.txt_refer_id = sms_Data.txt_refer_id;
                //sms.txt_phone = sms_Data.txt_phone;
                //sms.datetime_sent_sms = sms_Data.datetime_sent_sms;
                //result = _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message +" "+ ex.InnerException.StackTrace+" "+ex.Source);
            }
            return result;
        }

        public int Update(IList<Sms_Data> sms_Datas)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value
                        foreach (var model in sms_Datas)
                        {
                            _db.Entry(model).State = EntityState.Modified;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Select all
        public IList<Sms_Data> GetAll()
        {
            IList<Sms_Data> models;

            try
            {
                models = _db.Sms_Data.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select by id
        public Sms_Data GetById(int id)
        {
            Sms_Data model;
            try
            {
                model = _db.Sms_Data.Find(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        //Select by id
        public Sms_Data GetByRefId(string id)
        {
            Sms_Data model;
            try
            {
                model = _db.Sms_Data.FirstOrDefault(i => i.txt_refer_id.Equals(id));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        public IList<Sms_Data> Fetch(Expression<Func<Sms_Data, bool>> predicate, bool active = true)
        {
            IList<Sms_Data> models;

            try
            {
                models = _db.Set<Sms_Data>().Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }
    }
}

namespace DINFDAL.Interfaces
{
    public interface ISms_DataService
    {
        int Add(Sms_Data sms_Data);

        int Add(IList<Sms_Data> sms_Datas);

        int Update(Sms_Data sms_Data);

        int Update(IList<Sms_Data> sms_Datas);

        IList<Sms_Data> GetAll();

        Sms_Data GetById(int id);

        Sms_Data GetByRefId(string id);

        IList<Sms_Data> Fetch(Expression<Func<Sms_Data, bool>> predicate, bool active = true);
    }
}