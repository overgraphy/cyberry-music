﻿using DINFDAL.Interfaces;
using DINFDAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DINFDAL.Services
{
    public class Song_OriginalService : ISong_OriginalService
    {
        private CyberryMusicEntities _db = new CyberryMusicEntities();
        private DbContextTransaction _dbContextTransaction;

        //Insert
        public int Add(Song_Original song_Original)
        {
            int result = 0;

            try
            {
                _db.Entry(song_Original).State = EntityState.Added;
                result = _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Insert
        public int Add(Song_Original song_Original, Song_Contest song_Contest)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        _db.Song_Original.Add(song_Original);
                        result = _db.SaveChanges();
                        song_Contest.int_song_original_id = song_Original.int_song_original_id;
                        _db.Song_Contest.Add(song_Contest);
                        result += _db.SaveChanges();
                        song_Contest.txt_vote_code = song_Contest.int_song_contest_id.ToString("CM00000");
                        result += _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Add(IList<Song_Original> song_Originals)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var model in song_Originals)
                        {
                            _db.Entry(model).State = EntityState.Added;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Update
        public int Update(Song_Original song_Original)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value

                        _db.Entry(song_Original).State = EntityState.Modified;
                        result = _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Update(IList<Song_Original> song_Originals)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value
                        foreach (var model in song_Originals)
                        {
                            _db.Entry(model).State = EntityState.Modified;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Select all
        public IList<Song_Original> GetAll()
        {
            IList<Song_Original> models;

            try
            {
                models = _db.Song_Original.Where(i => i.is_active == true && i.is_approved == true).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }


        public IList<Song_Original> GetPopularSong(int take = 0)
        {
            IList<Song_Original> models;

            try
            {
                if (take > 0)
                {
                    models = _db.Song_Original.Where(i => i.is_active == true && i.is_approved == true)
                        .OrderByDescending(i => i.Song_Contest.Count(c => c.is_active == true && c.is_approved == true)).Take(take).ToList();
                }
                else
                {
                    models = _db.Song_Original.Where(i => i.is_active == true && i.is_approved == true)
                        .OrderByDescending(i => i.Song_Contest.Count(c => c.is_active == true && c.is_approved == true)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        public IList<Song_Original> GetNewSong(int take = 0)
        {
            IList<Song_Original> models;

            try
            {
                if (take > 0)
                {
                    models =
                        _db.Song_Original.Where(i => i.is_active == true && i.is_approved == true)
                            .Take(take)
                            .OrderByDescending(i => i.datetime_create)
                            .ToList();
                }
                else
                {
                    models =
                      _db.Song_Original.Where(i => i.is_active == true && i.is_approved == true)
                          .OrderByDescending(i => i.datetime_create)
                          .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        public IList<Song_Original> GetRandomSong(int take = 0)
        {
            IList<Song_Original> models;

            try
            {
                if (take > 0)
                {
                    models = _db.Song_Original.Where(i => i.is_active == true && i.is_approved == true).OrderBy(r => Guid.NewGuid()).Take(take).ToList();
                }
                else
                {
                    models = _db.Song_Original.Where(i => i.is_active == true && i.is_approved == true).OrderBy(r => Guid.NewGuid()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        public IList<Song_Original> GetEditorPickSong(int take = 0)
        {
            IList<Song_Original> models;

            try
            {
                if (take > 0)
                {
                    models = _db.Song_Original.Where(i => i.is_active == true && i.is_approved == true && i.is_edittor_pick == true).Take(take).ToList();
                }
                else
                {
                    models = _db.Song_Original.Where(i => i.is_active == true && i.is_approved == true && i.is_edittor_pick == true).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select by id
        public Song_Original GetById(int id)
        {
            Song_Original model;

            try
            {
                model = _db.Song_Original.FirstOrDefault(i => i.is_active && i.is_approved && i.int_song_original_id == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        //Select by User id
        public IList<Song_Original> GetByUserId(string id)
        {
            IList<Song_Original> model;

            try
            {
                model =
                    _db.Song_Original.Where(i => i.is_active == true && i.is_approved == true && i.int_user_id == id)
                        .ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        public IList<Song_Original> Fetch(Expression<Func<Song_Original, bool>> predicate, bool active = true)
        {
            IList<Song_Original> models;

            try
            {
                models = _db.Set<Song_Original>().Where(m => m.is_active == active).Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }
    }
}

namespace DINFDAL.Interfaces
{
    public interface ISong_OriginalService
    {
        int Add(Song_Original song_Original);

        int Add(Song_Original song_Original, Song_Contest song_Contest);

        int Add(IList<Song_Original> song_Originals);

        int Update(Song_Original song_Original);

        int Update(IList<Song_Original> song_Originals);

        IList<Song_Original> GetAll();

        IList<Song_Original> GetPopularSong(int take = 0);

        IList<Song_Original> GetNewSong(int take = 0);

        IList<Song_Original> GetRandomSong(int take = 0);

        IList<Song_Original> GetEditorPickSong(int take = 0);

        Song_Original GetById(int id);

        IList<Song_Original> GetByUserId(string id);

        IList<Song_Original> Fetch(Expression<Func<Song_Original, bool>> predicate, bool active = true);
    }
}