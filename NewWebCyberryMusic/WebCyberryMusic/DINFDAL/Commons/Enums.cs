﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DINFDAL.Commons
{
    public static class Role
    {
        public static string Admin = "Admin";
        public static string ComposorArtist = "Composor/Artist";
        public static string Member = "Member";
    }
}
