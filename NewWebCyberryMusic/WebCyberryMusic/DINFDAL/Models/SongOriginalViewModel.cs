﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DINFDAL.Models
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(SongOriginalViewModel))]
    public partial class Song_Original
    {
        public string lnk_lyric_file { get; set; }
    }

    //
    public class SongOriginalViewModel
    {
        [Required(ErrorMessage = "The Field is required")]
        [Display(Name = "ชื่อเพลง")]
        public string txt_songname { get; set; }
       
        [Display(Name = "ไฟล์เนื้อเพลง")]
        public string txt_lyric_file { get; set; }

        [YoutubeUrlValidator]
        public string txt_youtube_url { get; set; }

        [SoundCloudUrlValidator]
        public string txt_soundcloud_url { get; set; }

    }
}
