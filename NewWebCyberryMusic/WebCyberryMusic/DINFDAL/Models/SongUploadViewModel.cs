﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DINFDAL.Models
{
    public class SongUploadViewModel
    {
        [Required(ErrorMessage = "The Field is required")]
        [Display(Name = "ชื่อเพลง")]
        public string txt_song_name { get; set; }
       
        [YoutubeUrlValidator]
        public string txt_youtube_url { get; set; }
        
        public string txt_youtube_id { get; set; }
       
        [Display(Name = "ไฟล์เนื้อเพลง")]
        public string txt_lyric_file { get; set; }

        [SoundCloudUrlValidator]
        public string txt_soundcloud_url { get; set; }

        public bool IsContest { get; set; }
    }
}
