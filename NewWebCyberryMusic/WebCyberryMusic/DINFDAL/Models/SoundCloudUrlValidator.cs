﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DINFDAL.Models
{
    class SoundCloudUrlValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ValidationResult result;
            if (value == null || (string)value == "")
            {
                result = ValidationResult.Success;
            }
            else
            {
                if (this.VerifySoundCloudUrl(value.ToString()))
                {
                    result = ValidationResult.Success;
                }
                else
                {
                    result = new ValidationResult("Please Enter a SoundCloud Url.");
                }
            }
            return result;
        }
        private bool VerifySoundCloudUrl(string url)
        {
            string b = null;
            bool result = true;
            Regex regex = new Regex(@"^((https:\/\/)|(http:\/\/)|(www.)|(\s))+(soundcloud.com\/)+[a-zA-Z0-9\-\.]+(\/)+[a-zA-Z0-9\-\.]+");
            result = regex.Match(url).Success;
            return result;
        }
    }
}
