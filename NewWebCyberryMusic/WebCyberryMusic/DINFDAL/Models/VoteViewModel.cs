﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DINFDAL.Models
{
  public  class VoteViewModel
    {
      [Required(ErrorMessage = "The Field is required")]
      [Display(Name = "Cyberry Point")]
      public int Point { get; set; }

      public int CurrentPoint { get; set; }

      public int SongId { get; set; }
    }
}
