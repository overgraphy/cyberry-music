﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DINFDAL.Models
{
   public class YoutubeUrlValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ValidationResult result;
            if (value == null || (string) value=="")
            {
                result = ValidationResult.Success;
            }
            else
            {
                if (this.VerifyYoutubeUrl(value.ToString()))
                {
                    result = ValidationResult.Success;
                }
                else
                {
                    result = new ValidationResult("Please Enter a Youtube Url.");
                }
            }
            return result;
        }
        private bool VerifyYoutubeUrl(string url)
        {
            string b = null;
            bool result = true;
            Regex regex = new Regex(@"^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$");
            result = regex.Match(url).Success;
            return result;
        }
    }
}
