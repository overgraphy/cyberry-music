using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
namespace DINFDAL.Models
{
	public class IdCardValidator : ValidationAttribute
	{
		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			ValidationResult result;
			if (value == null)
			{
				result = ValidationResult.Success;
			}
			else
			{
				if (this.VerifyPeopleId(value.ToString()))
				{
					result = ValidationResult.Success;
				}
				else
				{
					result = new ValidationResult("Please Enter a Valid ID Card.");
				}
			}
			return result;
		}
		private bool VerifyPeopleId(string pid)
		{
			string b = null;
			bool result;
			if (!pid.ToCharArray().All((char c) => char.IsNumber(c)))
			{
				result = false;
			}
			else
			{
				if (pid.Trim().Length != 13)
				{
					result = false;
				}
				else
				{
					int num = 0;
					char c2;
					for (int i = 0; i < pid.Length - 1; i++)
					{
						int arg_79_0 = num;
						c2 = pid[i];
						num = arg_79_0 + int.Parse(c2.ToString()) * (13 - i);
					}
					int num2 = 11 - num % 11;
					if (num2.ToString().Length == 2)
					{
						b = num2.ToString().Substring(1, 1);
					}
					else
					{
						b = num2.ToString();
					}
					c2 = pid[12];
					result = (c2.ToString() == b);
				}
			}
			return result;
		}
	}
}
