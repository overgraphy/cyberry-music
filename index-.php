<?php

$myURL = $_SERVER['REQUEST_URI'];
$hd = array(
	"title" => "Cyberry Music",
	"description" => "เวทีเพื่อการแข่งขันประกวดร้องเพลงทางอินเทอร์เน็ตที่แบ่งปันรายได้จากการโหวตให้กับทุกๆ คนที่เกี่ยวข้อง",
	"keyword" => "cyberry, cyberry music, กติกา, แข่งขัน, ประกวด, ร้องเพลง, เล่นดนตรี, youtube, ประกวดดนตรี,ประกวดร้องเพลง",
	"og_title" => "Cyberry Music ประกวดแข่งขันดนตรีออนไลน์ ",
	"og_sitename" => "CyberryMusic.com",
	"og_url" => "http://www.cyberrymusic.com/index.php",
	"og_image" => "http://www.cyberrymusic.com/images/ogimage.jpg",
	"og_description" => "เวทีประกวดดนตรีสำหรับทุกคน ทุกอาชีพ ทุกอายุ เพราะทุกคนมีความฝัน "
);
?>

<?php include realpath( __DIR__ . '/__display.php' ); ?>
<?php include_once realpath(__DIR__ . "/googleanalytic.php") ?>
<?php
write_header($hd);
?>
<div class="container">
<!---- Content ----->
<div class="row-fluid">
	<div class="col-md-7">
	<strong>Cyberry Music Contest Season 1</strong> คือ การประกวดดนตรีทาง Internet ผ่านเว็บไซต์ 

		CyberryMusic.com โดยใช้เพลงที่ผู้แข่งขันแต่งขึ้นเอง แน่นอนว่าลิขสิทธิ์ก็จะเป็นของผู้แข่งขันด้วย ไม่ต้องกลัวเรื่องข้อกฎหมายใดๆ และหากเพลงไหนดีมาก มีผู้โหวต ก็จะได้ส่วนแบ่งจากการโหวตด้วยอย่างชัดเจน (ที่ไหนเค้าแบ่งมั่ง ไม่มี๊!) แล้วถ้าเพลงเราดี มีคนเอาเพลงเราไปใช้ประกวด เราก็ได้เงินค่าส่วนแบ่งเยอะขึ้นเป็นพิเศษอีกด้วย
		</P>
		<p>
		เงินค่าโหวตก็จะนำมาสมทบกับรางวัลชนะเลิศให้มากขึ้นเรื่อยด้วย เราตั้งเริ่มต้นเอาไว้ 100,000 บาท แต่สุดท้ายจะไปอยู่ที่เท่าไหร่ก็ไม่รู้ น่าจะเยอะมากๆ เลยล่ะ  </p>
		<div class="set-right">
			<a href="rules.php"><img src="images/b_readmore.jpg" width="125" height="41" /></a>
		</div>
	</div>
	<div class="col-md-5">
		<img src="images/cyberrymusic_04.jpg" class="big-image" />
	</div>
</div>

<div class="row-fluid">
	<div class="col-md-6">
	<h4 class="caption">คุณจะได้อะไร เมื่อประกวดคลิปเพลงกับ Cyberry Music</h4>
	<img src="images/info1.jpg" class="big-image set-center shadow-glow gap-bottom">
	</div>
	<div class="col-md-6">
	<h4 class="caption">วิธีการส่งคลิป / รายได้มาจากไหนบ้าง</h4>	
	<img src="images/info2.jpg" class="big-image set-center shadow-glow gap-bottom">
	</div>
</div>

<h2 class="text-center">อีกไม่กี่วัน จะเปิดรับสมัครประกวดคลิปนะจ๊ะ</h2>
<!---- /Content ----->
</div>
<?php
write_footer();
?>
</body>
</html>
