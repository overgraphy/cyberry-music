<?php

$myURL = $_SERVER['REQUEST_URI'];
$hd = array(
	"title" => "ลงทะเบียน Cyberry Music Contest",
	"description" => "ลงทะเบียนประกวดดนตรี Cyberry Music Contest",
	"keyword" => "cyberry, cyberry music, กติกา, แข่งขัน, ประกวด, ร้องเพลง, เล่นดนตรี, youtube, ประกวดดนตรี,ประกวดร้องเพลง , ลงทะเบียน",
	"og_title" => "ลงทะเบียนประกวดดนตรี Cyberry Music Contest",
	"og_sitename" => "CyberryMusic.com",
	"og_url" => "http://www.cyberrymusic.com/register.php",
	"og_image" => "http://www.cyberrymusic.com/images/ogimage.jpg",
	"og_description" => "เวทีประกวดดนตรีสำหรับทุกคน ทุกอาชีพ ทุกอายุ เพราะทุกคนมีความฝัน "
);
?>

<?php include realpath( __DIR__ . '/__display.php' ); ?>
<?php include_once realpath(__DIR__ . "/googleanalytic.php") ?>
<?php
write_header($hd);
?>
<?php
$e = isset($_GET['e'])? $_GET['e'] : '';
$f = isset($_GET['f'])? $_GET['f'] : '';
$l = isset($_GET['l'])? $_GET['l'] : '';
$u = isset($_GET['u'])? $_GET['u'] : '';
$p = isset($_GET['p'])? $_GET['p'] : '';
$email = isset($_GET['email'])? $_GET['email'] : '';
$d = isset($_GET['d'])? $_GET['d'] : '';
$m = isset($_GET['m'])? $_GET['m'] : '';
$y = isset($_GET['y'])? $_GET['y'] : '';
$address = isset($_GET['address'])? $_GET['address'] : '';
$phone = isset($_GET['phone'])? $_GET['phone'] : '';
$c = isset($_GET['c'])? $_GET['c'] : '';
$b = isset($_GET['b'])? $_GET['b'] : '';
$ba = isset($_GET['ba'])? $_GET['ba'] : '';
$date = isset($_GET['date'])? $_GET['date'] : '';

?>
<div class="container myfont">
	<h1>ลงทะเบียนประกวดดนตรี Cyberry Music Contest</h1>
	<h3>กรอกข้อมูล</h3>
	<?php
		if($e==1){
			echo "<h4>";
			echo "<font color=red><b>กรุณากรอกข้อมูลบางส่วนให้ถูกต้อง</b></font>";
			echo "</h4>";
		}
		?>
	<form role="form" method="post" action="registering.php">
		<div class="form-group">
			<label for="firstname" class="control-label col-md-offset-2 col-md-2">ชื่อจริง <span class="must">*</span>:</label>
			<input class="form-control form-w25" name="firstname" type="text" id="firstname" value="<?=$f?>"/>
		</div>
		<div class="form-group">
			<label for="lastname" class="control-label col-md-offset-2 col-md-2">นามสกุล <span class="must">*</span>:</label>
			<input class="form-control form-w40" name="lastname" type="text" id="lastname" value="<?=$l?>"/>
		</div>
		<div class="form-group">
			<label for="username" class="control-label col-md-offset-2 col-md-2">Username <span class="must">*</span>:</label>
			<input class="form-control form-w30" name="username" type="text" id="username" value="<?=$u?>" placeholder="ความยาวมากกว่า 5 ตัวอักษร" /><span id="checkexisting"></span>
		</div>
		<p></p>
		<div class="form-group">
			<label for="password" class="control-label col-md-offset-2 col-md-2">Password <span class="must">*</span>:</label>
			<input class="form-control form-w20" name="password" type="password" id="password" /><span id="checkpassword1"></span>
		</div>
		<div class="form-group">
			<label for="pwd" class="control-label col-md-offset-2 col-md-2">Re-Type Password  <span class="must">*</span>:</label>
			<input class="form-control form-w20" name="pwd" type="password" id="pwd" /><span id="checkpassword2"></span>
		</div>
		<div class="form-group">
			<label for="email" class="control-label col-md-offset-2 col-md-2">email <span class="must">*</span>:</label>
			<input class="form-control form-w20" name="email" type="text" id="email" value="<?=$e?>"/><span id="checkemail"></span>
		</div>
		<div class="form-group">
			<label for="email" class="control-label col-md-offset-2 col-md-2">วันเกิด <span class="must">*</span>:</label>
			<input class="form-control form-w15 datepicker" name="date" type="text" id="date" readonly value="<?=$date?>">
		</div>

		<div class="form-group">
			<label form="address" class="control-label col-md-offset-2 col-md-2">ที่อยู่ <span class="must">*</span>:</label>
			<textarea name="address" cols="62" rows="5" id="address" class="form-control form-w50"><?=$address?></textarea>
		</div>
		
		<div class="form-group">
			<label for="phone" class="control-label col-md-offset-2 col-md-2">เบอร์โทรศัพท์ <span class="must">*</span>:</label>
			<input class="form-control form-w20" name="phone" type="text" id="phone" value="<?=$phone?>"/>
		</div>
		<hr>
		<div class="form-group">
		<?php
			$isContest = "";
			if($c=='on'){
				$isContest = "checked";
			}
			?>
			<label>
			<input type=checkbox name="iscontest" id="iscontest" <?=$isContest?> > ต้องการสมัครแข่งขัน</label>
		</div>
		<h4>ข้อมูลเพิ่มเติมสำหรับผู้สมัครเข้าแข่งขัน</h4>

			<p>
			ข้อมูลบัญชีธนาคาร เพื่อโอนเงินเมื่อได้รับส่วนแบ่งจากการโหวต<br>
(สามารถขอโอนเงินได้เมื่อมีเงินสะสมเกิน 500 บาท) 
</p>
		<div class="form-group">
			<label for="bankaccount" class="control-label col-md-offset-2 col-md-2">ธนาคาร  </label>
			<select name="bank" class="form-control form-w25">
				<?php
				$BANK = array(
					"scb" => "ธ.ไทยพานิชย์",
					"bbl" => "ธ.กรุงเทพ",
					"kbank" => "ธ.กสิกร",
					"ktb" => "ธ.กรุงไทย",
					"bay" => "ธ.กรุงศรี"
				);
				foreach($BANK as $b => $text){
					$sel = "";
					if($_GET['b']==$b){
						$sel = "selected";
					}
					echo "\n<option value='$b' $sel>$text</option>";
				}
				?>
			</select>
		</div>
		<div class="form-group">
			<label for="bankaccount" class="control-label col-md-offset-2 col-md-2">หมายเลขบัญชี  </label>
			<input class="form-control form-w20" name="bankaccount" type="text" id="bankaccount" value="<?=$ba?>"/>
		</div>
		<div class="form-group">
			<label for="bankaccount" class="control-label col-md-offset-2 col-md-2">เลขที่บัตรประชาชน  </label>
			<input class="form-control form-w20" name="bankaccount" type="text" id="bankaccount" value="<?=$ba?>"/>
		</div>
		
		<div class="row-fluid">
			<a href="rules.php" target="_blank">คลิกเพื่ออ่านกติกาและเงื่อนไข</a><br>
			<label>
		  <input type="checkbox" name="ck_accept" id="ck_accept" value="accept"> ยอมรับเงื่อนไขและกติกาการแข่งขัน (กรุณาอ่านให้ครบถ้วน เพื่อประโยชน์ของท่าน )
		  </label>
		</div>
		<div class="">
			<input type="submit" class="form-control set-center" name="submitbutton" id="submitbutton" value="สมัครสมาชิก" />
		</div>
	</form>
</div>


<script>
$(document).ready(function(){
	$('#submitbutton').attr('disabled',true);
	$('#ck_accept').attr('disabled',true);
	
	$(".datepicker").datepicker();
	
	
	$('#ck_accept').on( "click", function(){
		if($( "input:checkbox[name=ck_accept]:checked" ).val()=='accept'){
			$('#submitbutton').removeAttr('disabled');
		}else{
			$('#submitbutton').attr('disabled',true);
		}
	});
	
	
	$('#username').keyup(function() {
		console.log("L:" + $('#username').val().length);
		$('#ck_accept').attr('disabled','disabled');
		if($('#username').val().length >=5){
			$('#checkexisting').html("&nbsp;<font color='#FF6600'>Checking..</font>");
			console.log("change..");
			$.post('user_is_existing.php',{username: $('#username').val()}, function(data){
				if(data=='1'){
					//tell user that the username already exists
					$('#checkexisting').html("&nbsp;<font color=red>Existing Username</font>");
					
				}else{
					//username doesn't exist, do what you need to do
					$('#checkexisting').html("&nbsp;<font color=green>OK. You can use this username</font>");
					$('#ck_accept').removeAttr('disabled');
				}
			 }, 'JSON');
		}else{
			$('#checkexisting').html("&nbsp;<font color=#FF6600>Username length must longer than 5 characters..</font>");
		}
	});
});


function validateEmail($email) {
  var emailReg = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
  return emailReg.test( $email );
}

$('#email').blur(function(){
	if(!validateEmail($('#email').val())){
		$('#checkemail').html('&nbsp;&nbsp;<font color=red>Invalid email format</font>');
		$('#email').focus();
	}else{
		$('#checkemail').html('&nbsp;&nbsp;<font color=green>Email format OK.</font>');
	}
});

$('#password').blur(function(){
	if($(this).val().trim()==''){
		$('#checkpassword1').html("&nbsp;&nbsp;<font color=red>Password MUST NOT Empty!</font>");
	}else{
		$('#checkpassword1').html("&nbsp;");
	}
});

$('#pwd').blur(function(){
	if($('#password').val().trim()==''){
			$('#checkpassword2').html("&nbsp;&nbsp;<font color=red>Password MUST NOT Empty!</font>");
	}else{
		$('#checkpassword2').html("&nbsp;");
		if($('#password').val() != $('#pwd').val()){
			$('#checkpassword2').html("&nbsp;&nbsp;<font color=red>Password Not Match</font>");
		}else{
			$('#checkpassword2').html("&nbsp;&nbsp;<font color=green>Password Match</font>");
		}
	}
});
</script>

<?php
if($e==1){
	echo "<script>\n";
	echo "$(document).ready(function(){\n";
	echo "	$('#ck_accept').attr('checked', true);\n";
	echo "	$('#ck_accept').attr('disabled',false);\n";
	echo "	$('#submitbutton').removeAttr('disabled');\n";
	echo "});\n";
	echo "</script>\n";
  
}
?>

<?php
write_footer();
?>
</body>
</html>