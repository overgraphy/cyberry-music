<?php
require_once("db.php");

$myURL = $_SERVER['REQUEST_URI'];
$og_image = "http://www.cyberrymusic.com/images/ogimage-rules.jpg";
$hd = array(
	"title" => "Cyberry Music",
	"description" => "Cyberry Music",
	"keyword" => "cyberry, cyberry music, กติกา, แข่งขัน, ประกวด, ร้องเพลง, เล่นดนตรี, youtube",
	"og_title" => "กติกาการแข่งขันประกวดคลิปเพลง Cyberry Music",
	"og_sitename" => "CyberryMusic.com",
	"og_url" => "http://www.cyberrymusic.com/rules.php",
	"og_image" => "http://www.cyberrymusic.com/images/ogimage.jpg",
	"og_description" => "กติกาสำหรับการประกวดคลิปเพลงบน Cyberry Music"
);

$loginerror = false;
if(!isset($_GET['loginerror'])){
	$loginerror = true;
}

?>

<?php include realpath( __DIR__ . '/__display.php' ); ?>
<?php include_once realpath(__DIR__ . "/googleanalytic.php") ?>
<?php
write_header($hd);
?>
<div class="container">
<!---- Content ----->

<?php
if(!isset($_GET['id'])){
	die("No Data..");
}
$id = $_GET['id'];
if(!is_numeric($id)){
	die("Invalid ID..");
}
$sql = "select * from users where id = '$id' ";
$res = mysql_query($sql, $dbconnect);
$rw = mysql_fetch_array($res);
$username = $rw['username'];
$avatar = $rw['avatar'];


if(trim($avatar)==""){
	$avatar = "images/cyberry-avatar-01.jpg";
}
?>


<div class="row-fluid">
	<div class="col-md-4">
		<div class="artist-profile">
			<div class="row">
				<div class="col-md-5">
					<img src="images/cyberry-avatar-01.jpg" class="avatar">
				</div>
				<div class="col-md-7">
					<div class="user-info">
			
			
						User: <B><?=$username?></b><br>
						ID: <b><?=sprintf("%05d", $id)?></b><br>
						Point คงเหลือ: <b>10,000</b><br>
						Point รวม: <b>201,000</b>
					</div>
				</div>
			</div>
		
			<div class="row">
				<div class="col-md-12">
					<a href="#"><button type="button" class="btn btn-primary">Upload เพลงต้นแบบ</button></a>
				</div>
				<div class="col-md-12">
					<a href="#"><button type="button" class="btn btn-primary">Upload เพลงประกวด</button></a>
				</div>
			</div>
		
		</div>
	</div>
	<div class="col-md-8">
		<h4>เพลงของ <?=$username?></h4>
		<!--<div>-->
		<?php
		$sql = "select * from videos where user_id='$id' order by timestamp";
		$res = mysql_query($sql, $dbconnect);
		
		$max_videos = 10;
		$maxcol=3;
		$v = 1;
		$col = 1;
		while($rw=mysql_fetch_array($res)){
			$ytcode=$rw['vdo_code'];
			$name = $rw['vdo_title'];
			$code_vote = $rw['code_vote'];
			
			$thumb = "http://img.youtube.com/vi/" . $ytcode . "/mqdefault.jpg";
			
			
			if($col==1){
				//echo "\n<div class='row-fluid'>";
			}
			echo "\n<div class='col-md-4'>";
			
			echo "\n<a href='https://www.youtube.com/watch?v=$ytcode' target='_blank'><img src='$thumb' border=0 width=220 ></a>";
			echo "\n<div class='video-desc'>";
			echo "\nเพลง : $name<br>";
			echo "\nVote Code: $code_vote<br>";
			echo "\nVoted: x<br>";
			echo "\n<br>";
			echo "\n</div><!--DESC-->";
			echo "\n</div><!--COL-->";
			//echo "\n</div>\n";
			echo "\n";
			
			
			$v++;
			$col++;
			
			if($col > $maxcol){
				//echo "\n</div><!--- ROW -->";
				$col=1;
			}
		}
		//echo "</div>\n";
		for($x = $v ; $x <=$max_videos; $x++){
			if($col==1){
				//echo "\n<div class='row-fluid'>";
			}
			echo "\n<div class='col-md-4'>";
			echo "\n<img src='images/video-none.png' border=0 width=220>";
			echo "\n<div class='video-desc'>";
			echo "\nเพลง ...<br>";
			echo "\nVote Code: $code_vote<br>";
			echo "\nVoted: 0<br>";
			echo "\n<br>";
			echo "\n</div><!--desc-->";
			echo "\n</div><!-- col -->";
			//echo "\n</div>";
			echo "\n";
			
			$col++;
			
			if($col > $maxcol){
				//echo "\n</div><!-- row-->";
				$col=1;
			}
		}
		
		?>
		</div>
	</div>
</div>

<!---- /Content ----->
</div>
<?php
$show = isset($_GET['show'])? $_GET['show']:0;
write_footer($show);


?>
</body>
</html>