﻿using DINF.Helpers;
using DINFDAL.Interfaces;
using DINFDAL.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using WebCyberryMusic.Models;

namespace WebCyberryMusic.Controllers
{
    public class HomeController : BaseController
    {
        private ISong_OriginalService _originalService;
        private ISong_ContestService _songContestService;
        private IUserService _userService;
        private ISubscribedService _subscribedService;
        private IBalanceService _balanceService;

        public HomeController(ISong_OriginalService originalService,
            ISong_ContestService songContestService,
            IUserService userService,
            ISubscribedService subscribedService,
            IBalanceService balanceService)
        {
            _originalService = originalService;
            _songContestService = songContestService;
            _userService = userService;
            _subscribedService = subscribedService;
            _balanceService = balanceService;
        }

       // [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            ViewBag.HotVideo = _songContestService.GetHotVideo();
            ViewBag.NewVideo = _songContestService.GetNewVideo();
            //Session["reward"] = _balanceService.GetReward();
            return View();
        }

        public ActionResult Rules()
        {
            return View();
        }

        public ActionResult Rights()
        {
            return View();
        }

        public ActionResult WinnerVoter()
        {
            return View();
        }

        public ActionResult Artistlist(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            var allvdos = _songContestService.GetAllArtistlist();
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            //
            ViewBag.HotArtist = _songContestService.GetHotArtistlist();
            ViewBag.NewArtist = _songContestService.GetNewArtistlist();
            //
            return View(allvdos.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult OriginalSongList(string p, string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            var allsongs = new List<Song_Original>();
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            //
            if (string.IsNullOrEmpty(p) || p.Equals("all"))
            {
                ViewBag.EditorPickSong = _originalService.GetEditorPickSong(15);
                ViewBag.PopularSong = _originalService.GetPopularSong(15);
                ViewBag.RandomSong = _originalService.GetRandomSong(15);
                ViewBag.NewSong = _originalService.GetNewSong(15);
                return View();
            }
            else
            {
                ViewBag.Title = p;
                switch (p.ToLower())
                {
                    case "popular":
                        allsongs = _originalService.GetPopularSong().ToList();
                        break;
                    case "random":
                        allsongs = _originalService.GetRandomSong().ToList();
                        break;
                    case "new":
                        allsongs = _originalService.GetNewSong().ToList();
                        break;
                    case "editorpick":
                        allsongs = _originalService.GetEditorPickSong().ToList();
                        break;
                }
                return View("AllOriginalSongList", allsongs.ToPagedList(pageNumber, pageSize));
            }
        }

        public ActionResult SongList(string p, string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            var allsongs = new List<Song_Contest>();
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            //
            if (string.IsNullOrEmpty(p) || p.Equals("all"))
            {
                ViewBag.EditorPickSong = _songContestService.GetEditorPickSong(15);
                ViewBag.PopularSong = _songContestService.GetPopularSong(15);
                ViewBag.RandomSong = _songContestService.GetRandomSong(15);
                ViewBag.NewSong = _songContestService.GetNewSong(15);
                return View();
            }
            else
            {
                ViewBag.Title = p;
                switch (p.ToLower())
                {
                    case "popular":
                        allsongs = _songContestService.GetPopularSong().ToList();
                        break;
                    case "random":
                        allsongs = _songContestService.GetRandomSong().ToList();
                        break;
                    case "new":
                        allsongs = _songContestService.GetNewSong().ToList();
                        break;
                    case "editorpick":
                        allsongs = _songContestService.GetEditorPickSong().ToList();
                        break;
                }
                return View("AllSongList", allsongs.ToPagedList(pageNumber, pageSize));
            }
        }

        public ActionResult SearchResult(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            var allsongs = new List<Song_Contest>();
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                searchString = searchString.Trim();
                allsongs = _songContestService.Fetch(i => i.txt_song_name.Contains(searchString) || i.User.UserName.Contains(searchString) || i.Song_Original.txt_songname.Equals(searchString)).ToList();
            }
            ViewBag.CurrentFilter = searchString;

            int pageSize = 20;
            int pageNumber = (page ?? 1);
            return View(allsongs.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }



        [HttpPost]
        public ActionResult Contact(ContactViewModels model)
        {
            if (ModelState.IsValid)
            {
                var body = string.Format("<strong>Name: </strong>{0}<br/><strong>Email: </strong>{1}<br/><strong>Subject: </strong>{2}<br/><strong>Message: </strong>{3}", model.Name, model.Email, model.Subject, model.Message);
                var sendmail = new SendMail();
                if (sendmail.Send(System.Configuration.ConfigurationManager.AppSettings["MessageSendTo"], model.Subject, body))
                {
                    ViewBag.IsSuccess = true;
                    ModelState.Clear();
                    model = new ContactViewModels();
                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Subscribed(SubscribedViewModel model)
        {
            if (ModelState.IsValid)
            {
                var subscribe = new Subscribed
                {
                    datetime_create = DateTime.Now,
                    txt_email = model.Email,
                    txt_name = model.Name
                };
                //
                if (_subscribedService.Add(subscribe) > 0)
                {
                    ViewBag.SubscribeIsSuccess = true;
                    ModelState.Clear();
                    model = new SubscribedViewModel();
                }
            }
            return PartialView("_Subscribed");
        }

        public ActionResult TopScore()
        {

            return View();
        }

        [HttpPost]
        public ActionResult GetTopScore()
        {
            //Update ยอดเงิน
            // _balanceService.Update();
            Session["reward"] = _balanceService.GetReward();
            return PartialView("_GetTopScore", _songContestService.GetTopScores().Take(5).ToList());
        }

        public ActionResult TopScore2()
        {

            return View();
        }

        [HttpPost]
        public ActionResult GetTopScore2()
        {
            //Update ยอดเงิน
            // _balanceService.Update();
            Session["reward"] = _balanceService.GetReward();
            return PartialView("_GetTopScore2", _songContestService.GetTopScores().Take(5).ToList());
        }
    }
}