using System;
using System.Web;
using System.Web.Mvc;
using DINFDAL.Interfaces;
using DINFDAL.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;
using WebCyberryMusic.Areas.Admin.Controllers;
using WebCyberryMusic.Controllers;
using WebCyberryMusic.Models;

namespace WebCyberryMusic.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your types here
            // container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<IController, BaseController>();
            container.RegisterType<IController, HomeController>();
            container.RegisterType<IController, AccountController>();
            container.RegisterType<IController, ManageController>();
            container.RegisterType<IController, ErrorController>();
            container.RegisterType<IController, ProfileController>();
            container.RegisterType<IController, RolesController>();
            container.RegisterType<IController, UsersController>();
            container.RegisterType<IController, VoteController>();
            container.RegisterType<IController, PointController>();
            container.RegisterType<IController, BuyPointController>();
            container.RegisterType<IController, WithdrawalTransactionController>();
            //
            container.RegisterType<ISong_OriginalService, Song_OriginalService>();
            container.RegisterType<IPointService, PointService>();
            container.RegisterType<IScoreService, ScoreService>();
            container.RegisterType<ISong_ContestService, Song_ContestService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<ISubscribedService, SubscribedService>();
            container.RegisterType<ISms_DataService, Sms_DataService>();
            container.RegisterType<IScore_SongService, Score_SongService>();
            container.RegisterType<IPaysBuyConfigService, PaysBuyConfigService>();
            container.RegisterType<IPaysbuyService, PaysbuyService>();
            container.RegisterType<IPaysbuy_logService, Paysbuy_logService>();
            container.RegisterType<IProductService, ProductService>();
            container.RegisterType<IBalanceService, BalanceService>();
            container.RegisterType<IWithdrawal_TransactionService, Withdrawal_TransactionService>();
            container.RegisterType<IBankService, BankService>();
            container.RegisterType<ILINEPayConfigService, LINEPayConfigService>();
            //
            container.RegisterType<ApplicationDbContext>();
            container.RegisterType<ApplicationSignInManager>();
            container.RegisterType<ApplicationUserManager>();
            container.RegisterType<ApplicationRoleManager>();
            container.RegisterType<EmailService>();

            container.RegisterType<IAuthenticationManager>(
                new InjectionFactory(c => HttpContext.Current.GetOwinContext().Authentication));

            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>(
                new InjectionConstructor(typeof(ApplicationDbContext)));

            container.RegisterType<RolesController>(new InjectionConstructor());
            //container.RegisterType<UsersController>(new InjectionConstructor());
        }
    }
}
