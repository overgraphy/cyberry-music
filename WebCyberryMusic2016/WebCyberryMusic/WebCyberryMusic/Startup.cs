﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebCyberryMusic.Startup))]
namespace WebCyberryMusic
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
