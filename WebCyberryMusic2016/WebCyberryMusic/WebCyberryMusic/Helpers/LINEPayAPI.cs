﻿using System;
using System.Net;
using System.IO;

namespace LINEPay
{
    public class LINEPayAPI
    {
        private String _channelId;
        private String _channelSecret;
        private String _sendUrl;

        public LINEPayAPI(String channelId, String channelSecret, string sendUrl)
        {
            this._channelId = channelId;
            this._channelSecret = channelSecret;
            this._sendUrl = sendUrl;
        }

        public String request(String data)
        {
            return this._sendData(_sendUrl + "request", data);
        }

        public String confirm(String transactionId, String data)
        {
            return this._sendData(_sendUrl + transactionId + "/confirm", data);
        }

        private String _sendData(String url, String strJson)
        {
            //Console.WriteLine("Hello LINE Pay!");
            //Console.WriteLine(strJson);

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json; charset=UTF-8";
            httpWebRequest.Headers["X-LINE-ChannelId"] = this._channelId;
            httpWebRequest.Headers["X-LINE-ChannelSecret"] = this._channelSecret;
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(strJson);
                streamWriter.Flush();
                streamWriter.Close();
            }

            String value = "";
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                value += result;
                //Console.WriteLine (result);
            }
            //Console.WriteLine("Return=>" + value);
            return value;
        }


    }
}

