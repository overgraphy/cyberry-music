﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebCyberryMusic.Helpers
{
    public class HttpHeaderAttribute : ActionFilterAttribute
    {
        /// 
        /// Gets or sets the name of the HTTP Header.
        /// 
        /// The name.
        public string Name { get; set; }

        /// 
        /// Gets or sets the value of the HTTP Header.
        /// 
        /// The value.
        public string Value { get; set; }

            /// 
        /// Gets or sets the name of the HTTP Header.
        /// 
        /// The name.
        public string Name2 { get; set; }

        /// 
        /// Gets or sets the value of the HTTP Header.
        /// 
        /// The value.
        public string Value2 { get; set; }

        /// 
        /// Initializes a new instance of the  class.
        /// 
        /// The name.
        /// The value.
        public HttpHeaderAttribute(string name, string value)
        {
            Name = name;
            Value = value;
        }
        public HttpHeaderAttribute(string name, string value,string name2, string value2)
        {
            Name = name;
            Value = value;
            Name2 = name2;
            Value2 = value2;
        }
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.AppendHeader(Name, Value);
            filterContext.HttpContext.Response.AppendHeader(Name2, Value2);
            base.OnResultExecuted(filterContext);
        }
    }
}