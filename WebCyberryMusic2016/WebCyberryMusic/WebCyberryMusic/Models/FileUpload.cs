﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DINF.Models
{
    public class FileUpload
    {
        public bool IsValid { get; set; }
        public string Message { get; set; }
        public string ShortPath { get; set; }
        public string FullPath { get; set; }
        public string FileName { get; set; }
    }
}
