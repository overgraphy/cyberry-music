﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DINFDAL.Interfaces;

namespace WebCyberryMusic.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DashBoardController : Controller
    {
        private ISong_OriginalService _originalService;
        private ISong_ContestService _songContestService;
        private IUserService _userService;
        private ISubscribedService _subscribedService;
        private IBalanceService _balanceService;
        private IWithdrawal_TransactionService _withdrawalTransactionService;

        public DashBoardController(ISong_OriginalService originalService,
            ISong_ContestService songContestService,
            IUserService userService,
            ISubscribedService subscribedService,
            IBalanceService balanceService,
            IWithdrawal_TransactionService withdrawalTransactionService)
        {
            _originalService = originalService;
            _songContestService = songContestService;
            _userService = userService;
            _subscribedService = subscribedService;
            _balanceService = balanceService;
            _withdrawalTransactionService = withdrawalTransactionService;
        }
        // GET: Admin/DashBoard
        public ActionResult Index()
        {
            ViewBag.SongCount = _songContestService.GetAll().Count;
            ViewBag.SongOriginalCount = _originalService.GetAll().Count;
            ViewBag.UserCount = _userService.GetAll().Count(i => i.is_contest == true);
            ViewBag.Users = _userService.GetAll().Where(i => i.is_contest == true).OrderByDescending(i => i.datetime_create).Take(10).ToList();
            ViewBag.HotSong = _songContestService.GetHotVideo();
            ViewBag.NewSong = _songContestService.GetNewVideo();
            //
            var incomeTransact = _balanceService.GetAllIncomeTransact();
            var smsCount = incomeTransact.Where(i => i.int_vote_method == 1).Select(i => i.int_vote_history_id).Distinct().Count();
            var pointCount = incomeTransact.Where(i => i.int_vote_method == 2).Select(i => new { i.int_vote_history_id, i.int_vote_value }).Distinct().Sum(i => i.int_vote_value);
            //
            ViewBag.SMSVote = smsCount;
            ViewBag.PointVote = pointCount;
            ViewBag.Withdrawal = _withdrawalTransactionService.GetAll().OrderByDescending(i => i.datetime_create).Take(10).ToList();
            return View();
        }
    }
}