﻿using DINFDAL.Interfaces;
using DINFDAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DINFDAL.Services
{
    public class UserService : IUserService
    {
        private CyberryMusicEntities _db = new CyberryMusicEntities();
        private DbContextTransaction _dbContextTransaction;

        //Insert
        public int Add(User user)
        {
            int result = 0;

            try
            {
                _db.Entry(user).State = EntityState.Added;
                result = _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Add(IList<User> users)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var model in users)
                        {
                            _db.Entry(model).State = EntityState.Added;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Update
        public int Update(User user)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value

                        _db.Entry(user).State = EntityState.Modified;
                        result = _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Update(IList<User> users)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value
                        foreach (var model in users)
                        {
                            _db.Entry(model).State = EntityState.Modified;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Select all
        public IList<User> GetAll()
        {
            IList<User> models;

            try
            {
                models = _db.Users.Where(i => i.is_activated == true && i.is_banned == false).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select by id
        public User GetById(string id)
        {
            User model;

            try
            {
                model = _db.Users.Find(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        public IList<User> Fetch(Expression<Func<User, bool>> predicate, bool active = true)
        {
            IList<User> models;

            try
            {
                models = _db.Set<User>().Where(m => m.is_activated == active).Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }
    }
}

namespace DINFDAL.Interfaces
{
    public interface IUserService
    {
        int Add(User user);

        int Add(IList<User> users);

        int Update(User user);

        int Update(IList<User> users);

        IList<User> GetAll();

        User GetById(string id);

        IList<User> Fetch(Expression<Func<User, bool>> predicate, bool active = true);
    }
}