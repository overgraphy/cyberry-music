﻿using DINFDAL.Interfaces;
using DINFDAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DINFDAL.Services
{
    public class PaysBuyConfigService : IPaysBuyConfigService
    {
        private CyberryMusicEntities _db = new CyberryMusicEntities();
        private DbContextTransaction _dbContextTransaction;

        //Insert
        public int Add(PaysBuyConfig paysBuyConfig)
        {
            int result = 0;

            try
            {
                _db.Entry(paysBuyConfig).State = EntityState.Added;
                result = _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Add(IList<PaysBuyConfig> paysBuyConfigs)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var model in paysBuyConfigs)
                        {
                            _db.Entry(model).State = EntityState.Added;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Update
        public int Update(PaysBuyConfig paysBuyConfig)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value

                        _db.Entry(paysBuyConfig).State = EntityState.Modified;
                        result = _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Update(IList<PaysBuyConfig> paysBuyConfigs)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value
                        foreach (var model in paysBuyConfigs)
                        {
                            _db.Entry(model).State = EntityState.Modified;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Select all
        public IList<PaysBuyConfig> GetAll()
        {
            IList<PaysBuyConfig> models;

            try
            {
                models = _db.PaysBuyConfigs.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select by id
        public PaysBuyConfig GetById(int id)
        {
            PaysBuyConfig model;

            try
            {
                model = _db.PaysBuyConfigs.Find(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        public IList<PaysBuyConfig> Fetch(Expression<Func<PaysBuyConfig, bool>> predicate, bool active = true)
        {
            IList<PaysBuyConfig> models;

            try
            {
                models = _db.Set<PaysBuyConfig>().Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }
    }
}

namespace DINFDAL.Interfaces
{
    public interface IPaysBuyConfigService
    {
        int Add(PaysBuyConfig paysBuyConfig);

        int Add(IList<PaysBuyConfig> paysBuyConfigs);

        int Update(PaysBuyConfig paysBuyConfig);

        int Update(IList<PaysBuyConfig> paysBuyConfigs);

        IList<PaysBuyConfig> GetAll();

        PaysBuyConfig GetById(int id);

        IList<PaysBuyConfig> Fetch(Expression<Func<PaysBuyConfig, bool>> predicate, bool active = true);
    }
}