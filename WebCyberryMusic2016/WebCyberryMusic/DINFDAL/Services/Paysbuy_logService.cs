﻿using DINFDAL.Interfaces;
using DINFDAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DINFDAL.Services
{
    public class Paysbuy_logService : IPaysbuy_logService
    {
        private CyberryMusicEntities _db = new CyberryMusicEntities();
        private DbContextTransaction _dbContextTransaction;

        //Insert
        public int Add(Paysbuy_log paysbuy_log)
        {
            int result = 0;

            try
            {
                _db.Entry(paysbuy_log).State = EntityState.Added;
                result = _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Add(IList<Paysbuy_log> paysbuy_logs)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var model in paysbuy_logs)
                        {
                            _db.Entry(model).State = EntityState.Added;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Update
        public int Update(Paysbuy_log paysbuy_log)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value

                        _db.Entry(paysbuy_log).State = EntityState.Modified;
                        result = _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Update(IList<Paysbuy_log> paysbuy_logs)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value
                        foreach (var model in paysbuy_logs)
                        {
                            _db.Entry(model).State = EntityState.Modified;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Select all
        public IList<Paysbuy_log> GetAll()
        {
            IList<Paysbuy_log> models;

            try
            {
                models = _db.Paysbuy_log.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select by id
        public Paysbuy_log GetById(int id)
        {
            Paysbuy_log model;

            try
            {
                model = _db.Paysbuy_log.Find(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        public IList<Paysbuy_log> Fetch(Expression<Func<Paysbuy_log, bool>> predicate, bool active = true)
        {
            IList<Paysbuy_log> models;

            try
            {
                models = _db.Set<Paysbuy_log>().Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }
    }
}

namespace DINFDAL.Interfaces
{
    public interface IPaysbuy_logService
    {
        int Add(Paysbuy_log paysbuy_log);

        int Add(IList<Paysbuy_log> paysbuy_logs);

        int Update(Paysbuy_log paysbuy_log);

        int Update(IList<Paysbuy_log> paysbuy_logs);

        IList<Paysbuy_log> GetAll();

        Paysbuy_log GetById(int id);

        IList<Paysbuy_log> Fetch(Expression<Func<Paysbuy_log, bool>> predicate, bool active = true);
    }
}