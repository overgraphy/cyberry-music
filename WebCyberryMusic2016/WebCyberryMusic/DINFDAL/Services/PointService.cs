﻿using DINFDAL.Interfaces;
using DINFDAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DINFDAL.Services
{
    public class PointService : IPointService
    {
        private CyberryMusicEntities _db = new CyberryMusicEntities();
        private DbContextTransaction _dbContextTransaction;

        //Insert
        public int Add(Point point)
        {
            int result = 0;

            try
            {
                _db.Entry(point).State = EntityState.Added;
                result = _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Add(IList<Point> points)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var model in points)
                        {
                            _db.Entry(model).State = EntityState.Added;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Update
        public int Update(Point point)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value

                        _db.Entry(point).State = EntityState.Modified;
                        result = _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Update
        public int VotePoint(Point point, Score_Song scoreSong, Point_Transaction transfer,Vote_History votehis)
        {
            int result = 0;
            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        _db.Point_Transaction.Add(transfer);
                        _db.Vote_History.Add(votehis);
                        if (scoreSong.int_score_id == 0)
                        {
                            _db.Score_Song.Add(scoreSong);
                        }
                        else
                        {
                            var score = _db.Score_Song.Find(scoreSong.int_score_id);
                            score.datetime_modified = scoreSong.datetime_modified;
                            score.int_value = scoreSong.int_value;
                        }
                        _db.Entry(point).State = EntityState.Modified;
                        result = _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int TransferPoint(Point fromPoint, Point toPoint, Point_Transaction transfer)
        {
            int result = 0;
            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        _db.Point_Transaction.Add(transfer);
                        if (toPoint.int_point_id == 0)
                        {
                            _db.Points.Add(toPoint);
                        }
                        else
                        {
                            var updateToPoint = _db.Points.Find(toPoint.int_point_id);
                            updateToPoint.datetime_modified = toPoint.datetime_modified;
                            updateToPoint.int_value = toPoint.int_value;
                        }
                        var updateFormPoint = _db.Points.Find(fromPoint.int_point_id);
                        updateFormPoint.datetime_modified = fromPoint.datetime_modified;
                        updateFormPoint.int_value = fromPoint.int_value;
                        result = _db.SaveChanges();
                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public int Update(IList<Point> points)
        {
            int result = 0;

            try
            {
                using (_dbContextTransaction = _db.Database.BeginTransaction())
                {
                    try
                    {
                        // TODO implement update value
                        foreach (var model in points)
                        {
                            _db.Entry(model).State = EntityState.Modified;
                            result += _db.SaveChanges();
                        }

                        _dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        //Select all
        public IList<Point> GetAll()
        {
            IList<Point> models;

            try
            {
                models = _db.Points.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }

        //Select by id
        public Point GetById(int id)
        {
            Point model;

            try
            {
                model = _db.Points.Find(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        //Select by id
        public Point GetByUserId(string id)
        {
            Point model;

            try
            {
                model = _db.Points.FirstOrDefault(i => i.int_user_id == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return model;
        }

        public IList<Point> Fetch(Expression<Func<Point, bool>> predicate, bool active = true)
        {
            IList<Point> models;

            try
            {
                models = _db.Set<Point>().Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return models;
        }
    }
}

namespace DINFDAL.Interfaces
{
    public interface IPointService
    {
        int Add(Point point);

        int Add(IList<Point> points);

        int VotePoint(Point point, Score_Song scoreSong,Point_Transaction transfer,Vote_History votehis);

        int TransferPoint(Point fromPoint, Point toPoint, Point_Transaction transfer);

        int Update(Point point);

        int Update(IList<Point> points);

        IList<Point> GetAll();

        Point GetById(int id);

        Point GetByUserId(string id);

        IList<Point> Fetch(Expression<Func<Point, bool>> predicate, bool active = true);
    }
}