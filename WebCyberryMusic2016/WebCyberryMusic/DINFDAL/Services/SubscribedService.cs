using DINFDAL.Interfaces;
using DINFDAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
namespace DINFDAL.Services
{
    public class SubscribedService : ISubscribedService
    {
        private CyberryMusicEntities _db = new CyberryMusicEntities();
        private DbContextTransaction _dbContextTransaction;
        public int Add(Subscribed subscribed)
        {
            int result = 0;
            try
            {
                this._db.Entry<Subscribed>(subscribed).State = EntityState.Added;
                result = this._db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }
        public int Add(IList<Subscribed> subscribeds)
        {
            int num = 0;
            try
            {
                using (this._dbContextTransaction = this._db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (Subscribed current in subscribeds)
                        {
                            this._db.Entry<Subscribed>(current).State = EntityState.Added;
                            num += this._db.SaveChanges();
                        }
                        this._dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        this._dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return num;
        }
        public int Update(Subscribed subscribed)
        {
            int result = 0;
            try
            {
                using (this._dbContextTransaction = this._db.Database.BeginTransaction())
                {
                    try
                    {
                        this._db.Entry<Subscribed>(subscribed).State = EntityState.Modified;
                        result = this._db.SaveChanges();
                        this._dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        this._dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }
        public int Update(IList<Subscribed> subscribeds)
        {
            int num = 0;
            try
            {
                using (this._dbContextTransaction = this._db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (Subscribed current in subscribeds)
                        {
                            this._db.Entry<Subscribed>(current).State = EntityState.Modified;
                            num += this._db.SaveChanges();
                        }
                        this._dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        this._dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return num;
        }
        public IList<Subscribed> GetAll()
        {
            IList<Subscribed> result;
            try
            {
                result = this._db.Subscribeds.ToList<Subscribed>();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }
        public Subscribed GetById(int id)
        {
            Subscribed result;
            try
            {
                result = this._db.Subscribeds.Find(new object[]
				{
					id
				});
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }
        public IList<Subscribed> Fetch(Expression<Func<Subscribed, bool>> predicate, bool active = true)
        {
            IList<Subscribed> result;
            try
            {
                result = this._db.Set<Subscribed>().Where(predicate).ToList<Subscribed>();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }
    }
}

namespace DINFDAL.Interfaces
{
    public interface ISubscribedService
    {
        int Add(Subscribed subscribed);
        int Add(IList<Subscribed> subscribeds);
        int Update(Subscribed subscribed);
        int Update(IList<Subscribed> subscribeds);
        IList<Subscribed> GetAll();
        Subscribed GetById(int id);
        IList<Subscribed> Fetch(Expression<Func<Subscribed, bool>> predicate, bool active = true);
    }
}
