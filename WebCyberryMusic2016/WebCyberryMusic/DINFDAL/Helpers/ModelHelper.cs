﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DINFDAL.Models;

namespace DINFDAL.Helpers
{
    public class ModelHelper
    {
        static public T2 ModelMapping<T1, T2>(T1 srcModel)
        {
            Mapper.CreateMap<T1, T2>();
            return Mapper.Map<T1, T2>(srcModel);
        }
    }
}
