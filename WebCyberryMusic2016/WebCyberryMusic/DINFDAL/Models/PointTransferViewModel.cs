﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DINFDAL.Models
{
    public class PointTransferViewModel
    {
        [Required(ErrorMessage = "The Field is required")]
        public string int_user_id { get; set; }

        [Required(ErrorMessage = "The Field is required")]
        public string int_target_user_id { get; set; }

        [Required(ErrorMessage = "The Field is required")]
        [Display(Name = "Point")]
        public int Point { get; set; }

        public int CurrentPoint { get; set; }
    }
}
