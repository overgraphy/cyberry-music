﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DINFDAL.Models
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(WithdrawalTransactionViewModel))]
    public partial class Withdrawal_Transaction
    {
        // public string strTransferDate { get; set; }
    }

    //
    public class WithdrawalTransactionViewModel
    {
        public int int_withdrawal_transaction_id { get; set; }

        [Required(ErrorMessage = "The Field is required")]
        public int int_balance_id { get; set; }

        [Required(ErrorMessage = "The Field is required")]
        public string int_user_id { get; set; }

        [Required(ErrorMessage = "The Field is required")]
        public string amount { get; set; }

        public string transfer_amount { get; set; }

        [Required(ErrorMessage = "The Field is required")]
        public string balance { get; set; }

        [Required(ErrorMessage = "The Field is required")]
        public string vat { get; set; }

        public string include_vat { get; set; }

        public string txt_remark { get; set; }
        public string txt_payment_slip { get; set; }
        public string txt_withholding_tax { get; set; }
        public System.DateTime datetime_create { get; set; }
        public Nullable<System.DateTime> datetime_transfer_date { get; set; }

    }
}
