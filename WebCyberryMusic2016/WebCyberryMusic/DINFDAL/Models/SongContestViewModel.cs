﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;

namespace DINFDAL.Models
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(SongContestViewModel))]
    public partial class Song_Contest
    {

    }

    //
    public class SongContestViewModel
    {
        [Required(ErrorMessage = "The Field is required")]
        public int int_song_original_id { get; set; }

        [Required(ErrorMessage = "The Field is required")]
        [Display(Name = "ชื่อเพลง")]
        public string txt_song_name { get; set; }
       
        [YoutubeUrlValidator]
        public string txt_youtube_url { get; set; }
        
        public string txt_youtube_id { get; set; }

    }
}
