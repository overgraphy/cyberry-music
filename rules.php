<?php

$myURL = $_SERVER['REQUEST_URI'];
$og_image = "http://www.cyberrymusic.com/images/ogimage-rules.jpg";
$hd = array(
	"title" => "Cyberry Music: กติกา",
	"description" => "กติกาการแข่งขันประกวดคลิปเพลง Cyberry Music",
	"keyword" => "cyberry, cyberry music, กติกา, แข่งขัน, ประกวด, ร้องเพลง, เล่นดนตรี, youtube",
	"og_title" => "กติกาการแข่งขันประกวดคลิปเพลง Cyberry Music",
	"og_sitename" => "CyberryMusic.com",
	"og_url" => "http://www.cyberrymusic.com/rules.php",
	"og_image" => "$og_image",
	"og_description" => "กติกาสำหรับการประกวดคลิปเพลงบน Cyberry Music"
);
?>

<?php include realpath( __DIR__ . '/__display.php' ); ?>
<?php include_once realpath(__DIR__ . "/googleanalytic.php") ?>
<?php
write_header($hd);
?>
<div class="container">
<!---- Content ----->

<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
  <tr>
    <td align="left" valign="top" bgcolor="#F4F4F4"><br />
      <table width="90%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <td height="50" align="left" valign="top" class="Arial_gray">
		  <h2>กติกาการแข่งขันประกวดคลิปเพลง</h2>
		  <div class="date-announce">
ประกาศเมื่อวันที่ 4 สิงหาคม 2558
</div>
		  <ol>
			<li>ผู้แข่งขันประกอบอาชีพอะไรก็ได้ ไม่จำกัดอายุ เพศ การศึกษา</li>
			<li>ผู้แข่งขันต้องสมัครเข้าแข่งขันที่เว็บ CyberryMusic.com ก่อน </li>
			<li>เพลงที่จะนำมาแข่งขัน ห้ามนำเพลงที่มีลิขสิทธิ์จากค่ายเพลงต่างๆ มาใช้</li>
			<li>ผู้แข่งขันต้อง Upload เพลงไว้บนเว็บ Youtube.com หรือ Vimeo.com หรืออื่นๆ แล้วนำ URL มาลงทะเบียนเพลงกับเว็บ Cyberry Music</li>
			<li>เพลงที่จะนำมาแข่งขันต้องเป็นเพลงที่..
				<ol>
					<li>เป็นเพลงที่ผู้แข่งขันแต่งทำนองและเนื้อร้องเอง
						<ol>
							<li>เมื่อเพลงที่แต่งเองได้นำขึ้นสู่ระบบของ Cyberry Music แล้ว ผู้แข่งขันจะได้เป็น "ผู้แต่งเพลง" โดยอัตโนมัติ และเพลงของผู้แข่งขันที่แต่งเอง จะเป็นเพลงต้นแบบให้ผู้อื่นนำไป Cover ได้โดยอัตโนมัติ</li>
							<li>เมื่อมีการโหวตเพลงที่ผู้แข่งขันนำขึ้นระบบ และเกิดรายได้ขึ้น ผู้แข่งขันจะได้รับส่วนแบ่งจากการโหวตด้วย</li>
							<li>เมื่อมีคนนำเพลงของ "ผู้แต่งเพลง" ไป Cover เพื่อประกวด หากมีการโหวตและเกิดรายได้ขึ้น "ผู้แต่งเพลง" ก็จะได้รับส่วนแบ่งจากการโหวตนั้นด้วย</li>
							<li>เมื่อ "ผู้แต่งเพลง" Upload เพลงขึ้นสู่สาธารณะ ลิขสิทธิ์เพลงจะเป็นของผู้แต่งเพลงทันทีโดยอัตโนมัติ (<a href="rights.php">คลิกอ่านรายละเอียด</a>)</li>
							<li>หากพบว่า "ผู้แต่งเพลง" ได้ทำการละเมิดลิขสิทธิ์เพลงของผู้อื่น ไม่ว่าจะเป็นเพลงจากค่ายต่างๆ หรือเพลงของผู้ประกวดท่านอื่นๆ แล้วแอบอ้างว่าเป็นเพลงของตัวเอง เพลงนั้นจะเป็นโมฆะ ไม่สามารถเข้าสู่การประกวดได้อีก และส่วนแบ่งจากการโหวตเพลงนั้น จะเป็นโมฆะ ไม่สามารถเบิกถอนได้ และ Accout ของท่าน จะไม่สามารถประกวดได้</li>
							<li>บริษัท ไซเบอร์รี่ มิวสิค จะไม่รับผิดชอบใดๆ ในกรณีที่ตรวจพบว่า นักร้อง นักดนตรี ละเมิดลิขสิทธิ์เพลงของผู้อื่น</li>
						</ol>
					</li>
					<li>หากไม่มีความสามารถด้านการแต่งเพลง ผู้แข่งขันสามารถเลือกเพลงที่ต้องการได้จากคลังเพลงที่มี "ผู้แต่งเพลง" ได้ลงทะเบียนไว้
						<ol>
							<li>เมื่อมีการโหวตเพลงที่ผู้แข่งขันนำไปร้อง และเกิดรายได้ขึ้น ผู้แข่งขันจะได้รับส่วนแบ่งจากการโหวตด้วย</li>
							<li>การนำเพลงของ "ผู้แต่งเพลง" มาใช้ประกวด ผู้ประกวดจะต้องระบุ ID ของเพลงต้นแบบให้ถูกต้อง เพื่อให้มีการแบ่งปันรายได้อย่างถูกต้องและเป็นธรรม</li>
							<li>หากพบว่าผู้ประกวดกรอก ID ของเพลงที่นำมาประกวดผิด ไม่ว่าตั้งใจหรือไม่ตั้งใจ เพลงนั้นจะเป็นโมฆะ ไม่สามารถเข้าสู่การประกวดได้อีก </li>
						</ol>
					</li>
				</ol>
			</li>
			<li>ผู้แข่งขัน จะต้องกรอกหมายเลขบัญชีธนาคารเพิ่มเติมในขั้นตอนการสมัคร เพื่อจะใช้ในการโอนเงินค่าส่วนแบ่งจากการโหวตให้ท่าน </li>
			<li>ค่าส่วนแบ่งที่จะสามารถโอนกลับให้ผู้แข่งขันได้นั้น ต้องมีมูลค่ามากกว่า 500 บาทขึ้นไป</li>
			<li>ห้ามโพสคลิปที่ขัดต่อความสงบเรียบร้อย และศีลธรรมอันดีของสังคม</li>
		  </ol>
		  
		  
		  </td>
        </tr>
    </table>
      <p>&nbsp;</p>
	  
	  <table width="90%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <td height="50" align="left" valign="middle" class="Arial_gray"><h2><strong>กติกาการโหวต</strong></h2></td>
        </tr>
        <tr>
          <td height="50" align="left" valign="top" class="Arial_gray">
		  
		  <ol>
			<li>สามารถโหวตได้ 2 แบบคือ
				<ol>
					<li>โหวตทาง SMS</li>
					<li>โหวตด้วย Point ผ่านเว็บ Cyberry Music (จะต้องมีการซื้อ Point เพื่อใช้โหวต โดยทำการซื้อหรือโอนเงินผ่าน Paysbuy)</li>
				</ol>
			</li>
			<li>ต้องสมัครสมาชิกเว็บ Cyberry Music เสียก่อนจึงจะโหวตได้</li>
			<li>สมาชิกทั้งแบบธรรมดา และแบบเป็นผู้เข้าแข่งขัน สามารถโหวตได้</li>
			<li>สามารถโหวตได้ไม่จำกัดจำนวนครั้ง</li>
			
		  </td>
        </tr>
    </table>
	  
	  <p>&nbsp;</p>
	 <table width="90%" border="0" align="center" cellpadding="2" cellspacing="2">
	 <tr>
          <td height="50" align="left" valign="middle" class="Arial_gray"><h2><strong>ผังระบบการของการประกวด Cyberry Music Content</strong></h2></td>
        </tr>
		<tr>
          <td height="50" align="left" valign="top" class="Arial_gray">
			<img src="images/rule_page_1.jpg" style="width:96%;">
			</td>
		</tr>
	</table>
	
	<p>&nbsp;</p>
	 <table width="90%" border="0" align="center" cellpadding="2" cellspacing="2">
	 <tr>
          <td height="50" align="left" valign="middle" class="Arial_gray"><h2><strong>ผังของระบบการโหวตและการแบ่งปันรายได้</strong></h2></td>
        </tr>
		<tr>
          <td height="50" align="left" valign="top" class="Arial_gray">
			<img src="images/rule_page_2.jpg" style="width:96%;">
			</td>
		</tr>
	</table>
	
	  </td>
  </tr>
</table>



<!---- /Content ----->
</div>
<?php
write_footer();
?>
</body>
</html>