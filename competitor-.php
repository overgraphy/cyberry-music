<?php

require_once("db.php");

$myURL = $_SERVER['REQUEST_URI'];
$og_image = "http://www.cyberrymusic.com/images/ogimage-rules.jpg";
$hd = array(
	"title" => "Cyberry Music: Competitor",
	"description" => "ผู้เข้าแข่งขัน Cyberry Music",
	"keyword" => "cyberry, cyberry music, กติกา, แข่งขัน, ประกวด, ร้องเพลง, เล่นดนตรี, youtube",
	"og_title" => "กติกาการแข่งขันประกวดคลิปเพลง Cyberry Music",
	"og_sitename" => "CyberryMusic.com",
	"og_url" => "http://www.cyberrymusic.com/rules.php",
	"og_image" => "$og_image",
	"og_description" => "ผู้เข้าแข่งขัน Cyberry Music"
);
?>

<?php include realpath( __DIR__ . '/display.php' ); ?>
<?php include_once realpath(__DIR__ . "/googleanalytic.php") ?>
<?php
write_header($hd);
?>
<link href="css/list-order.css" rel="stylesheet" type="text/css" />
</head>

<body onload="MM_preloadImages('images/icon_fb2.jpg','images/icon_twitter2.jpg','images/icon_youtube2.jpg','images/icon_googleplus2.jpg','images/footer_fb2.jpg','images/footer_twitter2.jpg','images/footer_youtube2.jpg','images/footer_googleplus2.jpg')">
<table width="1200" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="225" height="100" align="left" valign="middle" scope="col"><img src="images/logo_cyberrymusic.jpg" width="225" height="100" /></td>
    <td align="center" valign="bottom" scope="col"><table width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
      <tr>
        <td height="60" align="right" valign="top"><table width="160" border="0" align="right" cellpadding="2" cellspacing="2">
          <tr>
            <td width="40" align="right" valign="top" class="blue"><div align="right"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image21','','images/icon_fb2.jpg',1)"><img src="images/icon_fb.jpg" width="40" height="40" id="Image21" /></a></div>
              <a href="#"></a></td>
            <td width="40" align="right" valign="top" class="blue"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image20','','images/icon_twitter2.jpg',1)"><img src="images/icon_twitter.jpg" width="40" height="40" id="Image20" /></a></td>
            <td width="40" align="right" valign="top" class="blue"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image19','','images/icon_youtube2.jpg',1)"><img src="images/icon_youtube.jpg" width="40" height="40" id="Image19" /></a></td>
            <td width="40" align="right" valign="top" class="blue"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image23','','images/icon_googleplus2.jpg',1)"><img src="images/icon_googleplus.jpg" width="40" height="40" id="Image23" /></a></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="35" align="right" valign="middle" class="blue">
		<div align="right">
			<span class="Arial_gray">
					<a href="index.php">หน้าแรก</a>&nbsp;&nbsp;
					<a href="rules.php">&nbsp;กติกา</a>&nbsp;&nbsp;
					&nbsp;ผู้เข้าแข่งขัน&nbsp;&nbsp;&nbsp;
					สมัครสมาชิก&nbsp;&nbsp;
					&nbsp;เข้าสู่ระบบ
				</span>
			
			</span></div></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="1200" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="190" align="left" valign="middle" background="images/head_register.jpg" scope="col"><table width="1000" border="0" align="center" cellpadding="0" cellspacing="5">
      <tr>
        <td class="whitebig"><!--<span id="result_box" lang="en" xml:lang="en">กติกา</span>--></td>
      </tr>
    </table>
      <br /></td>
  </tr>
</table>
<!--
<table width="1180" border="0" align="center" cellpadding="2" cellspacing="2">
  <tr>
    <td height="30" align="left" valign="middle" class="blue"><a href="#">Home</a> &gt;<span id="result_box2" lang="en" xml:lang="en"> กติกา</span></td>
  </tr>
</table>
<br />
<br />
-->
<!--- DAta-->

<?php
if(!isset($_GET['id'])){
	die("No Data..");
}
$id = $_GET['id'];
$sql = "select * from users where id = '$id' ";
$res = mysql_query($sql, $dbconnect);
$rw = mysql_fetch_array($res);
$username = $rw['username'];
$avatar = $rw['avatar'];


if(trim($avatar)==""){
	$avatar = "images/cyberry-avatar-01.jpg";
}
?>

<table width="1200" border="0" align="center" cellpadding="2" cellspacing="2">
  <tr>
    <td width="350" align="left" valign="top">
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="350" height="150" align="left" valign="top" bgcolor="#e4e4e4">
		<table width="100%" border="0" align="center" cellpadding="4" cellspacing="4">
          <tr>
            <td width="150" align="center" valign="top" bgcolor="#FFFFFF"><img src="<?=$avatar?>" width="150" height="150" /></td>
            <td align="left" valign="top" class="Arial_gray">
			<table width="100%" border="0" cellspacing="4" cellpadding="1">
              <tr>
                <td height="55" colspan="2" align="left" valign="top">Welcome : <?=$username?><br />
                  Number : <?=sprintf("%05d", $id)?></td>
              </tr>
              <tr>
                <td height="35" align="left" valign="top">&nbsp;</td>
                <td align="left" valign="top">&nbsp;</td>
              </tr>
			  <!--
              <tr>
                <td width="43" height="43" align="left" valign="top"><img src="images/i_edit_profile.png" width="43" height="43" /></td>
                <td align="left" valign="middle">Edite Profile</td>
              </tr>
			  -->
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table>
      <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <td width="350" height="150" align="left" valign="top" bgcolor="#575757"><table width="100%" border="0" align="center" cellpadding="4" cellspacing="4">
            <tr>
              <td align="left" valign="top" class="Arial_gray"><table width="90%" border="0" cellspacing="2" cellpadding="2">
                <tr>
                  <td width="43" height="50" rowspan="2" align="left" valign="top"><img src="images/i_music.png" width="43" height="43" /></td>
                  <td height="25" align="left" valign="middle" class="whitebig">Upload Your Song</td>
                </tr>
                <tr>
                  <td height="25" align="left" valign="top" class="white">เลือกเพลงที่คุณจะส่งเข้าประกวด</td>
                </tr>
              </table>
			<form method=post action="submitsong.php">
			  <table width="90%" border="0" align="center" cellpadding="2" cellspacing="2">
				<tr>
					<td class="white" colspan=2>YouTube URL
					<br>
						<input name="youtube" type="text" id="youtube" value="" size="30" placeholder="URL YouTube เพลงของคุณ" class="form-control" />
					</td>
				</tr>
				<tr>
					<td class="white" colspan=2>ชื่อเพลง<br>
						<input name="songname" type="text" id="songname" value="" size="30" placeholder="" class="form-control" />
					</td>
				</tr>
				<tr>
					<td class="white" valign="top">ผู้แต่ง</td>
					<td class="white">
						<label class="white"><input type="radio" name="composer" value="own"> เพลงแต่งเอง</label><br>
					<label class="white"><input type="radio" name="composer" value="other"> เพลงของผู้ประกวดท่านอื่น</label>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td >
						<span id="texturl" class="white">URL YouTube เพลงต้นฉบับ</span>
						<br>
						<input name="youtube2" type="text" id="youtube2" value="" size="30" placeholder="URL YouTube"/>
					</td>
				</tr>
				<tr>
					<td colspan=2 align=center>
					<br>
					<input type=submit value="Send Song">
					</td>
				</tr>
			  </table>
			  <input type="hidden" name="id" value="<?=$id?>">
			</form>
			
			<script>
			$(document).ready(function(){
				/*
				$('#youtube2').attr('disabled',true);
				$('#texturl').removeClass('white');
				$('#texturl').addClass('grey');
				
				$('input:radio').change(function() {
				   alert($('[name*=composer]').value());
				});
				
				$('[name*=composer]').click(function(){
					
					
					if($(this).value()=='other'){
						$('#youtube2').attr('disabled',false);
						$('#texturl').removeClass('grey');
						$('#texturl').addClass('white');
					}else{
						$('#youtube2').attr('disabled',true);
						$('#texturl').removeClass('white');
						$('#texturl').addClass('grey');
					}
				});
				*/
			});
			</script>
                </td>
            </tr>
          </table></td>
        </tr>
      </table>
	  <!--
      <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <td width="350" height="150" align="left" valign="top" bgcolor="#333333"><table width="100%" border="0" align="center" cellpadding="4" cellspacing="4">
            <tr>
              <td align="left" valign="top" class="Arial_gray"><table width="90%" border="0" cellspacing="2" cellpadding="2">
                <tr>
                  <td width="43" height="50" rowspan="2" align="left" valign="top"><img src="images/i_chatgroup.png" width="43" height="43" /></td>
                  <td height="25" align="left" valign="middle" class="whitebig">Chat Groups</td>
                </tr>
                <tr>
                  <td height="25" align="left" valign="top" class="white">&nbsp;</td>
                </tr>
              </table>
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                  <tr>
                    <td height="200" colspan="2" align="left" valign="top" class="white"><table width="100%" border="0" cellspacing="1" cellpadding="1">
                      <tr>
                          <td align="left" valign="top">วงพี่เล่นได้สุดยอดดดด // boy 23</td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" class="blue">29.07.58 / 18.00</td>
                        </tr>
                        <tr>
                          <td align="left" valign="top">เล่นแน่นดีครับ เป็นกำลังใจให้นะครับ // มาวิน</td>
                        </tr>
                        <tr>
                          <td align="left" valign="top"><span class="blue">29.07.58 / 18.50</span></td>
                        </tr>
                        <tr>
                          <td align="right" valign="top">ขอบคุณทุก ๆ คะแนนเสียงครับ // <span class="Arial_blue">Zuperstar </span></td>
                        </tr>
                        <tr>
                          <td align="right" valign="top"><span class="blue">29.07.58 / 19.00</span></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="left" valign="top">&nbsp;</td>
                        </tr>
                  </table></td>
                  </tr>
                  <tr>
                    <td align="right" valign="middle"><span class="white">ข้อความ :</span></td>
                    <td><input name="textfield" type="text" id="textfield3" size="30" />
                      <input type="submit" name="Browse" id="Browse2" value="Send" /></td>
                  </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table>
	  -->
	  </td>
    <td align="left" valign="top">
		<div class="songof">เพลงของ <?=$username?></div>
		
		<?php 
		$sql = "select * from videos where user_id='$id' order by timestamp";
		$res = mysql_query($sql, $dbconnect);
		
		$max_videos = 10;
		$v = 1;
		$col = 1;
		while($rw=mysql_fetch_array($res)){
			$ytcode=$rw['vdo_code'];
			$name = $rw['vdo_title'];
			$code_vote = $rw['code_vote'];
			
			$thumb = "http://img.youtube.com/vi/" . $ytcode . "/mqdefault.jpg";
			
			
			if($col==1){
				echo "<div class='row-fluid'>\n";
			}
			echo "<div class='col-md-4'>\n";
			
			echo "<a href='https://www.youtube.com/watch?v=$ytcode' target='_blank'><img src='$thumb' border=0 width=260 ></a>";
			echo "<div class='video-desc'>";
			echo "เพลง : $name<br>";
			echo "Vote Code: $code_vote<br>";
			echo "Voted: x<br>";
			echo "<br>";
			echo "</div>\n";
			echo "</div>\n";
			echo "</div>\n";
			
			
			
			$v++;
			$col++;
			
			if($col > 3){
				echo "</div>\n";
				$col=1;
			}
		}
		
		for($x = $v ; $x <=$max_videos; $x++){
			if($col==1){
				echo "<div class='row-fluid'>\n";
			}
			echo "<div class='col-md-4'>\n";
			echo "<div>";
			echo "<img src='images/video-none.png' border=0 width=260>";
			echo "<div class='video-desc'>";
			echo "เพลง ...<br>";
			echo "Voted: 0<br>";
			echo "<br>";
			echo "</div>\n";
			echo "</div>\n";
			echo "</div>\n";
			
			$col++;
			
			if($col > 3){
				echo "</div>\n";
				$col=1;
			}
		}
		
		?>
		</div>
	<!--
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td height="40" colspan="4" align="left" valign="top" class="HeadBlue">เพลงของ <?=$username?></td>
        </tr>
      <tr>
        <td width="200" align="left" valign="top" class="Arial_gray"><iframe width="200" height="113" src="https://www.youtube.com/embed/Pq6pn8iPp-E?rel=0&amp;controls=0" frameborder="0" allowfullscreen></iframe></td>
        <td width="200" align="left" valign="top" class="Arial_gray"><iframe width="200" height="113" src="https://www.youtube.com/embed/rx5KOTg1sVk?rel=0&amp;controls=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe></td>
        <td width="200" align="left" valign="top" class="Arial_gray"><iframe width="200" height="113" src="https://www.youtube.com/embed/rx5KOTg1sVk?rel=0&amp;controls=0" frameborder="0" allowfullscreen></iframe></td>
        <td width="200" align="left" valign="top" class="Arial_gray"><iframe width="200" height="113" src="https://www.youtube.com/embed/rx5KOTg1sVk?rel=0&amp;controls=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe></td>
        </tr>
      <tr>
        <td height="50" align="center" valign="top" bgcolor="#CCCCCC" class="Arial_gray">Artist : Zuperstarr<br />
          Song : Somebody To Love</td>
        <td align="center" valign="top" bgcolor="#CCCCCC" class="Arial_gray">Artist : Zuperstarr<br />
          Song : Rewind</td>
        <td align="center" valign="top" bgcolor="#CCCCCC" class="Arial_gray">Artist : Zuperstarr<br />
          Song : ดาวคะนอง</td>
        <td align="center" valign="top" bgcolor="#CCCCCC" class="Arial_gray">Artist : Zuperstarr<br />
          Song : ดาวคะนอง</td>
        </tr>
      <tr>
        <td align="center" valign="bottom" bgcolor="#EEEEEE" class="Arial_gray">View : 544 <br />
          Vote : 32</td>
        <td align="center" valign="bottom" bgcolor="#EEEEEE" class="Arial_gray">View : 544<br />
Vote : 32</td>
        <td align="center" valign="bottom" bgcolor="#EEEEEE" class="Arial_gray">View : 544<br />
          Vote : 32</td>
        <td align="center" valign="bottom" bgcolor="#EEEEEE" class="Arial_gray">View : 544<br />
          Vote : 32</td>
        </tr>
      <tr>
        <td height="40" align="center" valign="middle" bgcolor="#EEEEEE" class="Arial_gray">แก้ไข l ลบคลิป</td>
        <td align="center" valign="middle" bgcolor="#EEEEEE" class="Arial_gray">แก้ไข l ลบคลิป</td>
        <td align="center" valign="middle" bgcolor="#EEEEEE" class="Arial_gray">แก้ไข l ลบคลิป</td>
        <td align="center" valign="middle" bgcolor="#EEEEEE" class="Arial_gray">แก้ไข l ลบคลิป</td>
        </tr>
      <tr>
        <td align="left" valign="top" class="Arial_gray">&nbsp;</td>
        <td align="left" valign="top" class="Arial_gray">&nbsp;</td>
        <td align="left" valign="top" class="Arial_gray">&nbsp;</td>
        <td align="left" valign="top" class="Arial_gray">&nbsp;</td>
      </tr>
    </table>
	-->
	
	<!--
      <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <td height="40" align="left" valign="bottom" class="HeadBlue">Check Stat</td>
        </tr>
    </table>
      <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <td height="25" align="center" valign="middle" bgcolor="#CCCCCC" class="HeadBlue"><span class="Arial_gray"><strong>เพลงของคุณ</strong></span></td>
          <td width="15%" align="center" valign="middle" bgcolor="#CCCCCC" class="HeadBlue"><strong><span class="Arial_gray">วันที่อัพโหลด</span></strong></td>
          <td width="10%" align="center" valign="middle" bgcolor="#CCCCCC" class="HeadBlue"><strong><span class="Arial_gray">คะแนนโหวต</span></strong></td>
          <td width="10%" align="center" valign="middle" bgcolor="#CCCCCC" class="HeadBlue"><strong><span class="Arial_gray">จำนวนคนดู</span></strong></td>
          <td width="25%" align="center" valign="middle" bgcolor="#CCCCCC" class="HeadBlue"><strong><span class="Arial_gray">คนแต่งเพลง</span></strong></td>
        </tr>
        <tr>
          <td height="25" align="left" valign="middle" bgcolor="#eeeeee" class="HeadBlue"><span class="Arial_gray">1. กลับมารักกัน</span></td>
          <td height="25" align="center" valign="middle" bgcolor="#eeeeee" class="HeadBlue"><span class="Arial_gray">27 July 15 / 11.31</span></td>
          <td height="25" align="center" valign="middle" bgcolor="#eeeeee" class="HeadBlue"><span class="Arial_gray">32</span></td>
          <td height="25" align="center" valign="middle" bgcolor="#eeeeee" class="HeadBlue"><span class="Arial_gray">544</span></td>
          <td height="25" align="center" valign="middle" bgcolor="#eeeeee" class="HeadBlue"><span class="Arial_gray"><a href="#">แดน วัดดาว</a></span></td>
        </tr>
        <tr>
          <td height="25" align="left" valign="middle" bgcolor="#CCCCCC" class="HeadBlue"><span class="Arial_gray"> 2. Rewind</span></td>
          <td align="center" valign="middle" bgcolor="#CCCCCC" class="HeadBlue"><span class="Arial_gray">26 July 15 / 12.55</span></td>
          <td height="25" align="center" valign="middle" bgcolor="#CCCCCC" class="HeadBlue"><span class="Arial_gray">67</span></td>
          <td height="25" align="center" valign="middle" bgcolor="#CCCCCC" class="HeadBlue"><span class="Arial_gray">4674</span></td>
          <td height="25" align="center" valign="middle" bgcolor="#CCCCCC" class="HeadBlue"><span class="Arial_gray"><a href="#">แดน วัดดาว</a></span></td>
        </tr>
        <tr>
          <td height="25" align="left" valign="middle" bgcolor="#EEEEEE" class="HeadBlue"><span class="Arial_gray">3. ดาวคะนอง</span></td>
          <td align="center" valign="middle" bgcolor="#EEEEEE" class="HeadBlue"><span class="Arial_gray">25 July 15 / 14.34</span></td>
          <td height="25" align="center" valign="middle" bgcolor="#eeeeee" class="HeadBlue"><span class="Arial_gray">127</span></td>
          <td height="25" align="center" valign="middle" bgcolor="#eeeeee" class="HeadBlue"><span class="Arial_gray">3564</span></td>
          <td height="25" align="center" valign="middle" bgcolor="#eeeeee" class="HeadBlue"><span class="Arial_gray"><a href="#">แดน วัดดาว</a></span></td>
        </tr>
    </table>
	-->
	</td>
  </tr>
</table>



<!--/Data-->
<p>&nbsp;</p>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="100" align="center" valign="top" bgcolor="#1e1e1e"><table width="1100" border="0" align="center" cellpadding="1" cellspacing="1">
      <tr>
        <td width="714" align="left" valign="top" class="Arial_gray">
		
		<!--
		<table width="250" border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td colspan="2" align="left" valign="middle"><span class="blue"><img src="images/space.jpg" alt="" width="5" height="5" /></span></td>
          </tr>
          <tr>
            <td width="150" align="left" valign="middle">Visitor Counter</td>
            <td align="right" valign="middle" class="white">23</td>
          </tr>
          <tr>
            <td align="left" valign="middle">Online</td>
            <td align="right" valign="middle" class="white">45</td>
          </tr>
          <tr>
            <td align="left" valign="middle">Today</td>
            <td align="right" valign="middle" class="white">356</td>
          </tr>
          <tr>
            <td align="left" valign="middle">This Week</td>
            <td align="right" valign="middle" class="white">3,671</td>
          </tr>
          <tr>
            <td align="left" valign="middle">All</td>
            <td align="right" valign="middle" class="white">6,970</td>
          </tr>
        </table>
		-->
          <p><br />
          </p></td>
        <td width="379" align="right" valign="top" class="gray"><table width="98%" border="0" align="left" cellpadding="2" cellspacing="2">
          <tr>
            <td colspan="5" align="right" valign="top" class="blue"><img src="images/space.jpg" width="5" height="5" /></td>
          </tr>
          <tr>
            <td align="right" valign="top" class="blue">&nbsp;</td>
            <td width="45" align="right" valign="top" class="blue"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image14','','images/footer_fb2.jpg',1)"><img src="images/footer_fb.jpg" width="40" height="40" id="Image14" /></a></td>
            <td width="45" align="right" valign="top" class="blue"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image15','','images/footer_twitter2.jpg',1)"><img src="images/footer_twitter.jpg" width="40" height="40" id="Image15" /></a></td>
            <td width="45" align="right" valign="top" class="blue"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image16','','images/footer_youtube2.jpg',1)"><img src="images/footer_youtube.jpg" width="40" height="40" id="Image16" /></a></td>
            <td width="45" align="right" valign="top" class="blue"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image17','','images/footer_googleplus2.jpg',1)"><img src="images/footer_googleplus.jpg" width="40" height="40" id="Image17" /></a></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="left" valign="middle" class="black">&nbsp;</td>
        <td align="right" valign="top" class="gray"><table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td height="40" align="right" valign="top"><table width="100%" border="0" align="right" cellpadding="2" cellspacing="2">
              <tr>
                <td align="right" valign="top" class="blue"><span class="Arial_gray">Copyright © 2015 Cyberrymusic.com All Rights Reserved</span></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
