USE [cyberrymusicdb]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 12/3/2015 11:28:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](max) NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
	[IdentityUser_Id] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[IdentityUser_Id] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
	[IdentityUser_Id] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Balance]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Balance](
	[int_balance_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NULL,
	[balance] [decimal](18, 2) NULL,
	[net_balance] [decimal](18, 2) NULL,
	[datetime_create] [datetime] NULL,
	[datetime_modified] [datetime] NULL,
 CONSTRAINT [PK_Balance] PRIMARY KEY CLUSTERED 
(
	[int_balance_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Bank]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bank](
	[int_bank_id] [int] IDENTITY(1,1) NOT NULL,
	[txt_bank_name] [nvarchar](500) NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_BANK] PRIMARY KEY CLUSTERED 
(
	[int_bank_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Forget_Password]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Forget_Password](
	[int_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[txt_email] [nvarchar](256) NOT NULL,
	[txt_code] [nchar](16) NOT NULL,
	[datetime_create] [datetime] NOT NULL,
	[is_check] [bit] NOT NULL,
	[datetime_check] [datetime] NULL,
 CONSTRAINT [PK_Forget_Password] PRIMARY KEY CLUSTERED 
(
	[int_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Income_Transaction]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Income_Transaction](
	[int_income_transaction_id] [int] IDENTITY(1,1) NOT NULL,
	[int_vote_history_id] [int] NULL,
	[int_user_id] [nvarchar](128) NULL,
	[int_song_contest_id] [int] NULL,
	[int_song_original_id] [int] NULL,
	[int_vote_value] [int] NULL,
	[dec_amount] [decimal](18, 2) NULL,
	[dec_net_amount] [decimal](18, 2) NULL,
	[int_type] [int] NULL,
	[datetime_create] [datetime] NULL,
 CONSTRAINT [PK_Income_Transaction] PRIMARY KEY CLUSTERED 
(
	[int_income_transaction_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Login_Log]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Login_Log](
	[int_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[txt_ip] [nvarchar](50) NULL,
	[txt_username] [nvarchar](100) NOT NULL,
	[is_success] [bit] NOT NULL,
	[datetime_create] [datetime] NOT NULL,
 CONSTRAINT [PK_Login_Log] PRIMARY KEY CLUSTERED 
(
	[int_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Paysbuy]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Paysbuy](
	[int_paysbuy_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[inv] [nvarchar](200) NULL,
	[itm] [nvarchar](200) NULL,
	[amt] [nvarchar](10) NULL,
	[point] [int] NULL,
	[paypal_amt] [nvarchar](10) NULL,
	[curr_type] [int] NULL,
	[com] [nvarchar](200) NULL,
	[method] [nchar](2) NULL,
	[language] [nchar](10) NULL,
	[resp_front_url] [nvarchar](200) NULL,
	[resp_back_url] [nvarchar](200) NULL,
	[opt_fix_redirect] [nchar](1) NULL,
	[opt_fix_method] [nchar](1) NULL,
	[opt_name] [nvarchar](200) NULL,
	[opt_email] [nvarchar](200) NULL,
	[opt_mobile] [nvarchar](200) NULL,
	[opt_address] [nvarchar](500) NULL,
	[opt_detail] [nvarchar](500) NULL,
	[result] [nvarchar](200) NULL,
	[apCode] [nvarchar](200) NULL,
	[fee] [nvarchar](10) NULL,
	[confirm_cs] [nvarchar](10) NULL,
	[tax_inv_no] [nvarchar](200) NULL,
	[receipt_no] [nvarchar](200) NULL,
	[remark] [nvarchar](2000) NULL,
	[datetime_create] [datetime] NULL,
	[datetime_modified] [datetime] NULL,
 CONSTRAINT [PK_Paysbuy] PRIMARY KEY CLUSTERED 
(
	[int_paysbuy_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Paysbuy_log]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Paysbuy_log](
	[int_paysbuy_log_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[inv] [nvarchar](200) NULL,
	[itm] [nvarchar](200) NULL,
	[amt] [nvarchar](10) NULL,
	[point] [int] NULL,
	[paypal_amt] [nvarchar](10) NULL,
	[curr_type] [int] NULL,
	[com] [nvarchar](200) NULL,
	[method] [nchar](2) NULL,
	[language] [nchar](10) NULL,
	[resp_front_url] [nvarchar](200) NULL,
	[resp_back_url] [nvarchar](200) NULL,
	[opt_fix_redirect] [nchar](1) NULL,
	[opt_fix_method] [nchar](1) NULL,
	[opt_name] [nvarchar](200) NULL,
	[opt_email] [nvarchar](200) NULL,
	[opt_mobile] [nvarchar](200) NULL,
	[opt_address] [nvarchar](500) NULL,
	[opt_detail] [nvarchar](500) NULL,
	[result] [nvarchar](200) NULL,
	[apCode] [nvarchar](200) NULL,
	[fee] [nvarchar](10) NULL,
	[confirm_cs] [nvarchar](10) NULL,
	[txt_method] [nvarchar](200) NULL,
	[datetime_create] [datetime] NULL,
 CONSTRAINT [PK_Paysbuy_log] PRIMARY KEY CLUSTERED 
(
	[int_paysbuy_log_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PaysBuyConfig]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaysBuyConfig](
	[psb] [nchar](10) NOT NULL,
	[biz] [nvarchar](200) NULL,
	[secureCode] [nvarchar](200) NULL,
	[postUrl] [nvarchar](200) NULL,
	[reqUrl] [nvarchar](200) NULL,
	[paysbuyUrl] [nvarchar](200) NULL,
 CONSTRAINT [PK_PaysBuyConfig] PRIMARY KEY CLUSTERED 
(
	[psb] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Point]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Point](
	[int_point_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[int_value] [int] NOT NULL,
	[datetime_create] [datetime] NOT NULL,
	[datetime_modified] [datetime] NULL,
 CONSTRAINT [PK_Point] PRIMARY KEY CLUSTERED 
(
	[int_point_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Point_Transaction]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Point_Transaction](
	[int_transaction_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NULL,
	[int_target_user_id] [nvarchar](128) NULL,
	[int_value] [int] NULL,
	[txt_method] [nvarchar](200) NULL,
	[datetime_create] [datetime] NULL,
 CONSTRAINT [PK_Point_Transaction] PRIMARY KEY CLUSTERED 
(
	[int_transaction_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[int_product_id] [int] IDENTITY(1,1) NOT NULL,
	[txt_product_no] [nvarchar](50) NULL,
	[txt_product_name] [nvarchar](200) NULL,
	[txt_product_desc] [nvarchar](max) NULL,
	[txt_image] [nvarchar](500) NULL,
	[dec_price_exvat] [decimal](18, 2) NULL,
	[dec_price] [decimal](18, 2) NULL,
	[int_amount] [int] NULL,
	[datetime_create] [datetime] NOT NULL,
	[datetime_modified] [datetime] NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[int_product_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Score]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Score](
	[int_score_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[int_value] [int] NOT NULL,
	[datetime_create] [datetime] NOT NULL,
	[datetime_modified] [datetime] NULL,
 CONSTRAINT [PK_Score] PRIMARY KEY CLUSTERED 
(
	[int_score_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Score_Song]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Score_Song](
	[int_score_id] [int] IDENTITY(1,1) NOT NULL,
	[int_song_contest_id] [int] NULL,
	[int_value] [int] NULL,
	[datetime_create] [datetime] NULL,
	[datetime_modified] [datetime] NULL,
 CONSTRAINT [PK_Score_Song] PRIMARY KEY CLUSTERED 
(
	[int_score_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sms_Data]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sms_Data](
	[int_sms_id] [int] IDENTITY(1,1) NOT NULL,
	[txt_refer_id] [nvarchar](256) NULL,
	[txt_phone] [nvarchar](50) NULL,
	[int_vote_number] [int] NULL,
	[datetime_sent_sms] [datetime] NULL,
	[datetime_create] [datetime] NULL,
 CONSTRAINT [PK_Sms_Data] PRIMARY KEY CLUSTERED 
(
	[int_sms_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Song_Contest]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Song_Contest](
	[int_song_contest_id] [int] IDENTITY(1,1) NOT NULL,
	[int_song_original_id] [int] NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[txt_vote_code] [nvarchar](100) NULL,
	[txt_song_name] [nvarchar](200) NOT NULL,
	[txt_youtube_url] [nvarchar](200) NULL,
	[txt_soundcloud_url] [nvarchar](200) NULL,
	[txt_youtube_id] [nvarchar](100) NULL,
	[is_approved] [bit] NOT NULL,
	[date_approved] [date] NULL,
	[int_admin_approved_id] [nvarchar](128) NULL,
	[is_active] [bit] NOT NULL,
	[is_edittor_pick] [bit] NULL,
	[datetime_create] [datetime] NOT NULL,
	[datetime_modified] [datetime] NULL,
 CONSTRAINT [PK_Song_Contest] PRIMARY KEY CLUSTERED 
(
	[int_song_contest_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Song_Original]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Song_Original](
	[int_song_original_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[txt_songname] [nvarchar](200) NULL,
	[txt_youtube_url] [nvarchar](500) NULL,
	[txt_soundcloud_url] [nvarchar](500) NULL,
	[txt_lyric_file] [nvarchar](500) NOT NULL,
	[is_approved] [bit] NOT NULL,
	[date_approved] [date] NULL,
	[int_admin_approved] [nvarchar](128) NULL,
	[is_active] [bit] NOT NULL,
	[is_edittor_pick] [bit] NULL,
	[datetime_create] [datetime] NOT NULL,
	[datetime_modified] [datetime] NULL,
 CONSTRAINT [PK_Song_Original] PRIMARY KEY CLUSTERED 
(
	[int_song_original_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Subscribed]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subscribed](
	[int_subscribed_id] [int] IDENTITY(1,1) NOT NULL,
	[txt_name] [nvarchar](256) NULL,
	[txt_email] [nvarchar](256) NULL,
	[datetime_create] [datetime] NULL,
 CONSTRAINT [PK_Subscribed] PRIMARY KEY CLUSTERED 
(
	[int_subscribed_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[txt_firstname] [nvarchar](256) NULL,
	[txt_lastname] [nvarchar](256) NULL,
	[txt_username] [nvarchar](256) NULL,
	[txt_avatar] [nvarchar](500) NULL,
	[is_contest] [bit] NULL,
	[is_composer] [bit] NULL,
	[is_activated] [bit] NULL,
	[is_banned] [bit] NULL,
	[txt_banned_reason] [nvarchar](max) NULL,
	[int_bank_id] [int] NULL,
	[txt_bank_name] [nvarchar](256) NULL,
	[txt_bank_account] [nvarchar](256) NULL,
	[txt_phone] [nvarchar](256) NULL,
	[txt_id_card] [nvarchar](13) NULL,
	[txt_img_id_card_path] [nvarchar](max) NULL,
	[txt_new_password_key] [nvarchar](max) NULL,
	[txt_new_password_requested] [nvarchar](max) NULL,
	[txt_new_email] [nvarchar](256) NULL,
	[txt_new_email_key] [nvarchar](256) NULL,
	[date_birth] [datetime] NULL,
	[txt_address] [nvarchar](max) NULL,
	[txt_last_ip] [nvarchar](256) NULL,
	[datetime_lastlogin] [datetime] NULL,
	[datetime_create] [datetime] NULL,
	[datetime_modified] [datetime] NULL,
	[Discriminator] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Vote_History]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vote_History](
	[int_vote_history_id] [int] IDENTITY(1,1) NOT NULL,
	[int_song_contest_id] [int] NULL,
	[int_vote_method] [int] NULL,
	[int_vote_value] [int] NULL,
	[int_vote_by_user_id] [nvarchar](128) NULL,
	[is_calculated] [bit] NULL,
	[datetime_create] [datetime] NULL,
 CONSTRAINT [PK_Vote_History] PRIMARY KEY CLUSTERED 
(
	[int_vote_history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Withdrawal_Transaction]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Withdrawal_Transaction](
	[int_withdrawal_transaction_id] [int] IDENTITY(1,1) NOT NULL,
	[int_balance_id] [int] NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[amount] [decimal](18, 2) NOT NULL,
	[balance] [decimal](18, 2) NOT NULL,
	[vat] [decimal](18, 2) NOT NULL,
	[include_vat]  AS ([amount]-([amount]*[vat])/(100)),
	[transfer_amount] [decimal](18, 2) NULL,
	[txt_remark] [nvarchar](1000) NULL,
	[datetime_transfer_date] [datetime] NULL,
	[txt_payment_slip] [nvarchar](1000) NULL,
	[datetime_create] [datetime] NOT NULL,
 CONSTRAINT [PK_Withdrawal_Transaction] PRIMARY KEY CLUSTERED 
(
	[int_withdrawal_transaction_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[vUserSongContest]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vUserSongContest]
AS
SELECT        dbo.Users.Id, dbo.Users.Email, dbo.Users.EmailConfirmed, dbo.Users.PasswordHash, dbo.Users.SecurityStamp, dbo.Users.PhoneNumber, dbo.Users.PhoneNumberConfirmed, dbo.Users.TwoFactorEnabled, 
                         dbo.Users.LockoutEndDateUtc, dbo.Users.LockoutEnabled, dbo.Users.AccessFailedCount, dbo.Users.UserName, dbo.Users.txt_firstname, dbo.Users.txt_lastname, dbo.Users.txt_username, dbo.Users.txt_avatar,
                          dbo.Users.is_contest, dbo.Users.is_composer, dbo.Users.is_activated, dbo.Users.is_banned, dbo.Users.txt_banned_reason, dbo.Users.int_bank_id, dbo.Users.txt_bank_name, dbo.Users.txt_bank_account, 
                         dbo.Users.txt_phone, dbo.Users.txt_id_card, dbo.Users.txt_img_id_card_path, dbo.Users.txt_new_password_key, dbo.Users.txt_new_password_requested, dbo.Users.txt_new_email, 
                         dbo.Users.txt_new_email_key, dbo.Users.date_birth, dbo.Users.txt_address, dbo.Users.txt_last_ip, dbo.Users.datetime_lastlogin, dbo.Users.datetime_create, dbo.Users.datetime_modified, 
                         dbo.Users.Discriminator, dbo.Song_Contest.int_song_contest_id, dbo.Song_Contest.int_song_original_id, dbo.Song_Contest.txt_vote_code, dbo.Song_Contest.txt_song_name, dbo.Song_Contest.txt_youtube_url, 
                         dbo.Song_Contest.txt_youtube_id, dbo.Song_Contest.is_approved, dbo.Song_Contest.date_approved, dbo.Song_Contest.int_admin_approved_id, dbo.Song_Contest.is_active, dbo.Song_Contest.is_edittor_pick, 
                         dbo.Score_Song.int_score_id, dbo.Score_Song.int_value
FROM            dbo.Score_Song LEFT OUTER JOIN
                         dbo.Song_Contest ON dbo.Score_Song.int_song_contest_id = dbo.Song_Contest.int_song_contest_id RIGHT OUTER JOIN
                         dbo.Users ON dbo.Song_Contest.int_user_id = dbo.Users.Id

GO
/****** Object:  View [dbo].[vUserScoreSong]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vUserScoreSong]
AS
SELECT        Id, UserName, txt_firstname, txt_lastname, datetime_create, txt_avatar, SUM(int_value) AS score
FROM            dbo.vUserSongContest AS v
WHERE        (is_activated = 1) AND (is_banned = 0) AND (EmailConfirmed = 1) AND (is_active = 1 OR
                         is_active IS NULL) AND (is_approved = 1 OR
                         is_approved IS NULL) AND (is_contest = 1)
GROUP BY Id, UserName, txt_firstname, txt_lastname, datetime_create, txt_avatar

GO
/****** Object:  View [dbo].[vVoteIncomeTransaction]    Script Date: 12/3/2015 11:28:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vVoteIncomeTransaction]
AS
SELECT        dbo.Vote_History.int_vote_history_id, dbo.Vote_History.int_song_contest_id, dbo.Vote_History.int_vote_method, dbo.Vote_History.int_vote_value, dbo.Vote_History.int_vote_by_user_id, 
                         dbo.Vote_History.is_calculated, dbo.Income_Transaction.int_income_transaction_id, dbo.Income_Transaction.int_user_id, dbo.Income_Transaction.dec_amount, dbo.Income_Transaction.dec_net_amount, 
                         dbo.Income_Transaction.int_type, dbo.Income_Transaction.datetime_create
FROM            dbo.Vote_History INNER JOIN
                         dbo.Income_Transaction ON dbo.Vote_History.int_vote_history_id = dbo.Income_Transaction.int_vote_history_id

GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.Users_IdentityUser_Id] FOREIGN KEY([IdentityUser_Id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.Users_IdentityUser_Id]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.Users_IdentityUser_Id] FOREIGN KEY([IdentityUser_Id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.Users_IdentityUser_Id]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.Users_IdentityUser_Id] FOREIGN KEY([IdentityUser_Id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.Users_IdentityUser_Id]
GO
ALTER TABLE [dbo].[Balance]  WITH CHECK ADD  CONSTRAINT [FK_Balance_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Balance] CHECK CONSTRAINT [FK_Balance_Users]
GO
ALTER TABLE [dbo].[Forget_Password]  WITH CHECK ADD  CONSTRAINT [FK_Forget_Password_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Forget_Password] CHECK CONSTRAINT [FK_Forget_Password_Users]
GO
ALTER TABLE [dbo].[Income_Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Income_Transaction_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Income_Transaction] CHECK CONSTRAINT [FK_Income_Transaction_Users]
GO
ALTER TABLE [dbo].[Login_Log]  WITH CHECK ADD  CONSTRAINT [FK_Login_Log_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Login_Log] CHECK CONSTRAINT [FK_Login_Log_Users]
GO
ALTER TABLE [dbo].[Paysbuy]  WITH CHECK ADD  CONSTRAINT [FK_Paysbuy_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Paysbuy] CHECK CONSTRAINT [FK_Paysbuy_Users]
GO
ALTER TABLE [dbo].[Point]  WITH CHECK ADD  CONSTRAINT [FK_Point_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Point] CHECK CONSTRAINT [FK_Point_Users]
GO
ALTER TABLE [dbo].[Point_Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Point_Transaction_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Point_Transaction] CHECK CONSTRAINT [FK_Point_Transaction_Users]
GO
ALTER TABLE [dbo].[Score]  WITH CHECK ADD  CONSTRAINT [FK_Score_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Score] CHECK CONSTRAINT [FK_Score_Users]
GO
ALTER TABLE [dbo].[Score_Song]  WITH CHECK ADD  CONSTRAINT [FK_Score_Song_Song_Contest] FOREIGN KEY([int_song_contest_id])
REFERENCES [dbo].[Song_Contest] ([int_song_contest_id])
GO
ALTER TABLE [dbo].[Score_Song] CHECK CONSTRAINT [FK_Score_Song_Song_Contest]
GO
ALTER TABLE [dbo].[Song_Contest]  WITH CHECK ADD  CONSTRAINT [FK_Song_Contest_Song_Original] FOREIGN KEY([int_song_original_id])
REFERENCES [dbo].[Song_Original] ([int_song_original_id])
GO
ALTER TABLE [dbo].[Song_Contest] CHECK CONSTRAINT [FK_Song_Contest_Song_Original]
GO
ALTER TABLE [dbo].[Song_Contest]  WITH CHECK ADD  CONSTRAINT [FK_Song_Contest_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Song_Contest] CHECK CONSTRAINT [FK_Song_Contest_Users]
GO
ALTER TABLE [dbo].[Song_Original]  WITH CHECK ADD  CONSTRAINT [FK_Song_Original_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Song_Original] CHECK CONSTRAINT [FK_Song_Original_Users]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_BANK] FOREIGN KEY([int_bank_id])
REFERENCES [dbo].[Bank] ([int_bank_id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_BANK]
GO
ALTER TABLE [dbo].[Vote_History]  WITH CHECK ADD  CONSTRAINT [FK_Vote_History_Song_Contest] FOREIGN KEY([int_song_contest_id])
REFERENCES [dbo].[Song_Contest] ([int_song_contest_id])
GO
ALTER TABLE [dbo].[Vote_History] CHECK CONSTRAINT [FK_Vote_History_Song_Contest]
GO
ALTER TABLE [dbo].[Vote_History]  WITH CHECK ADD  CONSTRAINT [FK_Vote_History_Users] FOREIGN KEY([int_vote_by_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Vote_History] CHECK CONSTRAINT [FK_Vote_History_Users]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1=ผู้แต่ง,2=ผู้ร้อง' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Income_Transaction', @level2type=N'COLUMN',@level2name=N'int_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1=vote by sms,2=vote by point' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Vote_History', @level2type=N'COLUMN',@level2name=N'int_vote_method'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[19] 4[30] 2[19] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "v"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 295
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vUserScoreSong'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vUserScoreSong'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Users"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 211
               Right = 279
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Song_Contest"
            Begin Extent = 
               Top = 6
               Left = 317
               Bottom = 185
               Right = 529
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "Score_Song"
            Begin Extent = 
               Top = 6
               Left = 567
               Bottom = 211
               Right = 760
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vUserSongContest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vUserSongContest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Vote_History"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 207
               Right = 231
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Income_Transaction"
            Begin Extent = 
               Top = 0
               Left = 435
               Bottom = 263
               Right = 662
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vVoteIncomeTransaction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vVoteIncomeTransaction'
GO
