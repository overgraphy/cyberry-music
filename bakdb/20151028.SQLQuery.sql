USE [cyberrymusicdb]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](max) NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
	[IdentityUser_Id] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[IdentityUser_Id] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
	[IdentityUser_Id] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Balance]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Balance](
	[int_balance_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NULL,
	[balance] [decimal](18, 2) NULL,
	[datetime_create] [datetime] NULL,
	[datetime_modified] [datetime] NULL,
 CONSTRAINT [PK_Balance] PRIMARY KEY CLUSTERED 
(
	[int_balance_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Bank]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bank](
	[int_bank_id] [int] IDENTITY(1,1) NOT NULL,
	[txt_bank_name] [nvarchar](500) NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_BANK] PRIMARY KEY CLUSTERED 
(
	[int_bank_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Forget_Password]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Forget_Password](
	[int_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[txt_email] [nvarchar](256) NOT NULL,
	[txt_code] [nchar](16) NOT NULL,
	[datetime_create] [datetime] NOT NULL,
	[is_check] [bit] NOT NULL,
	[datetime_check] [datetime] NULL,
 CONSTRAINT [PK_Forget_Password] PRIMARY KEY CLUSTERED 
(
	[int_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Income_Transaction]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Income_Transaction](
	[int_income_transaction_id] [int] IDENTITY(1,1) NOT NULL,
	[int_vote_history_id] [int] NULL,
	[int_user_id] [nvarchar](128) NULL,
	[int_song_contest_id] [int] NULL,
	[int_song_original_id] [int] NULL,
	[int_vote_value] [int] NULL,
	[dec_amount] [decimal](18, 2) NULL,
	[int_type] [int] NULL,
	[datetime_create] [datetime] NULL,
 CONSTRAINT [PK_Income_Transaction] PRIMARY KEY CLUSTERED 
(
	[int_income_transaction_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Login_Log]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Login_Log](
	[int_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[txt_ip] [nvarchar](50) NULL,
	[txt_username] [nvarchar](100) NOT NULL,
	[is_success] [bit] NOT NULL,
	[datetime_create] [datetime] NOT NULL,
 CONSTRAINT [PK_Login_Log] PRIMARY KEY CLUSTERED 
(
	[int_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Paysbuy]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Paysbuy](
	[int_paysbuy_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[inv] [nvarchar](200) NULL,
	[itm] [nvarchar](200) NULL,
	[amt] [nvarchar](10) NULL,
	[point] [int] NULL,
	[paypal_amt] [nvarchar](10) NULL,
	[curr_type] [int] NULL,
	[com] [nvarchar](200) NULL,
	[method] [nchar](2) NULL,
	[language] [nchar](10) NULL,
	[resp_front_url] [nvarchar](200) NULL,
	[resp_back_url] [nvarchar](200) NULL,
	[opt_fix_redirect] [nchar](1) NULL,
	[opt_fix_method] [nchar](1) NULL,
	[opt_name] [nvarchar](200) NULL,
	[opt_email] [nvarchar](200) NULL,
	[opt_mobile] [nvarchar](200) NULL,
	[opt_address] [nvarchar](500) NULL,
	[opt_detail] [nvarchar](500) NULL,
	[result] [nvarchar](200) NULL,
	[apCode] [nvarchar](200) NULL,
	[fee] [nvarchar](10) NULL,
	[confirm_cs] [nvarchar](10) NULL,
	[datetime_create] [datetime] NULL,
	[datetime_modified] [datetime] NULL,
 CONSTRAINT [PK_Paysbuy] PRIMARY KEY CLUSTERED 
(
	[int_paysbuy_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Paysbuy_log]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Paysbuy_log](
	[int_paysbuy_log_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[inv] [nvarchar](200) NULL,
	[itm] [nvarchar](200) NULL,
	[amt] [nvarchar](10) NULL,
	[point] [int] NULL,
	[paypal_amt] [nvarchar](10) NULL,
	[curr_type] [int] NULL,
	[com] [nvarchar](200) NULL,
	[method] [nchar](2) NULL,
	[language] [nchar](10) NULL,
	[resp_front_url] [nvarchar](200) NULL,
	[resp_back_url] [nvarchar](200) NULL,
	[opt_fix_redirect] [nchar](1) NULL,
	[opt_fix_method] [nchar](1) NULL,
	[opt_name] [nvarchar](200) NULL,
	[opt_email] [nvarchar](200) NULL,
	[opt_mobile] [nvarchar](200) NULL,
	[opt_address] [nvarchar](500) NULL,
	[opt_detail] [nvarchar](500) NULL,
	[result] [nvarchar](200) NULL,
	[apCode] [nvarchar](200) NULL,
	[fee] [nvarchar](10) NULL,
	[confirm_cs] [nvarchar](10) NULL,
	[txt_method] [nvarchar](200) NULL,
	[datetime_create] [datetime] NULL,
 CONSTRAINT [PK_Paysbuy_log] PRIMARY KEY CLUSTERED 
(
	[int_paysbuy_log_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PaysBuyConfig]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaysBuyConfig](
	[psb] [nchar](10) NOT NULL,
	[biz] [nvarchar](200) NULL,
	[secureCode] [nvarchar](200) NULL,
	[postUrl] [nvarchar](200) NULL,
	[reqUrl] [nvarchar](200) NULL,
	[paysbuyUrl] [nvarchar](200) NULL,
 CONSTRAINT [PK_PaysBuyConfig] PRIMARY KEY CLUSTERED 
(
	[psb] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Point]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Point](
	[int_point_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[int_value] [int] NOT NULL,
	[datetime_create] [datetime] NOT NULL,
	[datetime_modified] [datetime] NULL,
 CONSTRAINT [PK_Point] PRIMARY KEY CLUSTERED 
(
	[int_point_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Point_Transaction]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Point_Transaction](
	[int_transaction_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NULL,
	[int_target_user_id] [nvarchar](128) NULL,
	[int_value] [int] NULL,
	[txt_method] [nvarchar](200) NULL,
	[datetime_create] [datetime] NULL,
 CONSTRAINT [PK_Point_Transaction] PRIMARY KEY CLUSTERED 
(
	[int_transaction_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[int_product_id] [int] IDENTITY(1,1) NOT NULL,
	[txt_product_no] [nvarchar](50) NULL,
	[txt_product_name] [nvarchar](200) NULL,
	[txt_product_desc] [nvarchar](max) NULL,
	[txt_image] [nvarchar](500) NULL,
	[dec_price_exvat] [decimal](18, 2) NULL,
	[dec_price] [decimal](18, 2) NULL,
	[int_amount] [int] NULL,
	[datetime_create] [datetime] NOT NULL,
	[datetime_modified] [datetime] NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[int_product_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Score]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Score](
	[int_score_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[int_value] [int] NOT NULL,
	[datetime_create] [datetime] NOT NULL,
	[datetime_modified] [datetime] NULL,
 CONSTRAINT [PK_Score] PRIMARY KEY CLUSTERED 
(
	[int_score_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Score_Song]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Score_Song](
	[int_score_id] [int] IDENTITY(1,1) NOT NULL,
	[int_song_contest_id] [int] NULL,
	[int_value] [int] NULL,
	[datetime_create] [datetime] NULL,
	[datetime_modified] [datetime] NULL,
 CONSTRAINT [PK_Score_Song] PRIMARY KEY CLUSTERED 
(
	[int_score_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sms_Data]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sms_Data](
	[int_sms_id] [int] IDENTITY(1,1) NOT NULL,
	[txt_refer_id] [nvarchar](256) NULL,
	[txt_phone] [nvarchar](50) NULL,
	[int_vote_number] [int] NULL,
	[datetime_sent_sms] [datetime] NULL,
	[datetime_create] [datetime] NULL,
 CONSTRAINT [PK_Sms_Data] PRIMARY KEY CLUSTERED 
(
	[int_sms_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Song_Contest]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Song_Contest](
	[int_song_contest_id] [int] IDENTITY(1,1) NOT NULL,
	[int_song_original_id] [int] NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[txt_vote_code] [nvarchar](100) NULL,
	[txt_song_name] [nvarchar](200) NOT NULL,
	[txt_youtube_url] [nvarchar](200) NULL,
	[txt_youtube_id] [nvarchar](100) NULL,
	[is_approved] [bit] NOT NULL,
	[date_approved] [date] NULL,
	[int_admin_approved_id] [nvarchar](128) NULL,
	[is_active] [bit] NOT NULL,
	[is_edittor_pick] [bit] NULL,
	[datetime_create] [datetime] NOT NULL,
	[datetime_modified] [datetime] NULL,
 CONSTRAINT [PK_Song_Contest] PRIMARY KEY CLUSTERED 
(
	[int_song_contest_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Song_Original]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Song_Original](
	[int_song_original_id] [int] IDENTITY(1,1) NOT NULL,
	[int_user_id] [nvarchar](128) NOT NULL,
	[txt_songname] [nvarchar](200) NULL,
	[txt_youtube_url] [nvarchar](500) NULL,
	[txt_soundcloud_url] [nvarchar](500) NULL,
	[txt_lyric_file] [nvarchar](500) NOT NULL,
	[is_approved] [bit] NOT NULL,
	[date_approved] [date] NULL,
	[int_admin_approved] [nvarchar](128) NULL,
	[is_active] [bit] NOT NULL,
	[is_edittor_pick] [bit] NULL,
	[datetime_create] [datetime] NOT NULL,
	[datetime_modified] [datetime] NULL,
 CONSTRAINT [PK_Song_Original] PRIMARY KEY CLUSTERED 
(
	[int_song_original_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Subscribed]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subscribed](
	[int_subscribed_id] [int] IDENTITY(1,1) NOT NULL,
	[txt_name] [nvarchar](256) NULL,
	[txt_email] [nvarchar](256) NULL,
	[datetime_create] [datetime] NULL,
 CONSTRAINT [PK_Subscribed] PRIMARY KEY CLUSTERED 
(
	[int_subscribed_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[txt_firstname] [nvarchar](256) NULL,
	[txt_lastname] [nvarchar](256) NULL,
	[txt_username] [nvarchar](256) NULL,
	[txt_avatar] [nvarchar](500) NULL,
	[is_contest] [bit] NULL,
	[is_composer] [bit] NULL,
	[is_activated] [bit] NULL,
	[is_banned] [bit] NULL,
	[txt_banned_reason] [nvarchar](max) NULL,
	[int_bank_id] [int] NULL,
	[txt_bank_name] [nvarchar](256) NULL,
	[txt_bank_account] [nvarchar](256) NULL,
	[txt_phone] [nvarchar](256) NULL,
	[txt_id_card] [nvarchar](13) NULL,
	[txt_img_id_card_path] [nvarchar](max) NULL,
	[txt_new_password_key] [nvarchar](max) NULL,
	[txt_new_password_requested] [nvarchar](max) NULL,
	[txt_new_email] [nvarchar](256) NULL,
	[txt_new_email_key] [nvarchar](256) NULL,
	[date_birth] [datetime] NULL,
	[txt_address] [nvarchar](max) NULL,
	[txt_last_ip] [nvarchar](256) NULL,
	[datetime_lastlogin] [datetime] NULL,
	[datetime_create] [datetime] NULL,
	[datetime_modified] [datetime] NULL,
	[Discriminator] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Vote_History]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vote_History](
	[int_vote_history_id] [int] IDENTITY(1,1) NOT NULL,
	[int_song_contest_id] [int] NULL,
	[int_vote_method] [int] NULL,
	[int_vote_value] [int] NULL,
	[int_vote_by_user_id] [nvarchar](128) NULL,
	[is_calculated] [bit] NULL,
	[datetime_create] [datetime] NULL,
 CONSTRAINT [PK_Vote_History] PRIMARY KEY CLUSTERED 
(
	[int_vote_history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[vUserSongContest]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vUserSongContest]
AS
SELECT        dbo.Users.Id, dbo.Users.Email, dbo.Users.EmailConfirmed, dbo.Users.PasswordHash, dbo.Users.SecurityStamp, dbo.Users.PhoneNumber, dbo.Users.PhoneNumberConfirmed, dbo.Users.TwoFactorEnabled, 
                         dbo.Users.LockoutEndDateUtc, dbo.Users.LockoutEnabled, dbo.Users.AccessFailedCount, dbo.Users.UserName, dbo.Users.txt_firstname, dbo.Users.txt_lastname, dbo.Users.txt_username, dbo.Users.txt_avatar,
                          dbo.Users.is_contest, dbo.Users.is_composer, dbo.Users.is_activated, dbo.Users.is_banned, dbo.Users.txt_banned_reason, dbo.Users.int_bank_id, dbo.Users.txt_bank_name, dbo.Users.txt_bank_account, 
                         dbo.Users.txt_phone, dbo.Users.txt_id_card, dbo.Users.txt_img_id_card_path, dbo.Users.txt_new_password_key, dbo.Users.txt_new_password_requested, dbo.Users.txt_new_email, 
                         dbo.Users.txt_new_email_key, dbo.Users.date_birth, dbo.Users.txt_address, dbo.Users.txt_last_ip, dbo.Users.datetime_lastlogin, dbo.Users.datetime_create, dbo.Users.datetime_modified, 
                         dbo.Users.Discriminator, dbo.Song_Contest.int_song_contest_id, dbo.Song_Contest.int_song_original_id, dbo.Song_Contest.txt_vote_code, dbo.Song_Contest.txt_song_name, dbo.Song_Contest.txt_youtube_url, 
                         dbo.Song_Contest.txt_youtube_id, dbo.Song_Contest.is_approved, dbo.Song_Contest.date_approved, dbo.Song_Contest.int_admin_approved_id, dbo.Song_Contest.is_active, dbo.Song_Contest.is_edittor_pick, 
                         dbo.Score_Song.int_score_id, dbo.Score_Song.int_value
FROM            dbo.Score_Song LEFT OUTER JOIN
                         dbo.Song_Contest ON dbo.Score_Song.int_song_contest_id = dbo.Song_Contest.int_song_contest_id RIGHT OUTER JOIN
                         dbo.Users ON dbo.Song_Contest.int_user_id = dbo.Users.Id

GO
/****** Object:  View [dbo].[vUserScoreSong]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vUserScoreSong]
AS
SELECT        Id, UserName, txt_firstname, txt_lastname, datetime_create, txt_avatar, SUM(int_value) AS score
FROM            dbo.vUserSongContest AS v
WHERE        (is_activated = 1) AND (is_banned = 0) AND (EmailConfirmed = 1) AND (is_active = 1 OR
                         is_active IS NULL) AND (is_approved = 1 OR
                         is_approved IS NULL) AND (is_contest = 1)
GROUP BY Id, UserName, txt_firstname, txt_lastname, datetime_create, txt_avatar

GO
/****** Object:  View [dbo].[vVoteIncomeTransaction]    Script Date: 10/28/2015 9:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vVoteIncomeTransaction]
AS
SELECT        dbo.Vote_History.int_song_contest_id, dbo.Vote_History.int_vote_history_id, dbo.Vote_History.int_vote_method, dbo.Vote_History.int_vote_value, dbo.Vote_History.int_vote_by_user_id, 
                         dbo.Vote_History.is_calculated, dbo.Income_Transaction.int_income_transaction_id, dbo.Income_Transaction.int_user_id, dbo.Income_Transaction.int_song_original_id, dbo.Income_Transaction.dec_amount, 
                         dbo.Income_Transaction.int_type, dbo.Income_Transaction.datetime_create
FROM            dbo.Vote_History RIGHT OUTER JOIN
                         dbo.Income_Transaction ON dbo.Vote_History.int_song_contest_id = dbo.Income_Transaction.int_song_contest_id

GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201509111042264_InitialCreate', N'WebCyberryMusic.Models.ApplicationDbContext', 0x1F8B0800000000000400DD5DDB6EE4B8117D0F907F10FA6937F076FB92194C8CF62EBCBE24C68E2F98F66CF2D6604BEC3631BAF44894C746902FCB433E29BF105212258A174994D89A76B0C0C02D92A78AC5228B64156BFFFBEFFFCC7F79097CE719C6098AC2B3C9D1F470E2C0D08D3C146ECE26295EFFF461F2CBCF7FFCC3FCCA0B5E9CDF59BD135A8FB40C93B3C913C6DBD3D92C719F6000926980DC384AA2359EBA5130035E343B3E3CFCCBECE8680609C4846039CEFC531A6214C0EC07F97911852EDCE214F8B79107FDA4F84E4A1619AA730702986C810BCF267F87AB8BD7158CE3D7DB3441EE346F3171CE7D0408370BE8AF270E08C308034C783DFD9CC0058EA370B3D8920FC07F7CDD42526F0DFC04167D38ADAA77EDCEE131EDCEAC6AC8A0DC34C1516008787452C8672636EF25E549293F22C12B2269FC4A7B9D49F16C72E3C1ECD3A7C8270210099E5EF831AD7C36B92D499C27DB3B88A7ACE13487BC8E09DCB728FE32E5110F9CCEED0E4A7D3A9E1ED2FF0E9C8BD4C7690CCF4298E218F807CE43BAF291FB1B7C7D8CBEC0F0ECE468B53EF9F0EE3DF04EDEFF199EBCE37B4AFA4AEAD53E904F0F71B48531E10DAECBFE4F9C59BDDD4C6C5836E3DAE45221BA44A6C6C4B9052F1F61B8C14F64D21C7F9838D7E8057AEC4BA15C9F434466126984E394FCBC4B7D1FAC7C5896CF1A69D27F1BA81EBF7B6F85EA1D78469B6CE805FA64E2C4645E7D827E569A3CA16D3EBD6AE3BD2CAA5DC751407FD7F52B2F5D2EA234766967226D9547106F20AE73379F55CADB49A529947DB566A8FBAFDA945359BD95556987FACC044662ECD9C0F8DD2DDDCE1A77BEDD92C1CB548B4AA449E1D4066B2A201C3842BD4A858EBAAA5048BA36717E05092CA4C44FD68CCD4611E317BC5CA338C161F3CA43FEEC24E9E601A5D47C3022B19408603462E0196010EF9C144A966E1462986046EAD7884C14101ACFAF0C29D84699320F86022E464402D0B380B522936A38101D941C69194390D09DD5AEC726CC287E59A292F99B109F1CF765FDCB7234EDCDA801D78DC8367D1482DBA7281CA76BC85BBA206E32241669051B466F490E1F4FA3100DE137422C49C836C45B7E81AFE3138DE1D794AC48701C2153D2E44884FC71A98D225A8F2CA1CB158A2BD5B9245F1E51D0CD06795E0C9364344BBE44DB5124426F0E32827EB441A1A9644A0497D8010C7B370F220FAD51A5E46A805E47980B1FA0C0FE192683DDFF43CCB0F3B9CEC472625CE028867F85218CE90EE501604C3686D508B46940EB09C88EA667A345698C43E977E0A7B649F552FE8FF9A4B6ADFC19ECFE2B7FC626F9FC8C3CBA11EF709067957F8392E61ADC11B44F3181B3B1CFFFB56E8E4D7C9C4B8F5ED3C5FE4CD9FF49F2566E70AF5AB6A55DAF703B50B988C2358A83E1E7E4876217FF3790ECFEBCB2806E1A13855A6010EC7EEFF8400F997769B06A5CBFACD3B236348FDFA26BE092CDCB55485B0DC6FB18B95FA2145F851EDDBB7EC6AEE95EB804B0C2CEB9EB92D3CA355166E85DF0370FFD6E4DE852F6BD5D28D9CEAAD98742D95CB27AB213852BD67A51F83A2A374A13879955EFC021ABA7E1302F6EE6B0A863CA2145EAC060514DC35F56DACC5E5EC5C40D759E24918B32AE14AED5C23156EF25996B4EBB974CBEBCCF9D5AB7C4B622EA35205F88D19A8896F23EBC843EC4D0397773D7F305485CE0C90A4B3AE41930C684A2F12AA898FB934493986F18D346801AAB846C0E5088655B8F42176D81DF2A25A165C77D02ED7B49432CB9845B185282AD92E8425CED60A30C947484416993D07CC6695C3745E45797B601572E351A3F9230DA87D3E99134E0CD145A74AAB8026954AABEF228D6B24EDC8A0B9B7D7908CBA28642712ADE893CF2A5B313B3C23A6A5F1AF555B8FF82A39545BE9493F985C9EC82B1ECCEBD5CD142F88215272B42BC385C25C5DE40EC14055F405CB75D95F9502EEB9264EA2062CF9B002BE91880B2DD47236A31210D60D996A111B6D06B03D83644098C530419910F61E12AEA035DC4D5BC93452FFB53AA8464153A19600E47A115A291AF77DC4028B53DA95E2A5AE3D2D9BC68FAD34D3E2A6BA201647DB12B21A6E02D1252999BCE0667B88404FBA201647DB12BA142315B04A4B03F5D2DD070F1D40D8EA509C64E0CA59929CBE6B33CA2B5F8309F69425FE7B760BB25E7552E14B6F8E22CF238D88B9F16E6C1A1418E317313458C68C96D49094731D840A19490269C5ED340A14B80C10AD013F1851748D5944655B3D03392BCDD9447902DFBAC36FD3B6FA109B2AA195A79D75EE05C935E0674DF9F7986143AA06EEED0F064E0835871D57811F96910EA8F21FAD6F99D05DF3EFF2223CC6702FFD249439298740CAC8BBFD3E0C813C3E240951B98FE83A587D0899C5DEFF342D78552EA51D8818F47D11D02F762F0AC0C9C1808396CDCC69A6442A8230F2414996156018D226455628658452D8A88558919220B4D14F1D8F7EE687CF4218FC67F3744631188121C2B30C2E3C2100540AEC4089105230A70ECB3D94808F188E28008C5067CF29187354EF902635E5900A2824F56D403B30C3354C296A566C8453CA108597C36C32A230645B4B2C0104F8A0A9480A51A6614E4104091825C6300052EDEAF910E57CF9C5A11E2A7225014F5C4D40B882BEE8ECD87ECF1A0FC77C3F59A85F1490B362B30B75159A89ECA44650566BD15C3F1C45E8BE53DD059A89E129A15F6C0AD62F894C855B1129B1C693C945DAA66C1538A47213CEA254ADC1805280464FFB38B1D61CBFEED26A17FDFAF7FE8B003FFF18D6CE3AEE445E1CA743110C3272430AEAC3B6A3D8C82C7AC9774471462257848A1C8804B3E22A2C6245FD00B4F2351758DEE14E418081E5D2EED8EAC8886E0A115C53DB0153C8B65DD511501133CB0A2B83B76153D211E8A77751B31FCDE60FF162DC942D41D15A39A070B1706DABBFE41C3967B96868D9D06633756C7CE751117DFCD03719F0DB18A086E09ACF8BE971AA5F58D0CD2A8DCA9384CA33418FA15BE161D5D5FE01B43BAF598B590E79A116D0AF9D6E399E9EDAEB5A3EE3CD1AB48E988345281B295F1AA411D45D272A17561CA62EBB8E07088EA28218E8D7E1C6A63957A2F898D4C55B6EE26A1019365B0A441BF4597DA30C5613E4D33C561AD8C1787B661A97B76F75171EAAEE28E1C0E59F9ECAE7BC357BD37A6E0855BDA4CBF8B46A6DEB336DDA9F9E5F751B96B8EFE1DE88E0DB7E01EE89F14BA205629F70565088310AA302FC206DA5379497104799589C326FAD964F19A60184C6985E9E2AB7FE123480FB2ACC22D08D11A26387F5B34393E3C3A163281ED4F56AE599278BE416AAEFAC08DF0302A7C06B1FB445D80E2D3A80199AB2A50299AF226F4E0CBD9E49F59ABD32C3887FE957D3E204AFE39445F5352F018A7D0F997FC72C3FE63BABE72D7DA93AE61E1A67997AC0D553DAD52F7C1BAF9C7326F7AE0DCC764229E3A87C210F5E1465ABD7AB0256074E5CFD2EBE5373C6B6BAF1595A8C2ACEBFF387185B09587898CCB1F02F0F2A3296BCAC7878310150F0C6DE15911A1EE01611F2CEDE341E63D33EDACFA31611FD6B40F0951680E263E23ECBE16B19676EC99CC99325DDB207D53A5641B0C5885295902ACA7561B0427A74F13F4AD33483D735A3F1429695A3F987ABEB41E18DA5469C3642DA74313E6A3517632AB2AA5CA773618B496D36C309A90B76C389E2637D960605DFE31BBC0528E312BF050B5051A8E684D0C723EB0BED656911FCC8AE9E072800DEEA93ACF57DF1E6BF27E0D8613F380F505141CC08336F703B28E8D7E7EC98C406B4AAE41097B0669A294876B385A2DD7D620B8B77F6056B852FE2F3365593BA63FC889B0AC61EFE892E94D68A9B56C1EFB92C0A37A0BFE7DF3768C99AAA3E1F958D7AB5867BF3374EC22EB42F1B67F402A8EA17AA209DFDBA99EE8E2CE54445BFD92636B8C410E935D68CC477E47DE2B59C95BD4185D5CD99BD098EE595E76A130DFDB107D0F7531307EE36B8BE0E567C713E965BF38B845C088CA61DD96E726F7EE9313F22A22AA90EFCFF204A3EAFC0A3A6295CA68095655F444F5891D9A08B7669F511154E6A468CFC9D32D254F730F35A94F9A6817CB7723EDA24E336D4D5291EF91904799DA439529A965056B8A986A89B53110422D82D566021E6D073A8BA1A6989A885ACB82D8459E9DE182A8CD124D84A86541D84FA7335C0C36A78541DA1C39C68CD842EE7F2A490C73823615048D9E0BA15BB382659D9B701D31AB2C70C4AA08371AB710038F98C8F318A335703129A63EE72CC16CF1D4E72A5841EF26BC4FF136C5A4CB3058F9B57B276AD49BE867B981EA3CCFEFB7590E541B5D206C227A577D1FFE9A22DF2BF9BE565C276B20E86EA1B8B4A56389E9E5EDE6B544BA931EDBEB800AF1959B9C47186C7D0296DC870BF00CFBF046D4EF23DC00F7B5BA88D381B40F445DECF34B043631089202A36A4F7E121DF682979FFF0749C95D2A5B750000, N'6.1.3-40302')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', N'Composor/Artist')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'5a69557d-0266-4fdb-871c-e07d5e1016f0', N'Admin')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', N'Member')
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId], [IdentityUser_Id]) VALUES (N'Facebook', N'10201162100931525', N'791a3952-4b3a-4320-af9e-97ec0cd24b9b', NULL)
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId], [IdentityUser_Id]) VALUES (N'Facebook', N'1069635356379882', N'5ef3dc15-82d6-4179-97ea-7b1dafabc1f7', NULL)
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId], [IdentityUser_Id]) VALUES (N'Facebook', N'1073861542624606', N'bd9247d1-aa76-4806-9122-456c1c6bd6c5', NULL)
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId], [IdentityUser_Id]) VALUES (N'Facebook', N'1506461729674691', N'0f886982-4f8c-4c75-8226-a5f0532af711', NULL)
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId], [IdentityUser_Id]) VALUES (N'Facebook', N'1513738382273723', N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', NULL)
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId], [IdentityUser_Id]) VALUES (N'Facebook', N'631388453671064', N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', NULL)
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId], [IdentityUser_Id]) VALUES (N'Facebook', N'688775524598287', N'22e2e456-5c07-461d-9147-be16885498c4', NULL)
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId], [IdentityUser_Id]) VALUES (N'Facebook', N'932458110168694', N'7dfb6b53-02d1-463a-9e60-60d8751c0662', NULL)
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId], [IdentityUser_Id]) VALUES (N'Facebook', N'962870953778550', N'7bbe84c8-cb39-4d6a-bb6c-80641181c4b4', NULL)
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId], [IdentityUser_Id]) VALUES (N'Facebook', N'966891106690634', N'da6f13e9-449e-47ec-bbd9-deec4bdd82e8', NULL)
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId], [IdentityUser_Id]) VALUES (N'Facebook', N'989628937764932', N'7bc4f6f3-4411-4236-b7e3-39390983fc3d', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'01f96c09-09ca-47d5-9f76-a87acea70f0c', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'02638332-a6a3-42c3-84b9-4d49ec910a5d', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'028dc1c0-9c9a-456f-aa6a-601229058f0f', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'0b1c73bb-2483-4d86-b5c3-f7be71e3edb5', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'0f886982-4f8c-4c75-8226-a5f0532af711', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'11bd01f2-5ac4-4042-af70-6148a5903bfd', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'168334b8-79d0-4470-a682-33fb1e93a308', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'1817ae75-a184-42be-8261-90ce17afe46d', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'192c991e-8547-4d93-8b23-1a015a62d1cf', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'1930dee3-f415-4374-b428-e4f56a493861', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'19b9e67d-5c7f-4265-b824-2af33fd84965', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'1d2ce3cf-c4b3-4dd0-aba8-36d15a2d718d', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'20dfc455-1d84-46e9-9035-0e265f5a011a', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'22e2e456-5c07-461d-9147-be16885498c4', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'23d7157e-b307-4759-94a7-2b2cce185ff0', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'25afa257-39d7-4213-b1ca-2b713e7f873f', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'2b614352-f430-45a9-bb44-4c2d4f15ac1c', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'2cb6bfde-71d8-416a-b3af-63e1a0bcc685', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'2db8baf8-ba73-48e5-8991-a3adf77003ac', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'300f194f-91d3-4379-ac99-700e58acdeb7', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'30fbd33b-f19c-42f1-a544-c275fe2076bb', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'37314781-e88a-4e08-b2b9-d7e2f918ac31', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'38dceb4e-8dfc-4e64-a590-c19e5eb1f643', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'3a911a81-e7f6-4bd2-811c-5ee7d539ca04', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'3c588550-1c7c-40c7-923a-1cd8d912d980', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'3e2d1f5a-8192-4dc8-bc0f-3d4b008ac02f', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'407c67b2-b470-4394-916a-aec7f70c940d', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'40836442-808f-42d3-865c-7f8f3584675c', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'44e0e865-eb8e-4a39-bb10-75e38b3659d5', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'4d82462f-eec1-49f5-b40b-9574f7b71237', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'52e7d8e8-6b76-498b-924c-c4d696cdb1be', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'5c7c8473-93ee-420d-a7eb-7cc917ed19bf', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'5d37e0a8-3dde-4e26-ad65-d30604ad60f6', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'5ef3dc15-82d6-4179-97ea-7b1dafabc1f7', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'645a5fb5-e893-4a8e-b0fb-6b9223a60ac4', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'668311d9-f7f3-4bf7-aa7c-4e20a928c4ee', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'66dbb691-4e42-4417-8283-a2c6571fea7e', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'67d9cbd7-2116-454f-9a2a-bc0da6332380', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'6980bdbb-5fb4-4240-a65e-0cbad3d1d339', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'6cdfda6a-148f-48f8-9390-778e4cdb6b18', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'6fd98021-a860-451e-ac9b-2cc3877fefbd', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'71ebf717-7aa7-459e-b803-2dbc75a2f864', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'73de51f1-2199-4fe8-8078-8671c397532b', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'791a3952-4b3a-4320-af9e-97ec0cd24b9b', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'7bbe84c8-cb39-4d6a-bb6c-80641181c4b4', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'7bc4f6f3-4411-4236-b7e3-39390983fc3d', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'7bf764b6-2b0e-49ff-9491-2f82cd1a3f9e', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'7ceb217c-e8bb-497e-adcb-107404a3251f', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'7dfb6b53-02d1-463a-9e60-60d8751c0662', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'80766062-cbc8-4846-94f3-ca36dbacbfb9', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'840d684c-4377-414b-92c4-2b0e1276e02e', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'84c59159-e8d3-4ca6-9d86-c7129502caeb', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'875d9abb-b8bd-42ab-8fbe-a222b53b0608', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'89dec678-e0b1-491e-ac89-250725b5c517', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'8ef4f4ec-43ef-476d-892e-39b63a2866d1', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'905a2c22-51d5-4521-9a1c-f56eca97f497', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'952aa50c-9b28-49da-8020-a361a5e856a5', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'99a35506-b6d9-45e8-90e5-51e15602af94', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'9a516f64-7619-4df0-a7a4-d9ebaeeea27a', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'9a67901b-8c64-4f79-88bc-111f5624e0c5', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'9fa60428-ef64-43c1-aebf-a7be62600488', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'a0b6d49e-3b2c-4fe1-adf0-ce8d097af65c', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'a20ad65b-ba1f-48bc-823f-94ff61831e6f', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'a7d089ec-0816-42ee-9da9-38dca1242ca6', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'aa254860-5318-4132-89e4-b958ef5b184b', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'aa41753e-1465-4a27-8a3d-20c1652178d4', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'aadb27d1-82f0-4879-b48e-cc623acdd240', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'acbc0310-bd04-4280-8bb4-ff1af6b9beaa', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'ae7c4a58-aca7-4d38-9876-e6d592a067b0', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'b54fb0d9-c305-4568-bad9-a0248cff5309', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'b882b15e-bfd2-4e5e-a027-b9d7c0822517', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'bab38b84-7e1c-44de-b819-def7e8a3b067', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'bd9247d1-aa76-4806-9122-456c1c6bd6c5', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'be44bef2-5552-40d1-baf3-0122ad8c121d', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'bfab75fa-4cf1-43e6-a4a4-9ebab9b4fab7', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'bfebc6ac-68c6-4032-a382-d2b1bf13b788', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'c296069d-2699-4336-ba6d-739ee158904a', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'c485ae2e-ab5f-4ca1-a3f9-992ea9cdd079', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'c6ac2778-8630-4dd1-88ef-59f61033e4bb', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'd3022e10-2bcf-41fc-8068-a6daff456957', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'da6f13e9-449e-47ec-bbd9-deec4bdd82e8', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'e0bb7763-01ac-4617-bc84-2c3abe678f04', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'e10af38e-0641-45a9-974e-c94693e509d5', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'e29993f4-62aa-4bf1-86b4-83cbf3b774a0', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'e89f5791-3ab4-4f6e-acac-1d732fdf0672', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'ec12150d-d863-40fc-890b-14c147e54c91', N'e99b6507-de55-47f7-a3ba-d5b0b1b19712', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'ecc35b51-ccaa-45ca-ab0b-3fee802d830f', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'ed0eb0fa-bd26-4483-b9a4-ad0d22d6c448', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'eee3ef8c-b347-4456-ab03-0dca2581d6a9', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'efaaf60a-80a3-481e-aa6d-1a3b5ec1959a', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'f01815e6-883a-44bb-82d7-66ab9bce1863', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'f1c62d2f-85d3-49a3-88f6-5ba4c0d520bb', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'f32f8c0b-e069-4f80-9c2e-24785747c607', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'f471733f-ac65-401a-a453-502eeb64d204', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'f76d565f-e63b-45bb-84a6-a25947064495', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'f8dd0bfa-0f82-4a5f-b814-695165160fa5', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'fb77b5b8-a47c-46f5-9e71-ac5dfb970df1', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [IdentityUser_Id]) VALUES (N'fc0755e3-517f-451a-85df-d3c68fd89381', N'2fa37945-cc79-4b89-a77a-8499cb9e47ac', NULL)
GO
SET IDENTITY_INSERT [dbo].[Balance] ON 

GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (1, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', CAST(19.50 AS Decimal(18, 2)), CAST(0x0000A53801148B43 AS DateTime), CAST(0x0000A53E00C58BEF AS DateTime))
GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (2, N'9a67901b-8c64-4f79-88bc-111f5624e0c5', CAST(2.00 AS Decimal(18, 2)), CAST(0x0000A53A0173ACAB AS DateTime), CAST(0x0000A53A0173ACF6 AS DateTime))
GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (3, N'89dec678-e0b1-491e-ac89-250725b5c517', CAST(10.25 AS Decimal(18, 2)), CAST(0x0000A53A01883294 AS DateTime), CAST(0x0000A53B011FD059 AS DateTime))
GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (4, N'b01eba5d-03b8-4e16-8984-95cd02efe904', CAST(64.50 AS Decimal(18, 2)), CAST(0x0000A53B009CF94D AS DateTime), CAST(0x0000A53D00C44773 AS DateTime))
GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (5, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', CAST(52.50 AS Decimal(18, 2)), CAST(0x0000A53B00DAC47F AS DateTime), CAST(0x0000A53D00DD451D AS DateTime))
GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (6, N'875d9abb-b8bd-42ab-8fbe-a222b53b0608', CAST(10.00 AS Decimal(18, 2)), CAST(0x0000A53B00E69C38 AS DateTime), CAST(0x0000A53B00E69C3C AS DateTime))
GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (7, N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', CAST(1.75 AS Decimal(18, 2)), CAST(0x0000A53B011FD054 AS DateTime), CAST(0x0000A53E011E3BDA AS DateTime))
GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (8, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', CAST(48.00 AS Decimal(18, 2)), CAST(0x0000A53B018B0D5B AS DateTime), CAST(0x0000A53E01850C51 AS DateTime))
GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (9, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', CAST(56.00 AS Decimal(18, 2)), CAST(0x0000A53C00B0A947 AS DateTime), CAST(0x0000A53E00C58BEA AS DateTime))
GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (10, N'aa41753e-1465-4a27-8a3d-20c1652178d4', CAST(9.50 AS Decimal(18, 2)), CAST(0x0000A53C01227D55 AS DateTime), CAST(0x0000A53E009EFE4F AS DateTime))
GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (11, N'c6ac2778-8630-4dd1-88ef-59f61033e4bb', CAST(2.00 AS Decimal(18, 2)), CAST(0x0000A53C01763325 AS DateTime), CAST(0x0000A53C01763325 AS DateTime))
GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (12, N'30fbd33b-f19c-42f1-a544-c275fe2076bb', CAST(124.50 AS Decimal(18, 2)), CAST(0x0000A53D00FD811C AS DateTime), CAST(0x0000A53E0169FF5C AS DateTime))
GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (13, N'f01815e6-883a-44bb-82d7-66ab9bce1863', CAST(8.00 AS Decimal(18, 2)), CAST(0x0000A53D0101EA32 AS DateTime), CAST(0x0000A53D0108127C AS DateTime))
GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (14, N'f32f8c0b-e069-4f80-9c2e-24785747c607', CAST(4.00 AS Decimal(18, 2)), CAST(0x0000A53D017822D0 AS DateTime), CAST(0x0000A53D01782316 AS DateTime))
GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (15, N'71ebf717-7aa7-459e-b803-2dbc75a2f864', CAST(81.00 AS Decimal(18, 2)), CAST(0x0000A53E009EFE53 AS DateTime), CAST(0x0000A53E012F1615 AS DateTime))
GO
INSERT [dbo].[Balance] ([int_balance_id], [int_user_id], [balance], [datetime_create], [datetime_modified]) VALUES (16, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', CAST(102.00 AS Decimal(18, 2)), CAST(0x0000A53E00B59F19 AS DateTime), CAST(0x0000A53E00D33698 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Balance] OFF
GO
SET IDENTITY_INSERT [dbo].[Bank] ON 

GO
INSERT [dbo].[Bank] ([int_bank_id], [txt_bank_name], [is_active]) VALUES (1, N'ธนาคารไทยพาณิชย์', 1)
GO
INSERT [dbo].[Bank] ([int_bank_id], [txt_bank_name], [is_active]) VALUES (2, N'ธนาคารกสิกรไทย', 1)
GO
INSERT [dbo].[Bank] ([int_bank_id], [txt_bank_name], [is_active]) VALUES (3, N'ธนาคารทหารไทย', 1)
GO
SET IDENTITY_INSERT [dbo].[Bank] OFF
GO
SET IDENTITY_INSERT [dbo].[Income_Transaction] ON 

GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (1, 1, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(1.00 AS Decimal(18, 2)), 1, CAST(0x0000A53801148B48 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (2, 1, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(1.00 AS Decimal(18, 2)), 2, CAST(0x0000A53801148B86 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (3, 2, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 2, CAST(2.00 AS Decimal(18, 2)), 1, CAST(0x0000A53900A4C444 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (4, 2, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 2, CAST(2.00 AS Decimal(18, 2)), 2, CAST(0x0000A53900A4C460 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (5, 3, N'9a67901b-8c64-4f79-88bc-111f5624e0c5', 4, 7, 1, CAST(1.00 AS Decimal(18, 2)), 1, CAST(0x0000A53A0173ACAB AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (6, 3, N'9a67901b-8c64-4f79-88bc-111f5624e0c5', 4, 7, 1, CAST(1.00 AS Decimal(18, 2)), 2, CAST(0x0000A53A0173ACF6 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (7, 4, N'89dec678-e0b1-491e-ac89-250725b5c517', 6, 2, 4, CAST(4.00 AS Decimal(18, 2)), 1, CAST(0x0000A53A01883294 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (8, 4, N'89dec678-e0b1-491e-ac89-250725b5c517', 6, 2, 4, CAST(4.00 AS Decimal(18, 2)), 2, CAST(0x0000A53A01883299 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (9, 5, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(1.00 AS Decimal(18, 2)), 1, CAST(0x0000A53B009CF952 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (10, 5, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(1.00 AS Decimal(18, 2)), 2, CAST(0x0000A53B009CF9A7 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (11, 6, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 10, 18, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B009EB6A7 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (12, 6, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 10, 18, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B009EB6A7 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (13, 7, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 10, 18, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B009EB6A7 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (14, 7, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 10, 18, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B009EB6BA AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (15, 8, N'89dec678-e0b1-491e-ac89-250725b5c517', 6, 2, 1, CAST(1.00 AS Decimal(18, 2)), 1, CAST(0x0000A53B00A88745 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (16, 8, N'89dec678-e0b1-491e-ac89-250725b5c517', 6, 2, 1, CAST(1.00 AS Decimal(18, 2)), 2, CAST(0x0000A53B00A8874A AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (17, 9, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00AF840C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (18, 9, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00AF840C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (19, 10, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00AF840C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (20, 10, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00AF840C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (21, 11, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00AF8411 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (22, 11, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00AF8411 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (23, 12, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00AF8411 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (24, 12, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00AF8416 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (25, 13, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00AF8416 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (26, 13, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00AF8416 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (27, 14, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00AF8416 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (28, 14, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00AF8416 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (29, 15, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00DAC484 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (30, 15, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00DAC484 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (31, 16, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 5, CAST(5.00 AS Decimal(18, 2)), 1, CAST(0x0000A53B00DF06CD AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (32, 16, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 5, CAST(5.00 AS Decimal(18, 2)), 2, CAST(0x0000A53B00DF06CD AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (33, 17, N'875d9abb-b8bd-42ab-8fbe-a222b53b0608', 13, 20, 5, CAST(5.00 AS Decimal(18, 2)), 1, CAST(0x0000A53B00E69C38 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (34, 17, N'875d9abb-b8bd-42ab-8fbe-a222b53b0608', 13, 20, 5, CAST(5.00 AS Decimal(18, 2)), 2, CAST(0x0000A53B00E69C3C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (35, 18, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 2, CAST(2.00 AS Decimal(18, 2)), 1, CAST(0x0000A53B00EBA7E7 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (36, 18, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 2, CAST(2.00 AS Decimal(18, 2)), 2, CAST(0x0000A53B00EBA7E7 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (37, 19, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00FCFD0D AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (38, 19, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00FCFD0D AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (39, 20, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00FCFD0D AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (40, 20, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00FCFD12 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (41, 21, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00FCFD12 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (42, 21, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00FCFD12 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (43, 22, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00FCFD12 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (44, 22, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00FCFD12 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (45, 23, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00FCFD12 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (46, 23, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00FCFD16 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (47, 24, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00FCFD16 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (48, 24, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00FCFD16 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (49, 25, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00FCFD16 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (50, 25, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00FCFD1B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (51, 26, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00FCFD1B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (52, 26, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00FCFD1B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (53, 27, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00FCFD1B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (54, 27, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00FCFD20 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (55, 28, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00FCFD20 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (56, 28, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00FCFD20 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (57, 29, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00FCFD20 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (58, 29, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00FCFD20 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (59, 30, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B00FCFD24 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (60, 30, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B00FCFD24 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (61, 31, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B01051E90 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (62, 31, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B01051E90 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (63, 32, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B0108E0F3 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (64, 32, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B0108E0F3 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (65, 33, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B010C77F9 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (66, 33, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B010C77F9 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (67, 34, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B011C98EB AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (68, 34, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B011C98F0 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (69, 35, N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', 5, 2, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B011FD054 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (70, 35, N'89dec678-e0b1-491e-ac89-250725b5c517', 5, 2, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B011FD059 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (71, 36, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B013A0EF1 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (72, 36, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B013A0EF1 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (73, 37, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B0162460D AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (74, 37, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B01624613 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (75, 38, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B0164D1DA AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (76, 38, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B0164D1DA AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (77, 39, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B0164D1DA AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (78, 39, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B0164D1DA AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (79, 40, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B0164D1DA AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (80, 40, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B0164D1DF AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (81, 41, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B0164D1DF AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (82, 41, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B0164D1DF AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (83, 42, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B0164D1DF AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (84, 42, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B0164D1DF AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (85, 43, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B0164D1DF AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (86, 43, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B0164D1DF AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (87, 44, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B0164D1DF AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (88, 44, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B0164D1E3 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (89, 45, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B0164D1E3 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (90, 45, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B0164D1E3 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (91, 46, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B0164D1E3 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (92, 46, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B0164D1E3 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (93, 47, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B0164D1E8 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (94, 47, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B0164D1E8 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (95, 48, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B0165C39B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (96, 48, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B0165C39B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (97, 49, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B0165C39B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (98, 49, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B0165C3A0 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (99, 50, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B0165C3A0 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (100, 50, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B0165C3A0 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (101, 51, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 36, 45, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53B018B0D5B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (102, 51, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 36, 45, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53B018B0D5B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (103, 52, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C009AE09F AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (104, 52, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C009AE0EA AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (105, 53, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 36, 45, 20, CAST(20.00 AS Decimal(18, 2)), 1, CAST(0x0000A53C00B0A900 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (106, 53, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 36, 45, 20, CAST(20.00 AS Decimal(18, 2)), 2, CAST(0x0000A53C00B0A947 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (107, 54, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 30, 36, 6, CAST(6.00 AS Decimal(18, 2)), 1, CAST(0x0000A53C00B0A947 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (108, 54, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 30, 36, 6, CAST(6.00 AS Decimal(18, 2)), 2, CAST(0x0000A53C00B0A95E AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (109, 55, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 28, 34, 10, CAST(10.00 AS Decimal(18, 2)), 1, CAST(0x0000A53C00B0A95E AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (110, 55, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 28, 34, 10, CAST(10.00 AS Decimal(18, 2)), 2, CAST(0x0000A53C00B0A95E AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (111, 56, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C00B0A963 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (112, 56, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C00B0A963 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (113, 57, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C00B0A963 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (114, 57, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C00B0A963 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (115, 58, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C00B0A963 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (116, 58, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C00B0A967 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (117, 59, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C00B0A967 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (118, 59, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C00B0A967 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (119, 60, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C00B0A967 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (120, 60, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C00B0A967 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (121, 61, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C00B0A967 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (122, 61, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C00B0A96C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (123, 62, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C00B0A96C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (124, 62, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C00B0A96C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (125, 63, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C00B0A96C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (126, 63, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C00B0A96C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (127, 64, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C00B0A96C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (128, 64, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C00B0A971 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (129, 65, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C00BF412B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (130, 65, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C00BF412B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (131, 66, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 28, 34, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C00BF412B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (132, 66, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 28, 34, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C00BF412F AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (133, 67, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 28, 34, 1, CAST(1.00 AS Decimal(18, 2)), 1, CAST(0x0000A53C00BF412F AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (134, 67, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 28, 34, 1, CAST(1.00 AS Decimal(18, 2)), 2, CAST(0x0000A53C00BF412F AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (135, 68, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C010374B2 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (136, 68, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C010374B7 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (137, 69, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C01227D55 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (138, 69, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C01227D59 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (139, 70, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 2, CAST(2.00 AS Decimal(18, 2)), 1, CAST(0x0000A53C0122EAA5 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (140, 70, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 2, CAST(2.00 AS Decimal(18, 2)), 2, CAST(0x0000A53C0122EAA5 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (141, 71, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 8, CAST(8.00 AS Decimal(18, 2)), 1, CAST(0x0000A53C0122EAA5 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (142, 71, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 8, CAST(8.00 AS Decimal(18, 2)), 2, CAST(0x0000A53C0122EAA9 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (143, 72, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C0129979C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (144, 72, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 12, 17, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C012997A0 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (145, 73, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C012E5E30 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (146, 73, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C012E5E30 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (147, 74, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 38, 50, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C012E5E30 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (148, 74, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 38, 50, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C012E5E35 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (149, 75, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 38, 50, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C0159C402 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (150, 75, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 38, 50, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C0159C407 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (151, 76, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53C017270D0 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (152, 76, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53C017270D5 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (153, 77, N'c6ac2778-8630-4dd1-88ef-59f61033e4bb', 39, 14, 1, CAST(1.00 AS Decimal(18, 2)), 1, CAST(0x0000A53C01763325 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (154, 77, N'c6ac2778-8630-4dd1-88ef-59f61033e4bb', 39, 14, 1, CAST(1.00 AS Decimal(18, 2)), 2, CAST(0x0000A53C01763325 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (155, 78, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D00451B69 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (156, 78, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D00451CAC AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (157, 79, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D00451CAC AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (158, 79, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D00451CB1 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (159, 80, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D00A195DD AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (160, 80, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D00A195E2 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (161, 81, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 41, 55, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D00A9AE00 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (162, 81, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 41, 55, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D00A9AE04 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (163, 82, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D00AFCC08 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (164, 82, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D00AFCC08 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (165, 83, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 21, CAST(21.00 AS Decimal(18, 2)), 1, CAST(0x0000A53D00C4476E AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (166, 83, N'b01eba5d-03b8-4e16-8984-95cd02efe904', 7, 16, 21, CAST(21.00 AS Decimal(18, 2)), 2, CAST(0x0000A53D00C44773 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (167, 84, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 10, CAST(10.00 AS Decimal(18, 2)), 1, CAST(0x0000A53D00DD4518 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (168, 84, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 26, 32, 10, CAST(10.00 AS Decimal(18, 2)), 2, CAST(0x0000A53D00DD451D AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (169, 85, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D00DD451D AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (170, 85, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D00DD4527 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (171, 86, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D00DD452B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (172, 86, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D00DD452B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (173, 87, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D00DD452B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (174, 87, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D00DD452B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (175, 88, N'30fbd33b-f19c-42f1-a544-c275fe2076bb', 45, 59, 2, CAST(2.00 AS Decimal(18, 2)), 1, CAST(0x0000A53D00FD811C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (176, 88, N'30fbd33b-f19c-42f1-a544-c275fe2076bb', 45, 59, 2, CAST(2.00 AS Decimal(18, 2)), 2, CAST(0x0000A53D00FD8121 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (177, 89, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D00FD8121 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (178, 89, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D00FD8121 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (179, 90, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D00FD8121 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (180, 90, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D00FD8121 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (181, 91, N'f01815e6-883a-44bb-82d7-66ab9bce1863', 46, 58, 2, CAST(2.00 AS Decimal(18, 2)), 1, CAST(0x0000A53D0101EA32 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (182, 91, N'f01815e6-883a-44bb-82d7-66ab9bce1863', 46, 58, 2, CAST(2.00 AS Decimal(18, 2)), 2, CAST(0x0000A53D0101EA32 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (183, 92, N'30fbd33b-f19c-42f1-a544-c275fe2076bb', 45, 59, 10, CAST(10.00 AS Decimal(18, 2)), 1, CAST(0x0000A53D0101EA32 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (184, 92, N'30fbd33b-f19c-42f1-a544-c275fe2076bb', 45, 59, 10, CAST(10.00 AS Decimal(18, 2)), 2, CAST(0x0000A53D0101EA32 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (185, 93, N'f01815e6-883a-44bb-82d7-66ab9bce1863', 46, 58, 2, CAST(2.00 AS Decimal(18, 2)), 1, CAST(0x0000A53D01081256 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (186, 93, N'f01815e6-883a-44bb-82d7-66ab9bce1863', 46, 58, 2, CAST(2.00 AS Decimal(18, 2)), 2, CAST(0x0000A53D0108127C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (187, 94, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D01110235 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (188, 94, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D01110235 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (189, 95, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D0115E986 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (190, 95, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D0115E986 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (191, 96, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D01193704 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (192, 96, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D01193704 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (193, 97, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D011A4547 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (194, 97, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D011A4547 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (195, 98, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D0133E101 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (196, 98, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D0133E105 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (197, 99, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 36, 45, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D0133E105 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (198, 99, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 36, 45, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D0133E105 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (199, 100, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53D0156CA1B AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (200, 100, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53D0156CA20 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (201, 101, N'30fbd33b-f19c-42f1-a544-c275fe2076bb', 45, 59, 50, CAST(50.00 AS Decimal(18, 2)), 1, CAST(0x0000A53D01674F02 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (202, 101, N'30fbd33b-f19c-42f1-a544-c275fe2076bb', 45, 59, 50, CAST(50.00 AS Decimal(18, 2)), 2, CAST(0x0000A53D01674F02 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (203, 102, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 47, 13, 2, CAST(2.00 AS Decimal(18, 2)), 1, CAST(0x0000A53D016928FA AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (204, 102, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 47, 13, 2, CAST(2.00 AS Decimal(18, 2)), 2, CAST(0x0000A53D016928FA AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (205, 103, N'f32f8c0b-e069-4f80-9c2e-24785747c607', 31, 42, 2, CAST(2.00 AS Decimal(18, 2)), 1, CAST(0x0000A53D017822D5 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (206, 103, N'f32f8c0b-e069-4f80-9c2e-24785747c607', 31, 42, 2, CAST(2.00 AS Decimal(18, 2)), 2, CAST(0x0000A53D01782316 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (207, 104, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 36, 45, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53E001CEA4A AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (208, 104, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 36, 45, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53E001CEA4F AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (209, 105, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 36, 45, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53E001CEA4F AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (210, 105, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 36, 45, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53E001CEA4F AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (211, 106, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53E009EFDC7 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (212, 106, N'aa41753e-1465-4a27-8a3d-20c1652178d4', 37, 49, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53E009EFE4F AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (213, 107, N'71ebf717-7aa7-459e-b803-2dbc75a2f864', 57, 76, 20, CAST(20.00 AS Decimal(18, 2)), 1, CAST(0x0000A53E009EFE53 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (214, 107, N'71ebf717-7aa7-459e-b803-2dbc75a2f864', 57, 76, 20, CAST(20.00 AS Decimal(18, 2)), 2, CAST(0x0000A53E009EFE61 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (215, 108, N'71ebf717-7aa7-459e-b803-2dbc75a2f864', 57, 76, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53E00B59F15 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (216, 108, N'71ebf717-7aa7-459e-b803-2dbc75a2f864', 57, 76, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53E00B59F19 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (217, 109, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', 51, 64, 50, CAST(50.00 AS Decimal(18, 2)), 1, CAST(0x0000A53E00B59F19 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (218, 109, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', 51, 64, 50, CAST(50.00 AS Decimal(18, 2)), 2, CAST(0x0000A53E00B59F19 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (219, 110, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 28, 34, 10, CAST(10.00 AS Decimal(18, 2)), 1, CAST(0x0000A53E00C58BDD AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (220, 110, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 28, 34, 10, CAST(10.00 AS Decimal(18, 2)), 2, CAST(0x0000A53E00C58BEA AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (221, 111, N'71ebf717-7aa7-459e-b803-2dbc75a2f864', 57, 76, 10, CAST(10.00 AS Decimal(18, 2)), 1, CAST(0x0000A53E00C58BEA AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (222, 111, N'71ebf717-7aa7-459e-b803-2dbc75a2f864', 57, 76, 10, CAST(10.00 AS Decimal(18, 2)), 2, CAST(0x0000A53E00C58BEA AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (223, 112, N'71ebf717-7aa7-459e-b803-2dbc75a2f864', 57, 76, 10, CAST(10.00 AS Decimal(18, 2)), 1, CAST(0x0000A53E00C58BEA AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (224, 112, N'71ebf717-7aa7-459e-b803-2dbc75a2f864', 57, 76, 10, CAST(10.00 AS Decimal(18, 2)), 2, CAST(0x0000A53E00C58BEA AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (225, 113, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 49, 72, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53E00C58BEF AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (226, 113, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 49, 72, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53E00C58BEF AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (227, 114, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 3, CAST(3.00 AS Decimal(18, 2)), 1, CAST(0x0000A53E00C58BEF AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (228, 114, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, 3, 3, CAST(3.00 AS Decimal(18, 2)), 2, CAST(0x0000A53E00C58BEF AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (229, 115, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', 51, 64, 1, CAST(1.00 AS Decimal(18, 2)), 1, CAST(0x0000A53E00D33693 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (230, 115, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', 51, 64, 1, CAST(1.00 AS Decimal(18, 2)), 2, CAST(0x0000A53E00D33698 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (231, 116, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 49, 72, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53E00DCD314 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (232, 116, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 49, 72, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53E00DCD314 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (233, 117, N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', 58, 80, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53E011E3BD1 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (234, 117, N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', 58, 80, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53E011E3BD6 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (235, 118, N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', 58, 80, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53E011E3BD6 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (236, 118, N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', 58, 80, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53E011E3BD6 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (237, 119, N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', 58, 80, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53E011E3BD6 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (238, 119, N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', 58, 80, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53E011E3BDA AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (239, 120, N'71ebf717-7aa7-459e-b803-2dbc75a2f864', 57, 76, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53E012F1615 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (240, 120, N'71ebf717-7aa7-459e-b803-2dbc75a2f864', 57, 76, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53E012F1615 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (241, 121, N'30fbd33b-f19c-42f1-a544-c275fe2076bb', 43, 61, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53E0169FF58 AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (242, 121, N'30fbd33b-f19c-42f1-a544-c275fe2076bb', 43, 61, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53E0169FF5C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (243, 122, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 49, 72, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53E01756DCC AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (244, 122, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 49, 72, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53E01756DCC AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (245, 123, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 49, 72, 1, CAST(0.25 AS Decimal(18, 2)), 1, CAST(0x0000A53E01850C4C AS DateTime))
GO
INSERT [dbo].[Income_Transaction] ([int_income_transaction_id], [int_vote_history_id], [int_user_id], [int_song_contest_id], [int_song_original_id], [int_vote_value], [dec_amount], [int_type], [datetime_create]) VALUES (246, 123, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 49, 72, 1, CAST(0.25 AS Decimal(18, 2)), 2, CAST(0x0000A53E01850C51 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Income_Transaction] OFF
GO
SET IDENTITY_INSERT [dbo].[Paysbuy] ON 

GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (1, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210310090001', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53800F9FBBD AS DateTime), CAST(0x0000A53800F9FBBD AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (2, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210316250002', N'เติม Cyberry Point 100 Points', N'561.73', 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53800FBB42E AS DateTime), CAST(0x0000A53800FBB42E AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (3, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210318420003', N'เติม Cyberry Point 20 Points', N'112.35', 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53800FC544F AS DateTime), CAST(0x0000A53800FC544F AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (4, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210318570004', N'เติม Cyberry Point 100 Points', N'561.73', 100, NULL, NULL, NULL, N'05', NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, N'00201510210318570004', N'10639147', N'21.07', NULL, CAST(0x0000A53800FC66C3 AS DateTime), CAST(0x0000A538010BEBA1 AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (5, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210406070005', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, N'06', NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, N'02201510210406070005', N'10639562', N'0.00', NULL, CAST(0x0000A53801095A6D AS DateTime), CAST(0x0000A53801099D01 AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (6, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210536230006', N'เติม Cyberry Point 20 Points', N'112.35', 20, NULL, NULL, NULL, N'06', NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, N'02201510210536230006', N'10640449', N'0.00', NULL, CAST(0x0000A5380122257F AS DateTime), CAST(0x0000A538012254FA AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (7, N'66dbb691-4e42-4417-8283-a2c6571fea7e', N'201510210540180007', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, N'06', NULL, NULL, NULL, NULL, NULL, N'parchaya anantamek', N'parchaya_1@windowslive.com', N'0802961465', N'5/6 test', NULL, N'02201510210540180007', N'10640480', N'0.00', NULL, CAST(0x0000A5380123393D AS DateTime), CAST(0x0000A53801234D61 AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (8, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210546510008', N'เติม Cyberry Point 50 Points', N'280.88', 50, NULL, NULL, NULL, N'06', NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, N'02201510210546510008', N'10640540', N'0.00', NULL, CAST(0x0000A5380125052E AS DateTime), CAST(0x0000A53801253A5B AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (9, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210805120009', N'เติม Cyberry Point 150 Points', N'842.63', 150, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A538014B053B AS DateTime), CAST(0x0000A538014B053B AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (10, N'89dec678-e0b1-491e-ac89-250725b5c517', N'201510221000200010', N'เติม Cyberry Point 500 Points', N'2808.75', 500, NULL, NULL, NULL, N'06', NULL, NULL, NULL, NULL, NULL, N'poramez kumarnboon', N'poramez@live.com', N'0814233848', N'111/107 Tharadi Nonthaburi 11000', NULL, N'02201510221000200010', N'10644861', N'0.00', NULL, CAST(0x0000A53900A4E3DF AS DateTime), CAST(0x0000A53900A5377F AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (11, N'89dec678-e0b1-491e-ac89-250725b5c517', N'201510221001580011', N'เติม Cyberry Point 500 Points', N'2808.75', 500, NULL, NULL, NULL, N'06', NULL, NULL, NULL, NULL, NULL, N'poramez kumarnboon', N'poramez@live.com', N'0814233848', N'111/107 Tharadi Nonthaburi 11000', NULL, N'02201510221001580011', N'10644873', N'0.00', NULL, CAST(0x0000A53900A5565E AS DateTime), CAST(0x0000A53900A58E3C AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (12, N'66dbb691-4e42-4417-8283-a2c6571fea7e', N'201510220425410012', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, N'06', NULL, NULL, NULL, NULL, NULL, N'parchaya anantamek', N'parchaya_1@windowslive.com', N'0802961465', N'5/6 test', NULL, N'02201510220425410012', N'10648573', N'0.00', NULL, CAST(0x0000A539010EBADA AS DateTime), CAST(0x0000A539010EE262 AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (13, N'bd9247d1-aa76-4806-9122-456c1c6bd6c5', N'201510220523360013', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, N'06', NULL, NULL, NULL, NULL, NULL, N'SALES TEST', N'psb.saletest@gmail.com', N'021605463', N'test
Patumwan', NULL, N'02201510220523360013', N'10649098', N'0.00', NULL, CAST(0x0000A539011EA2E8 AS DateTime), CAST(0x0000A539011EC73C AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (14, N'66dbb691-4e42-4417-8283-a2c6571fea7e', N'201510220523480014', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'parchaya anantamek', N'parchaya_1@windowslive.com', N'0802961465', N'5/6 test', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A539011EB10B AS DateTime), CAST(0x0000A539011EB10B AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (15, N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'201510240317560015', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'กันตกฤษณ์ จึงวงศ์ไพบูลย์', N'cgame_guntagrit@hotmail.com', N'0924756699', N'60/5 ม.3 ต.ย่านยาว อ.สวรรคโลก จ.สุโขทัย 64110', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53B00365DCC AS DateTime), CAST(0x0000A53B00365DCC AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (16, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510241218440016', N'เติม Cyberry Point 200 Points', N'1123.50', 200, NULL, NULL, NULL, N'06', NULL, NULL, NULL, NULL, NULL, N'Palida Kumarnboon', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, N'02201510241218440016', N'10666232', N'0.00', NULL, CAST(0x0000A53B00CAE72C AS DateTime), CAST(0x0000A53B00CB6B19 AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (17, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510240123380017', N'เติม Cyberry Point 500 Points', N'2808.75', 500, NULL, NULL, NULL, N'05', NULL, NULL, NULL, NULL, NULL, N'Palida Kumarnboon', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, N'00201510240123380017', N'10666694', N'105.33', NULL, CAST(0x0000A53B00DCB9EA AS DateTime), CAST(0x0000A53B00DD58B4 AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (18, N'89dec678-e0b1-491e-ac89-250725b5c517', N'201510240218520018', N'เติม Cyberry Point 200 Points', N'1123.50', 200, NULL, NULL, NULL, N'06', NULL, NULL, NULL, NULL, NULL, N'poramez kumarnboon', N'poramez@live.com', N'0814233848', N'111/107 Tharadi Nonthaburi 11000', NULL, N'02201510240218520018', N'10667090', N'0.00', NULL, CAST(0x0000A53B00EBE5E7 AS DateTime), CAST(0x0000A53B00EC1114 AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (19, N'f32f8c0b-e069-4f80-9c2e-24785747c607', N'201510240524280019', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'in the moon music', N'aof_appcoltd@hotmail.com', N'0621631637', N'181/27  ต นาจอมเทียน  อ. บางละมุง จ. ชลบุรี', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53B011EDF82 AS DateTime), CAST(0x0000A53B011EDF82 AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (20, N'bfebc6ac-68c6-4032-a382-d2b1bf13b788', N'201510260448370020', N'เติม Cyberry Point 20 Points', N'112.35', 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'nannapas ploywisut', N'nannapas.ploy@gmail.com', N'0905749956', N'403/60 ', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53D011506B6 AS DateTime), CAST(0x0000A53D011506B6 AS DateTime))
GO
INSERT [dbo].[Paysbuy] ([int_paysbuy_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [datetime_create], [datetime_modified]) VALUES (21, N'1930dee3-f415-4374-b428-e4f56a493861', N'201510260536170021', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ANURAK OUAM-IN', N'artgatlal555@gmail.com', N'0910044907', N'46 ม. 9 ต.ลาดบัวหลวง อ.ลาดบัวหลวง จ.พระนครศรีอยุธยา 13230', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53D01221EA6 AS DateTime), CAST(0x0000A53D01221EA6 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Paysbuy] OFF
GO
SET IDENTITY_INSERT [dbo].[Paysbuy_log] ON 

GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (1, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210310090001', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A53800F9FBBD AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (2, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210316250002', N'เติม Cyberry Point 100 Points', N'561.73', 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A53800FBB42E AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (3, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210318420003', N'เติม Cyberry Point 20 Points', N'112.35', 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A53800FC544F AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (4, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210318570004', N'เติม Cyberry Point 100 Points', N'561.73', 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A53800FC66C3 AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (5, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210406070005', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A53801095A6D AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (6, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210406070005', NULL, N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'02201510210406070005', N'10639562', N'0.00', NULL, N'Process', CAST(0x0000A53801099D01 AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (7, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210318570004', NULL, N'561.73', 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'00201510210318570004', N'10639147', N'21.07', N'true', N'Success', CAST(0x0000A538010BEBA1 AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (8, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210536230006', N'เติม Cyberry Point 20 Points', N'112.35', 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A5380122257F AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (9, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210536230006', NULL, N'112.35', 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'02201510210536230006', N'10640449', N'0.00', NULL, N'Process', CAST(0x0000A538012254FA AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (10, N'66dbb691-4e42-4417-8283-a2c6571fea7e', N'201510210540180007', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'parchaya anantamek', N'parchaya_1@windowslive.com', N'0802961465', N'5/6 test', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A5380123393D AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (11, N'66dbb691-4e42-4417-8283-a2c6571fea7e', N'201510210540180007', NULL, N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'02201510210540180007', N'10640480', N'0.00', NULL, N'Process', CAST(0x0000A53801234D61 AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (12, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210546510008', N'เติม Cyberry Point 50 Points', N'280.88', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A5380125052E AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (13, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210546510008', NULL, N'280.88', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'02201510210546510008', N'10640540', N'0.00', NULL, N'Process', CAST(0x0000A53801253A5B AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (14, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510210805120009', N'เติม Cyberry Point 150 Points', N'842.63', 150, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ปาลิดา กุมารบุญ', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A538014B053B AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (15, N'89dec678-e0b1-491e-ac89-250725b5c517', N'201510221000200010', N'เติม Cyberry Point 500 Points', N'2808.75', 500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'poramez kumarnboon', N'poramez@live.com', N'0814233848', N'111/107 Tharadi Nonthaburi 11000', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A53900A4E3DF AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (16, N'89dec678-e0b1-491e-ac89-250725b5c517', N'201510221000200010', NULL, N'2808.75', 500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'02201510221000200010', N'10644861', N'0.00', NULL, N'Process', CAST(0x0000A53900A5377F AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (17, N'89dec678-e0b1-491e-ac89-250725b5c517', N'201510221001580011', N'เติม Cyberry Point 500 Points', N'2808.75', 500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'poramez kumarnboon', N'poramez@live.com', N'0814233848', N'111/107 Tharadi Nonthaburi 11000', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A53900A5565E AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (18, N'89dec678-e0b1-491e-ac89-250725b5c517', N'201510221001580011', NULL, N'2808.75', 500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'02201510221001580011', N'10644873', N'0.00', NULL, N'Process', CAST(0x0000A53900A58E3C AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (19, N'66dbb691-4e42-4417-8283-a2c6571fea7e', N'201510220425410012', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'parchaya anantamek', N'parchaya_1@windowslive.com', N'0802961465', N'5/6 test', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A539010EBADA AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (20, N'66dbb691-4e42-4417-8283-a2c6571fea7e', N'201510220425410012', NULL, N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'02201510220425410012', N'10648573', N'0.00', NULL, N'Process', CAST(0x0000A539010EE262 AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (21, N'bd9247d1-aa76-4806-9122-456c1c6bd6c5', N'201510220523360013', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'SALES TEST', N'psb.saletest@gmail.com', N'021605463', N'test
Patumwan', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A539011EA2E8 AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (22, N'66dbb691-4e42-4417-8283-a2c6571fea7e', N'201510220523480014', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'parchaya anantamek', N'parchaya_1@windowslive.com', N'0802961465', N'5/6 test', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A539011EB10B AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (23, N'bd9247d1-aa76-4806-9122-456c1c6bd6c5', N'201510220523360013', NULL, N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'02201510220523360013', N'10649098', N'0.00', NULL, N'Process', CAST(0x0000A539011EC73C AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (24, N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'201510240317560015', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'กันตกฤษณ์ จึงวงศ์ไพบูลย์', N'cgame_guntagrit@hotmail.com', N'0924756699', N'60/5 ม.3 ต.ย่านยาว อ.สวรรคโลก จ.สุโขทัย 64110', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A53B00365DCC AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (25, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510241218440016', N'เติม Cyberry Point 200 Points', N'1123.50', 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Palida Kumarnboon', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A53B00CAE72C AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (26, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510241218440016', NULL, N'1123.50', 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'02201510241218440016', N'10666232', N'0.00', NULL, N'Process', CAST(0x0000A53B00CB6B19 AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (27, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510240123380017', N'เติม Cyberry Point 500 Points', N'2808.75', 500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Palida Kumarnboon', N'palida_pla@hotmail.com', N'0813429872', N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A53B00DCB9EA AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (28, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'201510240123380017', NULL, N'2808.75', 500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'00201510240123380017', N'10666694', N'105.33', NULL, N'Success', CAST(0x0000A53B00DD58B4 AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (29, N'89dec678-e0b1-491e-ac89-250725b5c517', N'201510240218520018', N'เติม Cyberry Point 200 Points', N'1123.50', 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'poramez kumarnboon', N'poramez@live.com', N'0814233848', N'111/107 Tharadi Nonthaburi 11000', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A53B00EBE5E7 AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (30, N'89dec678-e0b1-491e-ac89-250725b5c517', N'201510240218520018', NULL, N'1123.50', 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'02201510240218520018', N'10667090', N'0.00', NULL, N'Process', CAST(0x0000A53B00EC1114 AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (31, N'f32f8c0b-e069-4f80-9c2e-24785747c607', N'201510240524280019', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'in the moon music', N'aof_appcoltd@hotmail.com', N'0621631637', N'181/27  ต นาจอมเทียน  อ. บางละมุง จ. ชลบุรี', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A53B011EDF82 AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (32, N'bfebc6ac-68c6-4032-a382-d2b1bf13b788', N'201510260448370020', N'เติม Cyberry Point 20 Points', N'112.35', 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'nannapas ploywisut', N'nannapas.ploy@gmail.com', N'0905749956', N'403/60 ', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A53D011506B6 AS DateTime))
GO
INSERT [dbo].[Paysbuy_log] ([int_paysbuy_log_id], [int_user_id], [inv], [itm], [amt], [point], [paypal_amt], [curr_type], [com], [method], [language], [resp_front_url], [resp_back_url], [opt_fix_redirect], [opt_fix_method], [opt_name], [opt_email], [opt_mobile], [opt_address], [opt_detail], [result], [apCode], [fee], [confirm_cs], [txt_method], [datetime_create]) VALUES (33, N'1930dee3-f415-4374-b428-e4f56a493861', N'201510260536170021', N'เติม Cyberry Point 10 Points', N'56.18', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ANURAK OUAM-IN', N'artgatlal555@gmail.com', N'0910044907', N'46 ม. 9 ต.ลาดบัวหลวง อ.ลาดบัวหลวง จ.พระนครศรีอยุธยา 13230', NULL, NULL, NULL, NULL, NULL, N'send to paysbuy', CAST(0x0000A53D01221EA6 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Paysbuy_log] OFF
GO
INSERT [dbo].[PaysBuyConfig] ([psb], [biz], [secureCode], [postUrl], [reqUrl], [paysbuyUrl]) VALUES (N'0730260210', N'admin@cyberrymusic.com', N'9E541D1E1083F5097A06451CCD5DF77A', N'http://www.cyberrymusic.com/point/receive', N'http://www.cyberrymusic.com/point/receive', N'http://www.paysbuy.com/paynow.aspx')
GO
SET IDENTITY_INSERT [dbo].[Point] ON 

GO
INSERT [dbo].[Point] ([int_point_id], [int_user_id], [int_value], [datetime_create], [datetime_modified]) VALUES (1, N'9fa60428-ef64-43c1-aebf-a7be62600488', 84, CAST(0x0000A538010BEBAA AS DateTime), CAST(0x0000A53E00D2BB61 AS DateTime))
GO
INSERT [dbo].[Point] ([int_point_id], [int_user_id], [int_value], [datetime_create], [datetime_modified]) VALUES (2, N'89dec678-e0b1-491e-ac89-250725b5c517', 240, CAST(0x0000A538013FC33C AS DateTime), CAST(0x0000A53E00B66CE0 AS DateTime))
GO
INSERT [dbo].[Point] ([int_point_id], [int_user_id], [int_value], [datetime_create], [datetime_modified]) VALUES (3, N'bfebc6ac-68c6-4032-a382-d2b1bf13b788', 1, CAST(0x0000A53900B32311 AS DateTime), CAST(0x0000A53900B32315 AS DateTime))
GO
INSERT [dbo].[Point] ([int_point_id], [int_user_id], [int_value], [datetime_create], [datetime_modified]) VALUES (4, N'952aa50c-9b28-49da-8020-a361a5e856a5', 0, CAST(0x0000A53B00BE602A AS DateTime), CAST(0x0000A53B00BF4138 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Point] OFF
GO
SET IDENTITY_INSERT [dbo].[Point_Transaction] ON 

GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (1, N'9fa60428-ef64-43c1-aebf-a7be62600488', NULL, 100, N'buy point', CAST(0x0000A538010BEBAA AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (2, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 1, N'vote', CAST(0x0000A538010D506E AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (3, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'89dec678-e0b1-491e-ac89-250725b5c517', 8, N'transfer', CAST(0x0000A538013FC340 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (4, N'89dec678-e0b1-491e-ac89-250725b5c517', N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 2, N'vote', CAST(0x0000A53900A47EFB AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (5, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'bfebc6ac-68c6-4032-a382-d2b1bf13b788', 1, N'transfer', CAST(0x0000A53900B32315 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (6, N'89dec678-e0b1-491e-ac89-250725b5c517', N'9a67901b-8c64-4f79-88bc-111f5624e0c5', 1, N'vote', CAST(0x0000A53A01729AE8 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (7, N'89dec678-e0b1-491e-ac89-250725b5c517', N'89dec678-e0b1-491e-ac89-250725b5c517', 4, N'vote', CAST(0x0000A53A018797D6 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (8, N'89dec678-e0b1-491e-ac89-250725b5c517', N'b01eba5d-03b8-4e16-8984-95cd02efe904', 1, N'vote', CAST(0x0000A53B009AC23E AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (9, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'89dec678-e0b1-491e-ac89-250725b5c517', 1, N'vote', CAST(0x0000A53B00A24AC5 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (10, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'952aa50c-9b28-49da-8020-a361a5e856a5', 2, N'transfer', CAST(0x0000A53B00BE602F AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (11, N'952aa50c-9b28-49da-8020-a361a5e856a5', N'9fa60428-ef64-43c1-aebf-a7be62600488', 2, N'transfer', CAST(0x0000A53B00BF4138 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (12, N'9fa60428-ef64-43c1-aebf-a7be62600488', NULL, 500, N'buy point', CAST(0x0000A53B00DD58B4 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (13, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'89dec678-e0b1-491e-ac89-250725b5c517', 100, N'transfer', CAST(0x0000A53B00DE1A7C AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (14, N'89dec678-e0b1-491e-ac89-250725b5c517', N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 5, N'vote', CAST(0x0000A53B00DEE0A4 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (15, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'89dec678-e0b1-491e-ac89-250725b5c517', 400, N'transfer', CAST(0x0000A53B00DF0658 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (16, N'89dec678-e0b1-491e-ac89-250725b5c517', N'875d9abb-b8bd-42ab-8fbe-a222b53b0608', 5, N'vote', CAST(0x0000A53B00E5B878 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (17, N'89dec678-e0b1-491e-ac89-250725b5c517', N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 2, N'vote', CAST(0x0000A53B00EAC875 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (18, N'89dec678-e0b1-491e-ac89-250725b5c517', N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 20, N'vote', CAST(0x0000A53C009B2293 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (19, N'89dec678-e0b1-491e-ac89-250725b5c517', N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 6, N'vote', CAST(0x0000A53C009C0352 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (20, N'89dec678-e0b1-491e-ac89-250725b5c517', N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 10, N'vote', CAST(0x0000A53C009DB917 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (21, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 1, N'vote', CAST(0x0000A53C00BE8BF7 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (22, N'89dec678-e0b1-491e-ac89-250725b5c517', N'b01eba5d-03b8-4e16-8984-95cd02efe904', 2, N'vote', CAST(0x0000A53C0122AA3F AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (23, N'89dec678-e0b1-491e-ac89-250725b5c517', N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 8, N'vote', CAST(0x0000A53C0122CE89 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (24, N'89dec678-e0b1-491e-ac89-250725b5c517', N'c6ac2778-8630-4dd1-88ef-59f61033e4bb', 1, N'vote', CAST(0x0000A53C01762C2B AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (25, N'89dec678-e0b1-491e-ac89-250725b5c517', N'b01eba5d-03b8-4e16-8984-95cd02efe904', 21, N'vote', CAST(0x0000A53D00C41835 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (26, N'89dec678-e0b1-491e-ac89-250725b5c517', N'01f96c09-09ca-47d5-9f76-a87acea70f0c', 10, N'vote', CAST(0x0000A53D00CAB00F AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (27, N'89dec678-e0b1-491e-ac89-250725b5c517', N'30fbd33b-f19c-42f1-a544-c275fe2076bb', 2, N'vote', CAST(0x0000A53D00F3695C AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (28, N'89dec678-e0b1-491e-ac89-250725b5c517', N'f01815e6-883a-44bb-82d7-66ab9bce1863', 2, N'vote', CAST(0x0000A53D0101929E AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (29, N'89dec678-e0b1-491e-ac89-250725b5c517', N'30fbd33b-f19c-42f1-a544-c275fe2076bb', 10, N'vote', CAST(0x0000A53D0101E5BA AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (30, N'89dec678-e0b1-491e-ac89-250725b5c517', N'f01815e6-883a-44bb-82d7-66ab9bce1863', 2, N'vote', CAST(0x0000A53D0105BEB1 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (31, N'89dec678-e0b1-491e-ac89-250725b5c517', N'30fbd33b-f19c-42f1-a544-c275fe2076bb', 50, N'vote', CAST(0x0000A53D0166B6E7 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (32, N'89dec678-e0b1-491e-ac89-250725b5c517', N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', 2, N'vote', CAST(0x0000A53D0167B13A AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (33, N'89dec678-e0b1-491e-ac89-250725b5c517', N'f32f8c0b-e069-4f80-9c2e-24785747c607', 2, N'vote', CAST(0x0000A53D0170BFC5 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (34, N'89dec678-e0b1-491e-ac89-250725b5c517', N'71ebf717-7aa7-459e-b803-2dbc75a2f864', 20, N'vote', CAST(0x0000A53E009D8BE2 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (35, N'89dec678-e0b1-491e-ac89-250725b5c517', N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', 50, N'vote', CAST(0x0000A53E00B59BAD AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (36, N'89dec678-e0b1-491e-ac89-250725b5c517', N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', 10, N'vote', CAST(0x0000A53E00B5C022 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (37, N'89dec678-e0b1-491e-ac89-250725b5c517', N'71ebf717-7aa7-459e-b803-2dbc75a2f864', 10, N'vote', CAST(0x0000A53E00B5F0C9 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (38, N'89dec678-e0b1-491e-ac89-250725b5c517', N'71ebf717-7aa7-459e-b803-2dbc75a2f864', 10, N'vote', CAST(0x0000A53E00B66CE0 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (39, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', 3, N'vote', CAST(0x0000A53E00C1EB63 AS DateTime))
GO
INSERT [dbo].[Point_Transaction] ([int_transaction_id], [int_user_id], [int_target_user_id], [int_value], [txt_method], [datetime_create]) VALUES (40, N'9fa60428-ef64-43c1-aebf-a7be62600488', N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', 1, N'vote', CAST(0x0000A53E00D2BB61 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Point_Transaction] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

GO
INSERT [dbo].[Product] ([int_product_id], [txt_product_no], [txt_product_name], [txt_product_desc], [txt_image], [dec_price_exvat], [dec_price], [int_amount], [datetime_create], [datetime_modified]) VALUES (2, N'10', N'Cyberry Point 10 Points', N'เติม Cyberry Point 10 Points', N'/images/CYBERRY_50b_10P.jpg', CAST(50.00 AS Decimal(18, 2)), CAST(56.18 AS Decimal(18, 2)), 1, CAST(0x0000A53200000000 AS DateTime), CAST(0x0000A53200000000 AS DateTime))
GO
INSERT [dbo].[Product] ([int_product_id], [txt_product_no], [txt_product_name], [txt_product_desc], [txt_image], [dec_price_exvat], [dec_price], [int_amount], [datetime_create], [datetime_modified]) VALUES (3, N'20', N'Cyberry Point 20 Points', N'เติม Cyberry Point 20 Points', N'/images/CYBERRY_100b_20P.jpg', CAST(100.00 AS Decimal(18, 2)), CAST(112.35 AS Decimal(18, 2)), 1, CAST(0x0000A53200000000 AS DateTime), CAST(0x0000A53200000000 AS DateTime))
GO
INSERT [dbo].[Product] ([int_product_id], [txt_product_no], [txt_product_name], [txt_product_desc], [txt_image], [dec_price_exvat], [dec_price], [int_amount], [datetime_create], [datetime_modified]) VALUES (4, N'50', N'Cyberry Point 50 Points', N'เติม Cyberry Point 50 Points', N'/images/CYBERRY_250b_50P.jpg', CAST(250.00 AS Decimal(18, 2)), CAST(280.88 AS Decimal(18, 2)), 1, CAST(0x0000A53200000000 AS DateTime), CAST(0x0000A53200000000 AS DateTime))
GO
INSERT [dbo].[Product] ([int_product_id], [txt_product_no], [txt_product_name], [txt_product_desc], [txt_image], [dec_price_exvat], [dec_price], [int_amount], [datetime_create], [datetime_modified]) VALUES (5, N'100', N'Cyberry Point 100 Points', N'เติม Cyberry Point 100 Points', N'/images/CYBERRY_500b_100P.jpg', CAST(500.00 AS Decimal(18, 2)), CAST(561.73 AS Decimal(18, 2)), 1, CAST(0x0000A53200000000 AS DateTime), CAST(0x0000A53200000000 AS DateTime))
GO
INSERT [dbo].[Product] ([int_product_id], [txt_product_no], [txt_product_name], [txt_product_desc], [txt_image], [dec_price_exvat], [dec_price], [int_amount], [datetime_create], [datetime_modified]) VALUES (6, N'150', N'Cyberry Point 150 Points', N'เติม Cyberry Point 150 Points', N'/images/CYBERRY_750b_150P.jpg', CAST(800.00 AS Decimal(18, 2)), CAST(842.63 AS Decimal(18, 2)), 1, CAST(0x0000A53200000000 AS DateTime), CAST(0x0000A53200000000 AS DateTime))
GO
INSERT [dbo].[Product] ([int_product_id], [txt_product_no], [txt_product_name], [txt_product_desc], [txt_image], [dec_price_exvat], [dec_price], [int_amount], [datetime_create], [datetime_modified]) VALUES (7, N'200', N'Cyberry Point 200 Points', N'เติม Cyberry Point 200 Points', N'/images/CYBERRY_1000b_200P.jpg', CAST(1000.00 AS Decimal(18, 2)), CAST(1123.50 AS Decimal(18, 2)), 1, CAST(0x0000A53200000000 AS DateTime), CAST(0x0000A53200000000 AS DateTime))
GO
INSERT [dbo].[Product] ([int_product_id], [txt_product_no], [txt_product_name], [txt_product_desc], [txt_image], [dec_price_exvat], [dec_price], [int_amount], [datetime_create], [datetime_modified]) VALUES (8, N'500', N'Cyberry Point 500 Points', N'เติม Cyberry Point 500 Points', N'/images/CYBERRY_2500b_500P.jpg', CAST(2500.00 AS Decimal(18, 2)), CAST(2808.75 AS Decimal(18, 2)), 1, CAST(0x0000A53200000000 AS DateTime), CAST(0x0000A53200000000 AS DateTime))
GO
INSERT [dbo].[Product] ([int_product_id], [txt_product_no], [txt_product_name], [txt_product_desc], [txt_image], [dec_price_exvat], [dec_price], [int_amount], [datetime_create], [datetime_modified]) VALUES (9, N'1000', N'Cyberry Point 1000 Points', N'เติม Cyberry Point 1000 Points', N'/images/CYBERRY_5000b_1000P.jpg', CAST(5000.00 AS Decimal(18, 2)), CAST(5617.50 AS Decimal(18, 2)), 1, CAST(0x0000A53200000000 AS DateTime), CAST(0x0000A53200000000 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[Score_Song] ON 

GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (1, 2, 21, CAST(0x0000A538010D506E AS DateTime), CAST(0x0000A53E00C1EB63 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (2, 4, 1, CAST(0x0000A53A01729AE8 AS DateTime), CAST(0x0000A53A01729AE8 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (3, 6, 5, CAST(0x0000A53A018797D6 AS DateTime), CAST(0x0000A53B00A24AC5 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (4, 12, 23, CAST(0x0000A53B009AC23E AS DateTime), CAST(0x0000A53C0122AA3F AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (5, 10, 2, CAST(0x0000A53B009D2617 AS DateTime), CAST(0x0000A53B009D2617 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (6, 7, 32, CAST(0x0000A53B00AEFFE6 AS DateTime), CAST(0x0000A53D00C41835 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (7, 26, 30, CAST(0x0000A53B00DA181A AS DateTime), CAST(0x0000A53D00CAB00F AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (8, 13, 5, CAST(0x0000A53B00E5B878 AS DateTime), CAST(0x0000A53B00E5B878 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (9, 5, 1, CAST(0x0000A53B011FBA77 AS DateTime), CAST(0x0000A53B011FBA77 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (10, 36, 24, CAST(0x0000A53B01800B4D AS DateTime), CAST(0x0000A53C009B2293 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (11, 30, 6, CAST(0x0000A53C009C0352 AS DateTime), CAST(0x0000A53C009C0352 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (12, 28, 22, CAST(0x0000A53C009DB917 AS DateTime), CAST(0x0000A53E00B5C022 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (13, 37, 19, CAST(0x0000A53C01213D8E AS DateTime), CAST(0x0000A53C01213D8E AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (14, 38, 2, CAST(0x0000A53C012DAAD1 AS DateTime), CAST(0x0000A53C012DAAD1 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (15, 39, 1, CAST(0x0000A53C01762C2B AS DateTime), CAST(0x0000A53C01762C2B AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (16, 41, 1, CAST(0x0000A53D00A97EBD AS DateTime), CAST(0x0000A53D00A97EBD AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (17, 45, 62, CAST(0x0000A53D00F3695C AS DateTime), CAST(0x0000A53D0166B6E7 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (18, 46, 4, CAST(0x0000A53D0101929E AS DateTime), CAST(0x0000A53D0105BEB1 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (19, 47, 2, CAST(0x0000A53D0167B13A AS DateTime), CAST(0x0000A53D0167B13A AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (20, 31, 2, CAST(0x0000A53D0170BFC5 AS DateTime), CAST(0x0000A53D0170BFC5 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (21, 57, 42, CAST(0x0000A53E009D8BE2 AS DateTime), CAST(0x0000A53E00B66CE0 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (22, 51, 51, CAST(0x0000A53E00B59BAD AS DateTime), CAST(0x0000A53E00D2BB61 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (23, 49, 4, CAST(0x0000A53E00C17900 AS DateTime), CAST(0x0000A53E00C17900 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (24, 58, 3, CAST(0x0000A53E011C53B6 AS DateTime), CAST(0x0000A53E011C53B6 AS DateTime))
GO
INSERT [dbo].[Score_Song] ([int_score_id], [int_song_contest_id], [int_value], [datetime_create], [datetime_modified]) VALUES (25, 43, 1, CAST(0x0000A53E0163566D AS DateTime), CAST(0x0000A53E0163566D AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Score_Song] OFF
GO
SET IDENTITY_INSERT [dbo].[Sms_Data] ON 

GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (1, N'08fe2621d8e716b02ec0da35256a998d', N'66814233848', 10, CAST(0x0000A53B009D22F4 AS DateTime), CAST(0x0000A53B009D260E AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (2, N'5d616dd38211ebb5d6ec52986674b6e4', N'66814233848', 10, CAST(0x0000A53B009D44F0 AS DateTime), CAST(0x0000A53B009D4795 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (3, N'ef50c335cca9f340bde656363ebd02fd', N'66814233848', 7, CAST(0x0000A53B00AEFD44 AS DateTime), CAST(0x0000A53B00AEFFE2 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (4, N'03e0704b5690a2dee1861dc3ad3316c9', N'66814233848', 7, CAST(0x0000A53B00AF08FC AS DateTime), CAST(0x0000A53B00AF0BDC AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (5, N'65cc2c8205a05d7379fa3a6386f710e1', N'66814233848', 7, CAST(0x0000A53B00AF1130 AS DateTime), CAST(0x0000A53B00AF13D7 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (6, N'0768281a05da9f27df178b5c39a51263', N'66814233848', 7, CAST(0x0000A53B00AF1964 AS DateTime), CAST(0x0000A53B00AF1BAD AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (7, N'93d65641ff3f1586614cf2c1ad240b6c', N'66814233848', 7, CAST(0x0000A53B00AF5078 AS DateTime), CAST(0x0000A53B00AF5308 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (8, N'ce5140df15d046a66883807d18d0264b', N'66814233848', 7, CAST(0x0000A53B00AF5780 AS DateTime), CAST(0x0000A53B00AF5A14 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (9, N'021bbc7ee20b71134d53e20206bd6feb', N'66814233848', 26, CAST(0x0000A53B00DA154C AS DateTime), CAST(0x0000A53B00DA1815 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (10, N'82b8a3434904411a9fdc43ca87cee70c', N'66924756699', 12, CAST(0x0000A53B00FAC92C AS DateTime), CAST(0x0000A53B00FACC4A AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (11, N'24146db4eb48c718b84cae0a0799dcfc', N'66924756699', 12, CAST(0x0000A53B00FB0D24 AS DateTime), CAST(0x0000A53B00FB1071 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (12, N'883e881bb4d22a7add958f2d6b052c9f', N'66924756699', 12, CAST(0x0000A53B00FB0F7C AS DateTime), CAST(0x0000A53B00FB1287 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (13, N'3806734b256c27e41ec2c6bffa26d9e7', N'66924756699', 12, CAST(0x0000A53B00FB1300 AS DateTime), CAST(0x0000A53B00FB163F AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (14, N'84d2004bf28a2095230e8e14993d398d', N'66924756699', 12, CAST(0x0000A53B00FB17B0 AS DateTime), CAST(0x0000A53B00FB1A2A AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (15, N'e515df0d202ae52fcebb14295743063b', N'66924756699', 12, CAST(0x0000A53B00FB1C60 AS DateTime), CAST(0x0000A53B00FB1ECC AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (16, N'afdec7005cc9f14302cd0474fd0f3c96', N'66924756699', 12, CAST(0x0000A53B00FB1FE4 AS DateTime), CAST(0x0000A53B00FB229B AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (17, N'995e1fda4a2b5f55ef0df50868bf2a8f', N'66924756699', 12, CAST(0x0000A53B00FB2944 AS DateTime), CAST(0x0000A53B00FB2BF6 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (18, N'e17184bcb70dcf3942c54e0b537ffc6d', N'66924756699', 12, CAST(0x0000A53B00FB2B9C AS DateTime), CAST(0x0000A53B00FB2E37 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (19, N'bdb106a0560c4e46ccc488ef010af787', N'66924756699', 12, CAST(0x0000A53B00FB304C AS DateTime), CAST(0x0000A53B00FB3292 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (20, N'a34bacf839b923770b2c360eefa26748', N'66924756699', 12, CAST(0x0000A53B00FB32A4 AS DateTime), CAST(0x0000A53B00FB3603 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (21, N'83fa5a432ae55c253d0e60dbfa716723', N'66924756699', 12, CAST(0x0000A53B00FBAA2C AS DateTime), CAST(0x0000A53B00FBACDE AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (22, N'eddb904a6db773755d2857aacadb1cb0', N'66932306228', 12, CAST(0x0000A53B0104F9C4 AS DateTime), CAST(0x0000A53B0104FC8F AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (23, N'6d70cb65d15211726dcce4c0e971e21c', N'66987474088', 12, CAST(0x0000A53B01078EB4 AS DateTime), CAST(0x0000A53B010791E6 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (24, N'27ed0fb950b856b06e1273989422e7d3', N'66960608499', 26, CAST(0x0000A53B010B6A98 AS DateTime), CAST(0x0000A53B010B6DAF AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (25, N'537d9b6c927223c796cac288cced29df', N'66918585368', 12, CAST(0x0000A53B011C24A0 AS DateTime), CAST(0x0000A53B011C3334 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (26, N'd736bb10d83a904aefc1d6ce93dc54b8', N'66965968840', 5, CAST(0x0000A53B011FB7DC AS DateTime), CAST(0x0000A53B011FBA72 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (27, N'9ac403da7947a183884c18a67d3aa8de', N'66910268313', 12, CAST(0x0000A53B01391FC4 AS DateTime), CAST(0x0000A53B0139227F AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (28, N'b9141aff1412dc76340b3822d9ea6c72', N'66954562369', 2, CAST(0x0000A53B016216E0 AS DateTime), CAST(0x0000A53B016219A6 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (29, N'1019c8091693ef5c5f55970346633f92', N'66954562369', 2, CAST(0x0000A53B016254FC AS DateTime), CAST(0x0000A53B016258A3 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (30, N'a0e2a2c563d57df27213ede1ac4ac780', N'66954562369', 2, CAST(0x0000A53B01625AD8 AS DateTime), CAST(0x0000A53B01625DD6 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (31, N'1579779b98ce9edb98dd85606f2c119d', N'66954562369', 2, CAST(0x0000A53B01625E5C AS DateTime), CAST(0x0000A53B016261F0 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (32, N'20b5e1cf8694af7a3c1ba4a87f073021', N'66954562369', 2, CAST(0x0000A53B01626438 AS DateTime), CAST(0x0000A53B01626798 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (33, N'7cce53cf90577442771720a370c3c723', N'66954562369', 2, CAST(0x0000A53B01626A14 AS DateTime), CAST(0x0000A53B01626D25 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (34, N'58c54802a9fb9526cd0923353a34a7ae', N'66954562369', 2, CAST(0x0000A53B01626D98 AS DateTime), CAST(0x0000A53B016270E5 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (35, N'5055cbf43fac3f7e2336b27310f0b9ef', N'66954562369', 2, CAST(0x0000A53B0162711C AS DateTime), CAST(0x0000A53B0162740C AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (36, N'456ac9b0d15a8b7f1e71073221059886', N'66954562369', 2, CAST(0x0000A53B016274A0 AS DateTime), CAST(0x0000A53B016277D2 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (37, N'f4dd765c12f2ef67f98f3558c282a9cd', N'66831781455', 26, CAST(0x0000A53B016341B4 AS DateTime), CAST(0x0000A53B016344AD AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (38, N'7fec306d1e665bc9c748b5d2b99a6e97', N'66832447060', 2, CAST(0x0000A53B0164C91C AS DateTime), CAST(0x0000A53B0164CBFA AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (39, N'db576a7d2453575f29eab4bac787b919', N'66832447060', 2, CAST(0x0000A53B0164D4D4 AS DateTime), CAST(0x0000A53B0164D822 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (40, N'53c04118df112c13a8c34b38343b9c10', N'66832447060', 26, CAST(0x0000A53B0164FB80 AS DateTime), CAST(0x0000A53B0164FF48 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (41, N'4ca82782c5372a547c104929f03fe7a9', N'66832447060', 26, CAST(0x0000A53B0165141C AS DateTime), CAST(0x0000A53B01651790 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (42, N'd8700cbd38cc9f30cecb34f0c195b137', N'66914147936', 36, CAST(0x0000A53B0180081C AS DateTime), CAST(0x0000A53B01800B4D AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (43, N'b4d168b48157c623fbd095b4a565b5bb', N'66956976999', 12, CAST(0x0000A53C0093F300 AS DateTime), CAST(0x0000A53C0093F847 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (44, N'4a08142c38dbe374195d41c04562d9f8', N'66954562369', 2, CAST(0x0000A53C00A15464 AS DateTime), CAST(0x0000A53C00A1582C AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (45, N'299a23a2291e2126b91d54f3601ec162', N'66954562369', 2, CAST(0x0000A53C00A15DC4 AS DateTime), CAST(0x0000A53C00A16210 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (46, N'a89cf525e1d9f04d16ce31165e139a4b', N'66954562369', 7, CAST(0x0000A53C00A16724 AS DateTime), CAST(0x0000A53C00A16ACB AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (47, N'cd89fef7ffdd490db800357f47722b20', N'66954562369', 7, CAST(0x0000A53C00A17084 AS DateTime), CAST(0x0000A53C00A17430 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (48, N'6c340f25839e6acdc73414517203f5f0', N'66954562369', 7, CAST(0x0000A53C00A178B8 AS DateTime), CAST(0x0000A53C00A17D40 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (49, N'2290a7385ed77cc5592dc2153229f082', N'66954562369', 2, CAST(0x0000A53C00A180EC AS DateTime), CAST(0x0000A53C00A184D5 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (50, N'a2137a2ae8e39b5002a3f8909ecb88fe', N'66954562369', 7, CAST(0x0000A53C00A186C8 AS DateTime), CAST(0x0000A53C00A18AFC AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (51, N'43dd49b4fdb9bede653e94468ff8df1e', N'66954562369', 7, CAST(0x0000A53C00A18EFC AS DateTime), CAST(0x0000A53C00A1926B AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (52, N'31857b449c407203749ae32dd0e7d64a', N'66954562369', 2, CAST(0x0000A53C00A19604 AS DateTime), CAST(0x0000A53C00A199E4 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (53, N'53adaf494dc89ef7196d73636eb2451b', N'66957917054', 12, CAST(0x0000A53C00B8FB78 AS DateTime), CAST(0x0000A53C00B8FFAB AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (54, N'fc2c7c47b918d0c2d792a719dfb602ef', N'66814233848', 28, CAST(0x0000A53C00B93AC0 AS DateTime), CAST(0x0000A53C00B93F55 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (55, N'dc58e3a306451c9d670adcd37004f48f', N'66967531148', 12, CAST(0x0000A53C00FCFE04 AS DateTime), CAST(0x0000A53C00FD02E5 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (56, N'1b0114c51cc532ed34e1954b5b9e4b58', N'66874816136', 37, CAST(0x0000A53C01213968 AS DateTime), CAST(0x0000A53C01213D89 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (57, N'a1519de5b5d44b31a01de013b9b51a80', N'66863647698', 12, CAST(0x0000A53C0127800C AS DateTime), CAST(0x0000A53C012784DB AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (58, N'390e982518a50e280d8e2b535462ec1f', N'66885727083', 37, CAST(0x0000A53C012CA8C0 AS DateTime), CAST(0x0000A53C012CAD29 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (59, N'708f3cf8100d5e71834b1db77dfa15d6', N'66846583523', 38, CAST(0x0000A53C012DA5E0 AS DateTime), CAST(0x0000A53C012DAAD1 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (60, N'46072631582fc240dd2674a7d063b040', N'66814233848', 38, CAST(0x0000A53C014D5B74 AS DateTime), CAST(0x0000A53C014D5FCA AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (61, N'8a1e808b55fde9455cb3d8857ed88389', N'66949695255', 37, CAST(0x0000A53C01719930 AS DateTime), CAST(0x0000A53C01719E3E AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (62, N'062ddb6c727310e76b6200b7c71f63b5', N'66859594159', 37, CAST(0x0000A53C01859214 AS DateTime), CAST(0x0000A53C01859737 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (63, N'522a9ae9a99880d39e5daec35375e999', N'66824974456', 37, CAST(0x0000A53D00015888 AS DateTime), CAST(0x0000A53D00015DA3 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (64, N'43baa6762fa81bb43b39c62553b2970d', N'66959487985', 37, CAST(0x0000A53D009B265C AS DateTime), CAST(0x0000A53D009B217F AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (65, N'731c83db8d2ff01bdc000083fd3c3740', N'66846583523', 41, CAST(0x0000A53D00A983B4 AS DateTime), CAST(0x0000A53D00A97EAA AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (66, N'36a16a2505369e0c922b6ea7a23a56d2', N'66822436625', 37, CAST(0x0000A53D00AD0304 AS DateTime), CAST(0x0000A53D00ACFE3D AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (67, N'e5e63da79fcd2bebbd7cb8bf1c1d0274', N'66819311480', 37, CAST(0x0000A53D00CF9E64 AS DateTime), CAST(0x0000A53D00CF98D8 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (68, N'9f36407ead0629fc166f14dde7970f68', N'66922654977', 37, CAST(0x0000A53D00CF9F90 AS DateTime), CAST(0x0000A53D00CF9AB2 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (69, N'4f16c818875d9fcb6867c7bdc89be7eb', N'66968919543', 37, CAST(0x0000A53D00D08C48 AS DateTime), CAST(0x0000A53D00D08765 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (70, N'd91d1b4d82419de8a614abce9cc0e6d4', N'66968612229', 37, CAST(0x0000A53D00F852B4 AS DateTime), CAST(0x0000A53D00F84D87 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (71, N'a26398dca6f47b49876cbaffbc9954f9', N'66851401049', 37, CAST(0x0000A53D00F896AC AS DateTime), CAST(0x0000A53D00F891C5 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (72, N'b1563a78ec59337587f6ab6397699afc', N'66890744551', 37, CAST(0x0000A53D01101F48 AS DateTime), CAST(0x0000A53D01101A49 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (73, N'03f544613917945245041ea1581df0c2', N'66875127040', 37, CAST(0x0000A53D0113D0FC AS DateTime), CAST(0x0000A53D0113CBCA AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (74, N'8b4066554730ddfaa0266346bdc1b202', N'66834204779', 37, CAST(0x0000A53D0119331C AS DateTime), CAST(0x0000A53D01192DAD AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (75, N'754dda4b1ba34c6fa89716b85d68532b', N'66890744551', 37, CAST(0x0000A53D011A33C0 AS DateTime), CAST(0x0000A53D011A2F24 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (76, N'6a2feef8ed6a9fe76d6b3f30f02150b4', N'66898977454', 37, CAST(0x0000A53D012DC200 AS DateTime), CAST(0x0000A53D012DBD0A AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (77, N'68b1fbe7f16e4ae3024973f12f3cb313', N'66863873441', 36, CAST(0x0000A53D01308A80 AS DateTime), CAST(0x0000A53D01308536 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (78, N'41bfd20a38bb1b0bec75acf0845530a7', N'66884535689', 37, CAST(0x0000A53D0148839C AS DateTime), CAST(0x0000A53D01487EB7 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (79, N'd6723e7cd6735df68d1ce4c704c29a04', N'66612671881', 36, CAST(0x0000A53D0181CFF8 AS DateTime), CAST(0x0000A53D0181CB6B AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (80, N'4e2545f819e67f0615003dd7e04a6087', N'66821674470', 36, CAST(0x0000A53D01847B2C AS DateTime), CAST(0x0000A53D0184769B AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (81, N'db2b4182156b2f1f817860ac9f409ad7', N'66830098387', 37, CAST(0x0000A53E00982EFC AS DateTime), CAST(0x0000A53E00982C10 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (82, N'a5e0ff62be0b08456fc7f1e88812af3d', N'66800939338', 57, CAST(0x0000A53E00B46D74 AS DateTime), CAST(0x0000A53E00B469B0 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (83, N'a0160709701140704575d499c997b6ca', N'66821674470', 49, CAST(0x0000A53E00C17CD0 AS DateTime), CAST(0x0000A53E00C178E8 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (84, N'1e6e0a04d20f50967c64dac2d639a577', N'66914147936', 49, CAST(0x0000A53E00D5B880 AS DateTime), CAST(0x0000A53E00D5B4FE AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (85, N'c6bff625bdb0393992c9d4db0c6bbe45', N'66833772570', 58, CAST(0x0000A53E011C5704 AS DateTime), CAST(0x0000A53E011C53AD AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (86, N'c667d53acd899a97a85de0c201ba99be', N'66810980729', 58, CAST(0x0000A53E011CE3A4 AS DateTime), CAST(0x0000A53E011CE03F AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (87, N'aace49c7d80767cffec0e513ae886df0', N'66814233848', 58, CAST(0x0000A53E011DEB50 AS DateTime), CAST(0x0000A53E011DE759 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (88, N'4da04049a062f5adfe81b67dd755cecc', N'66903807640', 57, CAST(0x0000A53E012BEF98 AS DateTime), CAST(0x0000A53E012BEBC4 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (89, N'af21d0c97db2e27e13572cbf59eb343d', N'66615136136', 43, CAST(0x0000A53E01635924 AS DateTime), CAST(0x0000A53E01635655 AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (90, N'c9f95a0a5af052bffce5c89917335f67', N'66846193342', 49, CAST(0x0000A53E016C19C4 AS DateTime), CAST(0x0000A53E016C161A AS DateTime))
GO
INSERT [dbo].[Sms_Data] ([int_sms_id], [txt_refer_id], [txt_phone], [int_vote_number], [datetime_sent_sms], [datetime_create]) VALUES (91, N'e58cc5ca94270acaceed13bc82dfedf7', N'66959580591', 49, CAST(0x0000A53E01848360 AS DateTime), CAST(0x0000A53E01848083 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Sms_Data] OFF
GO
SET IDENTITY_INSERT [dbo].[Song_Contest] ON 

GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (1, 1, N'bfebc6ac-68c6-4032-a382-d2b1bf13b788', N'CM00001', N'mimi', N'https://www.youtube.com/watch?v=y1hEW64P44w', N'y1hEW64P44w', 1, NULL, NULL, 0, NULL, CAST(0x0000A529013B44AB AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (2, 3, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', N'CM00002', N'ต่อจากนี้', N'https://www.youtube.com/watch?v=H9NR8jP4-cc', N'H9NR8jP4-cc', 1, NULL, NULL, 1, NULL, CAST(0x0000A52B0187D821 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (3, 6, N'66dbb691-4e42-4417-8283-a2c6571fea7e', N'CM00003', N'ทดสอบ', N'https://www.youtube.com/watch?v=lAtXwW08NgM', N'lAtXwW08NgM', 1, NULL, NULL, 0, NULL, CAST(0x0000A53800DF2D71 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (4, 7, N'9a67901b-8c64-4f79-88bc-111f5624e0c5', N'CM00004', N'BTS', N'https://www.youtube.com/watch?v=VmNWHdW1FuE', N'VmNWHdW1FuE', 1, NULL, NULL, 1, NULL, CAST(0x0000A53A0113F128 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (5, 2, N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', N'CM00005', N'ทิ้งฉันในวันเกิด', N'https://www.youtube.com/watch?v=Wmitob541Rg', N'Wmitob541Rg', 1, NULL, NULL, 1, NULL, CAST(0x0000A53A0155B14F AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (6, 2, N'89dec678-e0b1-491e-ac89-250725b5c517', N'CM00006', N'แค่เจอเธอในฝัน', N'https://youtu.be/tjjN-0PlUP0', N'tjjN-0PlUP0', 1, NULL, NULL, 1, NULL, CAST(0x0000A53A01871463 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (7, 16, N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'CM00007', N'คุณมีหนึ่งคำร้องขอ', N'https://www.youtube.com/watch?v=Azh93mJle1Y', N'Azh93mJle1Y', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B002D5EB1 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (8, 15, N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'CM00008', N'คิดไม่เหมือนกัน', N'https://www.youtube.com/watch?v=gB7UxCYmgMc', N'gB7UxCYmgMc', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B002D8907 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (9, 15, N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'CM00009', N'คิด(ไม่)เหมือนกัน - พี่เกม สู้ๆนะครับ', N'https://www.youtube.com/watch?v=yYszNdkUwuU', N'yYszNdkUwuU', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B002DEEB4 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (10, 18, N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'CM00010', N'ได้สักครั้ง ... จะตั้งใจเรียน - พี่เกม สู้ๆนะครับ', N'https://www.youtube.com/watch?v=uM6iOUlPLRk', N'uM6iOUlPLRk', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B002EC5D6 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (11, 19, N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'CM00011', N'ฉันคนหนึ่ง...ไม่ซึ้งวันลอยกระทง - พี่เกม สู้ๆนะครับ', N'https://www.youtube.com/watch?v=F5oT91ilGFA', N'F5oT91ilGFA', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B003227D1 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (12, 17, N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'CM00012', N'Gift (ของขวัญ ของฉัน)- พี่เกม สู้ๆนะครับ', N'https://www.youtube.com/watch?v=bjNd0bcrJaE', N'bjNd0bcrJaE', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00336545 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (13, 20, N'875d9abb-b8bd-42ab-8fbe-a222b53b0608', N'CM00013', N'ปาท่องโก๋', N'https://www.youtube.com/watch?v=4ROjOUHm554', N'4ROjOUHm554', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B003F9ECD AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (14, 21, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'CM00014', N'จากกันวันนี้ดีกว่า - devanfore', N'https://youtu.be/XuR6QGtMZQI', N'XuR6QGtMZQI', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00890253 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (15, 22, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'CM00015', N'รักบังตา - devanfore', N'https://youtu.be/pDPpuU7_AWE', N'pDPpuU7_AWE', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008A853F AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (16, 23, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'CM00016', N'ไม่เคยลืม - Memoir', N'https://youtu.be/WkYxIqCP36k', N'WkYxIqCP36k', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008B635F AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (17, 24, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'CM00017', N'เพื่อน - devanfore', N'https://youtu.be/cxCmFTRgNbo', N'cxCmFTRgNbo', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008BFFF2 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (18, 25, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'CM00018', N'พรุ่งนี้จะเป็นยังไง - devanfore', N'https://youtu.be/QkQWb1JMhOY', N'QkQWb1JMhOY', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008CF956 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (19, 26, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'CM00019', N'ใจเสือกลายเป็นแมว - Memoir', N'https://youtu.be/nz5xoUL_-ig', N'nz5xoUL_-ig', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008D9B0E AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (20, 27, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'CM00020', N'เราจะเดินไปด้วยกัน - devanfore', N'https://youtu.be/kLwfio4bETA', N'kLwfio4bETA', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008E7709 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (21, 28, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'CM00021', N'มนต์รักคนใจดำ - Memoir', N'https://youtu.be/AfYaqRs3Yww', N'AfYaqRs3Yww', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008F2AA1 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (22, 29, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'CM00022', N'ชก.หลายใจ - Memoir', N'https://youtu.be/_r-oMf4Lyeo', N'_r-oMf4Lyeo', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008FD23E AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (23, 30, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'CM00023', N'คืนเหงา - devanfore', N'https://youtu.be/MGWPrK_UTXw', N'MGWPrK_UTXw', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00905E35 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (24, 16, N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'CM00024', N'คุณมีหนึ่งคำร้องขอ ( ....  sent you a friend request ) - พี่เกม สู้ๆนะครับ', N'https://www.youtube.com/watch?v=5DMChjWv8w0', N'5DMChjWv8w0', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00C3C4EE AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (25, 31, N'a20ad65b-ba1f-48bc-823f-94ff61831e6f', N'CM00025', N'เพลง บางปะกอก', N'https://www.youtube.com/watch?v=HPkXbLRGdc0', N'HPkXbLRGdc0', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00C8645C AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (26, 32, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', N'CM00026', N'ฟัง - แสนดี', N'https://youtu.be/VLExy7hwgdw', N'VLExy7hwgdw', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00D33061 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (27, 33, N'a20ad65b-ba1f-48bc-823f-94ff61831e6f', N'CM00027', N'เพลง คนเก่งฟ้าประทาน', N'https://www.youtube.com/watch?v=aUeQe7TDu0o', N'aUeQe7TDu0o', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00E42EAA AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (28, 34, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', N'CM00028', N'ลูกผู้ชายขาดเธอไม่ตาย Next T-SER', N'https://youtu.be/c1atjjMtcgo', N'c1atjjMtcgo', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00ED77F2 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (29, 35, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', N'CM00029', N'หากเธอเป็นรักจริง Next T-SER', N'https://youtu.be/HGCZ2rZNVyo', N'HGCZ2rZNVyo', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00F50613 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (30, 36, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', N'CM00030', N'ฐานะคนช่างจำ Next T-SER', N'https://youtu.be/NrEBtT32sCU', N'NrEBtT32sCU', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00F9C005 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (31, 42, N'f32f8c0b-e069-4f80-9c2e-24785747c607', N'CM00031', N'มีบ้างไหมใคร', N'https://youtu.be/A4N6cA07N88', N'A4N6cA07N88', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B011C1ED8 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (32, 43, N'f32f8c0b-e069-4f80-9c2e-24785747c607', N'CM00032', N'ยังรอ', N'https://youtu.be/A4N6cA07N88', N'A4N6cA07N88', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B011C509D AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (33, 43, N'f32f8c0b-e069-4f80-9c2e-24785747c607', N'CM00033', N'ยังรอ', N'https://youtu.be/A4N6cA07N88', N'A4N6cA07N88', 0, NULL, NULL, 0, NULL, CAST(0x0000A53B011C531A AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (34, 43, N'f32f8c0b-e069-4f80-9c2e-24785747c607', N'CM00034', N'ยังรอ', N'https://youtu.be/A4N6cA07N88', N'A4N6cA07N88', 0, NULL, NULL, 0, NULL, CAST(0x0000A53B011C5400 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (35, 41, N'f32f8c0b-e069-4f80-9c2e-24785747c607', N'CM00035', N'ครั้งหนึ่งของใจ', N'https://youtu.be/y-q50sNb27Y', N'y-q50sNb27Y', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B011C8449 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (36, 45, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', N'CM00036', N'ฝั่งฝัน', N'https://www.youtube.com/watch?v=bphHNFsx7CM', N'bphHNFsx7CM', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B01620FB9 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (37, 49, N'aa41753e-1465-4a27-8a3d-20c1652178d4', N'CM00037', N'แอบรัก', N'https://youtu.be/sjRteb5Mwps', N'sjRteb5Mwps', 1, NULL, NULL, 1, NULL, CAST(0x0000A53C011D518C AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (38, 50, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', N'CM00038', N'คนไม่เอาไหน Next T-SER', N'https://youtu.be/SB1LE0J72Dw', N'SB1LE0J72Dw', 1, NULL, NULL, 1, NULL, CAST(0x0000A53C012AD1CD AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (39, 14, N'c6ac2778-8630-4dd1-88ef-59f61033e4bb', N'CM00039', N'Diamond - อาจได้ยิน', N'https://www.youtube.com/watch?v=Fq9hbV7h8hQ', N'Fq9hbV7h8hQ', 1, NULL, NULL, 1, NULL, CAST(0x0000A53C0168439A AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (40, 54, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', N'CM00040', N'กลับตัวกลับใจ Next T-SER', N'https://youtu.be/rbpgviAIVlk', N'rbpgviAIVlk', 1, NULL, NULL, 1, NULL, CAST(0x0000A53C016B600E AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (41, 55, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', N'CM00041', N'ชีวิตดินๆลูกชาวนา Next T-SER', N'https://youtu.be/e3xzG0akSSw', N'e3xzG0akSSw', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D00A62D50 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (42, 56, N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', N'CM00042', N'ทิ้งฉันในวันเกิด', N'https://www.youtube.com/watch?v=Wmitob541Rg&feature=youtu.be', N'Wmitob541Rg', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D00AAF90E AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (43, 61, N'30fbd33b-f19c-42f1-a544-c275fe2076bb', N'CM00043', N'ไม่ต้องจดจำ', N'https://www.youtube.com/watch?v=M0ss8JGc-FU', N'M0ss8JGc-FU', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D00E56736 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (44, 60, N'30fbd33b-f19c-42f1-a544-c275fe2076bb', N'CM00044', N'ร้องไห้เป็นเพลง', N'https://www.youtube.com/watch?v=tqaBR0R3IXw', N'tqaBR0R3IXw', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D00E596B1 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (45, 59, N'30fbd33b-f19c-42f1-a544-c275fe2076bb', N'CM00045', N'ตามหา', N'https://www.youtube.com/watch?v=AQgEohkWpds', N'AQgEohkWpds', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D00E5C177 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (46, 58, N'f01815e6-883a-44bb-82d7-66ab9bce1863', N'CM00046', N'มีฉันอีกคน', N'https://www.youtube.com/watch?v=p0FUh6Dy3_0', N'p0FUh6Dy3_0', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D00FE49E3 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (47, 13, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', N'CM00047', N'อย่าเลย', N'https://www.youtube.com/watch?v=C9OjDO6gwxQ', N'C9OjDO6gwxQ', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D01658CE1 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (48, 8, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', N'CM00048', N'ข้องใจ เข้าใจ', N'https://www.youtube.com/watch?v=0cqOsfOeAPA', N'0cqOsfOeAPA', 1, NULL, NULL, 0, NULL, CAST(0x0000A53D0165D190 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (49, 72, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', N'CM00049', N'คืนเหงา', N'https://www.youtube.com/watch?v=l9bNg7jmms0', N'l9bNg7jmms0', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D0169EBF8 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (50, 63, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', N'CM00050', N'พรุ่งนี้ - DUST', N'https://www.youtube.com/watch?v=Za29zLR--Pk', N'Za29zLR--Pk', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E00243460 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (51, 64, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', N'CM00051', N'ยังคง - DUST', N'https://www.youtube.com/watch?v=YjDDgIkR6x8', N'YjDDgIkR6x8', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E00244D63 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (52, 65, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', N'CM00052', N'เกินรับไหว - DUST', N'https://www.youtube.com/watch?v=tiieqzefsNg', N'tiieqzefsNg', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E0024608F AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (53, 68, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', N'CM00053', N'เพราะอะไร - DUST', N'https://www.youtube.com/watch?v=7j8-2d_nHBw', N'7j8-2d_nHBw', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E002499FB AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (54, 66, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', N'CM00054', N'ลบความจำ - DUST', N'https://www.youtube.com/watch?v=kcA-UVTcpOg', N'kcA-UVTcpOg', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E0024B62A AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (55, 67, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', N'CM00055', N'ลืมมันไป - DUST', N'https://www.youtube.com/watch?v=BTL07-8G0DQ', N'BTL07-8G0DQ', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E0024CD1B AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (56, 76, N'71ebf717-7aa7-459e-b803-2dbc75a2f864', N'CM00056', N'ไฟเผาใจ', N'https://youtu.be/yCfC6rEG7so', N'yCfC6rEG7so', 1, NULL, NULL, 0, NULL, CAST(0x0000A53E005180D6 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (57, 76, N'71ebf717-7aa7-459e-b803-2dbc75a2f864', N'CM00057', N'ไฟเผาใจ', N'http://youtu.be/yCfC6rEG7so', N'yCfC6rEG7so', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E00549B72 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Contest] ([int_song_contest_id], [int_song_original_id], [int_user_id], [txt_vote_code], [txt_song_name], [txt_youtube_url], [txt_youtube_id], [is_approved], [date_approved], [int_admin_approved_id], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (58, 80, N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', N'CM00058', N'รักบ่รักบ่รู้ อ้ายเลิกเจ้าชู้เพราะเจ้า', N'https://www.youtube.com/watch?v=Nw_hpyzSBek', N'Nw_hpyzSBek', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E00FF74DD AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[Song_Contest] OFF
GO
SET IDENTITY_INSERT [dbo].[Song_Original] ON 

GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (1, N'bfebc6ac-68c6-4032-a382-d2b1bf13b788', N'mimi', N'https://www.youtube.com/watch?v=nlt5Wa13fFU', N'', N'/Upload/SongOriginals/20151006190718Cyberry-ER.pdf', 1, NULL, NULL, 0, NULL, CAST(0x0000A529013B24A9 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (2, N'89dec678-e0b1-491e-ac89-250725b5c517', N'แค่เจอเธอในฝัน', N'https://www.youtube.com/watch?v=zWcEhhqWd6g', N'', N'/Upload/SongOriginals/20151008223706แค่เจอเธอในฝัน.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A52B0174C75E AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (3, N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', N'ต่อจากนี้', N'https://www.youtube.com/watch?v=H9NR8jP4-cc', N'', N'/Upload/SongOriginals/20151008234517ต่อจากนี้.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A52B01877FEA AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (4, N'd3022e10-2bcf-41fc-8068-a6daff456957', N'เสียง', N'https://www.youtube.com/watch?v=lsqHExkMRAw', N'', N'/Upload/SongOriginals/20151014185815เสียง.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A5310138BBB5 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (5, N'd3022e10-2bcf-41fc-8068-a6daff456957', N'Mask', N'https://www.youtube.com/watch?v=UtT1CkAVQb0', N'', N'/Upload/SongOriginals/20151014190516Mask.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A531013A9302 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (6, N'66dbb691-4e42-4417-8283-a2c6571fea7e', N'ทดสอบ', N'https://www.youtube.com/watch?v=ZceMj_Tcu-g', N'', N'/Upload/SongOriginals/20151021133138Kinect_ref_ใส่บท2.pdf', 1, NULL, NULL, 0, NULL, CAST(0x0000A53800DEF19F AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (7, N'9a67901b-8c64-4f79-88bc-111f5624e0c5', N'หลอกตัวเอง', N'https://www.youtube.com/watch?v=ryyPPXBn9QE', N'', N'/Upload/SongOriginals/20151023163800หลอกตัวเอง.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53A011230EE AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (8, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', N'ข้องใจ เข้าใจ', N'', N'https://soundcloud.com/chawkaew-fly/9wwtgmn3vfui', N'/Upload/SongOriginals/20151023190017ข้องใจ เข้าใจ.pdf', 1, NULL, NULL, 0, NULL, CAST(0x0000A53A01395CE8 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (9, N'fb77b5b8-a47c-46f5-9e71-ac5dfb970df1', N'หัวถึงหมอน - เคเดนซ์ (Cadence)', N'https://www.youtube.com/watch?v=iUlwqk1J_4M', N'', N'/Upload/SongOriginals/20151023232146หัวถึงหมอน.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53A018104BF AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (10, N'da6f13e9-449e-47ec-bbd9-deec4bdd82e8', N'รหัสลับของคนหลายใจ', N'https://www.youtube.com/watch?v=ULRgyJz4hEQ', N'', N'/Upload/SongOriginals/20151023232227เนื้อเพลง รหัสลับของคนหลายใจ.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53A01813ADB AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (11, N'da6f13e9-449e-47ec-bbd9-deec4bdd82e8', N'รหัสลับของคนหลายใจ', N'https://www.youtube.com/watch?v=ULRgyJz4hEQ', N'', N'/Upload/SongOriginals/20151023232329เนื้อเพลง รหัสลับของคนหลายใจ.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53A01817E20 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (12, N'da6f13e9-449e-47ec-bbd9-deec4bdd82e8', N'รหัสลับของคนหลายใจ', N'https://www.youtube.com/watch?v=ULRgyJz4hEQ', N'', N'/Upload/SongOriginals/20151023232512เนื้อเพลง รหัสลับของคนหลายใจ.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53A0181FCCD AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (13, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', N'อย่าเลย', N'', N'https://soundcloud.com/chawkaew-fly/02-chawkaew-walker', N'/Upload/SongOriginals/20151024001043อย่าเลย.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00030553 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (14, N'c6ac2778-8630-4dd1-88ef-59f61033e4bb', N'Diamond - อาจได้ยิน', N'https://www.youtube.com/watch?v=Fq9hbV7h8hQ', N'', N'/Upload/SongOriginals/20151024002807อาจได้ยิน.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B0007BD17 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (15, N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'คิดไม่เหมือนกัน - พี่เกม สู้ๆนะครับ', N'https://www.youtube.com/watch?v=yYszNdkUwuU', N'', N'/Upload/SongOriginals/20151024022422New เอกสาร Microsoft Word.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B0027A999 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (16, N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'คุณมีหนึ่งคำร้องขอ ( ... sent you a friend request) - พี่เกม สู้ๆนะครับ', N'https://www.youtube.com/watch?v=5DMChjWv8w0', N'', N'/Upload/SongOriginals/20151024023346New เอกสาร Microsoft Word.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B002A4A88 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (17, N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'Gift (ของขวัญ ของฉัน) - พี่เกม สู้ๆนะครับ', N'https://www.youtube.com/watch?v=bjNd0bcrJaE&index=1&list=PLP2acz0kSnmiRflRrAjFp1pyUn5qxbF6t', N'', N'/Upload/SongOriginals/20151024024042New เอกสาร Microsoft Word.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B002C4547 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (18, N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'ได้สักครั้ง...จะตั้งใจเรียน - พี่เกม สู้ๆนะครับ', N'https://www.youtube.com/watch?v=uM6iOUlPLRk', N'', N'/Upload/SongOriginals/20151024024901New เอกสาร Microsoft Word.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B002E8952 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (19, N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'ฉันคนหนึ่ง...ไม่ซึ้งวันลอยกระทง - พี่เกม สู้ๆนะครับ', N'https://www.youtube.com/watch?v=F5oT91ilGFA', N'', N'/Upload/SongOriginals/20151024030132New เอกสาร Microsoft Word.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B0031E055 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (20, N'875d9abb-b8bd-42ab-8fbe-a222b53b0608', N'ปาท่องโก๋', N'https://www.youtube.com/watch?v=4ROjOUHm554', N'', N'/Upload/SongOriginals/20151024034625ปาท่องโก๋.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B003E3725 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (21, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'จากกันวันนี้ดีกว่า - devanfore', N'https://youtu.be/XuR6QGtMZQI', N'', N'/Upload/SongOriginals/20151024081708จากกันวันนี้ดีกว่า.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00888EFC AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (22, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'รักบังตา - devanfore', N'https://youtu.be/pDPpuU7_AWE', N'', N'/Upload/SongOriginals/20151024082326รักบังตา.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008A5DB2 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (23, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'ไม่เคยลืม - Memoir', N'https://youtu.be/WkYxIqCP36k', N'', N'/Upload/SongOriginals/20151024082649ไม่เคยลืม.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008B35CC AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (24, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'เพื่อน - devanfore', N'https://youtu.be/cxCmFTRgNbo', N'', N'/Upload/SongOriginals/20151024082913เพื่อน.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008BDE79 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (25, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'พรุ่งนี้จะเป็นยังไง - devanfore', N'https://youtu.be/QkQWb1JMhOY', N'', N'/Upload/SongOriginals/20151024083241พรุ่งนี้จะเป็นยังไง.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008CD179 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (26, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'ใจเสือกลายเป็นแมว - Memoir', N'https://youtu.be/nz5xoUL_-ig', N'', N'/Upload/SongOriginals/20151024083453ใจเสือกลายเป็นแมว.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008D6D3D AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (27, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'เราจะเดินไปด้วยกัน - devanfore', N'https://youtu.be/kLwfio4bETA', N'', N'/Upload/SongOriginals/20151024083807เราจะเดินไปด้วยกัน.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008E4F8A AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (28, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'มนต์รักคนใจดำ - Memoir', N'https://youtu.be/AfYaqRs3Yww', N'', N'/Upload/SongOriginals/20151024084044มนต์รักคนใจดำ.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008F087F AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (29, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'ชก.หลายใจ - Memoir', N'https://youtu.be/_r-oMf4Lyeo', N'', N'/Upload/SongOriginals/20151024084305ชก.หลายใจ.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B008FAEAA AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (30, N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'คืนเหงา - devanfore', N'https://youtu.be/MGWPrK_UTXw', N'', N'/Upload/SongOriginals/20151024084510คืนเหงา.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B0090402E AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (31, N'a20ad65b-ba1f-48bc-823f-94ff61831e6f', N'เพลง บางปะกอก', N'https://www.youtube.com/watch?v=HPkXbLRGdc0', N'', N'/Upload/SongOriginals/20151024120743เพลง บางปะกอก.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00C7E8CD AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (32, N'01f96c09-09ca-47d5-9f76-a87acea70f0c', N'ฟัง - แสนดี', N'https://youtu.be/VLExy7hwgdw', N'', N'ฟัง - แสนดี', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00D2A110 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (33, N'a20ad65b-ba1f-48bc-823f-94ff61831e6f', N'เพลง คนเก่งฟ้าประทาน', N'https://www.youtube.com/watch?v=aUeQe7TDu0o', N'', N'/Upload/SongOriginals/20151024134901เพลง คนเก่งฟ้าประทาน.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00E3B905 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (34, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', N'ลูกผู้ชายขาดเธอไม่ตาย Next T-SER', N'https://youtu.be/c1atjjMtcgo', N'', N'/Upload/SongOriginals/20151024140740ลูกผู้ชายขาดเธอไม่ตาย(1).pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00E8DA54 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (35, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', N'หากเธอเป็นรักจริง Next T-SER', N'https://youtu.be/HGCZ2rZNVyo', N'', N'/Upload/SongOriginals/20151024145013หากเธอเป็นรักจริง Next T-SER.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00F4C350 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (36, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', N'ฐานะคนช่างจำ Next T-SER', N'https://youtu.be/NrEBtT32sCU', N'', N'/Upload/SongOriginals/20151024150820ฐานะคนช่างจำ Next T-SER.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B00F97EA7 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (37, N'19b9e67d-5c7f-4265-b824-2af33fd84965', N'เพลง ปลิว', N'https://www.youtube.com/watch?v=4J9jBoukUQA', N'', N'/Upload/SongOriginals/20151024155030เนื้อเพลง ปลิว.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B01051302 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (38, N'19b9e67d-5c7f-4265-b824-2af33fd84965', N'เพลง หนึ่งค่ำคืน', N'https://www.youtube.com/watch?v=F_bzVQyVLNs', N'', N'/Upload/SongOriginals/20151024155929เนื้อเพลง หนึ่งค่ำคืน.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B01078AF1 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (39, N'19b9e67d-5c7f-4265-b824-2af33fd84965', N'เพลง Every girl', N'https://www.youtube.com/watch?v=ocfXqTrKRJY', N'', N'/Upload/SongOriginals/20151024160136เนื้อเพลง Every girl.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B01081F05 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (40, N'19b9e67d-5c7f-4265-b824-2af33fd84965', N'เพลง Rewind', N'https://www.youtube.com/watch?v=yPyDXTkJzzk', N'', N'/Upload/SongOriginals/20151024160409เนื้อเพลง Rewind.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B0108D617 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (41, N'f32f8c0b-e069-4f80-9c2e-24785747c607', N'ครั้งหนึ่งของใจ', N'https://youtu.be/y-q50sNb27Y', N'', N'/Upload/SongOriginals/20151024171019ครั้งหนึ่งของใจ.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B011B0483 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (42, N'f32f8c0b-e069-4f80-9c2e-24785747c607', N'มีบ้างไหมใคร', N'https://youtu.be/BKwiKzjRMYM', N'', N'/Upload/SongOriginals/20151024171248ครั้งหนึ่งของใจ.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B011BACDC AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (43, N'f32f8c0b-e069-4f80-9c2e-24785747c607', N'ยังรอ', N'https://youtu.be/A4N6cA07N88', N'', N'/Upload/SongOriginals/20151024171353ครั้งหนึ่งของใจ.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B011BF958 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (44, N'f32f8c0b-e069-4f80-9c2e-24785747c607', N'มีบ้างไหมใคร', N'https://youtu.be/BKwiKzjRMYM', N'', N'/Upload/SongOriginals/20151024173600ครั้งหนึ่งของใจ.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B01220D08 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (45, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', N'ฝั่งฝัน', N'https://www.youtube.com/watch?v=bphHNFsx7CM', N'', N'/Upload/SongOriginals/20151024212613ฝั่งฝัน.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53B0161AB61 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (46, N'1930dee3-f415-4374-b428-e4f56a493861', N'ดื่มเพื่อลืมเธอ', N'https://youtu.be/0zFbnPIBQl8', N'', N'/Upload/SongOriginals/20151025145345ดื่มเพื่อลืมเธอ.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53C00F5C81D AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (49, N'aa41753e-1465-4a27-8a3d-20c1652178d4', N'แอบรัก', N'https://youtu.be/sjRteb5Mwps', N'', N'/Upload/SongOriginals/20151025171709แอบรัก.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53C011CF8FF AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (50, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', N'คนไม่เอาไหน Next T-SER', N'https://youtu.be/SB1LE0J72Dw', N'', N'/Upload/SongOriginals/20151025180549คนไม่เอาไหน Next T-SER.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53C012A41C9 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (51, N'c485ae2e-ab5f-4ca1-a3f9-992ea9cdd079', N'นวลอนงค์ - TRIP BAND', N'https://www.youtube.com/watch?v=VGOP_-ITjFA', N'', N'/Upload/SongOriginals/20151025210552เพลง นวลอนงค์.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53C015BB4D4 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (52, N'c485ae2e-ab5f-4ca1-a3f9-992ea9cdd079', N'เก็บ - TRIP BAND', N'https://www.youtube.com/watch?v=rWca2OMKRCg', N'', N'/Upload/SongOriginals/20151025212935เพลง  เก็บ.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53C016235F0 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (53, N'c485ae2e-ab5f-4ca1-a3f9-992ea9cdd079', N'ยังคิดถึง - TRIP BAND', N'https://www.youtube.com/watch?v=M5XJYrocYPw', N'', N'/Upload/SongOriginals/20151025213922เพลง ยังคิดถึง.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53C0164E5CD AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (54, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', N'กลับตัวกลับใจ Next T-SER', N'https://youtu.be/rbpgviAIVlk', N'', N'/Upload/SongOriginals/20151025220024กลับตัวกลับใจ Next T-SER.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53C016AEF9C AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (55, N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', N'ชีวิตดินๆลูกชาวนา Next T-SER', N'https://youtu.be/e3xzG0akSSw', N'', N'/Upload/SongOriginals/20151026100325ชีวิตดินๆลูกชาวนา Next T-SER.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D00A5C4C7 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (56, N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', N'ทิ้งฉันในวันเกิด', N'https://www.youtube.com/watch?v=Wmitob541Rg&feature=youtu.be', N'', N'/Upload/SongOriginals/20151026102103เพลง ทิ้งฉันในวันเกิด.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D00AA99E0 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (57, N'1930dee3-f415-4374-b428-e4f56a493861', N'ขวัญใจเด็กแว้น', N'https://youtu.be/31w09hde7d0', N'', N'/Upload/SongOriginals/20151026103549ดื่มเพื่อลืมเธอ.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D00AF374B AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (58, N'f01815e6-883a-44bb-82d7-66ab9bce1863', N'มีฉันอีกคน', N'https://www.youtube.com/watch?v=p0FUh6Dy3_0', N'', N'/Upload/SongOriginals/20151026110044มีฉันอีกคน.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D00B581B1 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (59, N'30fbd33b-f19c-42f1-a544-c275fe2076bb', N'ตามหา', N'https://www.youtube.com/watch?v=AQgEohkWpds', N'', N'/Upload/SongOriginals/20151026133941ตามหา.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D00E126AF AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (60, N'30fbd33b-f19c-42f1-a544-c275fe2076bb', N'ร้องไห้เป็นเพลง', N'https://www.youtube.com/watch?v=tqaBR0R3IXw', N'', N'/Upload/SongOriginals/20151026134651ร้องไห้เป็นเพลง.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D00E31D4F AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (61, N'30fbd33b-f19c-42f1-a544-c275fe2076bb', N'ไม่ต้องจดจำ', N'https://www.youtube.com/watch?v=M0ss8JGc-FU', N'', N'/Upload/SongOriginals/20151026135050ไม่ต้องจดจำ.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D00E433FA AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (62, N'5ef3dc15-82d6-4179-97ea-7b1dafabc1f7', N'คงต้องทำใจ', N'https://www.youtube.com/watch?v=5dHhUvp8RrY', N'', N'/Upload/SongOriginals/20151026145651Doc1.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D00FAA2C4 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (63, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', N'พรุ่งนี้ - DUST', N'https://www.youtube.com/watch?v=Za29zLR--Pk', N'', N'/Upload/SongOriginals/20151026162937เพลง พรุ่งนี้.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D010FDA7D AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (64, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', N'ยังคง - DUST', N'https://www.youtube.com/watch?v=YjDDgIkR6x8', N'', N'/Upload/SongOriginals/20151026164133ยังคง.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D01131B96 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (65, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', N'เกินรับไหว - DUST', N'https://www.youtube.com/watch?v=tiieqzefsNg', N'', N'/Upload/SongOriginals/20151026164958เกินรับไหว.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D01156777 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (66, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', N'ลบความจำ - DUST', N'https://www.youtube.com/watch?v=kcA-UVTcpOg', N'', N'/Upload/SongOriginals/20151026165500ลบความจำ.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D0116C948 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (67, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', N'ลืมมันไป - DUST', N'https://www.youtube.com/watch?v=BTL07-8G0DQ', N'', N'/Upload/SongOriginals/20151026170205ลืมมันไป.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D0118BAD0 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (68, N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', N'เพราะอะไร - DUST', N'https://www.youtube.com/watch?v=7j8-2d_nHBw', N'', N'/Upload/SongOriginals/20151026170809เพราะอะไร.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D011A65DF AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (69, N'1930dee3-f415-4374-b428-e4f56a493861', N'สุดท้ายคือเธอ', N'https://youtu.be/kfjczMNRs-8', N'', N'/Upload/SongOriginals/20151026171642เพลง สุดท้ายคือเธอ.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D011CC350 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (70, N'1930dee3-f415-4374-b428-e4f56a493861', N'ขวัญใจเด็กแว้น ( อะคูสติคเวอร์ชั่น)', N'https://youtu.be/7rPtIlaGR48', N'', N'/Upload/SongOriginals/20151026173031ขวัญใจเด็กแว้น.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D0120C0B7 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (71, N'5c7c8473-93ee-420d-a7eb-7cc917ed19bf', N'ขออภัย', N'https://www.youtube.com/watch?v=mhIkrP8MbI0', N'', N'/Upload/SongOriginals/20151026214743เพลง ขออภัย.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D01673942 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (72, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', N'คืนเหงา', N'', N'https://soundcloud.com/chawkaew-fly/01-single', N'/Upload/SongOriginals/20151026215031คืนเหงา.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D0167F760 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (73, N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', N'คืนเหงา', N'', N'https://soundcloud.com/chawkaew-fly/01-single', N'/Upload/SongOriginals/20151026215031คืนเหงา.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53D0167F781 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (74, N'b882b15e-bfd2-4e5e-a027-b9d7c0822517', N'ผิดทีเราเจอกันช้าไป', N'https://youtu.be/E8wPTAqO110', N'', N'/Upload/SongOriginals/20151027021408เพลง ผิดที่เราเจอกันช้าไป.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E0024E27E AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (75, N'b882b15e-bfd2-4e5e-a027-b9d7c0822517', N'ผิดที่เราเจอกันช้าไป', N'https://youtu.be/E8wPTAqO110', N'', N'/Upload/SongOriginals/20151027021512เพลง ผิดที่เราเจอกันช้าไป.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E00252757 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (76, N'71ebf717-7aa7-459e-b803-2dbc75a2f864', N'ไฟเผาใจ', N'https://youtu.be/yCfC6rEG7so', N'', N'/Upload/SongOriginals/20151027045019เพลง ไฟเผาใจ.docx', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E00505DE7 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (77, N'5c7c8473-93ee-420d-a7eb-7cc917ed19bf', N'Memory', N'https://www.youtube.com/watch?v=hB8fQoL-M5c', N'', N'/Upload/SongOriginals/20151027124849เพลง Memory.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E00D32CBE AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (78, N'd3022e10-2bcf-41fc-8068-a6daff456957', N'เสียง', N'https://www.youtube.com/watch?v=lsqHExkMRAw', N'', N'/Upload/SongOriginals/20151027133013เสียง.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E00DE904D AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (79, N'23d7157e-b307-4759-94a7-2b2cce185ff0', N'คิดไปเองเมื่อชาติต้องการ', N'https://www.youtube.com/watch?v=ECFsUFiZYfU', N'', N'/Upload/SongOriginals/20151027144351คิดไปเองเมื่อชาติต้องการ.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E00F2DA27 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (80, N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', N'รักบ่รักบ่รู้อ้ายเลิกเจ้าชู้เพราะเจ้า', N'https://www.youtube.com/watch?v=Nw_hpyzSBek', N'', N'/Upload/SongOriginals/20151027152849รักบ่รักบ่รู้.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E00FF2B6B AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (81, N'f1c62d2f-85d3-49a3-88f6-5ba4c0d520bb', N'Slow life  / By  Flow-view', N'https://www.youtube.com/watch?v=EdS6LUcorGU', N'', N'/Upload/SongOriginals/20151027162725เพลง   Slow life.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E010F90E7 AS DateTime), NULL)
GO
INSERT [dbo].[Song_Original] ([int_song_original_id], [int_user_id], [txt_songname], [txt_youtube_url], [txt_soundcloud_url], [txt_lyric_file], [is_approved], [date_approved], [int_admin_approved], [is_active], [is_edittor_pick], [datetime_create], [datetime_modified]) VALUES (82, N'f1c62d2f-85d3-49a3-88f6-5ba4c0d520bb', N'อิจฉาควาย  / By Flow-view', N'https://www.youtube.com/watch?v=8VmSChTwklM', N'', N'/Upload/SongOriginals/20151027165448เพลง   อิจฉาควาย.pdf', 1, NULL, NULL, 1, NULL, CAST(0x0000A53E0116C299 AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[Song_Original] OFF
GO
SET IDENTITY_INSERT [dbo].[Subscribed] ON 

GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (1, N'Thai', N'test@email.com', CAST(0x0000A5230106DCEE AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (2, N'Thai', N'admin@email.com', CAST(0x0000A52301075C9B AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (3, N'sfsdf', N'thainingmaster@hotmail.com', CAST(0x0000A523011371FB AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (4, N'sfsdf', N'sdf', CAST(0x0000A52301139C36 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (5, N'sdfsdfsdfs', N'sdfsdf', CAST(0x0000A5230113A371 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (6, N'paadaa', N'palida_pla@hotmail.com', CAST(0x0000A52D000D1B73 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (7, N'Maxcat', N'Mag5032@hotmail.com', CAST(0x0000A52C016CBCC2 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (8, N'Maxcat', N'Mag5032@hotmail.com', CAST(0x0000A52C016CECC5 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (9, N'Maxcat', N'Mag5032@hotmail.com', CAST(0x0000A52C016DDAEA AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (10, N'Manachai', N'Mag5032@hotmail.com', CAST(0x0000A52C016FC0ED AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (11, N'เณร', N'Somsaknen5599@gmail.com', CAST(0x0000A5310062004B AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (12, N'Somsak', N'Somsaknen5599@gmail.com', CAST(0x0000A5310064829F AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (13, N'Somsak', N'Somsaknen5599@gmail.com', CAST(0x0000A5310064CB0B AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (14, N'Mild', N'Mildaf13@hotmail.com', CAST(0x0000A53300E3B15A AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (15, N'charnchai paluek', N'jekclup456789@gmail.com', CAST(0x0000A53901451468 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (16, N'แปดกันยา', N'cccoooppp.com@gmail.com', CAST(0x0000A53A013ABBB8 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (17, N'แปดกันยา', N'cccoooppp.com@gmail.com', CAST(0x0000A53A013AE45A AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (18, N'Rainemotion11', N'Emotionbland@hotmail.com', CAST(0x0000A53A01401A71 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (19, N'Lovely dear', N'Hathaikarn_suk@gmail.com', CAST(0x0000A53A0152A616 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (20, N'Amarin sairum', N'eic-comtun@hotmail.com', CAST(0x0000A53A0152FF88 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (21, N'SoodPhroad', N'hathaikarn.suk@gmail.com', CAST(0x0000A53A01558B7B AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (22, N'ภัทธเดช แก้ววิเชียร', N'theraytheray2009@hotmail.com', CAST(0x0000A53A01684706 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (23, N' Kritsanaxx', N'Kritsanaxx@gmail.com', CAST(0x0000A53A016FECF5 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (24, N'beamtoyou', N'beamtoyou@gmail.com', CAST(0x0000A53A017DA3F0 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (25, N'sanit', N'sanipatcharakamonwong@gmail.com', CAST(0x0000A53A017EE634 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (26, N'sanit', N'sanipatcharakamonwong@gmail.com', CAST(0x0000A53A017EF203 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (27, N'sanit', N'sanipatcharakamonwong@gmail.com', CAST(0x0000A53A017F01A2 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (28, N'sanit', N'sanipatcharakamonwong@gmail.com', CAST(0x0000A53A017F3053 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (29, N'SanitPatchara', N'sanipatcharakamonwong@gmail.com', CAST(0x0000A53A017FC94B AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (30, N'sanit', N'sanit_bt@hotmail.com', CAST(0x0000A53A01823D9E AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (31, N'indyman', N'sanit_bt@hotmail.com', CAST(0x0000A53A018274F0 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (32, N'ภัทธเดช  แก้ววิเชียร', N'theraytheray2009@hotmail.com', CAST(0x0000A53A01892F7C AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (33, N'theray', N'theraytheray2009@hotmail.com', CAST(0x0000A53B00014289 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (34, N'ภัทธเดช แก้ววิเชียร', N'theratheray2009@hotmail.com', CAST(0x0000A53B00021119 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (35, N'Black Blues', N'winai7273@gmail.com', CAST(0x0000A53B009D27CB AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (36, N'Black Blues', N'winai7273@gmail.com', CAST(0x0000A53B009D75A7 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (37, N'Black Blues', N'winai7273@gmail.com', CAST(0x0000A53B009D7612 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (38, N'ภัทธเดช', N'theraytheray2009@hotmail.com', CAST(0x0000A53B00A7C2D5 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (39, N'ภัทธเดช', N'pattadech2015@gmail.com', CAST(0x0000A53B00AD8A4B AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (40, N'นภดล นาคดี', N'porza_ha5@hotmail.com', CAST(0x0000A53B00AEEA21 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (41, N'Next T-SER', N'prasit-next@hotmail.com', CAST(0x0000A53B00B8633D AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (42, N'jame', N'jame456325@gmail.com', CAST(0x0000A53B00CEA3FE AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (43, N'pongdat', N'pongdat2011@gmail.com', CAST(0x0000A53B00D0D59D AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (44, N'pongdat', N'pongdat2011@gmail.com', CAST(0x0000A53B00D0F42D AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (45, N'top49130', N'pongdat2011@gmail.com', CAST(0x0000A53B00D13B6C AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (46, N'jame lovemusic', N'jame456325@gmail.com', CAST(0x0000A53B00D491F5 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (47, N'jame lovemusic', N'jamelovemusiclove@gmail.com', CAST(0x0000A53B00D4E151 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (48, N'sanit', N'sanipatcharakamonwong@gmail.com', CAST(0x0000A53B00D61438 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (49, N'Songkrant', N'Bangpakok1@hotmail.com', CAST(0x0000A53B00E39AB7 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (50, N'Songkrant', N'Bangpakok1@hotmail.com', CAST(0x0000A53B00E39B07 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (51, N'topten', N'Nickkypan77@gmail.com', CAST(0x0000A53B00F9410E AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (52, N'Top''pten', N'pongdat2011@gmail.com', CAST(0x0000A53B0100CCA3 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (53, N'Panudet   Singon', N'Nu_123.com@hotmail.com', CAST(0x0000A53C0014C81E AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (54, N'Chanathinart', N'zacarlosza@hotmail.com', CAST(0x0000A53C0125B4D5 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (55, N'Chanathinart', N'zacarlosza@hotmail.com', CAST(0x0000A53C0125C94D AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (56, N'Bontvoice', N'b_0_nt@hotmail.com', CAST(0x0000A53C0133D6E0 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (57, N'bontvoice', N'b_0_nt@hotmail.com', CAST(0x0000A53C0133F825 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (58, N'Tomorrow', N'tk_nung@hotmail.com', CAST(0x0000A53C01594404 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (59, N'warachai', N'fookfick@hotmail.com', CAST(0x0000A53D00F405A8 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (60, N'พีพีเสนาะอุบลลัมเปอร์', N'toyota_2518@hotmail.com', CAST(0x0000A53D0145F149 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (61, N'sanit', N'sanipatcharakamonwong@gmail.com', CAST(0x0000A53E000C451B AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (62, N'sanit', N'sanipatcharakamonwong@gmail.com', CAST(0x0000A53E000FCB94 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (63, N'sanit', N'sanitpatcharakamonwong@gmail.comt', CAST(0x0000A53E00122015 AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (64, N'sanit', N'sanipatcharakamonwong@gmail.com', CAST(0x0000A53E001CB08A AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (65, N'saen_ruk', N'saen_ruk@hotmail.com', CAST(0x0000A53E008E162E AS DateTime))
GO
INSERT [dbo].[Subscribed] ([int_subscribed_id], [txt_name], [txt_email], [datetime_create]) VALUES (66, N'flameart', N'flameart466@gmail.com', CAST(0x0000A53E0174B44A AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Subscribed] OFF
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'01f96c09-09ca-47d5-9f76-a87acea70f0c', N'do-gm-on-st-er_tum@hotmail.com', 1, N'AE4EWd2TorZTew7BSChbZlPVmyzMCi5I8pzoY53ypt7SDuRUn9jol21UeH5lVn9vYA==', N'c6dfbb7e-642b-4905-89ce-bb57a43d161a', NULL, 0, 0, NULL, 1, 0, N'sunthreeband', N'วงแสนดี', N'แสนดี', NULL, N'/Upload/Images/20151024123537FB_IMG_1440917527400.jpg', 1, 0, 1, 0, NULL, 1, NULL, N'4062893485', N'0960608499', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00007FF800000000 AS DateTime), N'135/20 พุทธมณฑลสาย4 ต.ศาลายา อ.พุทธมณฑล จ.นครปฐม 73170', NULL, NULL, CAST(0x0000A53B00CC3D4E AS DateTime), CAST(0x0000A53B00CF96C1 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'02638332-a6a3-42c3-84b9-4d49ec910a5d', N'beamtoyou@gmail.com', 0, N'ALjHBCm2cQBu3QfH4OwseTJMkqLdsAUGD1AufjdMkhI7VaGzgwQIfe+wyzeRP+nbiw==', N'b2821d34-357f-4eac-a96f-461a657aeb4b', NULL, 0, 0, NULL, 1, 0, N'beamtoyou', N'ิณัฐพงษ์ ทิพย์วิจารย์', N'ิบีม', NULL, NULL, 1, 0, 1, 0, NULL, 2, NULL, N'7322931454', N'0862016500', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53700000000 AS DateTime), N'241 ม.1 ต.ห้วยแก้ว อ.ภูกามยาว จ.พะเยา 56000', NULL, NULL, CAST(0x0000A53A017D81E6 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'028dc1c0-9c9a-456f-aa6a-601229058f0f', N'suttiraktammappatto@hotmail.com', 0, N'AN13SD9qRPPXnPrq1reziV2EFDnWKdfXrJ1u19KpLHw9v35zjet+cC22Gkf/yvEfng==', N'353ead7e-e2f4-4a0e-ad8f-dd1719cd22b5', NULL, 0, 0, NULL, 1, 0, N'ปาปิรันย่า', N'Suttirak', N'Tammapatto', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'+66844788351', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00007AC700000000 AS DateTime), N'23/128  Pruksa 81 Moo 11  T.Klongnung   A. Klongluang', NULL, NULL, CAST(0x0000A53A0160A26D AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'0b1c73bb-2483-4d86-b5c3-f7be71e3edb5', N'buckethong@outlook.com', 0, N'AO6t14PGZpAxYCM6YWDiBZovhvh7VqHd3zM2g2XQ8sO25yV0o3TbZRSG5cbYVBF59Q==', N'b23c714f-1385-431e-9cfc-0554406cf001', NULL, 0, 0, NULL, 1, 0, N'noppadon45787', N'hong', N'buckethead', NULL, NULL, 0, 0, 1, 0, NULL, NULL, NULL, NULL, N'0995585957', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00008C1500000000 AS DateTime), N'87/11 หมู่3 ซอย ธนากร4  ต.วัดชลอ อำเภอบางกรวย จ.นนทบุรี 11130', NULL, NULL, CAST(0x0000A53A014C3E8E AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'0f886982-4f8c-4c75-8226-a5f0532af711', N'KASIDEJ_R@D-INF.CO.TH', 1, NULL, N'fb0cd7c8-c4b7-40ac-aed9-219ddc16a5ee', NULL, 0, 0, NULL, 1, 0, N'kasidej', N'ภัคพล', N'รูปเชิด', NULL, NULL, 0, 0, 1, 0, NULL, NULL, NULL, NULL, N'+66832950168', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A52A00000000 AS DateTime), N'LEAPKLONGSONG RD.,', NULL, NULL, CAST(0x0000A52A015B2C15 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'11bd01f2-5ac4-4042-af70-6148a5903bfd', N'decha_k@d-inf.co.th', 1, N'ANYqOoeFOQWileUgjeYwdLUN8xtZxGpSdPsG1RhX0LF2fgxS+8GTdPATpUWzpvdYnw==', N'094dbf09-7ca9-4c85-9bdf-d745cdccc9df', NULL, 0, 0, NULL, 1, 0, N'dechadinf', N'dinf', N'dinf', NULL, N'/Upload/Images/2015102410524620151010005613แอบเปิ้ล.jpg', 0, 0, 1, 0, NULL, NULL, NULL, NULL, N'000000', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00008FCA00000000 AS DateTime), N'test', NULL, NULL, CAST(0x0000A53001124D47 AS DateTime), CAST(0x0000A53B00B34C9C AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'168334b8-79d0-4470-a682-33fb1e93a308', N'pradchaya.ana@student.mahidol.ac.th', 1, N'ANktC/pWwEE+XC4Xoo1vGK5w/14Y11961+RJ81Ne0yu4g4JlgmYz9ILTOd1rb8QlXg==', N'dbf6a664-4e5e-4943-8bf5-1fb3426bfe19', NULL, 0, 0, NULL, 1, 0, N'parchaya', N'parchaya', N'anantameak', NULL, NULL, 1, 0, 1, 0, NULL, 1, NULL, N'3572684621', N'0802961465', N'1860400096821', NULL, NULL, NULL, NULL, NULL, CAST(0x0000823200000000 AS DateTime), N'5/6', NULL, NULL, CAST(0x0000A5290094F1E3 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'1817ae75-a184-42be-8261-90ce17afe46d', N'prasit-next@hotmail.com', 0, N'AHUJBNVR1rDfWWMZsumPzaDQOAuPqgey5yGqsWzUa2RBh7E6tuAtcPebFQSTS24U1w==', N'b1e66597-4df9-4d75-aa7b-98478e60a457', NULL, 0, 0, NULL, 1, 0, N'Next T-SER', N'prasit', N'T-SER', NULL, NULL, 1, 0, 1, 0, NULL, 2, NULL, N'0772023849', N'0983707565', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000765B00000000 AS DateTime), N'80/69ถ.นวมินทร์ บึงกุ่ม คลองกุ่ม กทม
10240', NULL, NULL, CAST(0x0000A53B00BC494B AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'192c991e-8547-4d93-8b23-1a015a62d1cf', N'jamelovemusiclove@gmail.com', 1, N'AADLb4Y8iGlbWUqNlInMWQl2QTHyuSV6tnoyS5FXJZOKKwZSMb3efjzgzVKW1lwx+A==', N'31bd035b-57b9-405a-a08e-3ce93d39f125', NULL, 0, 0, NULL, 1, 0, N'jamelovemusic', N'jamelove', N'music', NULL, N'/Upload/Images/20151024191836qf.jpg', 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0832857200', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53B00000000 AS DateTime), N'จ. ร้อยเอ้ด อ. เสลภูมิ ต. กลาง', NULL, NULL, CAST(0x0000A53B01326B7B AS DateTime), CAST(0x0000A53B013E470C AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'1930dee3-f415-4374-b428-e4f56a493861', N'artgatlal555@gmail.com', 1, N'ANtrU4N7ceTsbQjmbTJe3mUPN7CMcvJN9MaAhvERL3Kgo0z4RFkJAuh0+jkQwi3GlQ==', N'bd65b050-2cc8-4f3c-91ad-c8219baf0de2', NULL, 0, 0, NULL, 1, 0, N'SPECIAL PUNK', N'ANURAK', N'OUAM-IN', NULL, N'/Upload/Images/20151026175709PhotoGrid_1445856965637.jpg', 1, 0, 1, 0, NULL, 2, NULL, N'4782342148', N'0910044907', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00007BB400000000 AS DateTime), N'46 ม. 9 ต.ลาดบัวหลวง อ.ลาดบัวหลวง จ.พระนครศรีอยุธยา 13230', NULL, NULL, CAST(0x0000A53C00E8DBEC AS DateTime), CAST(0x0000A53D0127F285 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'19b9e67d-5c7f-4265-b824-2af33fd84965', N'win_smile8@hotmail.com', 1, N'ANHnOYOtDpAIpDlaXBm7Mz8Ci+OMUXull2+epCH4oq0pt1U4/NVoLlUz+h+NFLRC2g==', N'edcc13b3-cb79-4d27-918b-51c49f49e036', NULL, 0, 0, NULL, 1, 0, N'FatalReaper', N'Assawin', N'Kijpanich', NULL, N'/Upload/Images/201510241551331377089_10153257339540562_811757755_n.jpg', 1, 0, 1, 0, NULL, 3, NULL, N'114-2-18947-9', N'0870207744', N'1100400422864', N'/Upload/Images/20151024154007IMG_1071.jpg', NULL, NULL, NULL, NULL, CAST(0x000081A500000000 AS DateTime), N'1148/7 ถ.วุฒกาศ แขวง ตลาดพลู เขต ธนบุรี กทม.10600', NULL, NULL, CAST(0x0000A53B0102489F AS DateTime), CAST(0x0000A53B01056204 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'1a64d0e6-b5e4-499d-8be4-d331258c25e6', N'fookfick@hotmail.com', 1, N'AAdU3tgnqGiBDrdO37XysGxgDDCEA8fFE8NxFuPjfPb/kTHaYuMhX00Oa/MR6l5pJA==', N'50730dc9-e004-4d1f-b5be-804f372a7569', NULL, 0, 0, NULL, 1, 0, N'fookfick', N'warachai', N'hannarong', NULL, N'/Upload/Images/2015102616202310429499_301838946641736_4244724514914110660_n.jpg', 1, 0, 1, 0, NULL, 1, NULL, N'4037390674', N'0900153731', N'1102200106201', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53D00000000 AS DateTime), N'บ้านเลขที่ 43 หมู่บ้านเศรษฐกิจ ถนนอัสสัมชัญ แขวงบางไผ่ เขตบางแค รหัสไปรษณีย์ 10160', NULL, NULL, CAST(0x0000A53D00F820D0 AS DateTime), CAST(0x0000A53D010D4AEB AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'2187534f-d06f-453f-aa1b-8f8d9fe1d327', N'little.dreamz2011@gmail.com', 1, N'AOIpc7dPF7y5ZxnfFhL6G8NDun+oZ2jvGyQw0MnYGRLPdw1PuCyYEld5jdHRj2lpAw==', N'4e380c83-37f1-4e45-a134-f3aa4d906c20', NULL, 0, 0, NULL, 1, 0, N'Dreamzproject', N'CHORKAEW', N'TUBTIMDANG', NULL, N'/Upload/Images/20151023184943199762_10150175438661201_2285872_n.jpg', 1, 0, 1, 0, NULL, 1, NULL, N'405-304-4201', N'0832338919', N'1559900151538', NULL, NULL, NULL, NULL, NULL, CAST(0x00007FEB00000000 AS DateTime), N'55/242  หมู่บ้านสราญรมย์ ประเสริฐมนูกิจ42  แขวงนวมินทร์ เขตบึงกลุ่ม กทม.10230', NULL, NULL, CAST(0x0000A53A012E5A6A AS DateTime), CAST(0x0000A53A0136596D AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'22e2e456-5c07-461d-9147-be16885498c4', N'not.cl@hotmail.com', 1, NULL, N'23c63e49-bb94-403d-bc7a-c46dcad9e9c0', NULL, 0, 0, NULL, 1, 0, N'น๊อท''น่ะคร้าาาฟ''', N'Anusorn', N'Kleawtang', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0943873646', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0003918900000000 AS DateTime), N'174 หมู่1 ต.เขาดินเหนือ อ.บ้านกรวด จ.บุรีรัมย์', NULL, NULL, CAST(0x0000A53C00BF40B1 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'23d7157e-b307-4759-94a7-2b2cce185ff0', N'sangabuay@hotmail.com', 1, N'AM6oU0nLz+LxRtSSg2WmkdPbdbQDYpYqNw2rU2XSc6PCU+6xg85oQtQlkQvc1+Xcqg==', N'610c7b53-1408-428b-9fbf-2ed6e187b3f0', NULL, 0, 0, NULL, 1, 0, N'sangabuay', N'วิวัธน์', N'จิโรจน์กุล', NULL, N'/Upload/Images/20151027144459251954_409063575818329_309590289_n.jpg', 1, 0, 1, 0, NULL, 1, NULL, N'4014425709', N'0905649551', N'3819900032656', N'/Upload/Images/2015102714363712045533_10154396054524012_6810795534627572469_o.jpg', NULL, NULL, NULL, NULL, CAST(0x00006FBF00000000 AS DateTime), N'1730 ซ.ลาดพร้าว 101 คลองจั่น บางกะปิ กทม.', NULL, NULL, CAST(0x0000A53E00F0FC28 AS DateTime), CAST(0x0000A53E00F317D7 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'25afa257-39d7-4213-b1ca-2b713e7f873f', N'the.wutthinun.amkamnoed@gmail.com', 0, N'AH7gdUpecOnmbjkttkfrfj+UlHmCE0H3XMG2eb9iRMdwAAQMybIWB/UQjNs4eAMbdQ==', N'734fb450-9b45-4ccb-b3e7-48f216d41d64', NULL, 0, 0, NULL, 1, 0, N'Tae_Wutthinun', N'Wutthinun ', N'Amkamnoed', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0998721796', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000814F00000000 AS DateTime), N'122 ม.2 ต.บ่อ อ.เมือง จ.น่าน 55000', NULL, NULL, CAST(0x0000A53E0097DD70 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'2cb6bfde-71d8-416a-b3af-63e1a0bcc685', N'palidarling@gmail.com', 1, N'AGjdt3kerxyEQSXat6tH9PFIbOpND+5ZucK1GQpgeljlG+bi3eOuNbKn6ThvCWjH9Q==', N'729476c9-fd78-45f7-834f-74012467e22d', NULL, 0, 0, NULL, 1, 0, N'Kumarnboon', N'PioPio', N'Palimae', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0813429872', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A02300000000 AS DateTime), N'99', NULL, NULL, CAST(0x0000A5390120F901 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'300f194f-91d3-4379-ac99-700e58acdeb7', N'nitisak_2005@hotmail.com', 1, N'AAuW7TUA2Is1rXHwbQ53weUhAo+7t2eAElaIggec981yw5iav/dqLXvi+hG1+tO+pA==', N'8f426708-016f-4228-a070-9a9334d1acb2', NULL, 0, 0, NULL, 1, 0, N'nitisak1', N'นิติศักดิ์', N'ชะโนวรรณะ', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0909814110', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00007B6400000000 AS DateTime), N'111/4 ม.1 ต.ท่าข้าม อ.หาดใหญ่ จ.สงขลา 90110', NULL, NULL, CAST(0x0000A53A01239530 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'30fbd33b-f19c-42f1-a544-c275fe2076bb', N'joejingjung@hotmail.com', 1, N'AEFIlPgnIAwmI2AsQ/WoM0JYw0/hq2g9zuODNMY9I6bStfcBpKY4Oao33bjdoczDkA==', N'698a8b7e-cc77-4301-86e3-89082ba63e8b', NULL, 0, 0, NULL, 1, 0, N'Joejingjung', N'Apichart', N'Seanglum', NULL, N'/Upload/Images/2015102613315410403770_791132680908891_6122294154350990998_o.jpg', 1, 0, 1, 0, NULL, 3, NULL, N'นาย อภิชาติ  เสียงล้ำ', N'0953653934', N'1430400016731', NULL, NULL, NULL, NULL, NULL, CAST(0x00007AFC00000000 AS DateTime), N'17/34 แขวงการทางปทุมธานี  ถนนพหลโยธิน  ตำบลคลองหนึ่ง อำเภอคลองหลวง จังหวัด ปทุมธานี 12120', NULL, NULL, CAST(0x0000A53D00DC830F AS DateTime), CAST(0x0000A53D00DF0ACC AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'38dceb4e-8dfc-4e64-a590-c19e5eb1f643', N'theraytheray2009@hotmail.com', 0, N'AIXjBD8CYsUSRB/bN98VA9xCCWDrZRrSZq8eZwKlPGS9JhWohotrnjRLyQJT4I8lAw==', N'92d25b30-2a97-48ae-9a80-5eb8f6dc9eb5', NULL, 0, 0, NULL, 1, 0, N'Theray', N'ภัทธเดช ', N'แก้ววิเชียร', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0924791502', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00036E7900000000 AS DateTime), N'กทม.', NULL, NULL, CAST(0x0000A53B0001052D AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'3c588550-1c7c-40c7-923a-1cd8d912d980', N'Kritsanaxx@gmail.com', 0, N'AN6fBgkSaMci+ihGYk60p4SwRbQmneSoKWDV07vTEs7z5+kJWepYBQBlnjvHFX16Zw==', N'f692bd1e-2f00-4118-ad30-195df3bc0f9e', NULL, 0, 0, NULL, 1, 0, N'Kritsanaxx', N'กฤษณะ', N'กฤตสิน', NULL, NULL, 1, 0, 1, 0, NULL, 1, NULL, N'1462620604', N'0868693467', NULL, N'/Upload/Images/2015102322180520151008_140803.jpg', NULL, NULL, NULL, NULL, CAST(0x0000A52600000000 AS DateTime), N'1030/31 ถ.วันลูกเสือ ต.เมืองใต้ อ.เมือง จ.ศรีสะเกษ 33001', NULL, NULL, CAST(0x0000A53A016FCCD7 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'3e2d1f5a-8192-4dc8-bc0f-3d4b008ac02f', N'Nickkypan77@gmail.com', 0, N'AJ/s/oJBd7LXZlsMLm8g7iD2qo9v9E5OHGFusw2X1pXejvsm3Hl9BFdgy/W8qhyeTw==', N'52e587d2-35f3-4fbc-9a9a-5bee0d558873', NULL, 0, 0, NULL, 1, 0, N'topten', N'pongdat', N'khasawang', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0935303376', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53B00000000 AS DateTime), N'Mukdahan', NULL, NULL, CAST(0x0000A53B00F92A68 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'40836442-808f-42d3-865c-7f8f3584675c', N'cccoooppp.com@gmail.com', 0, N'AKRivyEKwmmwgRzLtb/O1DotictodwHC91lAk2A2pCfLwpsAXxE1qtadPlx/Frlblw==', N'2be0bec9-1e6d-402c-9077-b116c859abcb', NULL, 0, 0, NULL, 1, 0, N'แปดกันยา', N'พีรบูรณ์', N'จริยานุสรณ์', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'083 3042171', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000776500000000 AS DateTime), N'212/45  คอนโด บางปูณคร  ต. บางปูใหม่  อ. เมือง จ.สมุทรปราการ', NULL, NULL, CAST(0x0000A53A013A9F31 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'44e0e865-eb8e-4a39-bb10-75e38b3659d5', N'boroman05@gmail.com', 1, N'AC1/QcoIla1I/Zcfktz9ymjfeTpz8b1lM7tWYMB764P/a6DKYHA5obOzRUsXLZGC5g==', N'c5d0d663-499f-4455-b66d-7e238823e676', NULL, 0, 0, NULL, 1, 0, N'Flow-view', N'Chitapon', N'Wichai', NULL, NULL, 0, 0, 1, 0, NULL, NULL, NULL, NULL, N'0990806625', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0xFFFFE99700000000 AS DateTime), N'75/1  ต.นาเชือก  อ.นาเชือก จ.มหาสารคาม', NULL, NULL, CAST(0x0000A53A01472C5A AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'4d82462f-eec1-49f5-b40b-9574f7b71237', N'pattadech2015@gmail.com', 0, N'AHxgV4XrVXZIXxAIHLxFi2If5b/twY4X9/7hqnTNAH+h3K9nJW/4KAJh2j2GeQWO0g==', N'4fe50455-3e4b-49ef-b329-8fa21ecb52fe', NULL, 0, 0, NULL, 1, 0, N'pattadech', N'ภัทธเดช', N'แก้ววิเชียร', NULL, NULL, 0, 0, 1, 0, NULL, NULL, NULL, NULL, N'0924791502', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x000067C300000000 AS DateTime), N'6 ร่มเกล้า1 แยก1.ถ.ร่มเกล้า
ลาดกระบัง กทม.', NULL, NULL, CAST(0x0000A53B00AD2558 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'52e7d8e8-6b76-498b-924c-c4d696cdb1be', N'MT.mutza@outlook.co.th', 0, N'ALyJeqISakGjfkFCzUqyukwQ6P9s+jenE9dfGIgWMForilFihdrqqi5GXCjAJTli8w==', N'94fafe4c-048f-4295-8827-e427a3a7bcf2', NULL, 0, 0, NULL, 1, 0, N'mt.mutza', N'Anumat', N'Yanatham', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0981284971', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000894C00000000 AS DateTime), N'42  ม.11 ต.สังขะ อ.สังขะ จ.สุรินทร์ 32150', NULL, NULL, CAST(0x0000A53D0154F811 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'5c7c8473-93ee-420d-a7eb-7cc917ed19bf', N'pramote_skb@hotmail.com', 1, N'AHIB4XQh2RGryMlUGsokUrM7bTbj3KG+AJVJkqAVibwDVJNaeB5xXYqTMrrycX7qRQ==', N'd12bb806-4f74-4132-9b35-cf2be74a74a7', NULL, 0, 0, NULL, 1, 0, N'iammote', N'Pramote', N'soongkitboon', NULL, N'/Upload/Images/20151026211224dis-re.jpg', 1, 0, 1, 0, NULL, 1, NULL, NULL, N'0850355775', N'3700501009394', N'/Upload/Images/20151026210734ID.jpg', NULL, NULL, NULL, NULL, CAST(0x000073A700000000 AS DateTime), N'Sanook Online Limited , MuangThai-Phatra Complex, Tower A, 16th Floor
252/19-20 Rachadapisek Road,', NULL, NULL, CAST(0x0000A53D015C5FFF AS DateTime), CAST(0x0000A53D01783419 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'5ef3dc15-82d6-4179-97ea-7b1dafabc1f7', N'toyot_2518@hotmail.com', 1, NULL, N'3ebf6f49-20c3-41ca-851d-ad79dbb83bfd', NULL, 0, 0, NULL, 1, 0, N'พีพีเสนาะอุบลลัมเปอร์', N'ปิยดนัย', N'วิชัสระเกษ', NULL, NULL, 1, 0, 1, 0, NULL, 1, NULL, N'9382337957', N'098-2204717', N'3349900729953', NULL, NULL, NULL, NULL, NULL, CAST(0x00006C5C00000000 AS DateTime), N'175 ม.2  ต. หัวเรือ   อ.เมือง   จ.อุบลราชธานี    34000', NULL, NULL, CAST(0x0000A53D00EC0B67 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'645a5fb5-e893-4a8e-b0fb-6b9223a60ac4', N'ak_anfeild9@hotmail.com', 0, N'AFEe8gvydT2fjYbKgqHdk4Y251Ip4uVgBAf38V5yjiq94WJqHpSkC50ruOgYODZX7w==', N'f06b0a6e-5564-41c7-921e-4a1ffb31018f', NULL, 0, 0, NULL, 1, 0, N'TRYNANA', N'อัครเดช', N'เนื่องเดช', NULL, NULL, 1, 0, 1, 0, NULL, 1, NULL, N'7992464118', N'091-4864571', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000B66E00000000 AS DateTime), N'LAMPANG', NULL, NULL, CAST(0x0000A53B014D4B5A AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'66dbb691-4e42-4417-8283-a2c6571fea7e', N'parchaya_1@windowslive.com', 1, N'AO1ylMTYrg2xr9VNd3jd5szBtSzsBoP3KwNNTFhvHkMBlyBHRLpl1lUIKFa5Z9boDA==', N'e00eb7c3-dc22-4fdf-8719-0bc23926a206', NULL, 0, 0, NULL, 1, 0, N'parchaya1', N'parchaya', N'anantamek', NULL, N'/Upload/Images/2015102313272711042254_10203753985832511_1197487163_n.jpg', 1, 0, 1, 0, NULL, 1, NULL, N'3572684621', N'0802961465', N'1860400096821', NULL, NULL, NULL, NULL, NULL, CAST(0x0000823200000000 AS DateTime), N'5/6 test', NULL, NULL, CAST(0x0000A52900485427 AS DateTime), CAST(0x0000A53A00DDCC4C AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'6980bdbb-5fb4-4240-a65e-0cbad3d1d339', N'Hathaikarn_suk@gmail.com', 0, N'AJItoAL+QduUW93x/TxN2l6g4a34Jy53h3HwnJsCK+RIY/eIUEhREXpSqoB1t5+XTg==', N'9b819e08-09d4-4399-8b8e-b00f7bb66289', NULL, 0, 0, NULL, 1, 0, N'Lovely dear', N'Hathaikarn', N'Sukkua', NULL, NULL, 0, 0, 1, 0, NULL, NULL, NULL, NULL, N'081-987-8122', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00006FE200000000 AS DateTime), N'98/426  หมู่บ้านพฤกษ์ลดา ต.บางแม่นาง อ.บางใหญ่ จ.นนทบุรี 11140', NULL, NULL, CAST(0x0000A53A01527FBE AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'6cdfda6a-148f-48f8-9390-778e4cdb6b18', N'bookfender@live.com', 0, N'AASHoF/akux4lMysQsajzkSE4IIKEHq29/EWPrcPCwEVhbX4+eXIlziedUSyRGQyMw==', N'96623b85-dd9f-4b4a-8d36-13c6be7b7374', NULL, 0, 0, NULL, 1, 0, N'bookfender', N'Apisit', N'Kongsamuk', NULL, NULL, 1, 0, 1, 0, NULL, 3, NULL, N'4867071013', N'0876062336', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000806100000000 AS DateTime), N'27/1 ม.5 ต.บ้านนา อ.แกลง จ.ระยอง', NULL, NULL, CAST(0x0000A53A0121E0A5 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'71ebf717-7aa7-459e-b803-2dbc75a2f864', N'Bontvoice@gmail.com', 1, N'AF/OV4DvZGILrtquOsflrOWjEUuABEbaWcqjVHn9gkQdYFcA1xJtYg9Vx5g4g1BtnQ==', N'cc7a7fc9-872c-4ebd-8527-1962165326d7', NULL, 0, 0, NULL, 1, 0, N'Bontvoice', N'Kamphol', N'Kaewkhao', NULL, N'/Upload/Images/20151027055134image.jpg', 1, 0, 1, 0, NULL, 2, NULL, N'0033520433', N'0800939338', N'1229900097447', NULL, NULL, NULL, NULL, NULL, CAST(0x00007AF600000000 AS DateTime), N'109/7 M.11 T.Thachan A.Muang
chantaburi', NULL, NULL, CAST(0x0000A53D016846FD AS DateTime), CAST(0x0000A53E0060CAB5 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'73de51f1-2199-4fe8-8078-8671c397532b', N'toyota_2518@hotmai.com', 0, N'APagVIVcfCVx0NC2zIptRFDPboFFcvZWftuAbPSGnDlT+QAFc77hVavy8jSE+H086Q==', N'0b4e754d-237e-4c59-8ec9-31fb22abb399', NULL, 0, 0, NULL, 1, 0, N'พีพีเสนาะ', N'ปิยดนัย', N'วิชัยสระเกษ', NULL, NULL, 1, 0, 1, 0, NULL, 1, NULL, N'938-233795-7', N'098-2204717', N'3349900729953', NULL, NULL, NULL, NULL, NULL, CAST(0x00006C5C00000000 AS DateTime), N'175  ม.2  ต.หัวเรือ  อ.เมือง  จ. อุบลราชธานี  34000', NULL, NULL, CAST(0x0000A53D00EA0E1D AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'78aaa7dd-8616-4cad-829b-dc6bc1057f04', N'cii3@msn.com', 1, N'APRP90Ut80CEgHKOuAeV9avYcIN4A/+TRCHgos7uRRHqjbZ9E6GKKx8xcwrYl86mcw==', N'1c51f873-447f-420f-adf5-a9388ea05198', NULL, 0, 0, NULL, 1, 0, N'cii3@msn.com', N'ธนวัฒน์', N'สิทธิกัน', NULL, N'/Upload/Images/2015102408094082615.jpg', 1, 0, 1, 0, NULL, 1, NULL, N'3832749852', N'0800857000', N'1550600055349', NULL, NULL, NULL, NULL, NULL, CAST(0x000080C700000000 AS DateTime), N'เลขที่ 26, หมู่ 3, ตำบลผาตอ, อำเภอท่าวังผา, จังหวัดน่าน, 55140', NULL, NULL, CAST(0x0000A53B008569FB AS DateTime), CAST(0x0000A53B0086883F AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'791a3952-4b3a-4320-af9e-97ec0cd24b9b', N'thainingmaster@hotmail.com', 1, NULL, N'a3ab9817-9c38-4bc0-8bf2-c950f254390d', NULL, 0, 0, NULL, 1, 0, N'DechaKenthaworn', N'Decha', N'Kenthaworn', NULL, N'/Upload/Images/20151010023409P_20151007_003143.jpg', 1, 0, 1, 0, NULL, 1, NULL, N'test', N'0099', N'1410300010910', N'/Upload/Images/20151010023449P_20150908_182834.jpg', NULL, NULL, NULL, NULL, CAST(0x00008FC400000000 AS DateTime), N'test', NULL, NULL, CAST(0x0000A52A015C0E91 AS DateTime), CAST(0x0000A52D002A94EB AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'7bbe84c8-cb39-4d6a-bb6c-80641181c4b4', N'artgatlal@hotmail.com', 1, NULL, N'b0f06064-a74a-4ce1-b36c-56a221a63304', NULL, 0, 0, NULL, 1, 0, N'AnurakUam-in', N'Anurak', N'Ouamin', NULL, NULL, 0, 0, 1, 0, NULL, 2, NULL, NULL, N'0910044907', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00007D2100000000 AS DateTime), N'46 ม. 9 ต. ลาดบัวหลวง อ.ลาดบัวหลวง จ.อยุธยา 13230', NULL, NULL, CAST(0x0000A53B006A83EF AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'7bc4f6f3-4411-4236-b7e3-39390983fc3d', N'saivaree.p@hotmail.com', 1, NULL, N'e2887f88-ef5c-4b9c-96ec-d90fa31a9736', NULL, 0, 0, NULL, 1, 0, N'สายวรีใหม่ตา', N'Kon  Chill', N'Chill', NULL, NULL, 0, 0, 1, 0, NULL, NULL, NULL, NULL, N'0914147936', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x000385CC00000000 AS DateTime), N'158 หมู่5 ต.ริม อ.ท่าวังผา 55140', NULL, NULL, CAST(0x0000A53E00D2E9CC AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'7bf764b6-2b0e-49ff-9491-2f82cd1a3f9e', N'neww610@gmail.com', 1, N'AOJeiH0Zo6NLBjO0S4p0qn29v47U3gPtj2JCk4nSlJtv7UV44cTpWjLP1hRp1crP9g==', N'05216e61-6eac-4cb3-ab62-efaf1da06268', NULL, 0, 0, NULL, 1, 0, N'Koalaman', N'Tanapat', N'Yakaew', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0834321193', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000859400000000 AS DateTime), N'495 Jarun 3', NULL, NULL, CAST(0x0000A53B00868E6F AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'7ceb217c-e8bb-497e-adcb-107404a3251f', N'Hathaikarn.suk@gmail.com', 1, N'AIuKAvToHdkM+HKQjxW/f9W7NYzLvFuz7PDOdbVYEEEsZ/jYZRxQfUFBYUhx+GoIww==', N'd06c1a38-775a-4970-95e5-7bccdcdf3178', NULL, 0, 0, NULL, 1, 0, N'SoodPhroad', N'Hathaikarn.s', N'S', NULL, N'/Upload/Images/20151024120658image.jpg', 0, 0, 1, 0, NULL, NULL, NULL, NULL, N'0819878122', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00006FE200000000 AS DateTime), N'98/426 หมู่ที่10 ต..บางแม่นาง อ.บางใหญ่  จ.นนทบุรี  11140', NULL, NULL, CAST(0x0000A53A0155395C AS DateTime), CAST(0x0000A53B00C7C291 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'7dfb6b53-02d1-463a-9e60-60d8751c0662', N'janya25391@hotmail.com', 1, NULL, N'5abea93f-9633-4682-8177-29bb3c35ea60', NULL, 0, 0, NULL, 1, 0, N'ชื่อ''โย', N'janya', N'khamdee', NULL, NULL, 0, 0, 1, 0, NULL, NULL, NULL, NULL, N'0986382662', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x000089ED00000000 AS DateTime), N'-', NULL, NULL, CAST(0x0000A53B00375F30 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'80766062-cbc8-4846-94f3-ca36dbacbfb9', N'Mildaf13@hotmail.com', 0, N'AA5iUolIyiCa9+Xws3CfzJ2I8NXsNx/IcSloigBYJ9h9CiFBNqnjPusxqN5D8FuT7w==', N'ded45c9a-d292-429a-8ac9-3bcbd5975937', NULL, 0, 0, NULL, 1, 0, N'Mildaf13', N'ณัฐริกา', N'เทพสถิต', NULL, NULL, 1, 0, 1, 0, NULL, 2, NULL, N'329-2-39342-2', N'0973342543', N'1449900592594', N'/Upload/Images/20151016134821image.jpg', NULL, NULL, NULL, NULL, CAST(0x00008FEF00000000 AS DateTime), N'38/1-2 ถ.เทวาภิบาล ต.ในเมือง อ.เมือง จ.ร้อยเอ็ด 45000', NULL, NULL, CAST(0x0000A53300E39903 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'84c59159-e8d3-4ca6-9d86-c7129502caeb', N'flameart466@gmail.com', 1, N'AE+2i8Dafq5C2xojVyt3FcNyaGgMgf9qa8EyqpGnQutqcupSNf6c7uOrCAufblxarg==', N'bcc8c6f7-6c3d-4f14-9d10-8d2ae85f84e4', NULL, 0, 0, NULL, 1, 0, N'flameart1', N'Suriya', N'phosri', NULL, NULL, 1, 0, 1, 0, NULL, 2, NULL, NULL, N'0971190173', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000889F00000000 AS DateTime), N'200/69   ม.4 ต.บางระกำ อ.นครหลวง จ.อยุธยา 13260', NULL, NULL, CAST(0x0000A53E01749D59 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'875d9abb-b8bd-42ab-8fbe-a222b53b0608', N'k.worachai@gmail.com', 1, N'ALPkxJKuiTu1uV7sYQ+6bgTQBXT6hf+PNZx2XacSWUiYcLXUlPlMbD/h2GHqd5pmgA==', N'56416787-345d-4c69-8b31-17604a7078f5', NULL, 0, 0, NULL, 1, 0, N'elinoy', N'Apisak', N'Kaewjiaranai', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'081-7605020', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x000074CA00000000 AS DateTime), N'218 Mittraphap Rd, Nong Sarai Subdistrict
Pak Chong Nakhon Ratchasima 30130
Thailand', NULL, NULL, CAST(0x0000A53B003A02F6 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'89dec678-e0b1-491e-ac89-250725b5c517', N'poramez@live.com', 1, N'AGOqRtEOsRi7fJfzH0vcRRFoUSBiajijkOn3FRk7CXQOST1BXekiiCKspatjrUp/Xg==', N'3a3851a6-6450-4814-ab84-4d3d38e0a0cd', NULL, 0, 0, NULL, 1, 0, N'poramez', N'poramez', N'kumarnboon', NULL, N'/Upload/Images/20151006132205IMG_2408.JPG', 1, 0, 1, 0, NULL, 1, NULL, N'6056087583', N'0814233848', N'3309900073934', NULL, NULL, NULL, NULL, NULL, CAST(0x0000671100000000 AS DateTime), N'111/107 Tharadi Nonthaburi 11000', NULL, NULL, CAST(0x0000A52900DBD8CE AS DateTime), CAST(0x0000A52900DC535C AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'8ef4f4ec-43ef-476d-892e-39b63a2866d1', N'Not.what174@gmail.com', 0, N'ADr4L+lRscaP0t7cZgGESvEYKprzAG+gsrmUBf10JRXdfe2ANA4aBb2gJ2WuOXewlQ==', N'9f9e9700-a5ed-49b2-aa9c-bbed0f36698c', NULL, 0, 0, NULL, 1, 0, N'0943873646', N'Anusorn', N'kleawtang', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0943873646', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0003918900000000 AS DateTime), N'174 หมู่1 ต.เขาดินเหนือ อ.บ้านกรวด จ.บุรีรัมย์', NULL, NULL, CAST(0x0000A53C00BB8608 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'905a2c22-51d5-4521-9a1c-f56eca97f497', N'olarnmail@gmail.com', 1, N'AK3TdnH++qvagpdVEOUycQSuZkM84kXzo90gGvoez77dU/+7+YKA/wWOw2SAZIhzLA==', N'9357cb01-5d43-4467-95b4-8f99f26045f1', NULL, 0, 0, NULL, 1, 0, N'ytwokdc', N'โอฬาร', N'กำจรไพศาล', NULL, NULL, 1, 0, 1, 0, NULL, 1, NULL, N'092244556', N'0818051165', N'3410100144820', NULL, NULL, NULL, NULL, NULL, CAST(0x00006C6E00000000 AS DateTime), N'63/44 ซ.120/4 เสนานิคม1 พหลโยธิน ลาดพร้าว กทม 10230', NULL, NULL, CAST(0x0000A53A01700F1B AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'952aa50c-9b28-49da-8020-a361a5e856a5', N'palida.ok@gmail.com', 1, N'ACnZzlPWo4yIj6PJRb5jRZqnX2n+8vZdaPdRXfZLTwO+85PjKmdbDuAnw4Eu/UuzTg==', N'bbc9ee09-015d-48b5-9b0d-8d78c234a7b6', NULL, 0, 0, NULL, 1, 0, N'PioPio', N'Palimae', N'Kumarnboon', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0813429872', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A02300000000 AS DateTime), N'99', NULL, NULL, CAST(0x0000A539012545E0 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'9a516f64-7619-4df0-a7a4-d9ebaeeea27a', N'Somsaknen5599@gmail.com', 1, N'AMNWiR2rDg3Ll9BeDOGALEb0F87z+uWZxbGLyZAuBEFoVvPvDGZL856wSXD7pmbBKQ==', N'7d873b7e-a099-481c-9c23-0a8a67c5489a', NULL, 0, 0, NULL, 1, 0, N'Mongnen', N'Somsak', N'Anan', NULL, NULL, 0, 0, 1, 0, NULL, NULL, NULL, NULL, N'0831385599', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53000000000 AS DateTime), N'Somsaknen5599@gmail.com', NULL, NULL, CAST(0x0000A53100641D88 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'9a67901b-8c64-4f79-88bc-111f5624e0c5', N'sutthiyothaband@gmail.com', 1, N'AC4gw7yfgoyBY39HZipednvOXMRADenhSDEH+HbTNq/IP9S+9K4VoaffLndLOUWkKA==', N'121f3cc1-72a9-4b42-a5d7-4f2698322473', NULL, 0, 0, NULL, 1, 0, N'greasy24', N'kongdech', N'sutthiyotha', NULL, N'/Upload/Images/20151023160012001.jpg', 1, 0, 1, 0, NULL, 1, NULL, N'3992600249', N'0995315521', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00007D9B00000000 AS DateTime), N'334/124 ซ.3/2 ม.พฤกษา62 หมุ่4 ถ.หนามแดงบางพลี', NULL, NULL, CAST(0x0000A53A01050193 AS DateTime), CAST(0x0000A53A0107D170 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'9fa60428-ef64-43c1-aebf-a7be62600488', N'palida_pla@hotmail.com', 1, N'AAfVkOsLpyPeYbnq0bAMvnxhvTsS0ND04t9tqFS8wn3hZ9u+19tLc/NGo7Sl6AS2Bg==', N'4d65e477-81d6-4459-9d70-db3549036377', NULL, 0, 0, NULL, 1, 0, N'paadaa', N'Palida', N'Kumarnboon', NULL, N'/Upload/Images/20151024112033image.png', 0, 0, 1, 0, NULL, NULL, NULL, NULL, N'0813429872', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00006FF000000000 AS DateTime), N'111/107 หมู่ 2 ต.บางรักน้อย อ.เมือง จ.นนทบุรี 11000', NULL, NULL, CAST(0x0000A52D000CC888 AS DateTime), CAST(0x0000A53B00BB12E0 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'a20ad65b-ba1f-48bc-823f-94ff61831e6f', N'phuchit@bangkokplace.co.th', 1, N'APK47hTf5lUVUkAVMlif57rASB7W7PG2QyMZsmR/e8+maoXOTSWamUrOg+GGKfmKvw==', N'b974db0a-edde-4a83-adef-59c3255823bc', NULL, 0, 0, NULL, 1, 0, N'คิ้วยักษ์', N'Phuchit', N'Pengkham', NULL, N'/Upload/Images/2015102411454312112047_813499488767248_2212948562961903234_n.jpg', 1, 0, 1, 0, NULL, 2, NULL, N'6362213752', N'0895549369', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53B00000000 AS DateTime), N'65 แยก 2 สุขสวัสดิ์ 26 ถ.สุขสวัสดิ์ บางปะกอก ราษฎร์บูรณะ  กรุงเทพมหานคร 10140', NULL, NULL, CAST(0x0000A53B00C07E4E AS DateTime), CAST(0x0000A53B00C1E95E AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'a7d089ec-0816-42ee-9da9-38dca1242ca6', N'info@d-inf.co.th', 1, N'ACnZzlPWo4yIj6PJRb5jRZqnX2n+8vZdaPdRXfZLTwO+85PjKmdbDuAnw4Eu/UuzTg==', N'eb085b37-f4a3-41b2-8e5c-ba100ba634f0', NULL, 0, 0, NULL, 1, 0, N'test01', N'ทดสอบ', N'test', NULL, N'/Upload/Images/2015102322431020151010005605ส้ม.jpg', 1, 0, 1, 0, NULL, 1, NULL, N'sfsadfsdf', N'0876862642', N'1410300010910', N'/Upload/Images/20151012111412info1.jpg', NULL, NULL, NULL, NULL, CAST(0x0000A52200000000 AS DateTime), N'82/78 Habitia, Leabklongsong Road,
Bangchun Klongsamwa', NULL, NULL, CAST(0x0000A5220133E4A3 AS DateTime), CAST(0x0000A53A017674EE AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'aa41753e-1465-4a27-8a3d-20c1652178d4', N'topiz_3pop@hotmail.com', 1, N'AD3utWc0zU64q0Tmx/9lyryaitdUn7JBXEYHKPj6M2Fgivce0oOg/E5D4XIwDzQINg==', N'5f69b1b9-0431-49c3-98dc-94bd8cf90de9', NULL, 0, 0, NULL, 1, 0, N'Toppy', N'กฤษณะ', N'รตะบุตร', NULL, N'/Upload/Images/201510251628181941395_561212363977721_1159947171_o.jpg', 1, 0, 1, 0, NULL, NULL, NULL, N'465-0-28203-9', N'0968919543', N'1102002026561', NULL, NULL, NULL, NULL, NULL, CAST(0x000085E200000000 AS DateTime), N'บ้านเลขที่2 (251/5) ซอย55 ถนนประชาอุทิศ แขวงบางมด เขตทุ่งครุ ', NULL, NULL, CAST(0x0000A53C010E3ED4 AS DateTime), CAST(0x0000A53C010F8095 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'acbc0310-bd04-4280-8bb4-ff1af6b9beaa', N'axl2james@gmail.com', 1, N'AGU/YUDD7ZCi5Uf74EZIVh6fTmjsYhrK2mGALXkxNx2PaboZBMxsbWXEBJ3YfaHhgQ==', N'5a6cb8d1-352d-4e41-92ec-213ce1e4813a', NULL, 0, 0, NULL, 1, 0, N'axl2james', N'สหราช', N'ฌาณมงคลพร', NULL, N'/Upload/Images/201510271005301445915086586-1312407335.jpg', 1, 0, 1, 0, NULL, 1, NULL, N'6222535227', N'0942919416', N'1360600106018', NULL, NULL, NULL, NULL, NULL, CAST(0x00007CAC00000000 AS DateTime), N'61/67 ซ.ท่ามะนาว1 ถ.ปากช่อง-ลำสมพุง ต.ปากช่อง อ.ปากช่อง จ.นครราชสีมา 30130', NULL, NULL, CAST(0x0000A53E00A4CEAF AS DateTime), CAST(0x0000A53E00A65B75 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'ae7c4a58-aca7-4d38-9876-e6d592a067b0', N'homhuayai@hotmail.com', 0, N'ADSkqPLhSbab/jhydWdfjGup9KFrwgIkQcsZrECPJ1g3tBDGSxpu3m2AUqsD14D5+g==', N'c060bafc-7780-4c76-8252-a930bea49711', NULL, 0, 0, NULL, 1, 0, N'homhuayai', N'hom', N'huayai', NULL, NULL, 1, 0, 1, 0, NULL, 1, NULL, N'3572413923', N'0928972022', N'3101201550932', NULL, NULL, NULL, NULL, NULL, CAST(0x0000818200000000 AS DateTime), N'Patumthani', NULL, NULL, CAST(0x0000A52C000BA273 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'b01eba5d-03b8-4e16-8984-95cd02efe904', N'cgame_guntagrit@hotmail.com', 1, N'AL27CAzTOT8lijaNC1HUANZOk/kYo8JCG2H38ezgbeTQR4//BYeJbodsVz/qD+J5qg==', N'844ea6d9-2c1f-43a5-bae4-54f0f8921d2c', NULL, 0, 0, NULL, 1, 0, N'พี่เกม', N'กันตกฤษณ์', N'จึงวงศ์ไพบูลย์', NULL, N'/Upload/Images/2015102402261412042739_10156003314240526_4915156352444333079_n.jpg', 1, 0, 1, 0, NULL, 1, NULL, N'8912132496', N'0924756699', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00007FEE00000000 AS DateTime), N'60/5 ม.3 ต.ย่านยาว อ.สวรรคโลก จ.สุโขทัย 64110', NULL, NULL, CAST(0x0000A53B002608E8 AS DateTime), CAST(0x0000A53B0028307E AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'b882b15e-bfd2-4e5e-a027-b9d7c0822517', N'sanipatcharakamonwong@gmail.com', 1, N'ALgAje+crtDDwcEfVxT3sgSP2kmFcwvRTCB28xKReT5WwdbI14/n+WbTiRKnXJ4nRw==', N'99f91c65-93fd-43dd-9485-935a3a45a79d', NULL, 0, 0, NULL, 1, 0, N'sanit', N'ศานิต', N'พัชรกมลวงศ์', NULL, NULL, 1, 0, 1, 0, NULL, 2, NULL, N'1182764597', N'0959902531', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000761A00000000 AS DateTime), N'อุบลราชธานี', NULL, NULL, CAST(0x0000A53E001C4AFD AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'bd9247d1-aa76-4806-9122-456c1c6bd6c5', N'psb.saletest@gmail.com', 1, NULL, N'0e57cf9e-4074-48d9-bc48-3a1a79ac82f0', NULL, 0, 0, NULL, 1, 0, N'SALES', N'SALES', N'TEST', NULL, NULL, 0, 0, 1, 0, NULL, NULL, NULL, NULL, N'021605463', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53900000000 AS DateTime), N'test
Patumwan', NULL, NULL, CAST(0x0000A539011E7424 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'be44bef2-5552-40d1-baf3-0122ad8c121d', N'test@email.com', 0, N'ADC7+0hkPaI1WBIFEwtGRGiH1jHArupiAVFTFEx/plu7jAqZ6u3RbiuU28gSbM768Q==', N'b5acf3fd-7705-49d1-a458-595446572ef3', NULL, 0, 0, NULL, 1, 0, N'testaaa', N'test', N'test', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'sfsdfsdf', NULL, N'/Upload/Images/20151007001157IMG_0025.JPG', NULL, NULL, NULL, NULL, CAST(0x0000A52A00000000 AS DateTime), N'sfsdfsdf', NULL, NULL, CAST(0x0000A52A00041C25 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'bfebc6ac-68c6-4032-a382-d2b1bf13b788', N'nannapas.ploy@gmail.com', 1, N'APavKaowm/QtYbUhiDQGlIn7l0Ht0lump0Ghqp0mUJJqVoWfaTB123uLWTfOkcPa8Q==', N'8e5e0005-7d3d-421f-ad40-bb557ba440b6', NULL, 0, 0, NULL, 1, 0, N'nannapas', N'nannapas', N'ploywisut', NULL, N'/Upload/Images/20151006190822IMG_9652.JPG', 1, 0, 1, 0, NULL, 2, NULL, N'0192822629', N'0905749956', N'1102099001500', NULL, NULL, NULL, NULL, NULL, CAST(0x00007A8B00000000 AS DateTime), N'403/60 ', NULL, NULL, CAST(0x0000A529013971AD AS DateTime), CAST(0x0000A529013B6DE7 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'c485ae2e-ab5f-4ca1-a3f9-992ea9cdd079', N'tk_nung@hotmail.com', 1, N'AASje1wTMsTZaFjKxbXg9Xkd1HiIGnH31Mcmvh1xIOSklV7xms6+oJz/8F3DACpQmQ==', N'1c2196c5-682c-448c-b541-33cf845b2b08', NULL, 0, 0, NULL, 1, 0, N'tomorrow', N'Tammarat', N'Khobkhokgrud', NULL, N'/Upload/Images/20151025210711TripLOGO.png', 1, 0, 1, 0, NULL, 1, NULL, N'813-236911-6', N'0821446106', N'1300200156379', N'/Upload/Images/201510252055521796468_991717644181732_4920444779809314829_n.jpg', NULL, NULL, NULL, NULL, CAST(0x0000842200000000 AS DateTime), N'14/35 ', NULL, NULL, CAST(0x0000A53C015904B3 AS DateTime), CAST(0x0000A53C015C11CB AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'c5bdd627-e3ab-4f2c-9f38-0d1430bc7aab', N'neverdietser@gmail.com', 1, NULL, N'ef47bfbe-c291-44b4-be3f-08c621ab7352', NULL, 0, 0, NULL, 1, 0, N'PrasitPungmak', N'Next', N'T-SER', NULL, N'/Upload/Images/20151024141410CYMERA_20150822_163219.jpg', 1, 0, 1, 0, NULL, 2, NULL, N'0772023849', N'0846583523', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x000077C800000000 AS DateTime), N'80/69 นวมินทร์ บึงกุ่ม คลองกุ่ม ก ท ม
10240', NULL, NULL, CAST(0x0000A53B00C29A5B AS DateTime), CAST(0x0000A53B00EAA807 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'c6ac2778-8630-4dd1-88ef-59f61033e4bb', N'diamond_inter@hotmail.com', 1, N'ABLDTiDcsEdI0qvDBdi0NpZU/57pNdiXjkM2XnNbbS4KmDteylHi5vtlz4Me4E5Fng==', N'8d0e9b8c-775b-4dbb-92ef-394062a401fd', NULL, 0, 0, NULL, 1, 0, N'diamond_inter', N'ชญานิน', N'ศาศวัตวลี', NULL, NULL, 1, 0, 1, 0, NULL, 2, NULL, N'8632362863', N'0844926633', NULL, N'/Upload/Images/20151024000721img004.jpg', NULL, NULL, NULL, NULL, CAST(0x0000854500000000 AS DateTime), N'234/73 หมู่ 3 ต.เทพารักษ์ อ.เมืองสมุทรปราการ จ.สมุทรปราการ 10270', NULL, NULL, CAST(0x0000A53B00032708 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'c6f23020-b77e-4dc6-b0e8-80f7c1a4542f', N'overconda@gmail.com', 1, N'AIR5SlVu2zhiILN0xsnGQDATNzgAms9uFHH1OGXK+Er+OQBMTmlV87TKuIRQPS3zcg==', N'9315e1e0-084e-4d9a-9ead-4d0488080f41', NULL, 0, 0, NULL, 1, 0, N'overconda', N'ศุภจิต', N'พันกมลศิลป์', NULL, N'/Upload/Images/20151007002914super one.jpg', 1, 0, 1, 0, NULL, 1, NULL, N'0324628899', N'0869967788', N'5309900068660', NULL, NULL, NULL, NULL, NULL, CAST(0x0000667E00000000 AS DateTime), N'206/96 Living Place Condo อาคาร 3 ซ.ศูนย์วิจัย 14 ถ.เพชรบุรีตัดใหม่ แขวงบางกะปิ เขตห้วยขวาง กรุงเทพ 10310', NULL, NULL, CAST(0x0000A52900464702 AS DateTime), CAST(0x0000A52A0008108F AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'd3022e10-2bcf-41fc-8068-a6daff456957', N'rachan.jack@gmail.com', 1, N'AGC0Mv3X8UTIVdjs18GCAqJjvVLIhBdcDpUa9Puz23DAOTbgzFKrpXztYUeVzOe6xQ==', N'aab40f62-6d56-4d80-904d-b49c75f6db9a', NULL, 0, 0, NULL, 1, 0, N'goo-jag', N'Rachan', N'Maichom', NULL, N'/Upload/Images/20151014211957IMG_2473.JPG', 1, 0, 1, 0, NULL, 2, NULL, N'7202282187', N'0917079893', N'3540600097242', NULL, NULL, NULL, NULL, NULL, CAST(0x0000739D00000000 AS DateTime), N'19/4 Baanmai village, Kuubon28, Ramintra, Kunnayao, Bangkok 10230', NULL, NULL, CAST(0x0000A5310135EC77 AS DateTime), CAST(0x0000A531015F9C97 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'da6f13e9-449e-47ec-bbd9-deec4bdd82e8', N'pangpang.supitcha@gmail.com', 1, NULL, N'f3f6c6b6-7531-4e09-b8c5-30a5fc183222', NULL, 0, 0, NULL, 1, 0, N'ณัฐพงษ์จิ้งหรีดฟาร์ม', N'nuttapong', N'tipwijan', NULL, NULL, 1, 0, 1, 0, NULL, 2, NULL, N'7322931454', N'0862016500', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53700000000 AS DateTime), N'241 ม.1 ต.ห้วยแก้ว อ.ภูกามยาว จ.พะเยา 56000', NULL, NULL, CAST(0x0000A53A017FDE5E AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'e10af38e-0641-45a9-974e-c94693e509d5', N'bang16631@gmail.com', 0, N'AGN8K4E8rn20jovTodrssNf8RRtXzVGC8JqRySMo5/dfcXMuC6sGo+CiKab8tz139g==', N'e07e0083-9a14-46b7-96c1-cd7de6eb8eef', NULL, 0, 0, NULL, 1, 0, N'bank16631', N'Theerayuth', N'chunueng', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0887103160', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00008B4F00000000 AS DateTime), N'39 หมู่ 9 ต.หงส์หิน  อ.จุน  จ.พะเยา  56150', NULL, NULL, CAST(0x0000A52E0060CA50 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'e29993f4-62aa-4bf1-86b4-83cbf3b774a0', N'Pasit.suttavee1@hotmail.com', 1, N'APbN9Vl7P7ZccT7MZi1ffQ4VXVccQoCOunDllr7M4lxVtaBxPAYxHL5kfDD3lZfIxw==', N'bed2f742-4d69-4d41-bb95-6c8296e19da8', NULL, 0, 0, NULL, 1, 0, N'Arm060914', N'Pasit ', N'Suttavee', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0841392298', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00008A3600000000 AS DateTime), N'Vote!!!!! 555555', NULL, NULL, CAST(0x0000A53C000DA760 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'ecc35b51-ccaa-45ca-ab0b-3fee802d830f', N'cyberrytest@d-inf.co.th', 1, N'AAaESsoR8C//0W2hvzJFrpIkY+2IBaxi0+n18tkJ0seo5M9OiZy0IgTb/tjWdKLWyQ==', N'ffc2902b-efb5-4ea1-b70d-98088743ef86', NULL, 0, 0, NULL, 1, 0, N'cyberrytest', N'test', N'test', NULL, N'/Upload/Images/20151006071405IMG_0022.JPG', 1, 0, 1, 0, NULL, 2, NULL, N'test', N'009', N'1410300010910', N'/Upload/Images/20151007002551IMG_0022.JPG', NULL, NULL, NULL, NULL, CAST(0x000078ED00000000 AS DateTime), N'tests', NULL, NULL, CAST(0x0000A528000CFF15 AS DateTime), CAST(0x0000A52A0007238B AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'ed0eb0fa-bd26-4483-b9a4-ad0d22d6c448', N'Emotionbland@hotmail.com', 0, N'AGf6R1O02PNaNskHF+0c1mrUJFw9EAOwjAZgcVQjqjcbycMF30P9fXEck2gRXmRIzg==', N'67f20c53-b7f6-4f5c-bb4b-eca9082ec78d', NULL, 0, 0, NULL, 1, 0, N'Rainemotion11', N'ศรัญญู', N'เจือจินดา', NULL, NULL, 1, 0, 1, 0, NULL, 2, NULL, N'8082000241', N'0852796087', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53A00000000 AS DateTime), N'46/20 หมู่6 ตำบล พลับพลา อำเภอ เมือง จ.จันทบุรี', NULL, NULL, CAST(0x0000A53A013EC16F AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'eee3ef8c-b347-4456-ab03-0dca2581d6a9', N'porza_ha5@hotmail.com', 0, N'AElzPqImjVXnqUjWMWrUhgS2tdBm6Hhom9mu/LnM91aEYQ14I3liiSZNubz4mxBugQ==', N'eea4c026-966f-4c75-aa6d-6e758d8f005f', NULL, 0, 0, NULL, 1, 0, N'porza_ha5@hotmail.com', N'นภดล', N'นาคดี', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0929701505', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x00008E2600000000 AS DateTime), N'0929701505', NULL, NULL, CAST(0x0000A53B00AECC23 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'efaaf60a-80a3-481e-aa6d-1a3b5ec1959a', N'zacarlosza@hotmail.com', 0, N'AA3gfoIEeZ7/XUJtO8oXyt+4z+dQPBsfzxJ6YrAwgXoULfvO8iYip7dfCkccSJGg2w==', N'76e90c08-6078-4e2b-8c13-cb5a94a7cc4b', NULL, 0, 0, NULL, 1, 0, N'zacarlosza', N'Chanathinart', N'Chaiyawong', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0983426571', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000887800000000 AS DateTime), N'https://www.facebook.com/rockamuse.carlos', NULL, NULL, CAST(0x0000A53C01259556 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'f01815e6-883a-44bb-82d7-66ab9bce1863', N'zupakyt@gmail.com', 1, N'AO3oNjbswJqaPTN4lVdJBWs7dCziCv4kZOyE5jg6FhxDYWP9Svrkv9RD/fwN6X23Rw==', N'ad49206a-34c9-4562-8f73-d50beac338e8', NULL, 0, 0, NULL, 1, 0, N'zupakyt', N'Zupakyt vanz', N'Pongpipat', NULL, N'/Upload/Images/2015102611260120151023_015141.jpg', 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0869233157', N'3509900941250', NULL, NULL, NULL, NULL, NULL, CAST(0x0000711A00000000 AS DateTime), N'patan', NULL, NULL, CAST(0x0000A53A01662BC6 AS DateTime), CAST(0x0000A53D00BCE061 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'f1c62d2f-85d3-49a3-88f6-5ba4c0d520bb', N'stonesparade@gmail.com', 1, N'AJeY58OI3aYMD4uW58iL60hlFjEQZmiKpo9YeLjSKvW0vciNWuRduTjZiyD2FvbA3w==', N'f4fe2a60-bf60-4766-a29a-b9075020c161', NULL, 0, 0, NULL, 1, 0, N'Flowview', N'พิพัฒน์', N'บุญตาแสง', NULL, N'/Upload/Images/20151027153318images.jpg', 1, 0, 1, 0, NULL, 2, NULL, N'913 2 01770 8', N'0990806625', N'3440700017071', NULL, NULL, NULL, NULL, NULL, CAST(0x00007D3000000000 AS DateTime), N'133 ถ.นาเชือก-พยัค  ต.นาเชือก อ.นาเชือก จ.มหาสารคาม 44170', NULL, NULL, CAST(0x0000A53E00FF5ACA AS DateTime), CAST(0x0000A53E0100605F AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'f32f8c0b-e069-4f80-9c2e-24785747c607', N'aof_appcoltd@hotmail.com', 1, N'ANuM4giG7VjoXQCWIoc2t4OQRoBT7MuPJ+e+wXWHrDmfk80oFjL8upCiD69v7xiyfw==', N'f742ef99-b683-428d-bee2-96f91e5ff4b8', NULL, 0, 0, NULL, 1, 0, N'123456', N'Phittaya', N'music', NULL, N'/Upload/Images/2015102417385910930158_764558076972160_8689868546841093149_n.jpg', 1, 0, 1, 0, NULL, 1, NULL, N'6692604630', N'0621631637', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53B00000000 AS DateTime), N'181/27  ต นาจอมเทียน  อ. บางละมุง จ. ชลบุรี', NULL, NULL, CAST(0x0000A53B010CD9DD AS DateTime), CAST(0x0000A53B0122DF5D AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'f471733f-ac65-401a-a453-502eeb64d204', N'sanit_bt@hotmail.com', 0, N'AE2sopRiGbA7uYhfVMCY10tHzp0+DITJjVPgf3dPMUEw2kL3fiTubyMrr3LChlL5+Q==', N'186121a9-4c28-4dbc-9664-045a4273bd74', NULL, 0, 0, NULL, 1, 0, N'indyman', N'ศานิต', N'พัชรกมลวงศ์', NULL, NULL, 1, 0, 1, 0, NULL, 2, NULL, N'1182764597', N'0959902531', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000761A00000000 AS DateTime), N'อุบลราชธานี', NULL, NULL, CAST(0x0000A53A01822A73 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'f76d565f-e63b-45bb-84a6-a25947064495', N'jame456325@gmail.com', 0, N'AGz+4HrzvhE6tau8nVwkpzYDLQt1ZPVZw0nNoYoXjhEj3rgdbb8/1xim87WYz3yuvQ==', N'3ebcb4da-b8b4-45ac-80fc-0e94323a3063', NULL, 0, 0, NULL, 1, 0, N'jamepop', N'เจมส์', N'ทศพล', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0832857200', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A53B00000000 AS DateTime), N'จ. ร้อยเอ็ด อ. เสลภูมิ ต. กลาง', NULL, NULL, CAST(0x0000A53B00CDF813 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'f7751179-21ff-4b6c-84c7-ca30e5ab0057', N'eric-comtu@hotmail.com', 1, NULL, N'40ee2ea3-5bfb-4bd3-a434-e10efb75b86a', NULL, 0, 0, NULL, 1, 0, N'SirumAmarin', N'amarin', N'sirum', NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, N'0963674698', NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A54900000000 AS DateTime), N'16/11บีเอ้ม อพารืทเม้นต์ชั้น2ห้อง202  ถนนสังฆสันติสุข ต กระทุ่มลาย เขตหนองจอก กทม  10530', NULL, NULL, CAST(0x0000A53A015439D5 AS DateTime), NULL, N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'f8dd0bfa-0f82-4a5f-b814-695165160fa5', N'jassada.trumpet@gmail.com', 1, N'ALFDhwTCYF8XVySXkMRwzm33MNjBz1Rq6+VKeOFE6DmBU99g4g5Ct7zBS+MObLeTvA==', N'fc5d11ab-deb2-49f6-bb66-0615422842a7', NULL, 0, 0, NULL, 1, 0, N'Slow Rechord', N'่jassada', N'Sopat', NULL, N'/Upload/Images/20151024084949logo.jpg', 1, 0, 1, 0, NULL, 2, NULL, N'นายเจษฎา โสพัฒ', N'0934392694', N'1330500256361', NULL, NULL, NULL, NULL, NULL, CAST(0x000084C000000000 AS DateTime), N'(ห้องB3)  50/2 ถนนธานี. ตำบลในเมือง. อ.เมือง. จ.บุรีรัมย์.  31000', NULL, NULL, CAST(0x0000A53B00867E7D AS DateTime), CAST(0x0000A53B009195A3 AS DateTime), N'ApplicationUser')
GO
INSERT [dbo].[Users] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [txt_firstname], [txt_lastname], [txt_username], [txt_avatar], [is_contest], [is_composer], [is_activated], [is_banned], [txt_banned_reason], [int_bank_id], [txt_bank_name], [txt_bank_account], [txt_phone], [txt_id_card], [txt_img_id_card_path], [txt_new_password_key], [txt_new_password_requested], [txt_new_email], [txt_new_email_key], [date_birth], [txt_address], [txt_last_ip], [datetime_lastlogin], [datetime_create], [datetime_modified], [Discriminator]) VALUES (N'fb77b5b8-a47c-46f5-9e71-ac5dfb970df1', N'cadence.musicband@gmail.com', 1, N'AGG0zHJUy28T0R99olW6BHXpD882ihGnc4vwmjmOq20sUDJdrprliND1S03uN8N9Ag==', N'b25c38b2-f8bd-4e0c-a75e-c4c2533a7b82', NULL, 0, 0, NULL, 1, 0, N'Cadenceband', N'Korn', N'Singdee', NULL, NULL, 1, 0, 1, 0, NULL, 2, NULL, N'731-2-61482-5', N'0835588701', N'3170600486765', NULL, NULL, NULL, NULL, NULL, CAST(0x00006F8900000000 AS DateTime), N'162/1  Moo. 4  Inburi  Singburi 16110', NULL, NULL, CAST(0x0000A53A01310185 AS DateTime), NULL, N'ApplicationUser')
GO
SET IDENTITY_INSERT [dbo].[Vote_History] ON 

GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (1, 2, 2, 1, N'9fa60428-ef64-43c1-aebf-a7be62600488', 1, CAST(0x0000A538010D506E AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (2, 2, 2, 2, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53900A47EFB AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (3, 4, 2, 1, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53A01729AE8 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (4, 6, 2, 4, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53A018797D6 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (5, 12, 2, 1, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53B009AC23E AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (6, 10, 1, 1, NULL, 1, CAST(0x0000A53B009D2617 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (7, 10, 1, 1, NULL, 1, CAST(0x0000A53B009D4795 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (8, 6, 2, 1, N'9fa60428-ef64-43c1-aebf-a7be62600488', 1, CAST(0x0000A53B00A24AC5 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (9, 7, 1, 1, NULL, 1, CAST(0x0000A53B00AEFFE2 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (10, 7, 1, 1, NULL, 1, CAST(0x0000A53B00AF0BDC AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (11, 7, 1, 1, NULL, 1, CAST(0x0000A53B00AF13D7 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (12, 7, 1, 1, NULL, 1, CAST(0x0000A53B00AF1BB2 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (13, 7, 1, 1, NULL, 1, CAST(0x0000A53B00AF5308 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (14, 7, 1, 1, NULL, 1, CAST(0x0000A53B00AF5A14 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (15, 26, 1, 1, NULL, 1, CAST(0x0000A53B00DA181A AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (16, 26, 2, 5, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53B00DEE0A4 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (17, 13, 2, 5, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53B00E5B878 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (18, 26, 2, 2, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53B00EAC875 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (19, 12, 1, 1, NULL, 1, CAST(0x0000A53B00FACC4F AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (20, 12, 1, 1, NULL, 1, CAST(0x0000A53B00FB1071 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (21, 12, 1, 1, NULL, 1, CAST(0x0000A53B00FB1287 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (22, 12, 1, 1, NULL, 1, CAST(0x0000A53B00FB1644 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (23, 12, 1, 1, NULL, 1, CAST(0x0000A53B00FB1A2A AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (24, 12, 1, 1, NULL, 1, CAST(0x0000A53B00FB1ECC AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (25, 12, 1, 1, NULL, 1, CAST(0x0000A53B00FB229B AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (26, 12, 1, 1, NULL, 1, CAST(0x0000A53B00FB2BF6 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (27, 12, 1, 1, NULL, 1, CAST(0x0000A53B00FB2E37 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (28, 12, 1, 1, NULL, 1, CAST(0x0000A53B00FB3292 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (29, 12, 1, 1, NULL, 1, CAST(0x0000A53B00FB3603 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (30, 12, 1, 1, NULL, 1, CAST(0x0000A53B00FBACDE AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (31, 12, 1, 1, NULL, 1, CAST(0x0000A53B0104FC8F AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (32, 12, 1, 1, NULL, 1, CAST(0x0000A53B010791EB AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (33, 26, 1, 1, NULL, 1, CAST(0x0000A53B010B6DAF AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (34, 12, 1, 1, NULL, 1, CAST(0x0000A53B011C3376 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (35, 5, 1, 1, NULL, 1, CAST(0x0000A53B011FBA77 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (36, 12, 1, 1, NULL, 1, CAST(0x0000A53B0139227F AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (37, 2, 1, 1, NULL, 1, CAST(0x0000A53B016219A6 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (38, 2, 1, 1, NULL, 1, CAST(0x0000A53B016258A3 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (39, 2, 1, 1, NULL, 1, CAST(0x0000A53B01625DD6 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (40, 2, 1, 1, NULL, 1, CAST(0x0000A53B016261F0 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (41, 2, 1, 1, NULL, 1, CAST(0x0000A53B01626798 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (42, 2, 1, 1, NULL, 1, CAST(0x0000A53B01626D25 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (43, 2, 1, 1, NULL, 1, CAST(0x0000A53B016270E5 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (44, 2, 1, 1, NULL, 1, CAST(0x0000A53B01627410 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (45, 2, 1, 1, NULL, 1, CAST(0x0000A53B016277D2 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (46, 26, 1, 1, NULL, 1, CAST(0x0000A53B016344AD AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (47, 2, 1, 1, NULL, 1, CAST(0x0000A53B0164CBFA AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (48, 2, 1, 1, NULL, 1, CAST(0x0000A53B0164D822 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (49, 26, 1, 1, NULL, 1, CAST(0x0000A53B0164FF48 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (50, 26, 1, 1, NULL, 1, CAST(0x0000A53B01651790 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (51, 36, 1, 1, NULL, 1, CAST(0x0000A53B01800B4D AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (52, 12, 1, 1, NULL, 1, CAST(0x0000A53C0093F85E AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (53, 36, 2, 20, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53C009B2293 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (54, 30, 2, 6, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53C009C0352 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (55, 28, 2, 10, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53C009DB917 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (56, 2, 1, 1, NULL, 1, CAST(0x0000A53C00A1582C AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (57, 2, 1, 1, NULL, 1, CAST(0x0000A53C00A16210 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (58, 7, 1, 1, NULL, 1, CAST(0x0000A53C00A16ACB AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (59, 7, 1, 1, NULL, 1, CAST(0x0000A53C00A17430 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (60, 7, 1, 1, NULL, 1, CAST(0x0000A53C00A17D40 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (61, 2, 1, 1, NULL, 1, CAST(0x0000A53C00A184D5 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (62, 7, 1, 1, NULL, 1, CAST(0x0000A53C00A18AFC AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (63, 7, 1, 1, NULL, 1, CAST(0x0000A53C00A1926B AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (64, 2, 1, 1, NULL, 1, CAST(0x0000A53C00A199E4 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (65, 12, 1, 1, NULL, 1, CAST(0x0000A53C00B8FFAF AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (66, 28, 1, 1, NULL, 1, CAST(0x0000A53C00B93F5A AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (67, 28, 2, 1, N'9fa60428-ef64-43c1-aebf-a7be62600488', 1, CAST(0x0000A53C00BE8BF7 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (68, 12, 1, 1, NULL, 1, CAST(0x0000A53C00FD030F AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (69, 37, 1, 1, NULL, 1, CAST(0x0000A53C01213D8E AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (70, 12, 2, 2, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53C0122AA3F AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (71, 26, 2, 8, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53C0122CE89 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (72, 12, 1, 1, NULL, 1, CAST(0x0000A53C012784DB AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (73, 37, 1, 1, NULL, 1, CAST(0x0000A53C012CAD2E AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (74, 38, 1, 1, NULL, 1, CAST(0x0000A53C012DAAD1 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (75, 38, 1, 1, NULL, 1, CAST(0x0000A53C014D5FD8 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (76, 37, 1, 1, NULL, 1, CAST(0x0000A53C01719E3E AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (77, 39, 2, 1, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53C01762C2B AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (78, 37, 1, 1, NULL, 1, CAST(0x0000A53C0185973C AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (79, 37, 1, 1, NULL, 1, CAST(0x0000A53D00015DA3 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (80, 37, 1, 1, NULL, 1, CAST(0x0000A53D009B2189 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (81, 41, 1, 1, NULL, 1, CAST(0x0000A53D00A97EAA AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (82, 37, 1, 1, NULL, 1, CAST(0x0000A53D00ACFE3D AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (83, 7, 2, 21, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53D00C41835 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (84, 26, 2, 10, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53D00CAB00F AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (85, 37, 1, 1, NULL, 1, CAST(0x0000A53D00CF98DD AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (86, 37, 1, 1, NULL, 1, CAST(0x0000A53D00CF9AB2 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (87, 37, 1, 1, NULL, 1, CAST(0x0000A53D00D08765 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (88, 45, 2, 2, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53D00F3695C AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (89, 37, 1, 1, NULL, 1, CAST(0x0000A53D00F84D8C AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (90, 37, 1, 1, NULL, 1, CAST(0x0000A53D00F891C5 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (91, 46, 2, 2, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53D0101929E AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (92, 45, 2, 10, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53D0101E5BA AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (93, 46, 2, 2, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53D0105BEB1 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (94, 37, 1, 1, NULL, 1, CAST(0x0000A53D01101A49 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (95, 37, 1, 1, NULL, 1, CAST(0x0000A53D0113CBCA AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (96, 37, 1, 1, NULL, 1, CAST(0x0000A53D01192DB2 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (97, 37, 1, 1, NULL, 1, CAST(0x0000A53D011A2F24 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (98, 37, 1, 1, NULL, 1, CAST(0x0000A53D012DBD0A AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (99, 36, 1, 1, NULL, 1, CAST(0x0000A53D01308536 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (100, 37, 1, 1, NULL, 1, CAST(0x0000A53D01487EC1 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (101, 45, 2, 50, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53D0166B6E7 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (102, 47, 2, 2, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53D0167B13A AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (103, 31, 2, 2, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53D0170BFC5 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (104, 36, 1, 1, NULL, 1, CAST(0x0000A53D0181CB75 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (105, 36, 1, 1, NULL, 1, CAST(0x0000A53D0184769B AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (106, 37, 1, 1, NULL, 1, CAST(0x0000A53E00982C15 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (107, 57, 2, 20, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53E009D8BE2 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (108, 57, 1, 1, NULL, 1, CAST(0x0000A53E00B469B0 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (109, 51, 2, 50, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53E00B59BAD AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (110, 28, 2, 10, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53E00B5C022 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (111, 57, 2, 10, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53E00B5F0C9 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (112, 57, 2, 10, N'89dec678-e0b1-491e-ac89-250725b5c517', 1, CAST(0x0000A53E00B66CE0 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (113, 49, 1, 1, NULL, 1, CAST(0x0000A53E00C178F6 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (114, 2, 2, 3, N'9fa60428-ef64-43c1-aebf-a7be62600488', 1, CAST(0x0000A53E00C1EB63 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (115, 51, 2, 1, N'9fa60428-ef64-43c1-aebf-a7be62600488', 1, CAST(0x0000A53E00D2BB61 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (116, 49, 1, 1, NULL, 1, CAST(0x0000A53E00D5B4FE AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (117, 58, 1, 1, NULL, 1, CAST(0x0000A53E011C53B1 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (118, 58, 1, 1, NULL, 1, CAST(0x0000A53E011CE03F AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (119, 58, 1, 1, NULL, 1, CAST(0x0000A53E011DE759 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (120, 57, 1, 1, NULL, 1, CAST(0x0000A53E012BEBC4 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (121, 43, 1, 1, NULL, 1, CAST(0x0000A53E01635664 AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (122, 49, 1, 1, NULL, 1, CAST(0x0000A53E016C161A AS DateTime))
GO
INSERT [dbo].[Vote_History] ([int_vote_history_id], [int_song_contest_id], [int_vote_method], [int_vote_value], [int_vote_by_user_id], [is_calculated], [datetime_create]) VALUES (123, 49, 1, 1, NULL, 1, CAST(0x0000A53E01848083 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Vote_History] OFF
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.Users_IdentityUser_Id] FOREIGN KEY([IdentityUser_Id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.Users_IdentityUser_Id]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.Users_IdentityUser_Id] FOREIGN KEY([IdentityUser_Id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.Users_IdentityUser_Id]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.Users_IdentityUser_Id] FOREIGN KEY([IdentityUser_Id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.Users_IdentityUser_Id]
GO
ALTER TABLE [dbo].[Balance]  WITH CHECK ADD  CONSTRAINT [FK_Balance_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Balance] CHECK CONSTRAINT [FK_Balance_Users]
GO
ALTER TABLE [dbo].[Forget_Password]  WITH CHECK ADD  CONSTRAINT [FK_Forget_Password_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Forget_Password] CHECK CONSTRAINT [FK_Forget_Password_Users]
GO
ALTER TABLE [dbo].[Income_Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Income_Transaction_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Income_Transaction] CHECK CONSTRAINT [FK_Income_Transaction_Users]
GO
ALTER TABLE [dbo].[Login_Log]  WITH CHECK ADD  CONSTRAINT [FK_Login_Log_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Login_Log] CHECK CONSTRAINT [FK_Login_Log_Users]
GO
ALTER TABLE [dbo].[Paysbuy]  WITH CHECK ADD  CONSTRAINT [FK_Paysbuy_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Paysbuy] CHECK CONSTRAINT [FK_Paysbuy_Users]
GO
ALTER TABLE [dbo].[Point]  WITH CHECK ADD  CONSTRAINT [FK_Point_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Point] CHECK CONSTRAINT [FK_Point_Users]
GO
ALTER TABLE [dbo].[Point_Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Point_Transaction_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Point_Transaction] CHECK CONSTRAINT [FK_Point_Transaction_Users]
GO
ALTER TABLE [dbo].[Score]  WITH CHECK ADD  CONSTRAINT [FK_Score_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Score] CHECK CONSTRAINT [FK_Score_Users]
GO
ALTER TABLE [dbo].[Score_Song]  WITH CHECK ADD  CONSTRAINT [FK_Score_Song_Song_Contest] FOREIGN KEY([int_song_contest_id])
REFERENCES [dbo].[Song_Contest] ([int_song_contest_id])
GO
ALTER TABLE [dbo].[Score_Song] CHECK CONSTRAINT [FK_Score_Song_Song_Contest]
GO
ALTER TABLE [dbo].[Song_Contest]  WITH CHECK ADD  CONSTRAINT [FK_Song_Contest_Song_Original] FOREIGN KEY([int_song_original_id])
REFERENCES [dbo].[Song_Original] ([int_song_original_id])
GO
ALTER TABLE [dbo].[Song_Contest] CHECK CONSTRAINT [FK_Song_Contest_Song_Original]
GO
ALTER TABLE [dbo].[Song_Contest]  WITH CHECK ADD  CONSTRAINT [FK_Song_Contest_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Song_Contest] CHECK CONSTRAINT [FK_Song_Contest_Users]
GO
ALTER TABLE [dbo].[Song_Original]  WITH CHECK ADD  CONSTRAINT [FK_Song_Original_Users] FOREIGN KEY([int_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Song_Original] CHECK CONSTRAINT [FK_Song_Original_Users]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_BANK] FOREIGN KEY([int_bank_id])
REFERENCES [dbo].[Bank] ([int_bank_id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_BANK]
GO
ALTER TABLE [dbo].[Vote_History]  WITH CHECK ADD  CONSTRAINT [FK_Vote_History_Song_Contest] FOREIGN KEY([int_song_contest_id])
REFERENCES [dbo].[Song_Contest] ([int_song_contest_id])
GO
ALTER TABLE [dbo].[Vote_History] CHECK CONSTRAINT [FK_Vote_History_Song_Contest]
GO
ALTER TABLE [dbo].[Vote_History]  WITH CHECK ADD  CONSTRAINT [FK_Vote_History_Users] FOREIGN KEY([int_vote_by_user_id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Vote_History] CHECK CONSTRAINT [FK_Vote_History_Users]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1=ผู้แต่ง,2=ผู้ร้อง' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Income_Transaction', @level2type=N'COLUMN',@level2name=N'int_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1=vote by sms,2=vote by point' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Vote_History', @level2type=N'COLUMN',@level2name=N'int_vote_method'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[19] 4[30] 2[19] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "v"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 295
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vUserScoreSong'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vUserScoreSong'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Users"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 211
               Right = 279
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Song_Contest"
            Begin Extent = 
               Top = 6
               Left = 317
               Bottom = 185
               Right = 529
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "Score_Song"
            Begin Extent = 
               Top = 6
               Left = 567
               Bottom = 211
               Right = 760
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vUserSongContest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vUserSongContest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Vote_History"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 207
               Right = 231
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Income_Transaction"
            Begin Extent = 
               Top = 1
               Left = 291
               Bottom = 209
               Right = 518
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vVoteIncomeTransaction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vVoteIncomeTransaction'
GO
