<?php
	
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

function write_header($data){
	$html = file_get_contents("_header.html");
	$html = str_replace("[title]", $data['title'], $html);
	$html = str_replace("[description]", $data['description'], $html);
	$html = str_replace("[keyword]", $data['keyword'], $html);
	$html = str_replace("[og_title]", $data['og_title'], $html);
	$html = str_replace("[og_sitename]", $data['og_sitename'], $html);
	$html = str_replace("[og_url]", $data['og_url'], $html);
	$html = str_replace("[og_image]", $data['og_image'], $html);
	$html = str_replace("[og_description]", $data['og_description'], $html);
	echo $html;
}
	

	
?>