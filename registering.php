<?php
/* POST DATA
    [username] => overcond
    [password] => overcond
    [pwd] => overcond
    [email] => overconda@gmail.com
    [firstname] => test
    [lastname] => test
    [day] => 3
    [month] => 11
    [year] => 2514
    [address] => test
    [phone] => 0869967788
    [myfile] => EQ.PNG
    [iscontest] => on
    [bank] => kbank
    [bankaccount] => aaaqqqweeee
    [cardid] => 111222333
    [ck_accept] => accept
*/
error_reporting(E_ALL);

$username = sanitize(is($_POST['username']));
$password = sanitize(is($_POST['password']));
$pwd = sanitize(is($_POST['pwd']));
$email = sanitize(is($_POST['email']));
$firstname = sanitize(is($_POST['firstname']));
$lastname = sanitize(is($_POST['lastname']));
$date=sanitize(is($_POST['date']));
/*
$day = $_POST['day'];
$month = $_POST['month'];
$year = $_POST['year'];
*/
$address = sanitize(is($_POST['address']));
$phone = sanitize(is($_POST['phone']));
$iscontest = $_POST['iscontest'];
$bank = sanitize(is($_POST['bank']));
$bankaccount = sanitize(is($_POST['bankaccount']));
$cardid = sanitize(is($_POST['cardid']));
$ch_accept = $_POST['ch_accept'];
$myfile = $_POST['myfile'];

$error = 0;

/*
echo "<br>User : " . $username;
echo "<br>password : " . $password;
echo "<br>email : " . $email;
echo "<br>firstname : " . $firstname;
echo "<br>lastname : " . $lastname;
echo "<br>address : " . $address;
echo "<br>phone : " . $phone;
echo "<br>bank : " . $bank;
echo "<br>bankaccount : " . $bankaccount;
echo "<br>cardid : " . $cartid;
exit;
*/

if(trim($username=="")) $error++; 
if(trim($email=="")) $error++; 
if(trim($firstname=="")) $error++; 
if(trim($lastname=="")) $error++; 
if($password <> $pwd) $error++; 
//// Check require field


if($error>0){
	$param = "e=1&u=$username&f=$firstname&l=$lastname&email=$email&d=&date=$date&a=$address&p=$phone&c=$iscontent&b=$bank&ba=$bankaccount&ci=$cardid&ferror=$ferror";
	header("Location: register.php?$param");
}


$birthdate = convertDate($date);

$ip="";
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
$contest=0;
if($iscontest == 'on'){ $contest=1;}
$now = date("Y-m-d H:i:s");
$password = md5($pwd);
$sql ="insert into users (username, password, email, user_contest, activated, bank_id, bank_name, bank_account, tel_number, id_card, birthdate, address, last_ip, created, modified) ";
$sql .= " values('$username', '$password', '$email', '$contest', 1, 1, '$bank', '$bank_account', '$phone', '$cardid', '$birthdate', '$address', '$ip', '$now', '$now' )";

require_once("db.php");
mysql_query($sql, $dbconnect);
//// mail for confirmation also.
header("Location: index.php");

function sanitize($str)
{
    return str_replace(array("\\", "'"), array("\\\\", "\\'"), $str) ;
}

function is($v, $default=""){
	/***************
	Usage: 
		$x = is($_GET['x']);
	or	$x = is($_GET['x'], 'Oh yeah');
	****************/
	
	$ret = isset($v)?$v:$default;
	return $ret;
}

function convertDate($date){
	// input format : 08/19/2015
	if(trim($date)=="") {
		return "0000-00-00";
	}
	
	$D = explode("/", $date);
	$month = $D[0]+0;
	$day = $D[1]+0;
	$year = $D[2]+0;
	
	return sprintf("%04d-%02d-%02d" , $year, $month, $day);
}
?>

